/**
 * Created by alex on 26.02.16.
 */
var winston = require('winston'),
    cfg = require('../../cfg/admin').logger;
module.exports = new winston.Logger({
    level: cfg.level,
    transports: [
        new (winston.transports.Console)({
            timestamp: true,
            colorize: true
        }),
        new (winston.transports.File)({
            timestamp: true,
            filename: cfg.dir + '/' + cfg.file,
        })
    ]
});
