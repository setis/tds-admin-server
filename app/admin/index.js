var express = require('express'),
    bodyParser = require("body-parser"),
    cors = require('cors'),
    fs = require('fs'),
    path = require('path'),
    session = require('express-session'),
    autoIncrement = require('mongoose-auto-increment'),
    controller = require('../kernel/controller')({}, __dirname + '/controllers/'),
    multer = require('multer'),
    app = express(),
    router = express.Router(),
    MongoDBStore = require('connect-mongodb-session')(session),
    logger = require('./logger'),
    cfg = require('../../cfg/admin.json');
if (cfg.pmx) {
    var pmx = require('pmx').init({
        http: true, // HTTP routes logging (default: true)
        ignore_routes: [/socket\.io/, /notFound/], // Ignore http routes with this pattern (Default: [])
        errors: true, // Exceptions loggin (default: true)
        custom_probes: true, // Auto expose JS Loop Latency and HTTP req/s as custom metrics
        network: true, // Network monitoring at the application level
        ports: true  // Shows which ports your app is listening on (default: false)});
    });
    app.use(pmx.expressErrorHandler());
}
app.use(cors({
    origin: function (origin, callback) {
        callback(null, origin);
    },
    credentials: true,
    methods: ['GET', 'POST', 'OPTIONS'],
    headers: ["Origin", "X-Requested-With", "Content-Type", "Accept"]
}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
var store = new MongoDBStore({
    uri: cfg.session.uri,
    collection: cfg.session.collection
});
store.on('error', function (error) {
    logger.error('mongo-session', error);
});
app.use(session({
    secret: cfg.session.secret,
    cookie: cfg.session.cookie,
    store: store
}));
app.use('/', express.static(__dirname + '/www/'));
app.use('/uploads', express.static(cfg.upload.dest));
var upload = multer(cfg.upload);
require('./loader')(function (core) {
    app.use(function (req, res, next) {
        if (!req.hasOwnProperty("mongoose")) {
            req.mongoose = core.models.odm;
        }
        if (!req.hasOwnProperty("logger")) {
            req.logger = core.logger;
        }
        if (!req.hasOwnProperty("sql")) {
            req.sql = core.clients.mariasql;
        }
        next();
    });
    app.post('/upload', upload, function (req, res, next) {
        controller.handler('authorization', 'check', req, res, function () {
            controller.handler('banner', 'create', req, res, next);
        });
    });
    app.use('/api', function (req, res, next) {
        if (req.query.length === 0 || req.query.action === undefined || req.query.action === '') {
            next();
            return;
        }
        var isAuth = new RegExp(/^authorization/i);
        if (isAuth.test(req.query.action)) {
            controller.run(req, res, next);
        } else if (req.query.action === 'static') {
            controller.handler('authorization', 'check', req, res, function () {
                require(__dirname + '/controllers/static')(req, res);
            });
        } else {
            logger.debug('controller: ', req.query);
            controller.handler('authorization', 'check', req, res, function () {
                logger.info('controller: %s ip:%s user:%s', req.query.action, req.ip, req.session.login);
                controller.run(req, res, next);
            });
        }

    });
    app.use(function (req, res, next) {
        logger.warn('%s %s ip:%s', req.method, req.url, req.ip);
        res.status(404).json({error: 404});
    });
    app.listen(cfg.express.port, cfg.express.ip, function () {
        logger.info('server listening on http://%s:%s ', cfg.express.ip, cfg.express.port);
    });
});

