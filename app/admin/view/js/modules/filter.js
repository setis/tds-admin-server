var func = require('func'),
    cfg = require('cfg'),
    action_index,
    change = false,
    db = require('db'),
    event = require('event'),
    setting = cfg.modules.exploit;
var form, input;
function country(data) {
    var html = '';
    if (data === '*' || data === undefined || data === null || data.length === 0) {
        html += 'All ';
    } else {
        if (typeof data === "string") {
            html += db.tmpl.country[data] + ' ' + data || '';
        } else {
            for (var i in data) {
                var v = data[i];
                html += db.tmpl.country[v] + ' ' + v || '';
            }
        }

    }
    return html;
}
function browser(data) {
    var html = '';
    if (data === '*' || data === undefined || data === null || data.length === 0) {
        html += 'All ';
    } else {
        for (var i in data) {
            var v = data[i];
            html += v.browser.name + ' (';
            html += version(v.version);
            html += ')';
        }
    }
    return html;
}
function version(data) {
    if (data === '*') {
        return 'All';
    }
    var html = '';
    if (data.from !== undefined && data.to !== undefined) {
        html += 'c ' + data.from + ' до ' + data.to;
    } else {
        if (data.from !== undefined) {
            html += 'c ' + data.from;
        }
        if (data.to !== undefined) {
            html += 'до ' + data.to;
        }
    }
    if (data.eq instanceof  Array && data.eq.length) {
        html += ', = ';
        html += data.eq.join(',') + ' ';

    }
    return html;
}
function exploit(data) {
    if (data === undefined) {
        return '';
    }
    if (db.exploit.data[data] !== undefined) {
        data = db.exploit.data[data];
        return '<a href="#" data-toggle="tooltip" data-placement="top" title="' + data.url + '(' + data.type + ')">' + data.id + ':' + data.name + '</a>';
    }
    return '<a href="#">' + data + ':?</a>';

}

function table() {
    var cfg = {
        toggle: "table",
        cache: false,
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "All"],
        sidePagination: "client",
//        toolbar: dom.toolbar,
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        detailView: true,
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'country',
                title: 'country',
                align: 'left',
                valign: 'top',
                sortable: false,
                formatter: country
            },
            {
                field: 'browser',
                title: 'browser',
                align: 'left',
                valign: 'top',
                sortable: false,
                formatter: browser

            },
            {
                field: 'exploit',
                title: 'exploit',
                align: 'left',
                valign: 'top',
                sortable: false,
                formatter: exploit

            },
//            {
//                field: 'operate',
//                title: 'Действия',
//                align: 'center',
//                valign: 'middle',
//                clickToSelect: false,
//                formatter: function (value, row, index) {
//                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
//                            '<i class="glyphicon glyphicon-edit"></i>' +
//                            '</a>' +
//                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
//                            '<i class="glyphicon glyphicon-remove"></i>' +
//                            '</a>';
//                },
//                events: {
//                    'click .edit': function (e, value, row, index) {
//
//                    },
//                    'click .remove': function (e, value, row, index) {
//
//                    }
//                }
//            }
        ]
    };
    $("[data-action='filter']").on('click', function () {
        form = $($(this).data('id'));
    });
    $("[data-action='filter']").each(function () {
        var self = $(this),
            id = self.data('id'),
            input = $(self.data('input')),
            save = self.data('save');
        if (save) {
            $(save).on('click', function () {
                var data = format($($(this).data('form')).serializeArray());
                var table = $(id);
                console.log('save', data, table);
                table.bootstrapTable('append', data);
                table.bootstrapTable('load', $.unique(table.bootstrapTable('getData')));
            });
        }
        if (id) {
            $(id).bootstrapTable(cfg)
                .on('click-row.bs.table', function (e, row, $element,index) {
                    //console.log('remove', $(id), arguments);
                    //$(id).bootstrapTable('removeByUniqueId', row.id);
                    $(id).bootstrapTable('remove', {field: 'id', values: [row.id]});
                })
                .on('pre-body.bs.table', function (event, data) {
                    input.val(JSON.stringify(data));
                });
        }
    });

}
function format(data) {
    var result = {};
    for (var i in data) {
        var row = data[i];
        switch (row.name) {
            case "country":
            case "exploit":
                result[row.name] = row.value.split(',');
                break;
            case "ua":
                result[row.name] = JSON.parse(row.value);
                break;
        }
    }
    var result2 = [];
    var exp = Object.create(result.exploit);
    while (exp.length !== 0) {
        var e = exp.splice(0, 1)[0];
        result2.push({browser: result.ua, exploit: e, country: result.country});
    }
    return result2.map(function (v) {
        v.id = JSON.stringify(v);
        return v;
    });

}
function main() {
    var dom = require('dom')['exploit-select'];
    var configure = {
        toggle: "table",
        cache: false,
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "All"],
        sidePagination: "client",
//        toolbar: dom.toolbar,
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        detailView: true,
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'country',
                title: 'country',
                align: 'left',
                valign: 'top',
                sortable: false,
                formatter: country
            },
            {
                field: 'browser',
                title: 'browser',
                align: 'left',
                valign: 'top',
                sortable: false,
                formatter: browser

            },
            {
                field: 'exploit',
                title: 'exploit',
                align: 'left',
                valign: 'top',
                sortable: false,
                formatter: exploit

            },
            {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                        '<i class="glyphicon glyphicon-edit"></i>' +
                        '</a>' +
                        '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {

                    },
                    'click .remove': function (e, value, row, index) {

                    }
                }
            }
        ]
    };
    dom.table.bootstrapTable(configure)
        .on('click-row.bs.table', function (e, row, $element) {
            form.bootstrapTable('append', [row]);
            var data = form.bootstrapTable('getData');
            form.bootstrapTable('load', $.unique(data));
            dom.modal.modal('hide');
        });
    dom.modal.on('shown.bs.modal', function () {
        dom.table.bootstrapTable('resetView');
    });
}
function init() {
    table();
//    main();

}
module.exports = function () {
    event.on('app.filter', init);
};