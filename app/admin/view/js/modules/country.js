var func = require('func'),
        resource = require('cfg').server.flag,
        db = require('db'),
        form,
        input,
        tmpl = {},
        event = require('event');
//        setting = cfg.modules.exploit;
db.country = require('data.country');
db.tmpl.country = template(db.country);
function template(data) {
    var result = {};
    for (var i in data) {
        var code = data[i].Code;
        result[code] = '<img src="' + resource + '32/' + code.toLowerCase() + '.png" alt="' + data[i].Name + '">';
    }
    return result;
}
function main() {
    var dom = require('dom').country;
    var configure = {
        toggle: "table",
        data: db.country,
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'All'],
        sidePagination: "client",
        toolbar: '#toolbar-country',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'Code',
                title: 'code',
                align: 'center',
                valign: 'middle',
                sortable: true

            },
            {
                field: 'Name',
                title: 'name',
                align: 'left',
                valign: 'top',
                sortable: true
            },
            {
                field: 'Code',
                title: 'flag',
                align: 'left',
                valign: 'top',
                formatter: function (value, row) {
                    return db.tmpl.country[value] || '';
                }
            }

        ]
    };
    dom.table.bootstrapTable(configure)
            .on('click-row.bs.table', function (e, row, $element) {
                form.bootstrapTable('append', [row]);
                var data = form.bootstrapTable('getData');
                form.bootstrapTable('load', $.unique(data));
                dom.modal.modal('hide');
            });
    dom.modal.on('shown.bs.modal', function () {
        dom.table.bootstrapTable('resetView');
    });
}
function table() {
    var configure = {
        toggle: "table",
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: false,
        showHeader: false,
        showRefresh: false,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'Code',
                title: 'code',
                align: 'center',
                valign: 'middle',
                sortable: true

            },
            {
                field: 'Name',
                title: 'code',
                align: 'center',
                valign: 'middle',
                sortable: true

            },
            {
                field: 'Code',
                title: 'code',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value) {
                    return db.tmpl.country[value];
                }

            }

        ]

    };
    $("[data-target='#modal-country']").on('click', function () {
        form = $($(this).data('id'));
    });
    $("[data-target='#modal-country']").each(function () {
        var self = $(this),
                id = self.data('id'),
                input = $(self.data('input'));
        if (id) {
            $(id).bootstrapTable(configure)
                    .on('click-row.bs.table', function (e, row, $element) {
                        $(id).bootstrapTable('remove', {field: 'Code', values: [row.Code]});
                    })
                    .on('pre-body.bs.table', function (event, data) {
                        var result = [];
                        for (var i in data) {
                            result.push(data[i].Code);
                        }
                        input.val(result.join());
                    });
        }
    });
}
function init(){
    main();
    table();
}
/**
 * jade template use
 label country
 #country-toolbar-exploit
 button.btn.btn-primary(data-toggle='modal', data-target='#modal-country', data-country="#country-list-exploit") Добавить
 table#country-list-exploit
 */
module.exports = function () {
    event.on('app.country', init);
};