var func = require('func'),
        db = require('db'),
        event = require('event');
var form;
var browser = {
    from: undefined,
    to: undefined,
    eq: [],
    type: 'list',
    list: undefined,
    custom: undefined
};
//        setting = cfg.modules.exploit;
db.browser = require('data.browser');
function table_save(data) {
    var result = [], i;
    if (data.from !== undefined) {
        result.push({condition: 'from', value: data.from});
    }
    if (data.to !== undefined) {
        result.push({condition: 'to', value: data.to});
    }
    if (data.eq.length) {
        data.eq = $.unique(data.eq);
        for (i in data.eq) {
            result.push({condition: 'eq', value: data.eq[i]});
        }
    }
    return result;
}
function table_remove(row, data) {
    switch (row.condition) {
        case 'from':
        case 'to':
            data[row.condition] = undefined;
            break
        case 'eq':
            for (var i in data.eq) {
                if (row.value === data.eq[i]) {
                    data.eq.splice(i, 1);
                    break;
                }
            }
            break;
    }
    return data;
}
function clear() {
    browser = {
        from: undefined,
        to: undefined,
        eq: [],
        type: 'list',
        list: undefined,
        custom: undefined
    };

}
var options = {
    from: '<option value="from" checked="">с</option>',
    to: '<option value="to">до</option>'
};
function generate(data) {
    var html = '';
    if (data.from === undefined) {
        html += options.from;
    }
    if (data.to === undefined) {
        html += options.to;
    }
    html += '<option value="eq">равно</option>';
    return html;
}
function main() {
    var dom = require('dom').browser;
    var configure = {
        toggle: "table",
        strexploited: true,
        pagination: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        undefinedText: '',
        silent: true,
        showHeader: false,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'condition',
                title: 'условия',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row) {
                    var text;
                    switch (value) {
                        case 'to':
                            text = 'до';
                            break;
                        case 'from':
                            text = 'от';
                            break;
                        case 'eq':
                            text = 'равно';
                            break;
                    }
                    return text;
                }

            },
            {
                field: 'value',
                title: 'name',
                align: 'left',
                valign: 'top',
                sortable: true
            }

        ]
    };
    dom.table.bootstrapTable(configure)
            .on('click-row.bs.table', function (e, row, $element) {
                browser = table_remove(row, db.browser.data);
                dom.table.bootstrapTable('load', table_save(browser));
                dom.condition.html(generate(browser));
            });
    dom.modal
            .on('shown.bs.modal', function (e) {
                dom.table.bootstrapTable('resetView');
            });
    dom.list.html(func.options_list(db.browser.list, db.browser.default));
    dom.custom
            .on('change', function () {
                browser.custom = $(this).val();
            })
            .trigger('change');
    dom.list
            .on('change', function () {
                browser.list = $(this).val();
            })
            .trigger('change');
    dom.type
            .on('change', function () {
                var val = $(this).val();
                browser.type = val;
                if (val === 'list') {
                    dom.list.show();
                    dom.custom.hide();
                } else {
                    dom.custom.show();
                    dom.list.hide();
                }
            })
            .trigger('change');

    dom.action.add.on('click', function () {
        var c = dom.condition.val(), v = dom.value.val();
        if (v <= 0) {
            return;
        }
        switch (c) {
            case 'from':
            case 'to':
                dom.condition.find('[value=' + c + ']').remove();
                browser[c] = v;
                dom.condition.html(generate(browser));
                break;
            case 'eq':
                browser[c].push(v);
                break;

        }
        dom.table.bootstrapTable('load', table_save(browser));
    });
    dom.action.clear.on('click', function () {
        clear();
        dom.type.val('list');
        dom.custom.val('');
        dom.value.val('0');
        dom.condition.html(generate({from: undefined, to: undefined}));
        dom.table.bootstrapTable('load', []);
        dom.list.html(func.options_list(db.browser.list, db.browser.default));
    });
    dom.action.save.on('click', function () {
        form.bootstrapTable('append', [table_form(browser, dom)]);
        clear();
        dom.type.val('list');
        dom.custom.val('');
        dom.value.val('0');
        dom.condition.html(generate({from: undefined, to: undefined}));
        dom.table.bootstrapTable('load', []);
        dom.list.html(func.options_list(db.browser.list, db.browser.default));
    });

}
function table_form(data, dom) {
    if (data[data.type] === undefined) {
        switch (data.type) {
            case 'list':
                data[data.type] = dom.list.val();
                break;
            case 'custom':
                data[data.type] = dom.custom.val();
                break;
        }
    }
    var result = {
        browser: {
            type: data.type,
            name: data[data.type]
        }
    };
    if (data.from === undefined && data.to === undefined && data.eq.length === 0) {
        result.version = '*';
    } else {
        var version = {};
        if (data.from !== undefined) {
            version.from = data.from;
        }
        if (data.to !== undefined) {
            version.to = data.to;
        }
        if (data.eq.length > 0) {
            version.eq = data.eq;
        }
        result.version = version;
    }
    return result;
}
function table_load(data) {
    var browser = {
        from: undefined,
        to: undefined,
        eq: [],
        type: 'list',
        list: undefined,
        custom: undefined
    };
    browser.type = data.browser.type;
    if (browser.type === 'custom') {
        browser.custom = data.browser.name;
    } else {
        browser.list = data.browser.name;
    }
    if (data.version === '*') {
        return browser;
    } else {
        if (data.version.to !== undefined) {
            browser.to = data.version.to;
        }
        if (data.version.from !== undefined) {
            browser.from = data.version.from;
        }
        if (data.version.eq.length > 0) {
            browser.eq = data.version.eq;
        }
    }
    return browser;
}
function version(data) {
    if (data === '*') {
        return 'ALL';
    }
    var html = [];
    if (data.from !== undefined && data.to !== undefined) {
        html.push('c ' + data.from + ' до ' + data.to);
    } else {
        if (data.from !== undefined) {
            html.push('c ' + data.from);
        }
        if (data.to !== undefined) {
            html.push('до ' + data.to);
        }
    }
    if (data.eq instanceof  Array && data.eq.length) {
        for (var i in data.eq) {
            html.push(data.eq[i]);
        }
    }
    return html.join('<br>');
}
function table() {
    var configure = {
        toggle: "table",
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        undefinedText: '',
        silent: true,
        showColumns: false,
        showHeader: false,
        showRefresh: false,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'browser',
                title: 'browser',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row, index) {
                    var ad = (value.type === 'list') ? '' : ' *';
                    return value.name + ad;
                }

            },
            {
                field: 'version',
                title: 'version',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: version

            },
            {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var self = $(this).parents("[data-target='#modal-browser']");
                        form = $($(self).data('id'));
                        var dom = require('dom').browser;
                        browser = table_load(row);
                        dom.condition.html(generate(browser));
                        dom.table.bootstrapTable('load', table_save(browser));
                        dom.type.val(browser.type);
                        dom[browser.type].val(browser[browser.type]);
                        dom.modal.modal('show');

                    },
                    'click .remove': function (e, value, row, index) {
                        $(this).parents('table').bootstrapTable('remove', {field: 'browser', values: [row.browser]});
                    }
                }
            }

        ]

    };
    $("[data-target='#modal-browser']").on('click', function () {
        form = $($(this).data('id'));
    });
    $('[data-target="#modal-browser"]').each(function () {
        var self = $(this),
                id = self.data('id'),
                input = $(self.data('input'));
        if (id) {
            $(id).bootstrapTable(configure)
                    .on('pre-body.bs.table', function (event, data) {
                        input.val(JSON.stringify(data));
                    });
        }
    });
}
function init() {
    main();
    table();
}
module.exports = function () {
    event.on('app.browser', init);
};