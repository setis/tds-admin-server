var func = require('func'),
        event = require('event'),
        cfg = require('cfg'),
        action_index,
        change = false,
        db = require('db'),
        setting = cfg.modules.server;

function server(data) {
    var options = func.options(data, {name: 'ip', value: 'id'});
    $('select.server-select').each(function (index, dom) {
        $(dom).html(options);
    });
}
function handler(data) {
    db.server = {index: {}, user: {}, data: {}};
    var result = {};
    for (var k in data) {
        var row = data[k];
        result[row.id] = row;
        db.server.index[row.ip.main] = row.id;
        db.server.user[row.uid] = row.id;
        var ips = row.ip.addition;
        if (ips.length > 0) {
            for (var i in ips) {
                db.server.index[ips[i]] = row.id;
            }
        }
    }
    db.server.data = result;
    event.emit('data.server');
}
function init() {
    var dom = require('dom').server;
    var configure = {
        method: 'GET',
        url: cfg.action('server.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        toolbar: '#toolbar-server',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            }, {
                field: 'name',
                title: 'name',
                align: 'center',
                valign: 'middle',
                sortable: true

            }, {
                field: 'ip',
                title: 'ip main',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value) {
                    return value.main;
                }
            }, {
                field: 'ip',
                title: 'ip addition',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value) {
                    if (value.addition === undefined || (value.addition instanceof Array && value.addition === 0)) {
                        return '';
                    }
                    var result = [];
                    for (var i in value.addition) {
                        result.push('<p>' + value.addition[i] + '</p>');
                    }
                    return result.join('<br>');
                }
            }, {
                field: 'uid',
                title: 'user',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row, index) {
                    var user = db.users.index;
                    return (user[value] === undefined) ? 'None' : user[value];
                }
            }, {
                field: 'login',
                title: 'Login',
                align: 'center',
                valign: 'middle',
                sortable: true
            }, {
                field: 'password',
                title: 'Password',
                align: 'left',
                valign: 'top',
                sortable: true

            }, {
                field: 'description',
                title: 'description',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        for (var name in row) {
                            switch (name) {
                                case 'ip':
                                    var value = row[name];
                                    modal.find('[name="ip\[main\]"]').val(value.main);
                                    modal.find('.validation-list[data-id="#ip-server-change"]').text(value.addition.join()).trigger('focusout');
                                    break;
                                default :
                                    var value = row[name];
                                    modal.find('[name="' + name + '"]').val(value);
                                    break;
                            }

                        }
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            switch (name) {
                                case 'ip':
                                    var value = row[name];
                                    modal.find('[name="ip\[main\]"]').val(value.main);
                                    modal.find('[name="ip\[addition\]"]').val(value.addition.join());
                                    break;
                                default :
                                    var value = row[name];
                                    modal.find('[name="' + name + '"]').val(value);
                                    break;
                            }

                        }
                        action_index = index;
                    }
                }
            }]
    };
    dom.table.remove.bootstrapTable({
        toggle: "table",
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
           {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            }, {
                field: 'name',
                title: 'name',
                align: 'center',
                valign: 'middle',
                sortable: true

            }, {
                field: 'ip',
                title: 'ip main',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value) {
                    return value.main;
                }
            }, {
                field: 'ip',
                title: 'ip addition',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value) {
                    if (value.addition === undefined || (value.addition instanceof Array && value.addition === 0)) {
                        return '';
                    }
                    var result = [];
                    for (var i in value.addition) {
                        result.push('<p>' + value.addition[i] + '</p>');
                    }
                    return result.join('<br>');
                }
            }, {
                field: 'uid',
                title: 'user',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row, index) {
                    var user = db.users.index;
                    return (user[value] === undefined) ? 'None' : user[value];
                }
            }, {
                field: 'login',
                title: 'Login',
                align: 'center',
                valign: 'middle',
                sortable: true
            }, {
                field: 'password',
                title: 'Password',
                align: 'left',
                valign: 'top',
                sortable: true

            }, {
                field: 'description',
                title: 'description',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }

        ]
    });
    event.on('refresh.server', function (event, data) {
        change = true;
        func.refresh({
            cfg: setting.refreshTable,
            event: event,
            data: data,
            db: function () {
                db.server = {
                    index: {},
                    data: {}
                };
            },
            index: action_index,
            dom: dom.table
        });
    });
    dom.table.main.bootstrapTable(configure).on('pre-body.bs.table', function (e, data) {
        if (change) {
            change = false;
            server(data);
            handler(data);
        }
    }).on('load-success.bs.table', function (e, data) {
        server(data);
        handler(data);
    });
    var action = require('controller');
    dom.action.add.click(action['server.create']);
    dom.action.edit.click(action['server.change']);
    dom.action.remove.one.click(action['server.remove']);
    dom.modal.removeAll.on('shown.bs.modal', function () {
        dom.table.remove
                .bootstrapTable('load', dom.table.main.bootstrapTable('getSelections'))
                .bootstrapTable('resetView');
    });
    dom.action.remove.many.click(action['server.remove_all']);
}

module.exports = function () {
    db.server = {
        index: {},
        data: [],
        user: {}
    },
    event.on('app.server', function () {
        var id = setTimeout(init, 5e3);
        event.once('data.user', function () {
            clearTimeout(id);
            init();
        });
    });
};