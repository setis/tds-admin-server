var func = require('func'),
    event = require('event'),
    cfg = require('cfg'),
    db = require('db');
function result(count) {
    if (isNaN(count)) {
        return 0;
    }
    if (count === Infinity) {
        return 100;
    }
    return count;

}
function browser(dom, company, date) {
    var configure = {
        toggle: "table",
        cache: false,
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "All"],
        sidePagination: "client",
        method: 'GET',
        url: cfg.action('static'),
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {select: 'browser', company: company, type: "day", date: date};
        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        undefinedText: '',
        silent: true,
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        detailView: true,
        detailFormatter: function (row, index) {
            return 'sdhfjkhsdjkfkj';
        },
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'browser',
                title: 'browser',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'version',
                title: 'version',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'uniq',
                title: 'uniq',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'in',
                title: 'in',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'pre',
                title: 'pre',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'drop',
                title: 'drop',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'out',
                title: 'out',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'ban',
                title: 'ban',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'unban',
                title: 'unban',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                title: 'send',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row, index) {
                    ['out', 'uniq'].forEach(function (k) {
                        row[k] = Number(row[k]);
                    });
                    return result(Math.floor(row.out / (row.uniq / 100))) + '%';
                }
            },
            {
                title: 'fresh',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row, index) {
                    ['out', 'uniq', 'drop'].forEach(function (k) {
                        row[k] = Number(row[k]);
                    });
                    count = Math.floor(row.out / ((row.uniq - row.drop) / 100));
                    return (row.uniq - row.drop) + '(' +result(count) + '%)';
                }
            }

        ]
    };
    dom.bootstrapTable(configure);
}
function country(dom, company, date) {
    var configure = {
        toggle: "table",
        cache: false,
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "All"],
        sidePagination: "client",
        method: 'GET',
        url: cfg.action('static'),
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {select: 'country', company: company, type: "day", date: date};
        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        undefinedText: '',
        silent: true,
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        detailView: true,
        detailFormatter: function (row, index) {
            return 'sdhfjkhsdjkfkj';
        },
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'country',
                title: 'country',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'uniq',
                title: 'uniq',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'in',
                title: 'in',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'pre',
                title: 'pre',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'drop',
                title: 'drop',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'out',
                title: 'out',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'ban',
                title: 'ban',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'unban',
                title: 'unban',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                title: 'send',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row, index) {
                    ['out', 'uniq'].forEach(function (k) {
                        row[k] = Number(row[k]);
                    });
                    return result(Math.floor(row.out / (row.uniq / 100))) + '%';
                }
            },
            {
                title: 'fresh',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row, index) {
                    ['out', 'uniq', 'drop'].forEach(function (k) {
                        row[k] = Number(row[k]);
                    });
                    count = Math.floor(row.out / ((row.uniq - row.drop) / 100));
                    return (row.uniq - row.drop) + '(' +result(count) + '%)';
                }
            }

        ]
    };
    dom.bootstrapTable(configure);
}
function referer(dom, company, date) {
    var configure = {
        toggle: "table",
        cache: false,
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "All"],
        sidePagination: "client",
        method: 'GET',
        url: cfg.action('static'),
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {select: 'referer', company: company, type: "day", date: date};
        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        toolbar: dom.toolbar,
        undefinedText: '',
        silent: true,
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'referer',
                title: 'referer',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'uniq',
                title: 'uniq',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'in',
                title: 'in',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'pre',
                title: 'pre',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'drop',
                title: 'drop',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'out',
                title: 'out',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'ban',
                title: 'ban',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'unban',
                title: 'unban',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                title: 'send',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row, index) {
                    ['out', 'uniq'].forEach(function (k) {
                        row[k] = Number(row[k]);
                    });
                    return result(Math.floor(row.out / (row.uniq / 100))) + '%';
                }
            },
            {
                title: 'fresh',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row, index) {
                    ['out', 'uniq', 'drop'].forEach(function (k) {
                        row[k] = Number(row[k]);
                    });
                    count = Math.floor(row.out / ((row.uniq - row.drop) / 100));
                    return (row.uniq - row.drop) + '(' +result(count) + '%)';
                }
            }
        ]
    };
    dom.bootstrapTable(configure);
}
function general(dom, company, date) {
    var configure = {
        toggle: "table",
        cache: false,
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "All"],
        sidePagination: "client",
        method: 'GET',
        url: cfg.action('static'),
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {select: 'general', company: company, type: "day", date: date};
        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
//        toolbar: dom.toolbar,
        undefinedText: '',
        silent: true,
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'uniq',
                title: 'uniq',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'in',
                title: 'in',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'pre',
                title: 'pre',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'drop',
                title: 'drop',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'out',
                title: 'out',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'ban',
                title: 'ban',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'unban',
                title: 'unban',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                title: 'send',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row, index) {
                    ['out', 'uniq'].forEach(function (k) {
                        row[k] = Number(row[k]);
                    });
                    return result(Math.floor(row.out / (row.uniq / 100))) + '%';
                }
            },
            {
                title: 'fresh',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row, index) {
                    ['out', 'uniq', 'drop'].forEach(function (k) {
                        row[k] = Number(row[k]);
                    });
                    count = Math.floor(row.out / ((row.uniq - row.drop) / 100));
                    return (row.uniq - row.drop) + '(' +result(count) + '%)';
                }
            }
        ]
    };
    dom.bootstrapTable(configure);
}

exports.country = country;
exports.referer = referer;
exports.general = general;
exports.browser = browser;