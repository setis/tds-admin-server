var func = require('func'),
        cfg = require('cfg'),
        action_index,
        change = false,
        db = require('db'),
        event = require('event'),
        setting = cfg.modules.ip;
var server = func.server;
function init() {
    var dom = require('dom').ip;
    var configure = {
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        method: 'GET',
        url: cfg.action('ip.list'),
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        toolbar: '#toolbar-ip',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'ip',
                title: 'ip',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            },
            {
                field: 'ip',
                title: 'server',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: server
            },
            {
                field: 'avcheck',
                title: 'avdetect',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    var style;
                    switch (value.detect) {
                        case 0:
                            style = 'text-success';
                            break;
                        case 1:
                            style = 'text-danger';
                            break;
                    }
                    return '<p class="' + style + '"><a href="' + value.link + '" target="_blank">' + value.detect + '/' + value.total + '</a></p>';
                }
            },
            {
                field: 'alive',
                title: 'alive',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            },
            {
                field: 'www',
                title: 'access',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            },
            {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return  '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        modal.find('[name="ip"]').val(row.ip);
                        modal.find('[name="server"]').val(server(row.ip));
                        action_index = index;
                    }
                }
            }]
    };
    event.on('refresh.ip', function (event, data) {
        change = true;
        func.refresh({
            cfg: setting.refreshTable,
            event: event,
            data: data,
            db: function () {
                db.ip = {};
            },
            index: action_index,
            dom: dom.table
        });
    });
    dom.table.main.bootstrapTable(configure).on('pre-body.bs.table', function (e, data) {
        if (change) {
            change = false;
            db.ip = data;
            event.emit('data.ip', data);
        }
    }).on('load-success.bs.table', function (e, data) {
        db.ip = data;
        event.emit('data.ip', data);
    });
    var action = require('controller');
    dom.action.add.click(action['ip.create']);
    dom.action.remove.one.click(action['ip.remove']);
    dom.action.remove.many.click(action['ip.remove_all']);
    dom.table.remove.bootstrapTable({
        toggle: "table",
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'ip',
                title: 'ip',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            },
            {
                field: 'ip',
                title: 'server',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: server
            },
            {
                field: 'avcheck',
                title: 'avdetect',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    var style;
                    switch (value.detect) {
                        case 0:
                            style = 'text-success';
                            break;
                        case 1:
                            style = 'text-danger';
                            break;
                    }
                    return '<p class="' + style + '"><a href="' + value.link + '" target="_blank">' + value.detect + '/' + value.total + '</a></p>';
                }
            },
            {
                field: 'alive',
                title: 'alive',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            },
            {
                field: 'www',
                title: 'access',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            }
        ]
    });
    dom.modal.removeAll.on('shown.bs.modal', function () {
        dom.table.remove
                .bootstrapTable('load', dom.table.main.bootstrapTable('getSelections'))
                .bootstrapTable('resetView');
    });
}
module.exports = function () {
    db.ip = {};
    event.on('app.ip', function () {
        var id = setTimeout(init, 5e3);
        event.once('data.server', function () {
            clearTimeout(id);
            init();
        });

    });
};