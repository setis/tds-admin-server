var func = require('func'),
        event = require('event'),
        action_index,
        change = false,
        cfg = require('cfg'),
        db = require('db'),
        setting = cfg.modules.virustotal;

function init() {
    var dom = require('dom').virustotal;
    var configure = {
        method: 'GET',
        url: cfg.action('virustotal.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
//        ajax:function(params){
////            https://github.com/wenzhixin/bootstrap-table-examples/blob/master/options/custom-ajax.html
//        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        toolbar: '#toolbar-virustotal',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'login',
                title: 'Login',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'email',
                title: 'email',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'password',
                title: 'Password',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'key',
                title: 'api',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value);

                        }
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value);
                        }
                        action_index = index;
                    }
                }
            }]
    };
    event.on('refresh.virustotal', function (event, data) {
        change = true;
        func.refresh({
            cfg: setting.refreshTable,
            event: event,
            data: data,
            db: function () {
                db.virustotal = {};
            },
            index: action_index,
            dom: dom.table.main,
            id:'_id'
        });
    });
    dom.table.main
            .bootstrapTable(configure)
            .on('pre-body.bs.table', function (e, data) {
                if (change) {
                    change = false;
                    db.virustotal = data;
                }
            });
    var action = require('controller');
    dom.action.add.click(action['virustotal.create']);
    dom.action.edit.click(action['virustotal.change']);
    dom.action.remove.one.click(action['virustotal.remove']);
    dom.action.remove.many.click(action['virustotal.remove_all']);
    dom.form.add.find('input[type="checkbox"]').bootstrapSwitch();
    dom.table.remove.bootstrapTable({
        toggle: "table",
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'login',
                title: 'Login',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'email',
                title: 'email',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'password',
                title: 'Password',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'key',
                title: 'api',
                align: 'left',
                valign: 'middle',
                sortable: true
            }
        ]
    });
    dom.modal.removeAll.on('shown.bs.modal', function () {
        dom.table.remove
                .bootstrapTable('load', dom.table.main.bootstrapTable('getSelections'))
                .bootstrapTable('resetView');
    });

}

module.exports = function () {
    event.on('app.virustotal', init);
};