var event = require('event');
function initialize(dom) {
    dom.find('input[type="checkbox"]').bootstrapSwitch();
    var ttl = dom.find('input[name="jslayer[js][ttl]"]');
    ttl.on('change', function () {
        var day = parseInt(dom.find("input[name='jslayer[cache][day]']").val()) || 0;
        var hour = parseInt(dom.find("input[name='jslayer[cache][hour]']").val()) || 0;
        var minute = parseInt(dom.find("input[name='jslayer[cache][minut]']").val()) || 0;
        var sec = parseInt(dom.find("input[name='jslayer[cache][second]']").val()) || 0;
        var ttl = sec + (minute * 60) + (hour * 3600) + (day * 86400);
        $(this).val(ttl);
    });
    dom.find("input[name='jslayer[cache][day]']")
            .TouchSpin({
                min: 0,
                max: 100,
                prefix: 'day'
            })
            .on('change', function () {
                ttl.trigger('change');
            });
    dom.find("input[name='jslayer[cache][hour]']")
            .TouchSpin({
                min: 0,
                max: 23,
                prefix: 'hour'
            })
            .on('change', function () {
                ttl.trigger('change');
            });
    dom.find("input[name='jslayer[cache][minut]']")
            .TouchSpin({
                min: 0,
                max: 60,
                prefix: 'minut'
            })
            .on('change', function () {
                ttl.trigger('change');
            });
    dom.find("input[name='jslayer[cache][second]']")
            .TouchSpin({
                min: 0,
                max: 60,
                prefix: 'sec'
            })
            .on('change', function () {
                ttl.trigger('change');
            });
}
function load(dom, data) {
    var ttl = data.js.ttl;
    var day = Math.floor(ttl / 86400);
    ttl -= 86400 * day;
    var hour = Math.floor(ttl / 3600);
    ttl -= 3600 * hour;
    var minut = Math.floor(ttl / 60);
    ttl -= 60 * minut;
    dom.find("input[name='jslayer[cache][day]']").val(day);
    dom.find("input[name='jslayer[cache][hour]']").val(hour);
    dom.find("input[name='jslayer[cache][minut]']").val(minut);
    dom.find("input[name='jslayer[cache][second]']").val(ttl);
    dom.find("input[name='jslayer[crypt][avcheck]']").prop("checked", data.crypt.avcheck);
    dom.find("input[name='jslayer[crypt][domain]']").prop("checked", data.crypt.domain);
    dom.find("input[name='jslayer[short][mode]']").prop("checked", data.short.mode);
    dom.find("input[name='jslayer[short][ttl]']").val(data.short.ttl);
    dom.find("input[name='jslayer[js][count]']").val(data.js.count);
}
module.exports = function () {
    event.on('app.jslayer', function () {
        $('.jslayer').each(function () {
            initialize($(this));
        });
    });
};
