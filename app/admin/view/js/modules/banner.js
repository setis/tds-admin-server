var event = require('event'),
    db = require('db');
function banner(user, exception) {
    var list = db.banner.index[user], banner = db.banner.data;
    if (list === undefined || (list instanceof Array && list.length === 0)) {
        return [
            {
                text: 'None',
                value: null,
                selected: true,
                description: 'not found banner',
                imageSrc: ''
            }
        ];
    } else {
        var result = [];
        var flag = (exception === undefined || (exception instanceof  Array && exception.length === 0));
        for (var i in list) {
            var data = banner[list[i]];
            if (flag) {
                result.push({
                    text: data.id,
                    value: data.id,
                    selected: false,
                    description: data.width + 'x' + data.height,
                    imageSrc: data.src
                });
            } else if (exception.indexOf(data.id.toString()) === -1) {
                result.push({
                    text: data.id,
                    value: data.id,
                    selected: false,
                    description: data.width + 'x' + data.height,
                    imageSrc: data.src
                });
            }
        }
        if (result.length === 0) {
            return [
                {
                    text: 'None',
                    value: null,
                    selected: true,
                    description: 'not found banner',
                    imageSrc: ''
                }
            ];
        }
        return result;
    }

}
var thumbnail = '<div class="thumbnail repeater-thumbnail">\n\
<div class="caption"><input type="checkbox" class="pull-left"><a href="#" class="btn btn-danger btn-xs pull-right banner-remove" data-id="{{id}}" role="button"><i class="glyphicon glyphicon-remove"></i></a></div>\n\
<a href="{{url}}" title="{{title}}" data-gallery="{{gallery}}"><img class="banner" src="{{src}}" alt="{{alt}}" ></a>\n\
<div class="caption"><p>{{width}}x{{height}}</p></div></div>';
function init(self) {
    var dom = require('dom').banner;
    var banners = $(self.data('repeater')),
        input = $(self.data('input'));
    var form = self;
    input.on('change', function () {
        var self = $(this);
        banners
            .repeater({
                dataSource: function (options, callback) {
                    var data = self.val().split(',');
                    var result = [], banners = db.banner.data;
                    for (var i in data) {
                        var id = data[i];
                        result.push(banners[id]);
                    }
                    var responseData = {
                        count: result.length,
                        items: result,
                        page: options.pageIndex,
                        pages: Math.ceil(result.length / options.pageSize)
                    };
                    var firstItem, lastItem;
                    firstItem = options.pageIndex * options.pageSize;
                    lastItem = firstItem + options.pageSize;
                    lastItem = (lastItem <= responseData.count) ? lastItem : responseData.count;
                    responseData.start = firstItem + 1;
                    responseData.end = lastItem;
                    console.log(responseData);
                    callback(responseData);
                },
                thumbnail_template: thumbnail
            })
            .repeater('render');
        banners
            .find('.banner-remove')
            .on('click', function () {
                var self1 = $(this),
                    id = self1.data('id').toString();
                var list = self.val().split(',').filter(function (v) {
                    return (v !== id);
                });
                console.log(list,id);
                self
                    .val(list.join());
                self1.parent().parent().remove();
            });
    });

    self.on('click', function () {
        var user = $(self.data('user')).val(),
            input = $(self.data('input')),
            list = input.val().split(',');
        console.log(list);
        var d = list.indexOf("");
        if (d === 0) {
            list = [];
        }
        $(document.getElementById('input-banner')).ddslick('destroy');
        dom.input.ddslick({
            data: banner(user, list),
            width: '100%',
            selectText: "Select Banner",
            onSelected: function (selectedData) {
                var id = selectedData.selectedData.value;
                var handler = function () {
                    list.push(id);
                    input
                        .val(list.join())
                        .change();
                    dom.modal.modal('hide');
                };
                dom.action.one('click', handler);
                dom.modal.one('hidden.bs.modal', function () {
                    dom.action.off('click', handler);
                });
            }
        });
    });
    return false;
}
module.exports = init;