var func = require('func'),
        cfg = require('cfg'),
        action_index,
        change = false,
        db = require('db'),
        event = require('event'),
        setting = cfg.modules.domain;
var server = func.server;
function handler(data) {
    var result = {}, free = {}, index = {};
    for (var k in data) {
        var row = data[k];
        result[row.id] = row;
        if (!row.use && row.user !== undefined && row.user !== null) {
            if (free[row.user.id] === undefined) {
                free[row.user.id] = [];
            }
            free[row.user.id].push(row.id);
        }
        index[row.id] = row.host;
    }
    db.domain = {index: index, data: result, free: free};
    event.emit('data.domain', index);
}
function init() {
    var dom = require('dom').domain;
    var configure = {
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        method: 'GET',
        url: cfg.action('domain.list'),
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        toolbar: '#toolbar-domain',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
//                formatter: nameFormatter

            }, {
                field: 'host',
                title: 'host',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row) {
                    var status;
                    switch (row.use) {
                        case false:
                        default :
                            status = '<p class="text-success pull-rigth small">free</p>';
                            break;
                        case true:
                            status = '<p class="text-danger pull-rigth small">use</p>';
                            break;
                    }
                    return '<a href="http://' + value + '" class="pull-left">' + value + '</a>' + status;
                }
//                sorter: priceSorter
            },
            {
                field: 'user',
                title: 'type',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value === null || value === undefined) ? 'Layer' : 'User';
                }
            },
            {
                field: 'user',
                title: 'user',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value === null || value === undefined) ? '' : db.users.index[value.id] || 'None';
                }
            },
            {
                field: 'avcheck',
                title: 'avdetect',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    var style;
                    switch (value.detect || 1) {
                        case 0:
                            style = 'text-success';
                            break;
                        case 1:
                            style = 'text-danger';
                            break;
                    }
                    return '<p class="' + style + '"><a href="' + value.link + '" target="_blank">' + value.detect + '/' + value.total + '</a></p>';
                }
            },
            {
                field: 'alive',
                title: 'alive',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            },
            {
                field: 'www',
                title: 'access',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            },
            {
                field: 'ip',
                title: 'ip',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.ip) ? '<p class="text-success">' + value.ip + '</p>' : '<p class="text-danger">0.0.0.0</p>';
                }
            },
            {
                field: 'ip',
                title: 'server',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: server
            },
            {
                field: 'mode',
                title: 'mode',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            },
            {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        dom.modal.edit.modal('show');
                        var form = dom.form.edit;
                        for (var name in row) {
                            var value = row[name];
                            switch (name) {
                                case 'user':
                                    var layer = (value === undefined || value === null);
                                    form.find('input[type="checkbox"]').bootstrapSwitch('state', layer, true);
                                    if (!layer) {
                                        form.find('[name="uid"]').val(value.id);
                                    }
                                    var select = form.find('select').parent();
                                    if (layer) {
                                        select.hide();
                                    } else {
                                        select.show();
                                    }
                                    break;
                                default :
                                    form.find('[name="' + name + '"]').val(value);
                                    break;
                            }
                        }
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        dom.modal.remove.modal('show');
                        var form = dom.form.remove;
                        for (var name in row) {
                            var value = row[name];
                            switch (name) {
                                case 'user':
                                    var layer = (value === undefined || value === null);
                                    form.find('input[type="checkbox"]').bootstrapSwitch('state', layer, true);
                                    if (!layer) {
                                        form.find('[name="uid"]').val(value.id);
                                    }
                                    var select = form.find('select').parent();
                                    if (layer) {
                                        select.hide();
                                    } else {
                                        select.show();
                                    }
                                    break;
                                default :
                                    form.find('[name="' + name + '"]').val(value);
                                    break;
                            }
                        }
                        action_index = index;
                    }
                }
            }]
    };
    event.on('refresh.domain', function (event, data) {
        change = true;
        func.refresh({
            cfg: setting.refreshTable,
            event: event,
            data: data,
            db: function () {
                db.domain = {
                    index: {},
                    data: {}
                };
            },
            index: action_index,
            dom: dom.table.main
        });
    });
    dom.table.main.bootstrapTable(configure).on('pre-body.bs.table', function (e, data) {
        if (change) {
            change = false;
            handler(data);
        }
    }).on('load-success.bs.table', function (e, data) {
        handler(data);
    });
    var action = require('controller');
    dom.action.add.click(action['domain.create']);
    dom.action.edit.click(action['domain.change']);
    dom.action.remove.one.click(action['domain.remove']);
    dom.action.remove.many.click(action['domain.remove_all']);
    dom.form.add.find('input[type="checkbox"]').bootstrapSwitch('onSwitchChange', function (e, status) {
        var select = dom.form.add.find('select').parent();
        if (status) {
            select.hide();
        } else {
            select.show();
        }
    });
    dom.form.edit.find('input[type="checkbox"]').bootstrapSwitch('onSwitchChange', function (e, status) {
        var select = dom.form.edit.find('select').parent();
        if (status) {
            select.hide();
        } else {
            select.show();
        }
    });
    dom.form.remove.find('input[type="checkbox"]').bootstrapSwitch('onSwitchChange', function (e, status) {
        var select = dom.form.remove.find('select').parent();
        if (status) {
            select.hide();
        } else {
            select.show();
        }
    });
    dom.table.remove.bootstrapTable({
        toggle: "table",
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        undefinedText: '',
        silent: true,
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true

            }, {
                field: 'host',
                title: 'host',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value, row) {
                    var status;
                    switch (row.use) {
                        case false:
                        default :
                            status = '<p class="text-success pull-rigth small">free</p>';
                            break;
                        case true:
                            status = '<p class="text-danger pull-rigth small">use</p>';
                            break;
                    }
                    return '<a href="http://' + value + '" class="pull-left">' + value + '</a>' + status;
                }
//                sorter: priceSorter
            },
            {
                field: 'user',
                title: 'type',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value === null || value === undefined) ? 'Layer' : 'User';
                }
            },
            {
                field: 'user',
                title: 'user',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value === null || value === undefined) ? '' : db.users.index[value.id] || 'None';
                }
            },
            {
                field: 'avcheck',
                title: 'avdetect',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    var style;
                    switch (value.detect || 1) {
                        case 0:
                            style = 'text-success';
                            break;
                        case 1:
                            style = 'text-danger';
                            break;
                    }
                    return '<p class="' + style + '"><a href="' + value.link + '" target="_blank">' + value.detect + '/' + value.total + '</a></p>';
                }
            },
            {
                field: 'alive',
                title: 'alive',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            },
            {
                field: 'www',
                title: 'access',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            },
            {
                field: 'ip',
                title: 'ip',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value.ip) ? '<p class="text-success">' + value.ip + '</p>' : '<p class="text-danger">0.0.0.0</p>';
                }
            },
            {
                field: 'ip',
                title: 'server',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: server
            },
            {
                field: 'mode',
                title: 'mode',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: function (value) {
                    return (value.mode) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            }
        ]
    });
    dom.modal.removeAll.on('shown.bs.modal', function () {
        dom.table.remove
                .bootstrapTable('load', dom.table.main.bootstrapTable('getSelections'))
                .bootstrapTable('resetView');
    });
}
module.exports = function () {
    event.on('app.domain', function () {
        var id = setTimeout(init, 5e3);
        event.once('data.user', function () {
            clearTimeout(id);
            init();
        });

    });
};