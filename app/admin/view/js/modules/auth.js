module.exports = function () {
    var event = require('event');
    event.on('app.auth', function () {
        var dom = require('dom').authorization;
        dom.modal.modal({
            keyboard: false,
            backdrop: false,
            show: true
        });
        event.on('data.auth', function () {
            dom.modal.modal('hide');
        });
        var db = require('db');
        var action = require('controller');
        dom.action.on('click',action["authorization.login"]);
        if (db.auth) {
            dom.modal.modal('hide');
        }
        action["authorization.is"]();
        setTimeout(function(){
            action["authorization.is"]();
        },3e3);
    });
};