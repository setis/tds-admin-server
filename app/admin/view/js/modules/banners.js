var event = require('event'), db = require('db');

function users(dom) {
    var users = db.users.index, html = '';
    html += '<li data-value="0" data-selected="true"><a href="#">All</a></li>';
    for (var id in users) {
        var user = users[id];
        html += '<li data-value="' + id + '"><a href="#">' + user + '</a></li>';
    }
    dom.html(html);
}
function init() {
    var dom = require('dom').banners;
    var upload = {
        uploadUrl: require('cfg').server.up,
        uploadAsync: true,
        minFileCount: 1,
        maxFileCount: 15,
        overwriteInitial: false,
        showUpload: false,
        allowedFileExtensions: ['jpg', 'gif', 'png', 'jpeg'],
        uploadExtraData: function () {
            return {
                user: dom.form.add.val()
            };
        }
    };
    dom.upload.fileinput(upload);
//    dom.upload.on("filepredelete", remove);
    var action = require('controller');
    dom.upload.on('filebatchuploadcomplete', function (event, data, previewId, index) {
        console.log(data, previewId, index);
        action['banner.list']();
        dom.upload.fileinput('clear');
        dom.modal.add.modal('hide');
    });
    dom.upload.on('fileuploaded', function (event, file, previewId, index, reader) {
        console.log(file, previewId, index, reader);
    });
    dom.action.add.click(function () {
        dom.upload.fileinput('upload');
    });
    dom.action.update.on('click', action['banner.list']);
    dom.action.edit.click(function () {
    });
//    dom.action.remove.one.click(remove);
//    dom.action.remove.many.click(remove_many);
    dom.repeat.thumbnail.on('click', '.banner-remove', action['banner.remove']);
    dom.repeat.thumbnail.repeater({
        dataSource: function (options, callback) {
            var source = db.banner.data;
            if (options.filter.value > 0) {
                var data = [], user = options.filter.value;
                for (var i in source) {
                    if (user === source[i].uid) {
                        data.push(source[i]);
                    }
                }
            } else {
                var data = [];
                for (var i in source) {
                    data.push(source[i]);
                }
            }
//                blueimp.Gallery($('#blueimp-gallery-links'), {
//                    container: '#blueimp-gallery',
//                    carousel: true
//                });
            var responseData = {
                count: data.length,
                items: data,
                page: options.pageIndex,
                pages: Math.ceil(data.length / options.pageSize)
            };
            var firstItem, lastItem;
            firstItem = options.pageIndex * options.pageSize;
            lastItem = firstItem + options.pageSize;
            lastItem = (lastItem <= responseData.count) ? lastItem : responseData.count;
            responseData.start = firstItem + 1;
            responseData.end = lastItem;
            callback(responseData);
        },
        thumbnail_template: '<div class="thumbnail repeater-thumbnail">\n\
<div class="caption"><input type="checkbox" class="pull-left"><a href="#" class="btn btn-danger btn-xs pull-right banner-remove" data-id="{{id}}" role="button"><i class="glyphicon glyphicon-remove"></i></a></div>\n\
<a href="{{url}}" title="{{title}}" data-gallery="{{gallery}}"><img class="banner" src="{{src}}" alt="{{alt}}" ></a>\n\
<div class="caption"><h4>{{user}}</h4><p>{{width}}x{{height}}</p></div></div>'
    });
    event.on('data.banner', function () {
        dom.repeat.thumbnail.repeater('render');
    });
    users(dom.users);
    action['banner.list']();
}
module.exports = function () {
    event.on('data.user', function () {
        users(require('dom').banners.users);
    });
    event.once('data.user', function () {
        init();
    });
};