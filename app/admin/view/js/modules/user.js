var func = require('func'),
        event = require('event'),
        cfg = require('cfg'),
        action_index,
        change = false,
        db = require('db'),
        setting = cfg.modules.user;
db.users = {
        index: {},
        data: []
    };
function handler(data) {
    db.users = {index: {}, data: data};
    for (var k in data) {
        var row = data[k];
        db.users.index[row.id] = row.login;
    }
    event.emit('data.user', db.users.index);
}

function user(data, _default) {
    var options = func.options(data, {name: 'login', value: 'id'}, _default);
    $('select.user-select').each(function (index, dom) {
        $(dom).html(options);
    });
}
function init() {
    var dom = require('dom').user;
    var configure = {
        method: 'GET',
        url: cfg.action('user.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
//        ajax:function(params){
////            https://github.com/wenzhixin/bootstrap-table-examples/blob/master/options/custom-ajax.html
//        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        toolbar: '#toolbar-user',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            }, {
                field: 'login',
                title: 'Login',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'password',
                title: 'Password',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'mode',
                title: 'access',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            }, {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            switch(name){
                                case 'mode':
                                    dom.form.edit.find('input[type="checkbox"]').bootstrapSwitch('state', value, true);
                                    break;
                                default :
                                    modal.find('[name="' + name + '"]').val(value);
                                    break;
                            }
                            
                        }
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        for (var name in row) {
                            switch(name){
                                case 'mode':
                                    dom.form.edit.find('input[type="checkbox"]').bootstrapSwitch('state', value, true);
                                    break;
                                default :
                                    modal.find('[name="' + name + '"]').val(value);
                                    break;
                            }
                            
                        }
                        action_index = index;
                    }
                }
            }]
    };
    event.on('refresh.user', function (event, data) {
        change = true;
        func.refresh({
            cfg: setting.refreshTable,
            event: event,
            data: data,
            db: function () {
                db.users = {
                    index: {},
                    data: {}
                };
            },
            index: action_index,
            dom: dom.table.main
        });
    });

    dom.table.main.bootstrapTable(configure).on('pre-body.bs.table', function (e, data) {
        if (change) {
            change = false;
            user(data, db.user.uid);
            handler(data);
        }
    }).on('load-success.bs.table', function (e, data) {
        user(data, db.user.uid);
        handler(data);
    });
    var action = require('controller');
    dom.action.add.click(action['user.create']);
    dom.action.edit.click(action['user.change']);
    dom.action.remove.one.click(action['user.remove']);
    dom.action.remove.many.click(action['user.remove_all']);
    dom.form.add.find('input[type="checkbox"]').bootstrapSwitch();
    dom.table.remove.bootstrapTable({
        toggle: "table",
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            }, {
                field: 'login',
                title: 'Login',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'password',
                title: 'Password',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'mode',
                title: 'access',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return (value) ? '<p class="text-success">on</p>' : '<p class="text-danger">off</p>';
                }
            }
        ]
    });
    dom.modal.removeAll.on('shown.bs.modal', function () {
        dom.table.remove
                .bootstrapTable('load', dom.table.main.bootstrapTable('getSelections'))
                .bootstrapTable('resetView');
    });
    
}

module.exports = function () {
    
    event.on('app.user', function () {
        var id = setTimeout(init, 5e3);
        event.once('data.auth', function () {
            clearTimeout(id);
            init();
        });
    });
};