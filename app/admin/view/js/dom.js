module.exports = {
    authorization: {
        modal: $(document.getElementById('modal-login')),
        form: $(document.getElementById('form-login')),
        alert: $(document.getElementById('alert-login')),
        action: $(document.getElementById('action-login'))
    },
    user: {
        modal: {
            edit: $(document.getElementById('modal-user-change')),
            remove: $(document.getElementById('modal-user-delete')),
            add: $(document.getElementById('modal-user-create')),
            removeAll: $(document.getElementById('modal-user-many-delete'))
        },
        table: {
            main: $(document.getElementById('table-user-main')),
            remove: $(document.getElementById('table-user-remove'))
        },
        user: $('select.user-select'),
        alert: $(document.getElementById('alert-user')),
        form: {
            edit: $(document.getElementById('form-user-change')),
            remove: $(document.getElementById('form-user-delete')),
            add: $(document.getElementById('form-user-create'))
        },
        action: {
            panel: $(document.getElementById('panel-user-action')),
            add: $(document.getElementById('action-user-create')),
            edit: $(document.getElementById('action-user-change')),
            remove: {
                one: $(document.getElementById('action-user-delete')),
                many: $(document.getElementById('action-user-many-delete'))
            }
        }
    },
    virustotal: {
        modal: {
            edit: $(document.getElementById('modal-virustotal-change')),
            remove: $(document.getElementById('modal-virustotal-delete')),
            add: $(document.getElementById('modal-virustotal-create')),
            removeAll: $(document.getElementById('modal-virustotal-many-delete'))
        },
        table: {
            main: $(document.getElementById('table-virustotal-main')),
            remove: $(document.getElementById('table-virustotal-remove'))
        },
        alert: $(document.getElementById('alert-virustotal')),
        form: {
            edit: $(document.getElementById('form-virustotal-change')),
            remove: $(document.getElementById('form-virustotal-delete')),
            add: $(document.getElementById('form-virustotal-create'))
        },
        action: {
            panel: $(document.getElementById('panel-virustotal-action')),
            add: $(document.getElementById('action-virustotal-create')),
            edit: $(document.getElementById('action-virustotal-change')),
            remove: {
                one: $(document.getElementById('action-virustotal-delete')),
                many: $(document.getElementById('action-virustotal-many-delete'))
            }
        }
    },
    country: {
        modal: $(document.getElementById('modal-country')),
        table: $(document.getElementById('table-country'))
    },
    "exploit-select": {
        modal: $('#modal-exploit-select'),
        table: $('table#table-exploit-select')
    },
    browser: {
        modal: $(document.getElementById('modal-browser')),
        table: $(document.getElementById('table-browser')),
        action: {
            add: $(document.getElementById('action-browser-add')),
            clear: $(document.getElementById('action-browser-clear')),
            save: $(document.getElementById('action-browser-save'))
        },
        condition: $(document.getElementById('browser-condition')),
        value: $(document.getElementById('browser-value')),
        type: $(document.getElementById('browser-type')),
        list: $(document.getElementById('browser-list')),
        custom: $(document.getElementById('browser-custom'))
    },
    domain: {
        modal: {
            edit: $(document.getElementById('modal-domain-change')),
            remove: $(document.getElementById('modal-domain-delete')),
            add: $(document.getElementById('modal-domain-create')),
            removeAll: $(document.getElementById('modal-domain-many-delete'))
        },
        table: {
            main: $(document.getElementById('table-domain-main')),
            remove: $(document.getElementById('table-domain-remove'))
        },
        alert: $(document.getElementById('alert-domain')),
        form: {
            edit: $(document.getElementById('form-domain-change')),
            remove: $(document.getElementById('form-domain-delete')),
            add: $(document.getElementById('form-domain-create'))
        },
        action: {
            panel: $(document.getElementById('panel-domain-action')),
            add: $(document.getElementById('action-domain-create')),
            edit: $(document.getElementById('action-domain-change')),
            remove: {
                one: $(document.getElementById('action-domain-delete')),
                many: $(document.getElementById('action-domain-many-delete'))
            }
        }
    },
    ip: {
        modal: {
            edit: $(document.getElementById('modal-ip-change')),
            remove: $(document.getElementById('modal-ip-delete')),
            add: $(document.getElementById('modal-ip-create')),
            removeAll: $(document.getElementById('modal-ip-many-delete'))
        },
        table: {
            main: $(document.getElementById('table-ip-main')),
            remove: $(document.getElementById('table-ip-remove'))
        },
        alert: $(document.getElementById('alert-ip')),
        form: {
            edit: $(document.getElementById('form-ip-change')),
            remove: $(document.getElementById('form-ip-delete')),
            add: $(document.getElementById('form-ip-create'))
        },
        action: {
            panel: $(document.getElementById('panel-ip-action')),
            add: $(document.getElementById('action-ip-create')),
            edit: $(document.getElementById('action-ip-change')),
            remove: {
                one: $(document.getElementById('action-ip-delete')),
                many: $(document.getElementById('action-ip-many-delete'))
            }
        }
    },
    exploit: {
        modal: {
            edit: $(document.getElementById('modal-exploit-change')),
            remove: $(document.getElementById('modal-exploit-delete')),
            add: $(document.getElementById('modal-exploit-create')),
            removeAll: $(document.getElementById('modal-exploit-many-delete'))
        },
        table: {
            main: $(document.getElementById('table-exploit-main')),
            remove: $(document.getElementById('table-exploit-remove'))
        },
        toolbar: '#toolbar-exploit',
        alert: $(document.getElementById('alert-exploit')),
        form: {
            edit: $(document.getElementById('form-exploit-change')),
            remove: $(document.getElementById('form-exploit-delete')),
            add: $(document.getElementById('form-exploit-create'))
        },
        action: {
            add: $(document.getElementById('action-exploit-create')),
            edit: $(document.getElementById('action-exploit-change')),
            remove: {
                one: $(document.getElementById('action-exploit-delete')),
                many: $(document.getElementById('action-exploit-many-delete'))
            }
        }
    },
    filter: {
        tables: [
        ]
    },
    server: {
        modal: {
            edit: $(document.getElementById('modal-server-change')),
            remove: $(document.getElementById('modal-server-delete')),
            add: $(document.getElementById('modal-server-create')),
            removeAll: $(document.getElementById('modal-server-many-delete'))
        },
        table: {
            main: $(document.getElementById('table-server-main')),
            remove: $(document.getElementById('table-server-remove'))
        },
        alert: $(document.getElementById('alert-server')),
        form: {
            edit: $(document.getElementById('form-server-change')),
            remove: $(document.getElementById('form-server-delete')),
            add: $(document.getElementById('form-server-create'))
        },
        action: {
            panel: $(document.getElementById('panel-server-action')),
            add: $(document.getElementById('action-server-create')),
            edit: $(document.getElementById('action-server-change')),
            remove: {
                one: $(document.getElementById('action-server-delete')),
                many: $(document.getElementById('action-server-many-delete'))
            }
        }
    },
    company: {
        modal: {
            edit: $(document.getElementById('modal-company-change')),
            remove: $(document.getElementById('modal-company-delete')),
            add: $(document.getElementById('modal-company-create')),
            removeAll: $(document.getElementById('modal-company-many-delete'))
        },
        form: {
            edit: $(document.getElementById('form-company-change')),
            remove: $(document.getElementById('form-company-delete')),
            add: $(document.getElementById('form-company-create'))
        },
        interval: {
            add: {
                switch : $(document.getElementById('interval-switch-add')),
                rand: $(document.getElementById('interval-rand-add')),
                eq: $(document.getElementById('interval-eq-add'))
            },
            edit: {
                switch : $(document.getElementById('interval-switch-edit')),
                rand: $(document.getElementById('interval-rand-edit')),
                eq: $(document.getElementById('interval-eq-edit'))
            }
        },
        toolbar: '#toolbar-company',
        table: {
            main: $(document.getElementById('table-company-main')),
            remove: $(document.getElementById('table-company-remove')),
            edit: $('#table-company-change-filter')
        },
        action: {
            add: $(document.getElementById('action-company-create-clear')),
            edit: {
                save: $(document.getElementById('action-company-change-save')),
                default: $(document.getElementById('action-company-change-default')),
                next: $(document.getElementById('action-company-change-next')),
                prev: $(document.getElementById('action-company-change-prev'))
            },
            remove: {
                one: $(document.getElementById('action-company-delete')),
                many: $(document.getElementById('action-company-many-delete'))
            }

        },
        banners: {
            add: $(document.getElementById('company-banners-add')),
            remove: $(document.getElementById('company-banners-add')).find('.banner-remove'),
        }
    },
    jslayer: {
        modal: $(document.getElementById('modal-jslayer')),
    },
    banner: {
        modal: $(document.getElementById('modal-banner-select')),
        action: $(document.getElementById('action-banner-select')),
        input: $(document.getElementById('input-banner'))
    },
    banners: {
        modal: {
            edit: $(document.getElementById('modal-banner-create')),
            remove: $(document.getElementById('modal-domain-delete')),
            add: $(document.getElementById('modal-banner-create')),
            removeAll: $(document.getElementById('modal-domain-many-delete'))
        },
        users: $(document.getElementById('banner-user-list')),
        upload: $(document.getElementById('file-banner')),
        gallery: $(document.getElementById('gallery-links')),
        repeat: {
            list: $(document.getElementById('repeator-list-translator')),
            thumbnail: $(document.getElementById('repeator-thumbnail-banner'))
        },
        table: $(document.getElementById('table-domain')),
        alert: $(document.getElementById('alert-domain')),
        form: {
            edit: $(document.getElementById('form-domain-change')),
            remove: $(document.getElementById('form-domain-delete')),
            add: $(document.getElementById('form-banner-create')).find('select.user-select')
        },
        action: {
            update: $(document.getElementById('action-banner-update')),
            add: $(document.getElementById('action-banner-create')),
            edit: $(document.getElementById('action-domain-change')),
            remove: {
                one: $(document.getElementById('action-domain-delete')),
                many: $(document.getElementById('action-domain-many-delete'))
            }
        }
    }
};