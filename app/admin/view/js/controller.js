var
        cfg = require('cfg'),
        action = cfg.action,
        document = require('dom'),
        event = require('event'),
        db = require('db'),
        func = require('func'),
        msg = require('msg');
request = require('request').action;
module.exports = {
    "authorization.logout": function () {
        $.ajax({url: action('authorization.logout')}).done(function (data, textStatus, jqXHR) {
            db.auth = false;
            db.user = {};
            event.emit('data.auth', {});
            location.reload(true);
        });
    },
    "authorization.login": function () {
        var dom = document.authorization;
        $.ajax({
            url: action('authorization.login'),
            data: dom.form.serializeArray(),
            method: 'post'
        }).done(function (data, textStatus, jqXHR) {
            if (jqXHR.status === 200 && data.result) {
                db.auth = true;
                db.user = data.result;
                event.emit('data.auth', data);
                dom.modal.modal('hide');
            } else {
                db.auth = false;
                dom.modal.modal('show');
            }
        });
    },
    "authorization.is": function () {
        var dom = document.authorization;
        $.ajax({url: action('authorization.is')}).done(function (data, textStatus, jqXHR) {
            if (data.result) {
                db.auth = true;
                db.user = data.result;
                event.emit('data.auth', data);
                dom.modal.modal('hide');
            } else {
                db.auth = false;
                dom.modal.modal('show');
            }
        });
    },
    "user.create": function () {
        var dom = document.user;
        request('user.create', dom.form.add.serializeArray(), function (data) {
            if (data.result) {
                event.emit('refresh.user', 'add', data.result);
                dom.modal.add.modal('hide');
                dom.form.add.find('input[type="text"]').val('');
            } else {
                console.warn(data);
            }
        });
    },
    "user.change": function () {
        var dom = document.user;
        request('user.change', dom.form.edit.serializeArray(), function (data) {
            if (data.result) {
                event.emit('refresh.user', 'edit', data.result);
                dom.modal.edit.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "user.remove": function () {
        var dom = document.user;
        var id = dom.form.remove.find('input[name="id"]').val();
        request('user.remove', {id: id}, function (data) {
            if (data.result) {
                event.emit('refresh.user', 'remove', [parseInt(id)]);
                dom.modal.remove.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "user.remove_all": function () {
        var dom = document.user;
        var data = dom.table.main.bootstrapTable('getSelections');
        if (data.length === 0) {
            return;
        }
        var result = $.map(data, function (row) {
            return row.id;
        });
        request('user.remove_all', {id: result}, function (data) {
            if (data.result) {
                event.emit('refresh.user', 'remove', result);
                dom.modal.removeAll.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "company.create": function () {
        var dom = document.company;
        var data = dom.form.add.serializeArray();
        var intervalRand = false,
                banGlobal = false,
                cryptAvcheck = false,
                cryptDomain = false,
                shortMode = false;
        for (var i in data) {

            switch (data[i].name) {
                case "jslayer[cache][day]":
                case "jslayer[cache][hour]":
                case "jslayer[cache][minut]":
                case "jslayer[cache][second]":
                case "itemsPerPage":
                    data.splice(i, 1);
                    break;
                case "jslayer[crypt][avcheck]":
                    data[i].value = true;
                    cryptAvcheck = true;
                    break;
                case "jslayer[crypt][domain]":
                    data[i].value = true;
                    cryptDomain = true;
                    break;
                case "jslayer[short][mode]":
                    data[i].value = true;
                    shortMode = true;
                    break;
                case "ban[global]":
                    data[i].value = true;
                    banGlobal = true;
                    break;
                case "jslayer[interval][rand]":
                    intervalRand = true;
                    data[i].value = true;
                    break;
            }
        }
        if (!banGlobal) {
            data.push({name: "ban[global]", value: false});
        }
        if (!cryptAvcheck) {
            data.push({name: "jslayer[crypt][avcheck]", value: false});
        }
        if (!cryptDomain) {
            data.push({name: "jslayer[crypt][domain]", value: false});
        }
        if (!shortMode) {
            data.push({name: "jslayer[short][mode]", value: false});
        }
        if (!intervalRand) {
            data.push({name: "jslayer[interval][rand]", value: false});
        }
        data = $.grep(data, function (v) {
            switch (v.name) {
                case "jslayer[interval][time][from]":
                case "jslayer[interval][time][to]":
                    console.log(v.name, v.value, (!intervalRand));
                    if (!intervalRand) {
                        return false;
                    }
                    break;
                case "jslayer[interval][time]":
                    console.log(v.name, v.value, (intervalRand));
                    if (intervalRand) {
                        return false;
                    }
                    break;
            }
            return true;
        });
        request('company.create', data, function (data) {
            if (data.result) {
                event.emit('refresh.company', 'add', data.result);
                dom.modal.add.modal('hide');
                dom.form.add.find('input[type="text"]').val('');
            } else {
                console.warn(data);
            }
        });
    },
    "company.change": function () {
        var dom = document.company;
        var data = dom.form.edit.serializeArray();
        var intervalRand = false,
                banGlobal = false,
                cryptAvcheck = false,
                cryptDomain = false,
                shortMode = false;
        for (var i in data) {

            switch (data[i].name) {
                case "jslayer[cache][day]":
                case "jslayer[cache][hour]":
                case "jslayer[cache][minut]":
                case "jslayer[cache][second]":
                case "itemsPerPage":
                    data.splice(i, 1);
                    break;
                case "jslayer[crypt][avcheck]":
                    data[i].value = true;
                    cryptAvcheck = true;
                    break;
                case "jslayer[crypt][domain]":
                    data[i].value = true;
                    cryptDomain = true;
                    break;
                case "jslayer[short][mode]":
                    data[i].value = true;
                    shortMode = true;
                    break;
                case "ban[global]":
                    data[i].value = true;
                    banGlobal = true;
                    break;
                case "jslayer[interval][rand]":
                    intervalRand = true;
                    data[i].value = true;
                    break;
            }
        }
        if (!banGlobal) {
            data.push({name: "ban[global]", value: false});
        }
        if (!cryptAvcheck) {
            data.push({name: "jslayer[crypt][avcheck]", value: false});
        }
        if (!cryptDomain) {
            data.push({name: "jslayer[crypt][domain]", value: false});
        }
        if (!shortMode) {
            data.push({name: "jslayer[short][mode]", value: false});
        }
        if (!intervalRand) {
            data.push({name: "jslayer[interval][rand]", value: false});
        }
        data = $.grep(data, function (v) {
            switch (v.name) {
                case "jslayer[interval][time][from]":
                case "jslayer[interval][time][to]":
                    console.log(v.name, v.value, (!intervalRand));
                    if (!intervalRand) {
                        return false;
                    }
                    break;
                case "jslayer[interval][time]":
                    console.log(v.name, v.value, (intervalRand));
                    if (intervalRand) {
                        return false;
                    }
                    break;
            }
            return true;
        });
        request('company.change', data, function (data) {
            if (data.result) {
                event.emit('refresh.company', 'edit', data.result);
                dom.modal.edit.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "company.remove": function () {
        var dom = document.company;
        var id = dom.form.remove.find('input[name="id"]').val();
        request('company.remove', {id: id}, function (data) {
            if (data.result) {
                event.emit('refresh.company', 'remove', [parseInt(id)]);
                dom.modal.remove.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "company.remove_all": function () {
        var dom = document.company;
        var data = dom.table.main.bootstrapTable('getSelections');
        if (data.length === 0) {
            return;
        }
        var result = $.map(data, function (row) {
            return row.id;
        });
        request('company.remove_all', {id: result}, function (data) {
            if (data.result) {
                event.emit('refresh.company', 'remove', result);
                dom.modal.removeAll.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "exploit.create": function () {
        var dom = document.exploit;
        console.log(dom.form.add.serializeArray());
        request('exploit.create', dom.form.add.serializeArray(), function (data) {
            if (data.result) {
                event.emit('refresh.exploit', 'add', data.result);
                dom.modal.add.modal('hide');
                dom.form.add.find('input[type="text"]').val('');
            } else {
                console.warn(data);
            }
        });
    },
    "exploit.change": function () {
        var dom = document.exploit;
        request('exploit.change', dom.form.edit.serializeArray(), function (data) {
            if (data.result) {
                event.emit('refresh.exploit', 'edit', data.result);
                dom.modal.edit.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "exploit.remove": function () {
        var dom = document.exploit;
        var id = dom.form.remove.find('input[name="id"]').val();
        request('exploit.remove', {id: id}, function (data) {
            if (data.result) {
                event.emit('refresh.exploit', 'remove', [parseInt(id)]);
                dom.modal.remove.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "exploit.remove_all": function () {
        var dom = document.exploit;
        var data = dom.table.main.bootstrapTable('getSelections');
        if (data.length === 0) {
            return;
        }
        var result = $.map(data, function (row) {
            return row.id;
        });
        request('exploit.remove_all', {id: result}, function (data) {
            if (data.result) {
                event.emit('refresh.exploit', 'remove', result);
                dom.modal.removeAll.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "virustotal.create": function () {
        var dom = document.virustotal;
        request('virustotal.create', dom.form.add.serializeArray(), function (data) {
            if (data.result) {
                dom.modal.add.modal('hide');
                dom.form.add.find('input').val('');
                event.emit('refresh.virustotal', 'add', data.result);
            } else {
                console.warn(data);
            }
        });
    },
    "virustotal.change": function () {
        var dom = document.virustotal;
        request('virustotal.change', dom.form.edit.serializeArray(), function (data) {
            if (data.result) {
                dom.modal.edit.modal('hide');
                event.emit('refresh.virustotal', 'edit', data.result);
            } else {
                console.warn(data);
            }
        });
    },
    "virustotal.remove": function () {
        var dom = document.virustotal;
        var id = dom.form.remove.find('input[name="_id"]').val();
        request('virustotal.remove', {_id: id}, function (data) {
            if (data.result) {
                dom.modal.remove.modal('hide');
                event.emit('refresh.virustotal', 'remove', [id]);
            } else {
                console.warn(data);
            }
        });
    },
    "virustotal.remove_all": function () {
        var dom = document.virustotal;
        var data = dom.table.main.bootstrapTable('getSelections');
        if (data.length === 0) {
            return;
        }
        var result = $.map(data, function (row) {
            return row._id;
        });
        request('virustotal.remove_all', {_id: result}, function (data) {
            if (data.result) {
                dom.modal.removeAll.modal('hide');
                event.emit('refresh.virustotal', 'remove', result);
            } else {
                console.warn(data);
            }
        });
    },
    "domain.create": function () {
        var dom = document.domain;
        var data = func.form.parse(dom.form.add.serializeArray());
        if (data.layer !== undefined) {
            delete data.layer;
            delete data.uid;
            data.type = "layer";
        } else {
            data.type = "user";
        }
        request('domain.create', func.form.build(data), function (data) {
            if (data.result) {
                event.emit('refresh.domain', 'adds', data.result);
            }
            if (data.error) {
                console.warn(data.error);
            }
            console.log(data);
            if (data.error === null) {
                dom.modal.add.modal('hide');
                dom.form.add.find('input[type="text"]').val('');
            }
            event.emit('refresh.domain', 'add', data.result);
        });
    },
    "domain.change": function () {
        var dom = document.domain;
        var data = func.form.parse(dom.form.edit.serializeArray());
        if (data.layer !== undefined) {
            delete data.layer;
            delete data.uid;
            data.type = "layer";
            data.user = undefined;
        } else {
            data.type = "user";
            data.user = parseInt(data.uid);
            delete data.uid;
        }
        var result = {id: data.id};
        delete data.id;
        result.in = JSON.stringify(data);
        request('domain.change', func.form.build(result), function (data) {
            if (data.result) {
                event.emit('refresh.domain', 'edit', data.result);
                dom.modal.edit.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "domain.remove": function () {
        var dom = document.domain;
        var id = dom.form.remove.find('input[name="id"]').val();
        request('domain.remove', {id: id}, function (data) {
            if (data.result) {
                event.emit('refresh.domain', 'remove', [parseInt(id)]);
                dom.modal.remove.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "domain.search": function (event, text) {
        if (text.length === 0) {
            return;
        }
        var dom = document.domain;
        request('domain.search', {'search': text}, function (data) {
            if (data.result) {
                dom.table.bootstrapTable('load', {
                    data: data.result
                });
                dom.table.bootstrapTable('scrollTo', 'bottom');
            } else {
                console.warn(data);
            }
        });
    },
    "domain.remove_all": function (event, text) {
        var dom = document.domain;
        var data = dom.table.remove.bootstrapTable('getSelections');
        if (data.length === 0) {
            return;
        }
        var result = $.map(data, function (row) {
            return row.id;
        });
        request('domain.remove_all', {id: result}, function (data) {
            if (data.result) {
                event.emit('refresh.domain', 'remove', result);
                dom.modal.removeAll.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "ip.create": function () {
        var dom = document.ip;
        request('ip.create', dom.form.add.serializeArray(), function (data) {
            if (data.result) {
                event.emit('refresh.ip', 'add', data);
                dom.modal.add.modal('hide');
                dom.form.add.find('input[type="text"]').val('');
            } else {
                console.warn(data);
            }
        });
    },
    "ip.remove": function () {
        var dom = document.ip;
        var id = dom.form.remove.find('input[name="ip"]').val();
        request('ip.remove', {id: id}, function (data) {
            if (data.result) {
                event.emit('refresh.ip', 'remove', [parseInt(id)]);
                dom.modal.remove.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "ip.remove_all": function () {
        var dom = document.ip;
        var data = dom.table.remove.bootstrapTable('getSelections');
        if (data.length === 0) {
            return;
        }
        var result = $.map(data, function (row) {
            return row.id;
        });
        request('ip.remove_all', {id: result}, function (data) {
            if (data.result) {
                event.emit('refresh.ip', 'remove', result);
                dom.modal.removeAll.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "server.create": function () {
        var dom = document.server;
        request('server.create', dom.form.add.serializeArray(), function (data) {
            if (data.result) {
                event.emit('refresh.server', 'add', data.result);
                dom.modal.add.modal('hide');
                dom.form.add.find('input[type="text"]').val('');
            } else {
                console.warn(data);
            }
        });
    },
    "server.change": function () {
        var dom = document.server;
        request('server.change', dom.form.edit.serializeArray(), function (data) {
            if (data.result) {
                event.emit('refresh.server', 'edit', data.result);
                dom.modal.edit.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "server.remove": function () {
        var dom = document.server;
        var id = dom.form.remove.find('input[name="id"]').val();
        request('server.remove', {id: id}, function (data) {
            if (data.result) {
                event.emit('refresh.server', 'remove', [parseInt(id)]);
                dom.modal.remove.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "server.remove_all": function () {
        var dom = document.server;
        var data = dom.table.remove.bootstrapTable('getSelections');
        if (data.length === 0) {
            return;
        }
        var result = $.map(data, function (row) {
            return row.id;
        });
        request('server.remove_all', {id: result}, function (data) {
            if (data.result) {
                event.emit('refresh.server', 'remove', result);
                dom.modal.removeAll.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "banner.remove": function () {
        var wrapper = $(this), id = wrapper.data('id');
        request('banner.remove', {id: id}, function (data) {
            if (data.result) {
                wrapper.parent().parent().remove();
                delete db.banner.data[id];
                var data = db.banner.data, index = {};
                for (var id in data) {
                    var v = data[id];
                    if (index[v.uid] === undefined) {
                        index[v.uid] = [];
                    }
                    index[v.uid].push(v.id);
                }
                db.banner.index = index;
                event.emit('data.banner');
            } else {
                alert(dom.alert, 'alert-warning', data.error);
                console.warn(data.error);
            }
        });
    },
    "banner.list": function () {
        request('banner.list', function (data) {
            if (data.result) {
                var k, v, result = {}, index = {},ids ={}, down = cfg.server.down, users = db.users.index;
                for (k in data.result) {
                    v = data.result[k];
                    if (v.url) {
                        v.src = v.link;
                    } else {
                        v.src = down + v.link;
                    }
                    v.user = users[v.uid];
                    result[v.id] = v;
                    ids[v._id]= v.id;
                    if (index[v.uid] === undefined) {
                        index[v.uid] = [];
                    }
                    index[v.uid].push(v.id);
                }
                db.banner = {data: result, index: index,ids:ids};
                event.emit('data.banner', db.banner);
            } else {
                console.warn(data.error);
            }
        });
    },
    "static.general": function (company, date, callback) {
        request('static.general', {company: company, date: date}, function (data) {
            if (data.result) {
                var result = data.result;
                result.send = Math.floor(result.out / Math.floor(result.uniq / 100));
                result.lost = Math.floor((result.uniq - result.drop - result.out) / Math.floor(result.uniq / 100));
                callback(null, result);
            } else {
                callback(data.error, null);
            }
        });
    }
};