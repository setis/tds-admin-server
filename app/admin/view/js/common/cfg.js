module.exports = {
    server: require("data.cfg"),
    mode: 'api',
    alert: {
        timeClose: 5 * 1e3,
        eventShow: []
//            eventShow:['alert-success','alert-info','alert-warning','alert-danger']
    },
    modules: {
        user: {
            refreshTable: undefined
        },
        server: {
            refreshTable: undefined
        },
        domain: {
            refreshTable: undefined
        },
        virustotal: {
            refreshTable: undefined
        },
        exploit: {
            refreshTable: undefined
        },
        company: {
            refreshTable: undefined
        }
    }
};
function query(params) {
    var result = '';
    if (params !== undefined) {
        var pool = [];
        var name;
        if (params instanceof Array) {
            var obj;
            for (name in params) {
                obj = params[name];
                pool.push(obj.name + '=' + obj.value);
            }
        } else {
            for (name in params) {
                pool.push(name + '=' + params[name]);
            }
        }
        result = encodeURIComponent(pool.join('&'));
    }
    return result;
}
module.exports.action = function (action, params) {
    var url = module.exports.server.api + '?action=' + action;
    if (params !== undefined) {
        switch (typeof params) {
            case "object":
                url += '&'+query(params);
                break;
            case "string":
                url += params;
                break;
        }
    }
    return url;
};
