var configure = require('cfg'), db = require('db');
function alert(id, type, message) {
    var cfg = configure.cfg.alert;
    if (cfg.eventShow.indexOf(type) === -1) {
        return;
    }
    id.html('<div class="alert ' + type + '"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message + '</div>');
    var alert = id.find('.alert');
    alert.alert();
    if (cfg.timeClose !== undefined) {
        setTimeout(function () {
            alert.alert('close');
        }, cfg.timeClose);
    }

}
function server(value) {
    var id = db.server.index[value];
    if (id) {
        var uid = db.server.data[id].uid;
        if (uid) {
            return db.server.data[id].name + '#' + id + ' (' + db.users.index[uid] + ')';
        }
        return id + ' (None)';
    }
    return  'None';
}
function closed(e, block, select) {
    if (block === true) {
        var html = $(e).parent().find('.option-close').html();
        var id = $(e).attr('data-close');
        $(id).append(html);
    }
    if (select === true) {
        $(id).selectpicker('refresh');
    }
    $(e).parent().remove();
}
function options(data, fields, _use, exclusion) {
    var html = '', i, name, use, option;
    for (i in data) {
        option = data[i];
        if (exclusion === undefined || exclusion.indexOf(option[fields.value]) === -1) {
            use = (_use === option[fields.value]) ? 'selected' : '';
            html += '<option value="' + option[fields.value] + '"' + use + '>' + option[fields.name] + '</option>';
        }
    }
    return html;
}
function options_list(data, _use, exclusion) {
    var html = '', i, name, use, option;
    for (i in data) {
        option = data[i];
        if (exclusion === undefined || exclusion.indexOf(option) === -1) {
            use = (_use === option) ? 'selected' : '';
            html += '<option value="' + option + '" ' + use + '>' + option + '</option>';
        }
    }
    return html;
}
function refresh(params) {
    if (params.cfg !== undefined && params.cfg.indexOf(params.event) !== -1) {
        params.dom.bootstrapTable('refresh');
    } else {
        if (typeof params.db === 'function') {
            params.db();
        }
        switch (params.event) {
            case 'remove':
                params.dom.bootstrapTable('remove', {
                    field: (params.id) ? params.id : 'id',
                    values: params.data
                });
                break;
            case 'add':
                params.dom.bootstrapTable('append', [params.data]);
                break;
            case 'adds':
                params.dom.bootstrapTable('append', params.data);
                break;
            case 'edit':
                params.dom.bootstrapTable('updateRow', {
                    index: params.index,
                    row: params.data
                });
                break;
        }
    }
}
function modal(current, next) {
    $(current).modal('hide');
    $(next).modal('show');
    $(next).one('hidden.bs.modal', function () {
        $(current).modal('show');
    });
}
function validation_ip(self, input) {
    var arr, i, host, result = [], html = [];
    arr = self.text().split(/\s+|,|;/);
    for (i in arr) {
        host = /((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}/.exec(arr[i]);
        if (host !== null && host[0] !== '') {
            result.push(host[0]);
            html.push('<a href="http://' + host[0] + '">' + host[0] + '</a>');
        } else {
            html.push('<span class="validation-error">' + arr[i] + '</span>');
        }
    }
    self.html(html.join('&#160;'));
    input.val(result.filter(function (value, index, self) {
        return self.indexOf(value) === index;
    }).join());
}
function validation_domain(self, input) {
    var arr, i, host, result = [], html = [];
    arr = self.text().split(/\s+|,|;/);
    for (i in arr) {
        host = /(((?!-))(xn--)?[a-z0-9][a-z0-9-_]{0,61}[a-z0-9]{0,1}\.(xn--)?([a-z0-9\-]{1,61}|[a-z0-9-]{1,30}\.[a-z]{2,}))$/.exec(arr[i]);
        if (host !== null && host[0] !== '') {
            result.push(host[0]);
            html.push('<a href="http://' + host[0] + '">' + host[0] + '</a>');
        } else {
            html.push('<span class="validation-error">' + arr[i] + '</span>');
        }
    }
    self.html(html.join('&#160;'));
    input.val(result.filter(function (value, index, self) {
        return self.indexOf(value) === index;
    }).join());
}
function gallery_img(id, id_links, values, gallery, imgLinks, func) {
    var img = configure.img;
    var html = '<div><div id="' + id + '" class="carousel slide" data-ride="carousel"><ol class="carousel-indicators hide">';
    for (var i in Object.keys(values)) {
        if (i === 0) {
            html += '<li data-target="#' + id + '" data-slide-to="' + i + '" class="active">' + i + '</li>';
        } else {
            html += '<li data-target="#' + id + '" data-slide-to="' + i + '">' + i + '</li>';
        }
    }
    var size = ++i;
    html += '</ol><div class="carousel-inner" role="listbox">';
    var f = true;
    if (imgLinks === undefined) {
        imgLinks = [];
    }
    if (gallery === undefined) {
        gallery = '';
    }
    var wrapper = document.createElement('div');
    wrapper.id = id + '-m';
    for (var name in values) {
        var arr = values[name];
        var alt = (arr['original'].alt === null) ? '' : arr['original'].alt;
        if (f === false) {
            html += '<div class="item"><img src="' + img(arr['200x200'].url)[0] + '" alt="' + alt + '" class="img-responsive center-block"></div>';
        } else {
            f = false;
            html += '<div class="item active"><img src="' + img(arr['200x200'].url)[0] + '" alt="' + alt + '" class="img-responsive center-block"></div>';
        }
        $('<a>').append($('<img>').prop('src', img(arr['200x200'].url)[0]))
                .prop('href', img(arr['original'].url)[0])
                .prop('title', alt)
                .attr('data-gallery', gallery)
                .appendTo(wrapper);
        imgLinks.push({
            href: img(arr['75x75'].url)[0],
            title: alt
        });
    }
    $(id_links).append(wrapper);
    html += '</div><form class="form-inline"><div class="form-group"><div class="input-group"><input type="button" class="btn btn-info" value="‹" onclick="$(\'#' + id + '\').carousel(\'prev\');"><div class="input-group-addon slider-number">1/' + size + '</div><input type="button" class="btn btn-info" value="›" onclick="$(\'#' + id + '\').carousel(\'next\');"></div></div></form></div></div>';
    setTimeout(function () {
        var target = $('#' + id);
        target.carousel('pause').on('slid.bs.carousel', function (e) {
            $('div.slider-number').text(($(this).find('li.active').data('slide-to') + 1) + '/' + size);
        });
        func(target, imgLinks);
    }, 750);
    return html;
}
function build(params) {
    var result = [];
        var name;
        for (name in params) {
            result.push({name:name,value:params[name]});
        }
    return result;
}
function parse (params) {
    var result = {};
    var i;
    for (i in params) {
        var obj = params[i];
        result[obj.name] = obj.value;
    }
    return result;
}

module.exports = {
    alert: alert,
    closed: closed,
    options: options,
    refresh: refresh,
    modal: modal,
    gallery_img: gallery_img,
    validation_ip: validation_ip,
    validation_domain: validation_domain,
    options_list: options_list,
    server: server,
    form:{
        build:build,
        parse:parse
    }
};