var pool = [], status;
function event() {
    if (!status && pool.length !== 0) {
        var o = pool.splice(-1, 1)[0];
        var el = $(o.prev);
        if (pool.length > 0) {
            el.one('hide.bs.modal', event);
//            $('body').removeClass('modal-open');
        }
        setTimeout(function () {
            el.modal('show');
        }, 750);
    }
}
//$("body").on('DOMAttrModified', function (e) {
//    var self = $(this);
//        console.log('body:', self.attr('class'));    
//    if (!self.hasClass('modal-open') && pool.length > 0) {
//        self.addClass('modal-open');
//    }else if(!self.hasClass('modal-open') && pool.length === 0){
//        self.removeClass('modal-open');
//    }
//});
function modal(current, next) {
    var elCurrent = $(current), elNext = $(next);
    pool.push({prev: current, current: next});
    status = true;
    elCurrent.modal('hide');
    setTimeout(function () {
        elNext.modal('show');
    }, 750);
    elNext.one('shown.bs.modal', function () {
        status = false;
    });
    elNext.one('hide.bs.modal', event);
}
module.exports = modal;