var alert = require('func').alert,
        dom = require('dom'),
        statusClass = ['alert-info', 'alert-success', 'alert-warning', 'alert-danger'],
        msg = {
            "banner.view.0": "список загружен"
        };
module.exports = function (action, status) {
    if (status === undefined) {
        status = 1;
    }

    var select = [action.controller, action.method, status].join('.');
    var el = dom[[action.controller, action.method].join()] || dom[action.controller] || dom['alert'];
    alert(el, statusClass[status], msg[select]);
};