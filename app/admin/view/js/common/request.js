var cfg = require('cfg'),
        mode = cfg.mode,
        url = cfg.action;
module.exports = {
    api: function () {
        var action, data, handler;
        switch (arguments.length) {
            case 2:
                action = arguments[0];
                handler = arguments[1];
                break;
            case 3:
                action = arguments[0];
                data = arguments[1];
                handler = arguments[2];
                break;
            case 4:
                action = arguments[0];
                data = arguments[1];
                handler = arguments[2];
                break;
        }
        if (arguments.length === 2) {
            action = arguments[0];
            handler = arguments[1];
        } else if (arguments.length === 3) {
            action = arguments[0];
            data = arguments[1];
            handler = arguments[2];
        }
        $.ajax({
            url: url(action),
            data: data
        }).done(function (data, textStatus, jqXHR) {
            handler(data);
        });
    },
    action: function () {
        module.exports[mode].apply(this, arguments);
    }
};
