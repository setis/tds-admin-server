$.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['ru-RU']);
$.fn.bootstrapSwitch.defaults.size = 'small';
$.ajaxSetup({
    type: "GET",
    xhrFields: {withCredentials: true},
//    crossDomain: true,
    global: true,
    dataType: 'json',
    cache: false,
//    error: function (jqXHR, textStatus, errorThrown) {
//        if (jqXHR.status == 403) {
//            console.warn("no access");
//        }
//    }
});
window.modal = require('modal');
var event = require('event');
require('auth')();
require('user')();
require('server')();
require('domain')();
require('banners')();
require('jslayer')();
require('ip')();
require('exploit')();
require('filter')();
require('country')();
require('browser')();
require('company')();
require('virustotal')();
require('exploit.select')();
$(document).ready(function () {
    require('dom');
    setTimeout(function () {
        event.emit('app.*');
    }, 750);
    var func = require('func');
    $('.validation-list[role="ip"]').on('blur focusout', function () {
        var self = $(this), input = $(self.data('id'));
        func.validation_ip(self, input);
    });
    $('.validation-list[role="domain"]').on('blur focusout', function () {
        var self = $(this), input = $(self.data('id'));
        func.validation_domain(self, input);
    });
});
