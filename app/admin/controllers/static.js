var moment = require('moment');
module.exports = function (req, res, next) {
    var table = 's_' + req.query.select,
        data = {
            company: req.query.company
        },
        sql = '';
    switch (req.query.type) {
        case "day":
            sql = "SELECT * FROM "+table+" WHERE company_id = :company AND date  = :date;";
            data.date = moment(new Date()).format('YYYY-MM-DD');
            break;
        case "period":
            sql = "SELECT * FROM "+table+" WHERE company_id = :company  AND (date  > :from AND :to < date);";
            date.to = moment(new Date(req.query.date.to)).format('YYYY-MM-DD');
            date.from = moment(new Date(req.query.date.from)).format('YYYY-MM-DD');
            break;
    }
    req.sql.query(sql, data, function (err, result) {
        if (err) {
            res.status(500).json({
                error: 'static',
                action: req.action
            });
            return;
        }
        res.json({result: result.rows, action: req.action});
    });
};