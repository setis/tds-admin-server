var ObjectId = require('mongoose').Schema.ObjectId;
function add(req, res, next) {
    req.mongoose.virustotal.create({
        email: req.query.email,
        key: req.query.key,
        login: req.query.login,
        password: req.query.password
    }, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}
function view(req, res, next) {
    req.mongoose.virustotal.find({}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}
function remove(req, res, next) {
    req.mongoose.virustotal.findOne({_id: ObjectId(req.query._id)}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        result.remove(function (err) {
            if (err) {
                res.status(404).json({error: err, action: req.action});
                return;
            }
            res.json({result: true, action: req.action});
        });
    });
}

function edit(req, res, next) {
    var model = req.mongoose.virustotal;
    var id = ObjectId(req.query._id);
    var list = ['key','email','login','password'];
    var change = {};
    for(var i in list){
        var v = list[i];
        change[v] = req.query[v];
    }
    model.update({_id: id}, {'$set': change}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        model.findOne({_id: id}, function (err, result) {
            if (err) {
                res.status(404).json({error: err, action: req.action});
                return;
            }
            res.json({result: result, action: req.action});
        });

    });
}
function removeAll(req, res, next) {
    var result = [];
    for (var i in req.query._id) {
        result.push(ObjectId(req.query._id[i]));
    }
    req.mongoose.virustotal.remove({_id: {'$in': result}}, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: req.query._id, action: req.action});
    });
}
module.exports = {
    create: add,
    list: view,
    change: edit,
    remove: remove,
    remove_all: removeAll
};
