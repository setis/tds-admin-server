var validator = require('validator');
function add(req, res, next) {
    var params = req.query, model = req.mongoose.server;
    if (!validator.isIP(params.ip.main)) {
        res.status(404).json({error: "not validate ip:" + params.ip.main, action: req.action});
        return;
    }
    console.log(params.ip);
    params.ip.addition = (params.ip.addition === '')?[]:params.ip.addition.splite(',');
    for (var i in params.ip.addition) {
        var ip = params.ip.addition[i];
        if (!validator.isIP(ip)) {
            res.status(404).json({error: "not validate ip:" + ip, action: req.action});
            return;
        }
    }
    model.create(params, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });


}
function view(req, res, next) {
    req.mongoose.server.find({}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}
function remove(req, res, next) {
    req.mongoose.server.remove({id: req.query.id}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: true, action: req.action});
    });
}

function edit(req, res, next) {
    var model = req.mongoose.server;
    model.update({id: req.query.id}, {'$set': req.query}, {runValidators: true}, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        model.findOne({id: req.query.id}, function (err, result) {
            if (err) {
                res.status(404).json({error: err, action: req.action});
                return;
            }
            res.json({result: result, action: req.action});
        });

    });
}
function removeAll(req, res, next) {
    req.mongoose.server.remove({id: {'$in': req.query.id}}, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: true, action: req.action});
    });
}
module.exports = {
    create: add,
    list: view,
    change: edit,
    remove: remove,
    remove_all: removeAll
};
