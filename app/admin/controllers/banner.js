var fs = require('fs'),
    sizeOf = require('image-size'),
    async = require('async');
function upload(req, res, next) {
    var model = req.mongoose.banner,
        file = req.files.image,
        user = req.body.user;
    var data = {
        link: file.name,
        url: false,
        path: {
            originalname: file.originalname,
            data: null,
            size: file.size
        },
        uid: user,
        width: null,
        height: null
    };
    async.waterfall([
        function (callback) {
            fs.readFile(file.path, function (err, result) {
                if (err) {
                    callback(err);
                    return;
                }
                data.path.data = result;
                callback();
            });
        },
        function (callback) {
            sizeOf(file.path, function (err, img) {
                if (err) {
                    callback(err);
                    return;
                }
                data.height = img.height;
                data.width = img.width;
                callback();
            });
        },
        function (callback) {
            model.create(data, callback);
        }

    ], function (err, result) {
        if (err) {
            res.status(404).json({
                error: err,
                file: file.originalname,
                user: user,
                action: req.params.action
            });
            return;
        }
        delete result.path.data;
        res.json({
            result: result,
            action: {
                controller: 'banner',
                method: 'upload'
            }
        });

    });
}
function view(req, res, next) {
    var model = req.mongoose.banner;
    model.find({}, 'id uid url link height width', function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.params.action});
            return;
        }
        res.json({
            result: result,
            action: req.params.action
        });
    });
}
function remove(req, res, next) {
    var model = req.mongoose.banner, id = req.query.id;
    model.findOne({id: id}, 'url link', function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.params.action});
            return;
        }
        var path = result.link;
        model.remove({id: id}, function (err, result) {
            if (err) {
                res.status(404).json({error: err, action: req.params.action});
                return;
            }
            fs.exists(path, function (exists) {
                if (exists) {
                    fs.unlink(path);
                }
            });
            res.json({
                result: id,
                action: req.params.action
            });
        });
    });


}

module.exports = {
    create: upload,
    remove: remove,
    list: view
};