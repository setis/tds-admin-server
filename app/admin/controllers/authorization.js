function attempts(session) {
    if (session.auth_try === undefined) {
        session.auth_try = 0;
    } else {
        session.auth_try++;
    }
    if (session.auth_try > 5) {
        return false;
    }
    return true;
}
function is(req, res, next) {
    var s = req.session, data = {};
    if (s.islogin === true) {
        data = {
            uid: s.uid,
            login: s.login,
            roles: s.roles || {}
        };
    } else {
        data = false;
    }
    res.json({result: data, action: req.params.action});
}
function check(req, res, next) {
    if (req.session.islogin === true) {
        next();
        return;
    }
    req.logger.warn('controller: auth.check access deny ip:%s action:%s', req.ip, req.query.action);
    res.status(403).json({error: {
            code: 0,
            msg: "not is authorization"
        }});
}

function login(req, res, next) {
    var login = req.body.login, password = req.body.password, s = req.session, status = attempts(req.session);
    if (login === undefined || login.length <= 3 || password === undefined || password <= 5) {
        res.status(404).json({error: 'not validate login or password', action: req.params.action});
        return;
    }
    if (!status) {
        req.logger.warn('controller: auth.login brutocfore ip:%s', req.ip);
        res.status(404).json({error: 'not validate login or password', action: req.params.action});
        return;
    }
    req.mongoose.user.findOne({login: login, password: password, mode: true}, function (err, result) {
        if (err) {
            req.logger.error("controller: auth.login %s", err);
            res.status(404).json({error: err, action: req.params.action});
            return;
        }
        if (result === null) {
            req.logger.info("controller: auth.login not found login:%s password:", login, password);
            res.status(404).json({error: "not found login and password or mode off user auth", action: req.params.action});
            return;
        }

        s.uid = result.id || 0;
        s.login = result.login;
        s.auth_try = 0;
        s.islogin = true;
        res.json({
            result: {
                uid: result.id,
                login: result.login,
                roles: result.roles || {}
            }, action: req.params.action
        });
    });
}
function logout(req, res, next) {
    req.session.islogin = false;
    res.json({
        result: true, action: req.params.action
    });
}
module.exports = {
    is: is,
    check: check,
    logout: logout,
    login: login
};