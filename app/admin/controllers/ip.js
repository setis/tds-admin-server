var validator = require('validator');
function add(req, res, next) {
    var ips = req.query.ip.split(','), result = [], model = req.mongoose.ip, i;
    for (var i in ips) {
        var ip = ips[i];
        if (!validator.isIP(ip)) {
            res.status(404).json({error: "not validate ip:" + ip, action: req.action});
            return;
        }
        result.push({ip:ip});
    }
    model.create(result, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });

}
function view(req, res, next) {
    req.mongoose.ip.find({}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.params.action});
            return;
        }
        res.json({result: result, action: req.params.action});
    });
}
function remove(req, res, next) {
    req.mongoose.ip.remove({id: req.query.id}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.params.action});
            return;
        }
        res.json({result: true, action: req.params.action});
    });
}

function edit(req, res, next) {
    var model = req.mongoose.ip;
    model.update({id: req.query.id}, {'$set': req.query.in}, {runValidators: true}, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.params.action});
            return;
        }
        model.findOne({id: req.query.in.id}, function (err, result) {
            if (err) {
                res.status(404).json({error: err, action: req.params.action});
                return;
            }
            res.json({result: result, action: req.params.action});
        });

    });
}
function removeAll(req, res, next) {
    req.mongoose.ip.remove({id: {'$in': req.query.in}}, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.params.action});
            return;
        }
        res.json({result: true, action: req.params.action});
    });
}
module.exports = {
    create: add,
    list: view,
    change: edit,
    remove: remove,
    remove_all: removeAll
};
