var clone = require('node-v8-clone').clone,
    async = require('async');
function add(req, res, next) {
    async.waterfall([
        function (callback) {
            if (req.query.type !== 'user') {
                callback(null, null);
                return;
            }
            if (req.query.uid === undefined) {
                callback(new Error('not found user'))
                return;
            }
            var uid = Number(req.query.uid);
            req.mongoose.user.findOne({id: uid}, 'id', function (err, result) {
                if (err) {
                    callback(err, null);
                    return;
                }
                if (result === null) {
                    callback(new Error('not found user'))
                    return;
                }
                callback(null, {
                    id: uid,
                    key: result._id
                });
            });
        },
        function (user, callback) {
            var data = {description: req.query.description, type: req.query.type},
                hosts = req.query.domain.split(','),
                model = req.mongoose.domain;
            if (user) {
                data.user = user;
            }

            var tasks = hosts.map(function (host) {
                var d = clone(data, false);
                d.host = host;
                return function (callback) {
                    model.create(d, function (err, result) {
                        if (err) {
                            if (err.code === 11000) {
                                model.findOne({host: host}, callback);
                                return;
                            }
                        }
                        callback(err, result);
                    });
                };
            });
            async.parallel(tasks, function (err, result) {
                callback(null, {error: err, result: result});
            });
        }
    ], function (err, result) {
        if (err) {
            res.status(500).json({error: {msg: err.message, code: err.code}, action: req.action});
            return;
        }
        result.action = req.params.action;
        res.json(result);
    });
}
function view(req, res, next) {
    req.mongoose.domain.find({}, function (err, result) {
        if (err) {
            res.status(500).json({error: {msg: err.message, code: err.code}, action: req.action});
            return;
        }
        res.json({result: result, action: req.params.action});
    });
}
function remove(req, res, next) {
    req.mongoose.domain.remove({id: req.query.id}, function (err, result) {
        if (err) {
            res.status(500).json({error: {msg: err.message, code: err.code}, action: req.action});
            return;
        }
        res.json({result: true, action: req.params.action});
    });
}
function edit(req, res, next) {
    var model = req.mongoose.domain;
    async.waterfall([
        function (callback) {
            if (req.query.type !== 'user') {
                callback(null, null);
                return;
            }
            if (req.query.uid === undefined) {
                callback(new Error('not found user'))
                return;
            }
            var uid = Number(req.query.uid);
            req.mongoose.user.findOne({id: uid}, 'id', function (err, result) {
                if (err) {
                    callback(err, null);
                    return;
                }
                if (result === null) {
                    callback(new Error('not found user'))
                    return;
                }
                callback(null, {
                    id: uid,
                    key: result._id
                });
            });
        },
        function (user, callback) {
            try {
                var data = JSON.parse(req.query.in);
            } catch (e) {
                callback(e);
                return;
            }
            if (user) {
                data.user = user;
            }
            model.update({id: req.query.id}, {'$set': data}, {runValidators: true}, function (err) {
                if (err) {
                    callback(err);
                    return;
                }
                callback(null);
            });
        },
        function (callback) {
            model.findOne({id: req.query.id}, callback);
        }
    ], function (err, result) {
        if (err) {
            res.status(500).json({error: err, action: req.params.action});
            return;
        }
        res.json({result: result, action: req.params.action});
    });
}
function removeAll(req, res, next) {
    req.mongoose.domain.remove({id: {'$in': req.query.id}}, function (err) {
        if (err) {
            res.status(500).json({error: err, action: req.params.action});
            return;
        }
        res.json({result: req.query.id, action: req.params.action});
    });
}
module.exports = {
    create: add,
    list: view,
    change: edit,
    remove: remove,
    remove_all: removeAll
};
