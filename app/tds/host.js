var model,
    cfg = require('../../cfg/tds.json').domain,
    logger = require('./logger'),
    mode = cfg.cache.mode || false;
if (mode) {
    var NodeCache = require("node-cache"),
        loading = false,
        cache = new NodeCache({stdTTL: cfg.cache.ttl, checkperiod: cfg.cache.period});
    cache.on("expired", function (host, host_id) {
        update(host_id);
    });
    setInterval(load, cfg.reload * 1e3);
}
function load(status) {
    if (status && mode) {
        loading = false;
    }
    model.find({}, 'id host', function (err, result) {
        if (err) {
            logger.error('host.load ', err);
            setTimeout(load, 1e3);
            return;
        }
        if (result.length !== 0) {
            for (var i in result) {
                var data = result[i];
//                logger.debug('host.load ',data.host, data.id);
                cache.set(data.host, data.id, function (err) {
                    if (err) {
                        logger.error('host.load cache.set ', err);
                    }
                });
            }
        }
        if (status && mode) {
            loading = true;
        }

    });
}
function update(host_id) {
    model.findOne(
        {id: host_id}, 'host id',
        function (err, result) {
            if (err) {
                logger.error('host.update findOne ', host_id, err);
                return;
            }
            if (result !== null) {
                cache.set(result.host, result.id, function (err) {
                    if (err) {
                        logger.error('host.update cache.set host_id:', host_id, err);
                    }
                });
            }
        }
    );
}
function find(host, cb) {
    model.findOne({host: host}, 'host id',
        function (err, result) {
            if (err) {
                logger.error('host.find  ', host, err);
                cb(err, null);
                return;
            }

            cb(err, (result === null) ? null : result.id);
        });
}
function handler(id, cb) {
    cache.get(id, function (err, value) {
        if (err) {
            logger.error("host.handler cache.get host_id:", id, err);
            cb(err, null);
            return;
        }
        cb(err, (value === undefined) ? null : value);
    });
}
function wrapper(id, cb) {
    if (!loading) {
        find(id, cb);
    } else {
        handler(id, cb);
    }
}
require('./loader')(function (core) {
    model = core.models.odm.domain;
    if(mode){
        load(true);
    }
    core.events.on('host', wrapper);
    core.events.on('host.flush', function () {
        cache.flushAll();
    });
    core.events.on('host.reload', function () {
        cache.flushAll();
        load();
    });
    core.events.on('host.load', load);
});
exports.wrapper = wrapper;