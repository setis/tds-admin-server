var logger = require('./logger'), model;

function wrapperWrite(data, callback) {
    write(data.company.layer.id, {
        time: new Date(),
        uniq: data.uniq_id,
        token: data.token,
        referer: data.referer
    }, callback);
}
function wrapperRead(data, callback) {
    read(data.host_id, data.ts, callback);
}
function write(id, data, callback) {
    model.base(id).
        create(data, function (err, result) {
            if (err) {
                logger.error('traffic.write mode.create', err);
                callback(err, null);
                return;
            }
            callback(null, result._id.toString());
        });
}
function read(id, ts, callback) {
    model.base(id)
        .findById(ts)
        .exec(function (err, result) {
            if (err) {
                logger.error('traffic.read model.findOne host.id:%s id:%s', id, ts, err);
                callback(err);
                return;
            }
            callback(null, result);
        });
}
require('./loader')(function (core) {
    model = core.models.odm.traffic;
    core.events.on('traffic.write', write);
    core.events.on('traffic.read', read);
});
exports.wrapper = {
    read: wrapperRead,
    write: wrapperWrite
};
exports.read = read;
exports.write = write;
