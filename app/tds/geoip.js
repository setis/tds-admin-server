var geoip2 = require('node-geoip2'),
    cfg = require('../../cfg/tds.json').geoip,
    logger = require('./logger'),
    mode = cfg.cache.mode,
    ttl = cfg.cache.ttl || 60,
    method,
    i = 0;
function handler(ip, callback) {
    method(ip, function (error, result) {
            if (error) {
                logger.error("geoip.handler country ip:%s", ip, error);
                callback(error, null);
                return;
            }
            if (result === null) {
                console.log(method);
                logger.warn("geoip.handler not found country ip:%s method:%s", ip);
                callback(null, null);
                return;
            }
            callback(null, result);
            if (mode) {
                cache.set(ip, (result !== null) ? result : false, function (err) {
                    if (err) {
                        logger.error('geoip.handler cache.set ip:%s', ip, err);
                    }
                });
            }
            if (cfg.clear === 0) {
                geoip2.cleanup();
            } else {
                i++;
                if (i >= cfg.clear) {
                    geoip2.cleanup();
                    i = 0;
                }
            }
        }
    );
}
function wrapper(ip, cb) {
    cache.get(ip, function (err, value) {
        if (err) {
            logger.error('geoip.wrapper cache.get ', ip, err);
            handler(ip, cb);
            return;
        }
        if (value === null) {
            handler(ip, cb);
            return;
        }
        cb(null, value);
        cache.ttl(ip, ttl, function (err) {
            if (err) {
                logger.error('geoip.wrapper cache.ttl ', ip, err);
            }
        });
    });
}
if (cfg) {
    try {
        geoip2.init(cfg.path);
        method = geoip2.lookup;
        logger.info('geoip path:', cfg.path);
    } catch (e) {
        logger.error('geoip not load %s', cfg.path, e);
        method = geoip2.lookupSimple;
        logger.info('geoip net');
    }

}
require('./loader')(function (core) {
    core.events.on('geoip', handler);
    if (mode) {
        cache = require('./cache')(core.clients.redis, cfg.cache);
    }
});
exports.handler = handler;
