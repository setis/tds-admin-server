var cfg = require('../../cfg/tds').logger,
    winston = require('winston');
module.exports = new winston.Logger({
    level: cfg.level,
    transports: [
        new (winston.transports.Console)({
            timestamp: true,
            colorize: true
        }),
        new (winston.transports.File)({
            timestamp: true,
            filename: cfg.dir + '/' + cfg.file,
        })
    ]
});
    