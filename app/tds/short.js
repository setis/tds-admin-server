var request = require('request');
var cfg = require(__dirname + '/../../cfg/tds').shortUrl;
function api(action, cb, data) {
    if (data === undefined) {
        data = {
            key: cfg.key,
            action: action
        };
    } else {
        data.key = cfg.key;
        data.action = action;
    }
    request.get({
        url: cfg.server,
        qs: data,
        json: true,
        strictSSL: false,
        rejectUnauthorized: false
    }, function (error, response, body) {
        if (error) {
            cb(new Error('shortURL:' + cfg.server + ' msg:' + error.message, error.code), body);
        } else if (response.statusCode !== 200) {
            cb(new Error('shortURL:' + cfg.server + ' status code:' + response.statusCode, response.statusCode), body);
        } else {
            cb(null, body);
        }
    });
}
function view(cb) {
    api('link.list', function (err, result) {
        if (err) {
            cb(err, null);
            return;
        }
        cb(null, result);
    });
}
function add(url, ttl, cb) {
    api('link.add', function (err, result) {
        if (err) {
            cb(err, null);
            return;
        }

        cb(null, result.short);
    }, {ttl: ttl, url: url});
}
function remove(url, cb) {
    api('link.remove', function (err, result) {
        if (err) {
            cb(err, null);
            return;
        }
        cb(null, result.short);
    }, {url: url});
}
function short(url, cb) {
    api('link.short', function (err, result) {
        if (err) {
            cb(err, null);
            return;
        }
        cb(null, result);
    }, {in: url});
}
function long(url, cb) {
    api('link.long', function (err, result) {
        if (err) {
            cb(err, null);
            return;
        }
        cb(null, result.short);
    }, {in: url});
}
exports.view = view;
exports.add = add;
exports.remove = remove;
exports.short = short;
exports.long = long;
//add('http://ya.ru',60,function(err,result){
//    console.log(err,result);
//});
