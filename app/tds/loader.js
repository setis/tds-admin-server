'use strict';
var fs = require('fs'),
    path = require('path'),
    mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment'),
    cfg = require('../../cfg/tds'),
    logger = require('./logger'),
    loader = require('../kernel/loader');
var models = {
        odm: {},
        orm: {}
    },
    connections = {
        mongoose: {},
        orm: {}
    },
    clients = {};
var eventCount = 2;
var count = Object.keys(cfg.mongoose).length;
var dirOdm = path.join(__dirname, '../../models/odm');
var mongoose = require('mongoose');
for (var name in cfg.mongoose) {
    var load = function () {
        loader.odm(mongoose, connections.mongoose, dirOdm, models.odm, {autoIncrement: autoIncrement}, {
            error: function (err, path) {
                logger.error('odm path: %s %s', path, err);
            },
            success: function (path) {
                logger.info('odm load %s', path);
            },
            done: function () {
                eventCount--;
            }
        });
    };
    (function (cfg, name) {
        var connection = mongoose.createConnection(cfg.connect, cfg.options);
        connections.mongoose[name] = connection;
        connection.once('open', function (connection) {
            logger.info('mongoose connect: ' + name);
            count--;
            if (!count) {
                load();
            }
        });
        connection.on('error', function (err) {
            logger.error('not connect name: %s mongoose error: %s', name, err);
        });
        autoIncrement.initialize(connection);
    })(cfg.mongoose[name], name);

}

var mariasql = require('my_pool_sql');
var names = Object.keys(cfg['maria-pool']);
if (names.length === 1) {
    var conf = cfg['maria-pool'][names[0]];
    clients.mariasql = new mariasql(conf.pool, conf.options);

} else {
    clients.mariansql = {};
    for (var name in cfg['maria-pool']) {
        var conf = cfg['maria-pool'][name];
        clients.mariasql[name] = new mariasql(conf.pool, conf.options);
    }
}

var redis = require('pool-redis');
var names = Object.keys(cfg.redis);
if (names.length === 1) {
    clients.redis = redis(cfg.redis[names[0]]);
} else {
    clients.redis = {};
    for (var name in cfg.redis) {
        clients.redis[name] = redis(cfg.redis[name]);
    }
}
var EventEmitter = require('events').EventEmitter;
var events = new EventEmitter;
events.setMaxListeners(0);
var pmx = require('pmx').init({
    http: true, // HTTP routes logging (default: true)
    ignore_routes: [/socket\.io/, /notFound/], // Ignore http routes with this pattern (Default: [])
    errors: true, // Exceptions loggin (default: true)
    custom_probes: true, // Auto expose JS Loop Latency and HTTP req/s as custom metrics
    network: true, // Network monitoring at the application level
    ports: true  // Shows which ports your app is listening on (default: false)});
});
eventCount--;
module.exports = function (cb) {
    var id = setInterval(function () {
        if (eventCount) {
            return;
        }
        clearInterval(id);
        cb({models: models, connections: connections, clients: clients, cfg: {general: cfg}, events: events});
    }, 50);
    if (!eventCount) {
        cb({models: models, connections: connections, clients: clients, cfg: {general: cfg}, events: events});
    }
};
