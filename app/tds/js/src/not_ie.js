// var cfg = {
//     el: el,
//     token: null,
//     timeout: 35,
//     api: server,
//     ssl: false,
// };
(function (cfg) {
    var xhr;

    function statistic() {
        var navigator = window.navigator;
        return {
            lang: (navigator.language || navigator.browserLanguage || navigator.systemLanguage || navigator.userLanguage || null),
            screen: window.screen.width + 'x' + window.screen.height,
            time: Math.floor(new Date().getTime() / 1000),
            offset: (new Date()).getTimezoneOffset() / 60
        };
    }

    function rotation() {
        var data = statistic();
        var url = 'http';
        url += (cfg.ssl) ? 's' : '';
        url += '://' + cfg.api + '?';
        var f = false;
        for (var i in data) {
            if (f) {
                url += '&';
            }
            url += i + '=' + data[i];
            f = true;
        }
        if (cfg.rt !== undefined || cfg.rt !== false) {
            url += '&rt=' + cfg.rt;
        }
        url += '&token=' + cfg.token;
        xhr.open('GET', url, 1);
        try {
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        } catch (e) {
        }
        xhr.send();
    }

    function load() {
        if (xhr.responseText !== '') {
            var data = JSON.parse(xhr.responseText);
            if (data.adsData !== undefined && data.adsData !== '' && data.adsData !== null) {
                var content = document.getElementById(cfg.el);
                content.innerHTML = data.adsData;
            }
            if (typeof data.interval === 'number') {
                cfg.timeout = data.interval;
            }
        }
        setTimeout(rotation, cfg.timeout * 1e3);
    }

    function error() {
        setTimeout(rotation, cfg.timeout * 1e3);
    }

    xhr = new XMLHttpRequest();
    xhr.onload = load;
    xhr.onerror = error;
    if (typeof JSON === 'undefined' || typeof JSON.parse !== 'function') {
        var json2 = document.createElement('script');
        json2.src = 'http://cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.min.js';
        json2.type = "text/javascript";
        json2.async = true;
        json2.onload = rotation;
        document.getElementsByTagName('head')[0].appendChild(json2);
    } else {
        rotation();
    }
})('{{{cfg}}}');