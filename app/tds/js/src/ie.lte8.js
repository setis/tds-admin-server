// var cfg = {
//     el: el,
//     token: null,
//     timeout: 35,
//     api: server,
//     ssl: false,
// };
(function (cfg) {
    var xhr;
    var config = cfg;

    function statistic() {
        var navigator = window.navigator;
        return {
            lang: (navigator.language || navigator.browserLanguage || navigator.systemLanguage || navigator.userLanguage || null),
            screen: window.screen.width + 'x' + window.screen.height,
            time: Math.floor(new Date().getTime() / 1000),
            offset: (new Date()).getTimezoneOffset() / 60
        };
    }

    function rotation() {
        var data = statistic();
        var url = 'http';
        url += (config.ssl) ? 's' : '';
        url += '://' + config.api + '?';
        var f = false;
        for (var i in data) {
            if (f) {
                url += '&';
            }
            url += i + '=' + data[i];
            f = true;
        }
        if (config.rt !== undefined || config.rt !== false) {
            url += '&rt=' + config.rt;
        }
        url += '&token=' + config.token;
        xhr.open('GET', url, 1);
        try {
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        } catch (e) {

        }
        xhr.send();
    }

    function load() {

        if (xhr.responseText !== '') {
            var data = JSON.parse(xhr.responseText);
            if (data.adsData !== undefined && data.adsData !== '' && data.adsData !== null) {
                var content = document.getElementById(config.el);
                content.innerHTML = '';
                var re = /<script.*?src=['|"](.*?)['|"].*?>(.*?)<\/script>/gmi;
                var match;
                var scripts = [];
                while ((match = re.exec(data.adsData))) {
                    var e = document.createElement('script');
                    e.text = match[2];
                    e.src = match[1];
                    e.defer = true;
                    scripts.push(e);
                    data.adsData.replace(new RegExp(match[0], 'gm'), '');
                }
                content.innerHTML = data.adsData;
                if (scripts.length !== 0) {
                    for (var i in scripts) {
                        content.appendChild(scripts[i]);
                    }
                }
            }
            if (typeof data.interval === 'number') {
                config.timeout = data.interval;
            }
        }
        setTimeout(rotation, config.timeout * 1e3);
    }

    function error() {
        setTimeout(rotation, config.timeout * 1e3);
    }
    try {
        xhr = new XDomainRequest();
        xhr.onload = load;
        xhr.onerror = error;
    } catch (e) {
        try {
            xhr = new XMLHttpRequest();
            xhr.onload = load;
            xhr.onerror = error;
        } catch (e) {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    load();
                } else if (xhr.readyState === 4 && xhr.status !== 200) {
                    error();
                }
            };
        }
    }
    if (typeof JSON === 'undefined' || typeof JSON.parse !== 'function') {
        var json2 = document.createElement('script');
        json2.src = 'http://cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.min.js';
        json2.type = "text/javascript";
        json2.async = true;
        json2.onload = init;
        json2.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                setTimeout(rotation, 350);
            }
        };
        document.getElementsByTagName('head')[0].appendChild(json2);
    } else {
        rotation();
    }
})('{{{cfg}}}');