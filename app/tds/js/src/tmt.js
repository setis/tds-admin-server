// var cfg = {
//     el: el,
//     token: null,
//     timeout: 35,
//     api: server,
//     ssl: false,
// };
(function (cfg) {
    if(cfg.el === undefined || cfg.el === ''){
        cfg.el = "asd_" + (new Date()).getTime();
    }
    function statistic() {
        var navigator = window.navigator;
        return {
            lang: (navigator.language || navigator.browserLanguage || navigator.systemLanguage || navigator.userLanguage || null),
            screen: window.screen.width + 'x' + window.screen.height,
            time: Math.floor(new Date().getTime() / 1000),
            offset: (new Date()).getTimezoneOffset() / 60
        };
    }
    function url() {
        var data = statistic();
        var url = 'http';
        url += (cfg.ssl) ? 's' : '';
        url += '://' + cfg.api + '?';
        var f = false;
        for (var i in data) {
            if (f) {
                url += '&';
            }
            url += i + '=' + data[i];
            f = true;
        }
        if (cfg.rt !== undefined && cfg.rt !== false) {
            url += '&rt=' + cfg.rt;
        }
        url += '&token=' + cfg.token;
        return url;
    }

    function init(){
        var content = document.getElementById(cfg.el);
        var iframe = document.createElement('iframe');
        iframe.src = url();
        iframe.onload = function(){
            setTimeout(init,cfg.timeout*1e3);
        };
        iframe.onerror = function(){
            setTimeout(init,cfg.timeout*1e3);
        };
        content.innerHTML = '';
        content.appendChild(iframe);

    }
    init();
})('{{{cfg}}}');