// var cfg = {
//     el: el,
//     token: null,
//     timeout: 35,
//     api: server,
//     ssl: false,
// };
(function (cfg) {
    var xhr;
    function statistic() {
        var navigator = window.navigator;
        return {
            lang: (navigator.language || navigator.browserLanguage || navigator.systemLanguage || navigator.userLanguage || null),
            screen: window.screen.width + 'x' + window.screen.height,
            time: Math.floor(new Date().getTime() / 1000),
            offset: (new Date()).getTimezoneOffset() / 60
        };
    }
    function rotation() {
        var data = statistic();
        var url = 'http';
        url += (cfg.ssl) ? 's' : '';
        url += '://' + cfg.api + '?';
        var f = false;
        for (var i in data) {
            if (f) {
                url += '&';
            }
            url += i + '=' + data[i];
            f = true;
        }
        if (cfg.rt !== undefined || cfg.rt !== false) {
            url += '&rt=' + cfg.rt;
        }
        url += '&token=' + cfg.token;
        xhr.open('GET', url, 1);
        try {
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        } catch (e) {

        }
        xhr.send();
    }
    function load() {

        if (xhr.responseText !== '') {
            var data = JSON.parse(xhr.responseText);
            if (data.adsData !== undefined && data.adsData !== '' && data.adsData !== null) {
                var content = document.getElementById(cfg.el);
                var wrapper = document.createElement('div');
                wrapper.innerHTML = data.adsData;
                var arr = wrapper.getElementsByTagName('script');
                if (arr.length) {
                    var scripts = [];
                    for (var i in arr) {
                        if (arr[i] !== undefined && arr[i].src !== undefined && arr[i].src !== 'undefined') {
                            var e = document.createElement('script');
                            e.src = arr[i].src;
                            e.type = "text/javascript";
                            e.async = true;
                            e.defer = true;
                            scripts.push(e);
                            var elem = arr[i].parentNode || arr[i].parent;
                            if (elem !== undefined) {
                                elem.removeChild(arr[i]);
                            }
                        }

                    }
                    content.innerHTML = wrapper.innerHTML;
                    if (scripts.length !== 0) {
                        for (var i in scripts) {
                            content.appendChild(scripts[i]);
                        }
                    }

                }
            }
            if (typeof data.interval === 'number') {
                cfg.timeout = data.interval;
            }
        }
        setTimeout(rotation, cfg.timeout * 1e3);
    }
    function error() {
        console.log('error');
        setTimeout(rotation, cfg.timeout * 1e3);
    }
    try {
        xhr = new XDomainRequest();
        xhr.onload = load;
        xhr.onerror = error;
    } catch (e) {
        xhr = new XMLHttpRequest();
        xhr.onload = load;
        xhr.onerror = error;
    }
    if (typeof JSON === 'undefined' || typeof JSON.parse !== 'function') {
        var json2 = document.createElement('script');
        json2.src = 'http://cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.min.js';
        json2.type = "text/javascript";
        json2.async = true;
        json2.onload = init;
        json2.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                setTimeout(rotation, 350);
            }
        };
        document.getElementsByTagName('head')[0].appendChild(json2);
    } else {
        rotation();
    }
})('{{{cfg}}}');