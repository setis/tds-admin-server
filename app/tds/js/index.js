var mustache = require('mustache'),
        fs = require('fs'),
        async = require('async'),
        logger = require('../logger'),
        list = {
            js: {
                ie8: '/dist/ie.lte8.js',
                ie: '/dist/ie.js',
                tmt: '/dist/tmt.js',
                not_ie: '/dist/not_ie.js',
            },
            tmpl: {
                html: '/tmpl/wrapper.html',
                js: '/tmpl/wrapper.js'
            }
        },
db = {js: {}, tmpl: {}};
function load() {
    async.parallel([
        function (callback) {
            async.forEachOf(list.js, function (value, key, callback) {
                var path = __dirname + value;
                fs.readFile(path, function (err, data) {
                    if (err) {
                        logger.error('file not read js key:%s path:%s', key, path, err);
                    } else {
                        db.js[key] = data.toString();
                        logger.info('js load:js.%s path:%s', key, path);
                    }
                    callback(err, key);
                });
            }, callback);
        },
        function (callback) {
            async.forEachOf(list.tmpl, function (value, key, callback) {
                var path = __dirname + value;
                fs.readFile(path, function (err, data) {
                    if (err) {
                        logger.error('file not read tmpl key:%s path:%s', key, path, err);
                    } else {
                        db.tmpl[key] = data.toString();
                        logger.info('js load:tmpl.%s path:%s', key, path);
                    }
                    callback(err, key);
                });
            }, callback);
        }
    ]);
}
function rotation(ie, version, data) {
    var content = (ie) ? (version === 8) ? db.js.ie8 : db.js.ie : db.js.not_ie;
    return mustache.render(content, data);
}
function tmt(data) {
    return mustache.render(db.js.tmt, data);
}
function wrapper_js(ie, data) {
    var content = (ie) ? db.js.ie : db.js.not_ie;
    data.js = mustache.render(content, data);
    return mustache.render(db.tmpl.js, data);
}
function wrapper_html(ie, data) {
    var content = (ie) ? db.js.ie : db.js.not_ie;
    data.js = mustache.render(content, data);
    return mustache.render(db.tmpl.html, data);
}

load();
exports.load = load;
exports.db = db;
exports.wrapper = {
    js: wrapper_js,
    html: wrapper_html
};
exports.js = {
    rotation: rotation,
    tmt: tmt
};
