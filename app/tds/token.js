var crypto = require('crypto'),
    cfg = require('../../cfg/tds.json').token;

function create(host_id, company_id, uniq_id) {
    var str = [host_id, (new Date()).getTime(), company_id, uniq_id.toString()].join('||');
    return encrypt(str);
}
function parse(token) {
    var str = decrypt(token);
    var result = str.split('||',4);
    if (!result instanceof Array && result.length !== 4) {
        return false;
    }
    return {
        host_id: parseInt(result[0]),
        time: (new Date).setTime(result[1]),
        company_id: parseInt(result[2]),
        uniq_id: new Buffer(result[3])
    };
}
function encrypt(text) {
    var cipher = crypto.createCipher(cfg.algorithm, cfg.password);
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text) {
    var decipher = crypto.createDecipher(cfg.algorithm, cfg.password);
    var dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}
exports.crypt = create;
exports.decrypt = parse;