var model,
    cfg = require('../../cfg/tds.json').company,
    logger = require('./logger'),
    mode = cfg.cache.mode || false;
if (mode) {
    var NodeCache = require("node-cache"),
        loading = false,
        host = new NodeCache({stdTTL: cfg.cache.ttl, checkperiod: cfg.cache.period}),
        company = new NodeCache({stdTTL: cfg.cache.ttl, checkperiod: cfg.cache.period, useClones: false});
    host.on("expired", function (host_id, company_id) {
        update(company_id);
    });
    company.on("expired", function (company_id) {
        update(company_id);
    });
    setInterval(load, cfg.reload * 1e3);
}
function load(status) {
    if (status && mode) {
        loading = false;
    }
    model.find({}, function (err, result) {
        if (err) {
            logger.error('company not loading ', err);
            setTimeout(load, 1e3);
            return;
        }
        if (result.length === 0) {
            return;
        }
        for (var i in result) {
            var data = result[i];
            if (data.front.id !== undefined) {
                logger.debug('company.load host:%s company:%s', data.front.id, data.id);
                host.set(data.front.id, data.id, function (err) {
                    if (err) {
                        logger.error('company.load host.set ', err);
                        return;
                    }
                });
            }
            if (data.layer.id !== undefined) {
                logger.debug('company.load host:%s company:%s', data.layer.id, data.id);
                host.set(data.layer.id, data.id, function (err) {
                    if (err) {
                        logger.error('company.load host.set ', err);
                        return;
                    }
                });
            }
            company.set(data.id, data._doc, function (err) {
                if (err) {
                    logger.error('company.load company.set key:%s', data.id, err);
                    return;
                }
            });
        }
        if (status && mode) {
            loading = true;
        }
    });
}

function update(company_id) {
    model.findOne({id: company_id},
        function (err, result) {
            if (err) {
                logger.error('company.update company_id: ', company_id, err);
                return;
            }
            if (result === null) {
                company.del(company_id, function (err) {
                    if (err) {
                        logger.error('company.update company.del company_id:', company_id, err);
                    }
                });
                return;
            }
            host.set(result.front.id, company_id, function (err) {
                if (err) {
                    logger.error('company.update host.set key:', result.front.id, err);
                }
            });
            if (result.layer !== undefined && result.layer.id !== undefined) {
                host.set(result.layer.id, company_id, function (err) {
                    if (err) {
                        logger.error('company.update host.set key:', result.layer.id, err);
                    }
                });
            }
            company.set(company_id, result._doc, function (err) {
                if (err) {
                    logger.error('company.update company.set key:', company_id, err);
                }
            });

        });
}
function find(id, cb) {
    model.findOne({
            "$or": [
                {"front.id": id},
                {"layer.id": id}
            ]
        },
        function (err, result) {
            if (err) {
                logger.error('company.find host_id: ', id, err);
                cb(err, result);
                return;
            }
            if (result === null) {
                cb(err, result);
                return;
            }
            logger.debug('company.find', id, Object.keys(result._doc));
            host.set(id, result.id, function (err) {
                if (err) {
                    logger.error('company host.set key:', id, err);
                }
            });
            company.set(result.id, result._doc, function (err) {
                if (err) {
                    logger.error('company company.set key:', id, err);
                }
            });
            cb(null, result._doc);

        });
}
function handler(id, cb) {
    host.get(id, function (err, value) {
        if (err) {
            cb(err, null);
            logger.error("company.handler host.get host_id:", id, err);
            return;
        }
        if (value === undefined) {
            find(id, cb);
            return;
        }
        company.get(value, function (err, result) {
            if (err) {
                logger.error("company.handler company.get company_id:", value, err);
                cb(err, null);
                return;
            }
            if (result === undefined) {
                find(id, cb);
                return;
            }
            cb(null, result);
        });
    });
}
function wrapper(id, cb) {
    if (!mode && !loading) {
        find(id, cb);
    }
    else {
        handler(id, cb);
    }
}
require('./loader')(function (core) {
    model = core.models.odm.company;
    if (mode) {
        load(true);
    }
    core.events.on('company', wrapper);
    core.events.on('company.flush', function () {
        host.flushAll();
        company.flushAll();
    });
    core.events.on('company.reload', function () {
        host.flushAll();
        company.flushAll();
        load();
    });
    core.events.on('company.load', load);
});
exports.wrapper = wrapper;