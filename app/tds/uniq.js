var _ip = require('noi'),
    crypto = require('crypto'),
    logger = require('./logger'),
    model,
    timeout = 86400*1e3;
function id(ua, ip) {
    return new Buffer([ip, crypto.createHash('sha256').update(ua, 'utf8').digest().toString(), ua.length].join(':'));
}
function load(company_id, uniq, callback) {
    var m = model.base(company_id);
    m.findOne({uniq: uniq}, function (err, result) {
        if (err) {
            logger.error('uniq not found: ', uniq, err);
            callback(err, null);
            return;
        }
        var isIn = false;
        var isUniq = false;
        var isDrop = true;
        var isPre = false;
        if (result === null) {
            isIn = true;
            isUniq = true;
            isDrop = false;
            isPre = true;
            callback(null, {in: isIn, uniq: isUniq, drop: isDrop, pre: isPre, data: result, model: m});
        } else {
            var timeUniq = (new Date()).getTime();
            if (result.time.drop === undefined || timeUniq - result.time.drop.getTime() >= timeout) {
                isDrop = false;
            }
            if (result.time.in === undefined || timeUniq - result.time.in.getTime() >= timeout) {
                isIn = true;
            }
            if (result.time.uniq === undefined || timeUniq - result.time.uniq.getTime() >= timeout) {
                isUniq = true;
            }
            if (!isDrop && (result.time.pre === undefined || timeUniq - result.time.pre.getTime() >= timeout)) {
                isPre = true;
                result.layer = {time: new Date(), try: 0};
                result.exploits = [];
                result.time.pre = new Date();
                var data =  {in: isIn, uniq: isUniq, drop: isDrop, pre: isPre, data: result, model: m};
                m.update({_id: result._id}, {
                    '$set': {
                        layer: {time: new Date(), try: 0},
                        'time.pre': new Date(),
                        exploits: []
                    }
                }, function (err) {
                    if (err) {
                        if (err.code === 1) {
                            setTimeout(function () {
                                m.update({_id: result._id}, {
                                    '$set': {
                                        layer: {time: new Date(), try: 0},
                                        'time.pre': new Date(),
                                        exploits: []
                                    }
                                }, function (err) {
                                    if (err) {
                                        logger.error('uniq.load update', err);
                                    }
                                    callback(err, data);
                                });
                            }, 150);
                        } else {
                            logger.error('uniq.load update',err);
                            callback(err, data);
                        }
                    } else {
                        callback(null, data);
                    }
                });

            } else {
                callback(null, {in: isIn, uniq: isUniq, drop: isDrop, pre: isPre, data: result, model: m});
            }
        }
    });
}
function save(uniq, create, update, callback) {
    if (uniq.data) {
        uniq.model.update({"_id": uniq.data._id}, update, function (err) {
            if (err) {
                if (err.code === 1) {
                    setTimeout(function () {
                        uniq.model.update({"_id": uniq.data._id}, update, callback);
                    }, 150);
                } else {
                    logger.error('uniq.save create',err);
                    callback(err);
                }
            } else {
                callback();
            }
        });
    } else {
        uniq.model.create(create, function (err) {
            if (err) {
                if (err.code === 11000) {
                    uniq.model.update({"_id": uniq.data._id}, update, function (err) {
                        if (err) {
                            if (err.code === 1) {
                                setTimeout(function () {
                                    uniq.model.update({"_id": uniq.data._id}, update, callback);
                                }, 150);
                            } else {
                                logger.error('uniq.save update',err);
                                callback(err);
                            }
                        } else {
                            callback();
                        }
                    });
                } else {
                    logger.error('uniq.save create',err);
                    callback(err);
                }
            } else {
                callback();
            }
        });
    }
}
require('./loader')(function (core) {
    logger = core.logger;
    model = core.models.odm.uniq;
});
exports.id = id;
exports.save = save;
exports.load = load;