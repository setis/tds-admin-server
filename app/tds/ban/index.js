module.exports = function (core) {
   require('./ip/global')(core);
   require('./ip/local')(core);
};