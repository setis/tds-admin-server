var NodeCache = require("node-cache");
var model, cache, logger, ttl;

function is(ip, cb) {
    cache.get(ip, function (err, value) {
        if (err) {
            logger.error("ban.global cache.get ", ip, err);
            handler_is(ip, cb);
            return;
        }
        if (value === null) {
            handler_is(ip, cb);
            return;
        }
        cb(null, value);
    });
}
function handler_is(ip, cb) {
    model.is(ip, function (err, result) {
        if (err) {
            logger.error("not found ban.global ", ip, err);
        } else {
            cache.set(ip, result, function (err) {
                if (err) {
                    logger.error('ban.global cache.set ', ip, err);
                }
            });
        }
        cb(err, result);
    });
}

function ban(ip, code) {
    cache.set(ip, {time: new Date(), code: code}, function (err, value) {
        if (err) {
            logger.error("ban.ip.global.ban cache.set ", ip, err);
            return;
        }
    });
    model.ban(ip, code, function (err) {
        if (err) {
            logger.error("ban.ip.global.ban model.ban ", ip, err);
            return;
        }
    });
}
function unban(ip) {
    cache.del(ip, function (err) {
        if (err) {
            logger.error("ban.ip.global.unban cache.del ", ip, err);
            return;
        }
    });
    model.unban(ip, function (err) {
        if (err) {
            logger.error("ban.ip.global.ban model.unban ", ip, err);
            return;
        }
    });
}
module.exports = function (core) {
    var cfg = core.cfg.tds.ban.ip;
    model = core.models.odm.ban_ip;
    logger = core.logger;
    cache = new NodeCache({stdTTL: cfg.ttl, checkperiod: cfg.period});
    ttl = cfg.ttl;
    core.events.on('ban.ip.global.is', is);
    core.events.on('ban.ip.global.ban', ban);
    core.events.on('ban.ip.global.unban', unban);
};