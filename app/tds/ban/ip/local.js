var NodeCache = require("node-cache");
var model, cache, logger, ttl;

function is(company_id, ip, cb) {
    var key = [company_id, ip].join(':');
    cache.get(key, function (err, value) {
        if (err) {
            logger.error("ban.ip.local.ban cache.get ", key, err);
            handler(company_id, ip, cb);
            return;
        }
        if (value === null) {
            handler(company_id, ip, cb);
            return;
        }
        cb(null, value);
        cache.ttl(key, ttl, function (err) {
            if (err) {
                logger.error('ban.ip.local.ban cache.ttl ', key, err);
            }
        });
    });
}
function handler_is(company_id, ip, cb) {
    model.base(company_id)
            .ip(ip, function (err, result) {
                if (err) {
                    logger.error("ban.ip.local.ban model.ip ", company_id, ip, err);
                } else {
                    var key = [company_id, ip].join(':');
                    cache.set(key, result, function (err) {
                        if (err) {
                            logger.error('ban.ip.local.ban cache.set ', key, err);
                        }
                    });
                }
                cb(err, result);
            });
}

function ban(company_id, ip, code) {
    var key = [company_id, ip].join(':');
    cache.set(key, {time: new Date(), code: code}, function (err) {
        if (err) {
            logger.error("ban.ip.local.ban cache.set ", key, err);
            return;
        }
    });
    model.base(company_id)
            .ban(ip, code, function (err) {
                if (err) {
                    logger.error("ban.ip.local.ban model.ban ", ip, err);
                    return;
                }
            });
}
function unban(company_id, ip) {
    var key = [company_id, ip].join(':');
    cache.del(key, function (err) {
        if (err) {
            logger.error("ban.ip.local.unban cache.del ", key, err);
            return;
        }
    });
    model.base(company_id)
            .unban(ip, function (err) {
                if (err) {
                    logger.error("ban.ip.local.ban model.unban ", ip, err);
                    return;
                }
            });
}
module.exports = function (core) {
    var cfg = core.cfg.tds.ban.ip;
    model = core.models.odm.ban_ip;
    cache = new NodeCache({stdTTL: cfg.ttl, checkperiod: cfg.period});
    ttl = cfg.ttl;
    core.events.on('ban.ip.local.is', is);
    core.events.on('ban.ip.local.ban', ban);
    core.events.on('ban.ip.local.unban', unban);
};