var NodeCache = require("node-cache");
var model, cache, logger, ttl;

function wrapper(company_id, uniq_id, cb) {
    var key = [company_id, ip].join(':');
    cache.get(key, function (err, value) {
        if (err) {
            logger.error("ban.local cache.get ", key, err);
            handler(company_id, ip, cb);
            return;
        }
        if (value === null) {
            handler(company_id, ip, cb);
            return;
        }
        cb(null, value);
        cache.ttl(key, ttl, function (err) {
            if (err) {
                logger.error('ban.local cache.ttl ', key, err);
            }
        });
    });
}
function handler(company_id, uniq_id, cb) {
    model.base(company_id)
            .findOne(uniq_id, function (err, result) {
                if (err) {
                    logger.error("not found ban.local ", company_id, ip, err);
                } else {
                    var key = [company_id, ip].join(':');
                    cache.set(key, result, function (err) {
                        if (err) {
                            logger.error('ban.local cache.set ', key, err);
                        }
                    });
                }
                cb(err, result);
            });
}
module.exports = function (core) {
    var cfg = core.cfg.tds.uniq;
    model = core.models.odm.uniq_global;
    cache = new NodeCache({stdTTL: cfg.ttl, checkperiod: cfg.period});
    ttl = cfg.ttl;
    return wrapper;
};