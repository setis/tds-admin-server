var NodeCache = require("node-cache");
var model, cache, logger, ttl;

function wrapper(uniq_id, cb) {
    cache.get(uniq_id.toString(), function (err, value) {
        if (err) {
            logger.error("uniq.global cache.get ", uniq_id.toString(), err);
            handler(uniq_id, cb);
            return;
        }
        if (value === null) {
            handler(uniq_id,  cb);
            return;
        }
        cb(null, value);
        cache.ttl(uniq_id.toString(), ttl, function (err) {
            if (err) {
                logger.error('ban.local cache.ttl ', uniq_id.toString(), err);
            }
        });
    });
}
function handler(uniq_id, cb) {
    model.findOne(uniq_id, function (err, result) {
        if (err) {
            logger.error("uniq.global ", uniq_id.toString(), err);
        } else {
            cache.set(uniq_id.toString(), result, function (err) {
                if (err) {
                    logger.error('uniq.global cache.set ', uniq_id.toString(), err);
                }
            });
        }
        cb(err, result);
    });
}
module.exports = function (core) {
    var cfg = core.cfg.tds.uniq;
    model = core.models.odm.uniq_global;
    cache = new NodeCache({stdTTL: cfg.ttl, checkperiod: cfg.period});
    ttl = cfg.ttl;
    return wrapper;
};