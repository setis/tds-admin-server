var model,
    full,
    cache,
    ttl,
    method,
    cfg = require('../../cfg/tds.json').isp,
    mode = cfg.cache.mode
logger = require('./logger');

function wrapper_country(country, ip, cb) {
    cache.get(ip, function (err, value) {
        if (err) {
            logger.error("isp.wrapper_country cache.get country:%s ip:%s", country, ip, err);
            handler_country(country, ip, cb);
            return;
        }
        if (value === null) {
            handler_country(country, ip, cb);
            return;
        }
        cb(null, value);
        if (mode) {
            cache.ttl(ip, ttl, function (err) {
                if (err) {
                    logger.error('isp.wrapper_country cache.ttl ip:%s', ip, err);
                }
            });
        }
    });
}
function wrapper_full(ip, cb) {
    cache.get(ip, function (err, value) {
        if (err) {
            logger.error("isp.wrapper_full cache.get ip:%s", ip, err);
            handler_full(ip, cb);
            return;
        }
        if (value === null) {
            handler_full(ip, cb);
            return;
        }
        cb(null, value);
        if (mode && value !== null) {
            cache.ttl(ip, ttl, function (err) {
                if (err) {
                    logger.error('isp.wrapper_full cache.ttl ip:%s', ip, err);
                }
            });
        }
    });
}
function handler_country(country, ip, cb) {
    model.base(country)
        .ip(ip, function (err, result) {
            if (err) {
                logger.error("isp.handler_country country:%s ip:%s", country, ip, err);
            } else {
                if (mode) {
                    cache.set(ip, (result !== null) ? result._doc : false, function (err) {
                        if (err) {
                            logger.error('isp.handler_country cache.set country:%s ip:%s', country, ip, err);
                        }
                    });
                }
            }
            cb(err, status);
        });
}
function handler_full(ip, cb) {
    full.ip(ip, function (err, result) {
        if (err) {
            logger.error("isp.handler_full ip:%s ", ip, err);
        } else {
            if (mode) {
                cache.set(ip, (result !== null) ? result._doc : false, function (err) {
                    if (err) {
                        logger.error('isp.handler_full cache.set ip:%s', ip, err);
                    }
                });
            }
        }
        cb(err, result);
    });
}
require('./loader')(function (core) {
    model = core.models.odm.isp;
    full = core.models.odm.isp_full;
    if (mode) {
        var cacheClass = require('./cache');
        cache = new  cacheClass(core.clients.redis, cfg.cache);
        ttl = cfg.cache.ttl || 60;
        if (cfg.full) {
            method = wrapper_full
            core.events.on('isp', wrapper_full);
        } else {
            core.events.on('isp', wrapper_country);
            method = wrapper_country
        }
    } else {
        if (cfg.full) {
            core.events.on('isp', handler_full);
            method = handler_full;
        } else {
            core.events.on('isp', handler_country);
            method = handler_country;
        }
    }
});
exports.wrapper = method;