/**
 * Created by alex on 19.02.16.
 */
var async = require('async');
module.exports = function(clients, cfg) {
    var redis = (cfg.name === "default") ? clients : clients[cfg.name];
    var prefix = cfg.prefix || false, isJSON = cfg.json || false, ttl = cfg.ttl || false;
    if (ttl) {
        this.set = function (key, value, callback) {
            async.waterfall([
                function (callback) {
                    redis.getClient(function (client, done) {
                        if (isJSON) {
                            value = JSON.stringify(value);
                        }
                        client.set((prefix) ? prefix + key : key, value, function (err, result) {
                            done();
                            callback(err, result);
                        });
                    });
                },
                function (callback) {
                    redis.getClient(function (client, done) {
                        client.expire((prefix) ? prefix + key : key, ttl, function (err, result) {
                            done();
                            callback(err, result);
                        });
                    });
                }
            ], callback);
        };
    } else {
        this.set = function (key, value, callback) {
            redis.getClient(function (client, done) {
                if (isJSON) {
                    value = JSON.stringify(value);
                }
                client.set((prefix) ? prefix + key : key, value, function (err, result) {
                    done();
                    callback(err, result);
                });
            });
        };
    }
    this.get = function (key, callback) {
        redis.getClient(function (client, done) {
            client.get((prefix) ? prefix + key : key, function (err, result) {
                done();
                if(err){
                    callback(err,null);
                    return;
                }
                if (isJSON && result !== null) {
                    callback(null, JSON.parse(result));
                    return;
                }
                callback(null, result);
            });
        });
    };
    this.ttl = function (key, ttl, callback) {
        redis.getClient(function (client, done) {
            client.expire((prefix) ? prefix + key : key, ttl, function (err, result) {
                done();
                callback(err, result);
            });
        });
    };
};