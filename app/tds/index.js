var express = require('express'),
    logger = require('./logger'),
    cfg = require('../../cfg/tds'),
    app = express();
app.enable('trust proxy');
app.disable('x-powered-by');
if (cfg.pmx) {
    var pmx = require('pmx');
    var meter = pmx.probe().meter({
        name: 'general req/sec',
        samples: 1,
        timeframe: 60
    });
    app.use(function (req, res, next) {
        meter.mark();
        next();
    });
    app.use(pmx.expressErrorHandler());
}
app.use(function (err, req, res, next) {
    if (res.statusCode < 400) res.statusCode = 500;
    err = Object.create(err);
    logger.error('express', req.hostname, req.headers['X-Real-IP'] || req.ip, req._parsedUrl.pathname, err, err.stack);
    return next(err);
});
function kernel(core) {
    app.use(function (req, res, next) {
        if (!req.hasOwnProperty("logger")) {
            req.logger = core.logger;
        }
        if (!req.hasOwnProperty("action")) {
            req.action = core.events;
        }
        if (!req.hasOwnProperty("orm")) {
            req.orm = core.models.orm;
        }
        if (!req.hasOwnProperty("odm")) {
            req.odm = core.models.odm;
        }
        if (!req.hasOwnProperty("redis")) {
            req.redis = core.clients.redis;
        }
        if (!req.hasOwnProperty("mariasql")) {
            req.mariasql = core.clients.mariasql;
        }
        next();
    });
    //app.use('/img/', express.static(core.cfg.tds.upload));
    app.use(require('./route'));
    app.use(function (req, res, next) {
        req.logger.warn('not found', req.method, req.url);
        res.status(404).end();
    });
    var conf = cfg.express;
    app.listen(conf.port, conf.ip, function () {
        console.log('worker:%d server listening on http://%s:%s ', process.pid, conf.ip, conf.port);
    });
}
require('./loader')(function (core) {
    core.cfg.tds = cfg;
    core.logger = logger;
    require('./company');
    require('./host');
    require('./isp');
    require('./geoip');
    require('./ban');
    require('./traffic');

    kernel(core);

});