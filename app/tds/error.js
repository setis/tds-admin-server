module.exports = {
    not_found_host: 1,
    not_found_company: 2,
    not_validation_isp: 3,
    not_found_country: 4,
    not_validation_ban_ip: 5,
    not_validation_ban_uniq: 6,
    not_found_isp:7,
    not_found_lang:8,
    not_found_screen:9,
    not_found_offset:10,
    not_found_referer:11,
    not_ie:12,
    not_found_ts:13,
    not_validation_ts:14,
    not_found_record_ts:15,
    not_expired_ts:16,
    favicon:17,
    not_validate_token:18

};