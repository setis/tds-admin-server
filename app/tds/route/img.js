var router = require('express').Router(),
    fs = require('fs'),
    logger = require('../logger'),
    dir = require('path').normalize(__dirname + '/../../../uploads') + '/',
    cfg = require(__dirname + '/../../../cfg/tds');

if (cfg.pmx) {
    var pmx = require('pmx');
    var meter = pmx.probe().meter({
        name: 'action:img req/sec',
        samples: 1,
        timeframe: 60
    });
    router.use(function (req, res, next) {
        meter.mark();
        next();
    });
}
router.use(function (req, res, next) {
    var mode =true;
    //switch (req.headers['x-accel-mode']) {
    //    case 'on':
    //    default :
    //        mode = true;
    //        break;
    //    case 'off':
    //        mode = false;
    //        break;
    //}
    logger.warn('img', mode, req.headers['x-accel-mode']);
    if (mode) {
        logger.debug('img:', req.data.banner.link);
        res.setHeader("X-Accel-Redirect", '/img/' + req.data.banner.link);
        res.end();
    } else {
        var path = dir + req.data.banner.link;
        var file = fs.createReadStream(path);
        file.pipe(res);
        logger.warn('img download', path);

    }
});
module.exports = router;