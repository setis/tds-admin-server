var router = require('express').Router(),
    cfg = require(__dirname + '/../../../cfg/tds'),
    utils = require('../utils'),
    uniq = require('../uniq'),
    db = require('../static');
if (cfg.pmx) {
    var pmx = require('pmx');
    var meter = pmx.probe().meter({
        name: 'action:drop req/sec',
        samples: 1,
        timeframe: 60
    });
    router.use(function (req, res, next) {
        meter.mark();
        next();
    });
}
router.use(utils.data());
router.use(function (req, res, next) {
    req.data.action = 'drop';
    next();
});

//router.use(function (req, res, next) {
//    req.action.emit('ban.ip.global.is', req.data.ip, function (err, result) {
//        if (err) {
//            req.logger.error('ban.ip.global.is ', req.data.ip, err);
//            res.status(404).end();
//            return;
//        }
//        if (result) {
//            res.status(404).end();
//            return;
//        }
//        next();
//    });
//});
router.use(function (req, res, next) {
    uniq.load(req.data.company.id, req.data.uniq_id, function (err, result) {
        if (err) {
            req.logger.error('route:drop uniq.load', err);
            if (cfg.pmx) {
                pmx.notify({msg: 'route:drop uniq.load', data: req.data.ip, action: req.data.action, error: err});
            }
            res.status(404).end();
            return;
        }
        uniq.save(result,
            {
                uniq: req.data.uniq_id,
                "time.drop": new Date()
            }, {
                "$set": {"time.drop": new Date()}
            },
            function (err) {
                res.status(404).end();
                if (err) {
                    req.logger.error('route:drop uniq.save', err);
                    if (cfg.pmx) {
                        pmx.notify({
                            msg: 'route:drop uniq.save',
                            data: req.data.ip,
                            action: req.data.action,
                            error: err
                        });
                    }
                    return;
                }
                db(req, res);
            });
    });
});
module.exports = router;