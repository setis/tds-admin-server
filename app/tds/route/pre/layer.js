var router = require('express').Router(),
    bodyParser = require("body-parser"),
    cors = require('cors'),
    mustache = require('mustache'),
    url = require('url'),
    status = require('../../error'),
    utils = require('../../utils'),
    traffic = require('../../traffic'),
    token = require('../../token'),
    db = require('../../static'),
    cfg = require('../../../../cfg/tds'),
    uniq = require('../../uniq');
router.use(cors({
    origin: function (origin, callback) {
        callback(null, origin);
    },
    credentials: true,
    methods: ['GET', 'POST'],
    headers: ["Origin", "X-Requested-With", "Content-Type", "Accept"]
}));
router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());
router.use(require('express-useragent').express());
if (cfg.pmx) {
    var pmx = require('pmx');
    var meter = pmx.probe().meter({
        name: 'route:pre req/sec',
        samples: 1,
        timeframe: 60
    });
    router.use(function (req, res, next) {
        meter.mark();
        next();
    });
}
//router.use(function (req, res, next) {
//    req.action.emit('ban.ip.global.is', req.data.ip, function (err, result) {
//        if (err) {
//            req.logger.error('ban.ip.global.is ', req.data.ip, err);
//            res.status(500).end();
//            return;
//        }
//        if (result) {
//            res.status(404).end();
//            return;
//        }
//        next();
//    });
//});
router.use(utils.data());
router.use(utils.isp(function (err, req, res, next) {
    if (err) {
        req.logger.error('route:pre utils.isp', err);
        if (cfg.pmx) {

        }
    }
    req.logger.error('isp', err);
    res.status(500).end();

}));

router.use(function (req, res, next) {
    req.data.action = 'pre';
    req.data.ts = req.query.ts || null;
    req.data.screen = req.query.screen || null;
    req.data.token = req.query.token || null;
    req.data.rt = req.data.rt || req.query.rt || false;
    req.data.lang = req.query.lang || null;
    req.data.offset = req.query.offset || null;
    next();
});
router.use(function (req, res, next) {
    if (req.data.lang === null) {
        req.data.status = status.not_found_lang;
        req.logger.debug('route:pre status ', req.data.status);
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    if (req.data.screen === null) {
        req.data.status = status.not_found_screen;
        req.logger.debug('route:pre status ', req.data.status);
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    if (req.data.offset === null) {
        req.data.status = status.not_found_offset;
        req.logger.debug('route:pre status ', req.data.status);
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    if (req.data.token === null) {
        req.data.status = status.not_found_token;
        req.logger.debug('route:pre status ', req.data.status);
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    next();
});
router.use(function (req, res, next) {
    var data = token.decrypt(req.data.token);
    if (req.data.company_id !== data.company_id) {
        req.data.status = status.not_validate_token;
        req.logger.warn('route:pre status:not_validate_token company.id');
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    //if (req.data.uniq_id.toString() !== data.uniq_id.toString()) {
    //    req.data.status = status.not_validate_token;
    //    req.logger.warn('route:pre status:not_validate_token uniq.id');
    //    req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
    //    req.data.action = 'ban';
    //    db(req, res);
    //    res.status(404).end();
    //    return;
    //}
    if (req.data.company.front.id !== data.host_id) {
        req.data.status = status.not_validate_token;
        req.logger.warn('route:pre status:not_validate_token host_id');
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    //if ((new Date().getTime() - data.time) > 8234879845) {
    //    req.data.status = status.not_validate_token;
    //    req.logger.warn('route:pre status:not_validate_token time');
    //    req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
    //    req.data.action = 'ban';
    //    db(req, res);
    //    res.status(404).end();
    //    return;
    //}
    next();
});
router.use(function (req, res, next) {
    uniq.load(req.data.company.id, req.data.uniq_id, function (err, result) {
        if (err) {
            req.logger.error('route:pre uniq.load', err);
            if (cfg.pmx) {

            }
            res.status(500).end();
            return;
        }
        req.data.uniq = result;
        if (result.drop) {
            req.logger.debug('route:pre isDrop', result.drop);
            res.status(404).end();
            return;
        }
        next();
    });
});
router.use(utils.filter(function (err, req, res, next) {
    if (err) {
        req.logger.error('route:pre.layer utils.filter', err);
        if (cfg.pmx) {

        }
        res.status(500).end();
        return;
    }
    res.status(404).end();
}));

router.use(function (req, res, next) {
    traffic.wrapper.read(req.data, function (err, result) {
        if (err) {
            req.logger.error('route:pre traffic.read', err);
            if (cfg.pmx) {

            }
            res.status(500).end();
            return;
        }
        if (result === null) {
            req.data.status = status.not_found_record_ts;
            req.logger.warn('route:pre traffic.read not_found_record_ts collection:ts_%s ts:%s', req.data.company.id, req.data.ts, req.useragent.browser, req.useragent.version, err);
            if (cfg.pmx) {

            }
            res.status(500).end();
            return;
        }
//        if (!req.data.uniq_id.equals(result.uniq) || req.data.token !== result.token) {
        if (req.data.token !== result.token) {
            req.data.status = status.not_validation_ts;
//            req.logger.debug('action.out traffic.read not_validation_ts');
            req.logger.warn('route:pre traffic.read not_validation_ts token');
            if (cfg.pmx) {

            }
            res.status(500).end();
            return;
        }
//        if ((new Date().getTime() - result.create.getTime()) > 60000) {
//            req.data.status = errStatus.not_expired_ts;
//            req.logger.debug('action.out traffic.read not_expired_ts', req.data.ip, new Date().getTime() - result.create.getTime());
//            finish(req, res);
//            return;
//        }
//        req.data.links = (result.onShort) ? result.short : result.long;
        next();

    });
});
router.use(function (req, res, next) {
    utils.layer(req.data.company, function (err, js) {
        if (err) {
            req.logger.error('route:pre.layer utils.layer', err);
            if (cfg.pmx) {

            }
            res.status(500).end();
            return;
        }
        if (js === null) {
            res.status(500).end();
            return;
        }
        var url_success, url_fail, url_fiddler;
        url_success = utils.url.layer.success(req.data),
            url_fail = utils.url.layer.fail(req.data),
            url_fiddler = utils.url.layer.fiddler(req.data);
        var cfg = req.data.company.jslayer.short;
        cfg = {
            ttl: 90,
            mode: false
        };
        if (cfg.mode) {
            async.parallel({
                success: function (callback) {
                    short.add(url_success, cfg.ttl, function (err, uri) {
                        if (err) {
                            req.logger.error('jslayer success short.add', err);
                            callback(err, null);
                            return;
                        }
                        callback(err, '/' + uri);
                    });
                },
                fail: function (callback) {
                    short.add(url_fail, cfg.ttl, function (err, uri) {
                        if (err) {
                            req.logger.error('jslayer fail short.add', err);
                            callback(err, null);
                            return;
                        }
                        callback(err, '/' + uri);
                    });
                },
                fiddler: function (callback) {
                    short.add(url_fiddler, cfg.ttl, function (err, uri) {
                        if (err) {
                            req.logger.error('jslayer fiddler short.add', err);
                            callback(err, null);
                            return;
                        }
                        callback(err, '/' + uri);
                    });
                }
            }, function (err, result) {
                if (err) {
                    req.logger.error('jslayer short url service', err);
                    res.status(500).end();
                    return;
                }
                res.setHeader('Content-Type', 'text/javascript');
                res.end(mustache.render(js, result));
                next();
            });
        } else {
            res.setHeader('Content-Type', 'text/javascript');
            res.end(mustache.render(js, {
                success: url.parse(url_success).path,
                fail: url.parse(url_fail).path,
                fiddler: url.parse(url_fiddler).path
            }));
            next();
        }
    });
});
router.use(function (req, res, next) {
    uniq.save(req.data.uniq,
        {
            uniq: req.data.uniq_id,
            layer: {
                time: new Date(),
                try: 1
            }
        }, {
            "$inc": {
                "layer.try": 1
            },
            "$set": {
                "layer.time": new Date()
            }
        }, function (err) {
            if (err) {
                req.logger.error('route:pre uniq.save ', err);
                if (cfg.pmx) {
                    pmx.notify({
                        msg: 'route:pre uniq.save',
                        data: req.data.ip,
                        action: req.data.action,
                        error: err
                    });
                }
                return;
            }
            db(req, res);
        });
});
module.exports = router;