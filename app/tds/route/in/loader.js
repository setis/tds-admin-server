/**
 * Created by alex on 20.02.16.
 */
var router = require('express').Router(),
    mustache = require('mustache'),
    url = require("url"),
    utils = require('../../utils'),
    db = require('../../static'),
    status = require('../../error'),
    cfg = require('../../../../cfg/tds'),

    js = require('../../js').wrapper.js;
if (cfg.pmx) {
    var pmx = require('pmx');
    var meter = pmx.probe().meter({
        name: 'route:loader req/sec',
        samples: 1,
        timeframe: 60
    });
    router.use(function (req, res, next) {
        meter.mark();
        next();
    });
}
router.use(require('express-useragent').express());
router.use(utils.data());
router.use(function (req, res, next) {
    req.data.action = 'in';
    next();
});
router.use(function (req, res, next) {
    utils.banner(req.data.company, function (err, banner) {
        if (err) {
            req.logger.error('action.in.loader utils.banner', err);
            banner='';
        }
        res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.setHeader('Expires', '-1');
        res.setHeader('Pragma', 'no-cache');
        if (req.useragent.isIE) {
            res.setHeader('Content-Type', 'text/javascript');
        } else {
            res.setHeader('Content-Type', 'application/javascript');
        }
        var el = req.data.el || 'asd_'+ new Date().getTime();
        res.end(js(req.useragent.isIE, {
            url: url.format({
                protocol: 'http:',
                hostname: req.data.host,
                pathname: req.data.company.path.front.script[0] || req._parsedUrl.pathname,
                query: {
                    wrapper: false,
                    el:el
                }
            }),
            el: el,
            banner: banner
        }));
    });

});
module.exports = router;