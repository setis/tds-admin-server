/**
 * Created by alex on 20.02.16.
 */
var router = require('express').Router(),
    mustache = require('mustache'),
    url = require("url"),
    utils = require('../../utils'),
    status = require('../../error'),
    token = require('../../token'),
    js = require('../../js').js,
    uniq = require('../../uniq'),
    db = require('../../static'),
    cfg = require('../../../../cfg/tds');
if (cfg.pmx) {
    var pmx = require('pmx');
    var meter = pmx.probe().meter({
        name: 'route:script req/sec',
        samples: 1,
        timeframe: 60
    });
    router.use(function (req, res, next) {
        meter.mark();
        next();
    });
}
router.use(require('express-useragent').express());
function rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
router.use(utils.data());
router.use(function (req, res, next) {
    req.data.el = req.query.el || null;
    req.data.action = 'in';
    next();
});
router.use(function (req, res, next) {
    console.log(req.data.company.id, req.data.uniq_id);
    uniq.load(req.data.company.id, req.data.uniq_id, function (err, result) {
        if (err) {
            req.logger.error('route:script uniq.load', err);
            pmx.notify({
                msg: 'route:script uniq.load',
                data: {company: req.data.company.id, uniq: req.data.uniq_id.toString()},
                action: req.data.action,
                error: err
            });
        } else {
            req.data.uniq = result;
        }
        next();
    });
});
router.use(function (req, res, next) {
    if (req.data.uniq === undefined || !req.data.uniq.in) {
        next();
        return;
    }
    db(req, res);
    uniq.save(req.data.uniq,
        {uniq: req.data.uniq_id, "time.in": new Date()},
        {$set: {"time.in": new Date()}},
        function (err) {
            if (err) {
                req.logger.error('route:script uniq.save', err);
                pmx.notify({
                    msg: 'route:script uniq.save',
                    data: {company: req.data.company.id, uniq: req.data.uniq_id.toString()},
                    action: req.data.action,
                    error: err
                });
            }
            next();
        });
});
router.use(function (req, res, next) {
    utils.banner(req.data.company, function (err, banner) {
        if (err) {
            banner = '';
        }
        res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.setHeader('Expires', '-1');
        res.setHeader('Pragma', 'no-cache');
        if (req.useragent.isIE) {
            res.setHeader('Content-Type', 'text/javascript');
        } else {
            res.setHeader('Content-Type', 'application/javascript');
        }
        var paths = req.data.company.path.front.rotation;
        var cfg = JSON.stringify({
            el: req.data.el || 'asd_' + new Date().getTime(),
            api: req.hostname + paths[rand(0, paths.length - 1)],
            token: encodeURIComponent(token.crypt(req.data.host_id, req.data.company_id, req.data.uniq_id).toString()),
            //time: req.data.company.ajax.reload * 1e3,
            time: 45,
            ssl: false,
            rt: false
        });
        if (req.data.company.tmt) {
            res.end(js.tmt({
                banner: banner,
                cfg: cfg
            }));
        } else {
            res.end(js.rotation(req.useragent.isIE,Math.floor(req.useragent.version), {
                banner: banner,
                cfg: cfg
            }));
        }
    });
});
module.exports = router;