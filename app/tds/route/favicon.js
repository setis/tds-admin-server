var router = require('express').Router(),
    utils = require('../utils'),
    status = require('../error'),
    db = require('../static'),
    cfg = require( '../../../cfg/tds');
if (cfg.pmx) {
    var pmx = require('pmx');
    var meter =  pmx.probe().meter({
        name: 'route:favicon req/sec',
        samples: 1,
        timeframe: 60
    });
    router.use(function (req, res, next) {
        meter.mark();
        next();
    });
}
router.use(utils.data());
router.use(function (req, res, next) {
    req.data.action = 'favicon';
    next();
});
router.use(function (req, res, next) {
    req.action.emit('ban.ip.global.ban', req.data.ip, status.favicon, function (err, result) {
        if (err) {
            req.logger.error('action.favicon ban.ip.global', req.data.ip, err);
            if(cfg.pmx){
                pmx.notify({msg: 'action.favicon ban.ip.global', data: req.data.ip, action: req.data.action, error: err});
            }
            return;
        }
        req.data.action = 'ban';
        req.data.status = status.favicon;
        db(req,res);
    });
    res.status(404).end();
});
module.exports = router;