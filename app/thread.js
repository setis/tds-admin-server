var cluster = require('cluster'), cpuCount = require('os').cpus().length;
var i = 1;
module.exports = function (worker, cb) {
    var count = cpuCount;
    if (worker && cluster.isMaster) {
        if (worker !== 0 && typeof worker === "number") {
            count = worker;
        } else if (typeof worker === "string" && worker.toLowerCase() !== 'auto') {
            count = parseInt(worker);
        }
        for (var i =1 ; i <= count; i++) {
            cluster.fork();
        }
    } else {
        cb(process.pid);
    }
};