/**
 * Created by alex on 22.02.16.
 */
var company,
    redis,
    layer,
    shortUrl,
    script = require('path').normalize(__dirname + '/generator/BootStrap.php'),
    logger = require('../logger'),
    cfg = require('../../../cfg/cron.json').layer,
    crypto = require('crypto'),
    async = require('async');
require('fs').exists(script, function (status) {
    if (status) {
        logger.info('layer script:%s', script);
    } else {
        logger.error('layer not script:%s', script);
    }
});
function key(company, short_url_service) {
    var list = [company.id, company.front.id, company.layer.id];
    if (short_url_service) {
        //if (company.jslayer.short.mode) {
        list.push(short_url_service.id);
    }
    return 'l:' + list.join('-');
}
function parse(key) {
    var arr = key.replace('l:', '').split('-');
    return {
        company: parseInt(arr[0]),
        front: parseInt(arr[1]),
        layer: parseInt(arr[2]),
        short: (arr[3]) ? parseInt(arr[3]) : false
    };
}
function process(callback) {
    async.parallel({
        short_url: process_short_url,
        layer: process_layer
    }, callback);
}
function process_layer(callback) {
    company.find({layer: {"$exists": true}}, function (err, result) {
        if (err) {
            logger.error('layer company.find ', err);
            callback(err, null);
            return;
        }
        if (result.length === 0) {
            callback(null, null);
            return;
        }
        var companies = {}, keys = [];
        for (var i in result) {
            var k = key(result[i]);
            companies[k] = result[i];
            keys.push(k);
        }
        redis.getClient(function (client, done) {
            client.keys('l:*', function (err, result) {
                done();
                if (err) {
                    logger.error('layer redis.keys ', err);
                    return;
                }
                result = result.filter(function (key) {
                    return (!parse(key).short);
                });
                async.parallel([
                    function (callback) {
                        var insert = keys.filter(function (key) {
                            return (result.indexOf(key) === -1);
                        });
                        if (insert.length === 0) {
                            callback();
                            return;
                        }
                        var tasks = keys
                            .map(function (key) {
                                var company = companies[key];
                                if (company === undefined) {
                                    return;
                                }
                                return function (callback) {
                                    create(key, company, false, callback);
                                };
                            })
                            .filter(function (v) {
                                return (v !== undefined);
                            });
                        async.parallel(tasks, callback);
                    },
                    function (callback) {
                        var drop = result.filter(function (key) {
                            return (keys.indexOf(key) === -1);
                        });
                        if (drop.length === 0) {
                            callback();
                            return;
                        }
                        var tasks = drop.map(function (key) {
                            return function (callback) {
                                redis.getClient(function (client, done) {
                                    client.del(key, function (err, result) {
                                        done();
                                        callback(err, result);
                                        if (err) {
                                            logger.error('layer redis.del ', err);
                                        }
                                    });
                                });
                            }
                        });
                        async.parallel(tasks, callback);
                    },
                    //function (callback) {
                    //    var use = result.filter(function (key) {
                    //        return (keys.indexOf(key) !== -1);
                    //    });
                    //    if (use.length === 0) {
                    //        callback();
                    //        return;
                    //    }
                    //    var tasks = use
                    //        .map(function (key) {
                    //            var company = companies[key];
                    //            if (company === undefined) {
                    //                return;
                    //            }
                    //            return function (callback) {
                    //                redis.getClient(function (client, done) {
                    //                    client.scard(key, function (err, count) {
                    //                        done();
                    //                        if (err) {
                    //                            logger.error('layer redis.scard ', err);
                    //                            callback();
                    //                            return;
                    //                        }
                    //                        var size = company.jslayer.js.count || 20;
                    //                        logger.debug('layer.size', company.id, count !== size, count, size);
                    //                        if (count !== size) {
                    //                            logger.info('layer not size', company.id, key, size);
                    //                            create(key, company, false, callback);
                    //                        }
                    //                    });
                    //                });
                    //            }
                    //        })
                    //        .filter(function (v) {
                    //            return (v !== undefined);
                    //        });
                    //    async.parallel(tasks, callback);
                    //}
                ], callback);
            });
        });
    });
}
function process_short_url(callback) {
    shortUrl.findOne({use: true}, function (err, short_url_service) {
        if (err) {
            logger.error('layer short_url.findOne', err);
            callback(err, null);
            return;
        }
        if (short_url_service === null) {
            callback(null, null);
            return;
        }
        company.find({layer: {"$exists": true}}, function (err, result) {
            if (err) {
                logger.error('layer company.find ', err);
                callback(err, null);
                return;
            }
            if (result.length === 0) {
                callback(null, null);
                return;
            }
            var companies = {}, keys = [];
            for (var i in result) {
                var k = key(result[i], short_url_service);
                companies[k] = result[i];
                keys.push(k);
            }
            redis.getClient(function (client, done) {
                client.keys('l:*', function (err, result) {
                    done();
                    if (err) {
                        logger.error('layer redis.keys ', err);
                        return;
                    }
                    result = result.filter(function (key) {
                        return (parse(key).short);
                    });
                    async.parallel([
                        function (callback) {
                            var insert = keys.filter(function (key) {
                                return (result.indexOf(key) === -1);
                            });
                            if (insert.length === 0) {
                                callback();
                                return;
                            }
                            var tasks = keys
                                .map(function (key) {
                                    var company = companies[key];
                                    if (company === undefined) {
                                        return;
                                    }
                                    return function (callback) {
                                        create(key, company, short_url_service, callback);
                                    };
                                })
                                .filter(function (v) {
                                    return (v !== undefined);
                                });
                            async.parallel(tasks, callback);
                        },
                        function (callback) {
                            var drop = result.filter(function (key) {
                                return (keys.indexOf(key) === -1);
                            });
                            if (drop.length === 0) {
                                callback();
                                return;
                            }
                            var tasks = drop.map(function (key) {
                                return function (callback) {
                                    redis.getClient(function (client, done) {
                                        client.del(key, function (err, result) {
                                            done();
                                            callback(err, result);
                                            if (err) {
                                                logger.error('layer redis.del ', err);
                                            }
                                        });
                                    });
                                }
                            });
                            async.parallel(tasks, callback);
                        },
                        //function (callback) {
                        //    var use = result.filter(function (key) {
                        //        return (keys.indexOf(key) !== -1);
                        //    });
                        //    if (use.length === 0) {
                        //        callback();
                        //        return;
                        //    }
                        //    var tasks = use
                        //        .map(function (key) {
                        //            var company = companies[key];
                        //            if (company === undefined) {
                        //                return;
                        //            }
                        //            return function (callback) {
                        //                redis.getClient(function (client, done) {
                        //                    client.scard(key, function (err, count) {
                        //                        done();
                        //                        if (err) {
                        //                            logger.error('layer redis.scard ', err);
                        //                            callback();
                        //                            return;
                        //                        }
                        //                        var size = company.jslayer.js.count || 20;
                        //                        logger.debug('layer.size', company.id, count !== size, count, size);
                        //                        if (count !== size) {
                        //                            logger.info('layer not size', company.id, key, size);
                        //                            create(key, company, short_url_service, callback);
                        //                        }
                        //                    });
                        //                });
                        //            }
                        //        })
                        //        .filter(function (v) {
                        //            return (v !== undefined);
                        //        });
                        //    async.parallel(tasks, callback);
                        //}
                    ], callback);
                });
            });
        });
    });
}
function create(key, company, short_url_service, callback) {
    var store = {
        time: new Date(),
        company: {
            key: company._id,
            id: company.id
        },
        front: company.front,
        layer: company.layer,
        short: (short_url_service) ? {
            id: short_url_service.id,
            key: short_url_service._id,
            host: short_url_service.host
        } : false,
        key: key
    };
    var info = [];
    var options = {
        success: {
            host: (short_url_service) ? 'https://' + short_url_service : 'http://' + company.layer.host,
            uri: '{{{success}}}'
        },
        fiddler: {
            host: (short_url_service) ? 'https://' + short_url_service : 'http://' + company.layer.host,
            uri: '{{{fiddler}}}'
        },
        fail: {
            host: (short_url_service) ? 'https://' + short_url_service : 'http://' + company.layer.host,
            uri: '{{{fail}}}'
        }

    };
    var count = company.jslayer.js.count || 20, ttl = (company.jslayer.js.ttl) ? company.jslayer.js.ttl : 86400;
    var tasks = [];
    for (var i = 0; i < count; i++) {
        //tasks.push(function (callback) {
        //    generate(options, function (err, result) {
        //        info.push({
        //            hash: crypto.createHash('sha256').update(data, 'utf8').digest(),
        //            size: result.length
        //        });
        //        redis.getClient(function (client, done) {
        //            client.sadd(key, result, function (err) {
        //                done();
        //                callback(err);
        //                if (err) {
        //                    logger.error('cron.layer.create.generator.client.set', err);
        //                }
        //            });
        //        });
        //    });
        //});
        tasks.push(function (callback) {
            generator(options.success.host, options.success.uri, options.fail.host, options.fail.uri, options.fiddler.host, options.fiddler.uri, function (err, result) {
                info.push({
                    hash: crypto.createHash('sha256').update(result, 'utf8').digest(),
                    size: result.length
                });
                redis.getClient(function (client, done) {
                    client.sadd(key, result, function (err) {
                        done();
                        callback(err);
                        if (err) {
                            logger.error('cron.layer.create.generator.client.set', err);
                        }
                    });
                });
            });
        });
    }
    async.series(tasks, function (err) {
        async.parallel([
            function (callback) {
                redis.getClient(function (client, done) {
                    client.expire(key, ttl, function (err) {
                        done();
                        callback(err);
                        if (err) {
                            logger.error('cron.layer.create.generator.client.ttl', err);
                        }
                    });
                });
            },
            function (callback) {
                store.options = options;
                store.info = info;
                store.time = new Date();
                layer.create(store, function (err, result) {
                    logger.info('cron.layer.create.generator.layer.create', store.key);
                    if (err && err.code === 11000) {
                        layer.update(
                            {key: key},
                            store,
                            function (err) {
                                callback(err);
                                if (err) {
                                    logger.error('cron.layer.create.generator.layer.create', err);
                                }
                            });
                        return;
                    }
                    callback(err);
                });
            }
        ], callback);
    });
}

function generate(options, callback) {
    var php = require('uniter').createEngine('PHP');
    php.getStdout().on('data', function (text) {
        console.log(text);
    });
    php.execute('<?php require()";?>');
}
function generator(hostSuccess, success, hostFail, fail, hostFiddler, fiddler, cb) {
    logger.debug('php -f ' + script + ' ' + [hostSuccess, success, hostFail, fail, hostFiddler, fiddler].join(' '));
    require('child_process').exec('php -f ' + script + ' ' + [hostSuccess, success, hostFail, fail, hostFiddler, fiddler].join(' '), {maxBuffer: 1024 * 500}, function (err, stdout, stderr) {
        if (err) {
            cb(err, null);
            return;
        }
        if (stderr) {
            logger.error(stderr)
            cb(new Error(stderr), null);
            return;
        }
        cb(null, stdout);
    });
}
require('../loader')(function (core) {
    company = core.models.odm.company;
    layer = core.models.odm.layer;
    shortUrl = core.models.odm.shortUrl;
    redis = core.clients.redis;
});
exports.process = process;
exports.process_layer = process_layer;
exports.process_short_url = process_short_url;
exports.crypt = key;
exports.decrypt = parse;

