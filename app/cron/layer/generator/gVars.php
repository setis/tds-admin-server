<?php

class gVars {

    /**
     *
     * @var Array
     */
    public static $list;

    /**
     *
     * @var int
     */
    public static $size;

    /**
     *
     * @var array|int
     */
    public $value;
    /**
     *
     * @var int
     */
    public $count = 5;
    /**
     *
     * @var boolean
     */
    public $rand = false;

    public function __construct($rand=false) {
        self::createList();
        $this->value[] = -1;
        $this->rand = $rand;
    }

    public static function createList() {
        if (self::$list === null) {
            $list = range('a', 'z');
            array_map(function($value)use($list) {
                $list[] = $value;
            }, range('A', 'Z'));
            self::$size = count($list) - 1;
            self::$list = $list;
        }
    }

    public function reset() {
        $this->value = [-1];
    }

    public function run() {
        $size = count($this->value);
        $value = [];
        $this->value[$size - 1] ++;
        while (--$size >= 0) {
            $s = $this->value[$size];
            if (self::$size < $s) {
                $this->value[$size] = 0;
                if ($size === 0) {
                    $this->value[count($this->value)] = -1;
                } else {
                    $this->value[$size - 1] ++;
                }
            }
            $value[$size] = $this->value[$size];
        }
        return implode('', array_map(function($v) {
                    return self::$list[$v];
                }, $value));
    }

    public function rand() {
        $size = mt_rand(0, 4);
        $value = [];
        while ($size--) {
            $value[$size] = rand(0, self::$size);
        }
        return implode('', array_map(function($v) {
                    return self::$list[$v];
                }, $value));
    }

    public function __invoke() {
        return ($this->rand)?$this->rand():$this->run();
    }

}
