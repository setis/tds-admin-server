/**
 * Created by alex on 22.02.16.
 */
var logger = require('./logger');
function handler(func, options, name) {
    var self = this;
    this.func = func;
    this.options = options;
    this.start;
    this.end;
    this.handler = function () {
        self.start = new Date();
        self.func(self.wrapper);
    };
    this.id = setInterval(this.handler, options.interval * 1e3);
    this.time = function () {
        return (self.end.getTime() - self.start.getTime()) / 1000;
    };
    this.wrapper = function (err, result) {
        self.end = new Date();
        //console.log(name,err,result);
        //console.log((self.end.getTime() - self.start.getTime()), options.interval * 1e3, ((self.end.getTime() - self.start.getTime()) > (options.interval * 1e3)));
        if (err) {
            clearInterval(self.id);
            self.id = null;
            setTimeout(self.handler, self.options.timeout.error * 1e3);
            setInterval(self.handler, self.options.interval * 1e3);
            logger.error(self.options.msg.error, self.time(), err);
            return;
        }
        //if (result === null) {
        //    clearInterval(self.id);
        //    self.id = null;
        //    setTimeout(self.handler, self.options.timeout.not_found * 1e3);
        //    setInterval(self.handler, self.options.interval * 1e3);
        //    logger.info(self.options.msg.success, self.time());
        //    return;
        //}
        if ((self.end.getTime() - self.start.getTime()) > (options.interval * 1e3)) {
            clearInterval(self.id);
            self.id = null;
            self.handler();
            setInterval(self.handler, self.options.interval * 1e3);
            logger.info(self.options.msg.success, self.time());
            return;
        }
        logger.info(self.options.msg.success, self.time());
    };
    this.handler();
}
var options = {
    timeout: {
        error: 3,
        not_found: 15
    },
    interval: 60,
    msg: {
        error: "asdfasd",
        success: "asfdasd"
    }
};
module.exports = handler;