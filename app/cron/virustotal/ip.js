var queue = require('./pool').app;
var id = 0;
var cfg = {
    link: 'active',
    type: 'low',
    interval: 300,
    timeout: 1,
    time: {
        start: null,
        end: null

    }
};
var pool = [];
var db = function () {
    throw new Error('not error db');
};
function wrapper() {
    var time = cfg.time;
    var timeout = time.end - time.start - cfg.interval;
    if (timeout > 0) {
        setTimeout(check, timeout * 1e3);
    } else {
        check();
    }
}
function getIpReport(model, data, id) {
    queue.queue(function (virustotal, close) {
        virustotal.getIpReport(data, function (err, result) {
            close();
            if (err) {
                throw err;
            } else if (result === undefined) {
                return;
            }
            model.update({_id: id}, {'$set': {
                    virustotal: result.detected_urls
                }}, function (err) {
                if (err) {
                    throw err;
                }
            });

        });
    });
}

function check() {
    db(function (model, err, result) {
        if (err) {
            throw err;
        }
        if (result === null || result === undefined || (result instanceof Array && result.length === 0)) {
            return;
        }
        var i, start = result.splice(0, 1), end = result.splice(-1, 1);
        pool.push((function (model, data, id) {
            cfg.time.start = (new Date()).getTime();
            getIpReport(model, data, id);
        })(model, start.host, start._id));
        for (i in result) {
            pool.push((function (model, data, id) {
                getIpReport(model, data, id);
            })(model, result[i].host, result[i]._id));
        }
        pool.push((function (model, data, id) {
            cfg.time.end = (new Date()).getTime();
            getIpReport(model, data, id);
            setTimeout(function () {
                event.emit('end');
            }, cfg.timeout * 1e3);
        })(model, end.host, end._id));

    });
}
function loader() {
    if (pool.length > 0) {
        while (queue.free) {
            pool.splice(0, 1)();
        }
    }
}
function start() {
    if (!id) {
        check();
        id = setInterval(loader, 250);
    }
}
function stop() {
    if (id) {
        clearInterval(id);
        id = 0;
    }
}
module.exports = {
    cfg: cfg,
    db: db,
    start: start,
    stop: stop
};