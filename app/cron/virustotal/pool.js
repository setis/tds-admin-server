var request = {
    minute: 4,
    day: 5760,
    month: 178560
};
var api = {};
var core = {};
var pool = [];

function getLastDayOfMonth() {
    var d = new Date();
    var year = d.getYear(), month = d.getMonth();
    var date = new Date(year, month, 0, 23, 59, 59);
    return date.getTime();
}
function use(key, callback) {
    core[key].limit.minute.request--;
    core[key].limit.day.request--;
    core[key].limit.moth.request--;
    callback(api[key], function () {
        setTimeout(function () {
            core[key].limit.minute.time = (new Date()).getTime() + 60;
            core[key].limit.minute.request++;
            if (core[key].lock === false) {
                pool.push(key);
            }

        }, 60e3);
    });
}
function limit() {
    if (Object.keys(core).length === 0) {
        return;
    }
    var time, limit, key;
    for (key in core) {
        limit = core[key].limit.day;
        time = (new Date()).getTime() + 1;
        if (limit.time > time && limit.request <= 0) {
            core[key].lock = true;
        } else if (limit.time <= time) {
            core[key].lock = false;
            limit.time = (new Date()).setHours(23, 59, 59).getTime();
            limit.request = request.day;
        }
        limit = core[key].limit.moth;
        if (limit.time > time && limit.request <= 0) {
            core[key].lock = true;
        } else if (limit.time <= time) {
            core[key].lock = false;
            limit.time = getLastDayOfMonth();
            limit.request = request.day;
        }
    }
}
var counter = setInterval(limit, 60e3);
var app = {
    keys: function (callback) {
    },
    save: function (callback) {
        var result = [];
        for (var key in core) {
            result.push(core[key]);
        }
        callback(result);
    },
    count: function () {
        return pool.length;
    },
    free: function () {
        return (pool.length !== 0);
    },
    queue: function (callback) {
        if (pool.length === 0) {
            throw new Error('full pool');
        }
        var key = pool.splice(0, 1);
        use(key, callback);
    },
    load: function (result, callback) {
        if (result === undefined || result === null || (result instanceof  Array && result.legth === 0)) {
            var date = new Date(), minute = date.getTime() + 60, day = date.setHours(23, 59, 59).getTime(), month = getLastDayOfMonth();
            for (var key in core) {
                core[key].limit = {
                    minute: {
                        time: minute,
                        request: request.minute
                    },
                    day: {
                        time: day,
                        request: request.day
                    },
                    month: {
                        time: month,
                        request: request.month
                    }
                };
            }
        } else {
            for (var i in result) {
                var obj = result[i];
                core[obj.key].limit = obj.limit;
                core[obj.key].id = obj._id;
            }
        }
        var api = require('./api.js');
        for (var key in core) {
            api[key] = api().setKey(key);
            var i = 4;
            while (i--) {
                pool.push(key);
            }
        }
        callback();

    }
};

var db = {
    keys: function (callback) {
        throw new Error('not function db.keys');
    },
    save: function (key, callback) {
        throw new Error('not function db.save');
    },
    load: function (callback) {
        throw new Error('not function db.load');
    }
};
function init(callback) {
    db.keys(function (err, keys) {
        if (err) {
            throw err;
            return;
        }
        for (var i in keys) {
            core[keys[i]] = {};
        }
        db.list(function (err, result) {
            if (err) {
                throw err;
                return;
            }
            app.list(result, function () {
                callback({
                    db: db,
                    app: app
                });
            });
        });
    });
}
;
module.exports = {
    db: db,
    app: app,
    init: init
};