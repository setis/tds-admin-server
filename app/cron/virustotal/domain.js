var queue = require('./pool').app;
//var type = {
//    low: ['getDomainReport'],
//    normal: ['getDomainReport', 'getUrlReport'],
//    hight: ['getDomainReport', 'scanUrl', 'getUrlReport']
//};
var id = 0;
var cfg = {
    link: 'active',
    type: 'low',
    interval: 300,
    timeout: 5,
    time: {
        start: null,
        end: null

    }
};
var event = require('events').EventEmitter;
var pool = [];
var db = {
    active: function (callback) {
        throw  new Error('not function db.active');
        callback(model, err, result);
    },
    all: function (callback) {
        throw  new Error('not function db.all');
        callback(model, err, result);
    }
};
event.on('handler.getDomainReport', function (model, data, id) {
    switch (cfg.type) {
        case 'normal':
            pool.push((function (model, data, id) {
                getUrlReport(model, data, id);
            })(model, data, id));
            break;
        case 'hight':
            pool.push((function (model, data, id) {
                scanUrl(model, data, id);
            })(model, data, id));
            break;
    }
});
event.on('handler.scanUrl', function (model, data, id) {
    switch (cfg.type) {
        case 'hight':
            pool.push((function (model, data, id) {
                getUrlReport(model, data, id);
            })(model, data, id));
            break;
    }
});
event.on('end', function () {
    var time = cfg.time;
    var timeout = time.end - time.start - cfg.interval;
    if (timeout > 0) {
        setTimeout(check, timeout * 1e3);
    } else {
        check();
    }
});
function getDomainReport(model, data, id) {
    queue.queue(function (virustotal, close) {
        virustotal.getDomainReport(data, function (err, result) {
            close();
            if (err) {
                throw err;
            } else if (result === undefined) {
                return;
            }
            for (var i in result.detected_urls) {
                event.emit('handler.getDomainReport', model, result.detected_urls[i], id);
            }
            model.update({_id: id}, {'$set': {
                    virustotal: {
                        domain: {
                            urls: result.detected_urls,
                            time: new Date()
                        }
                    }
                }}, function (err) {
                if (err) {
                    throw err;
                }
            });
        });
    });
}
function scanUrl(model, data, id, callback) {
    queue.queue(function (virustotal, close) {
        virustotal.scanUrl(data, function (err, result) {
            close();
            if (err) {
                throw err;
            } else if (result === undefined) {
                return;
            }
            var data = {};
            data[result.resource] = result;
            event.emit('handler.scanUrl', model, result.resource, id);
            model.update({_id: id}, {'$set': {
                    virustotal: {
                        queue: data
                    }
                }}, function (err) {
                if (err) {
                    throw err;
                }
            });

        });
    });
}
function getUrlReport(model, data, id) {
    queue.queue(function (virustotal, close) {
        virustotal.getUrlReport(data, function (err, result) {
            close();
            if (err) {
                throw err;
            } else if (result === undefined) {
                return;
            }
            var data = {};
            data[result.resource] = result;
            model.update({_id: id}, {'$set': {
                    virustotal: {
                        url: data
                    }
                }}, function (err) {
                if (err) {
                    throw err;
                }
            });

        });
    });
}

function check() {
    db[cfg.link](function (model, err, result) {
        if (err) {
            throw err;
        }
        if (result === null || result === undefined || (result instanceof Array && result.length === 0)) {
            return;
        }
        var i, start = result.splice(0, 1), end = result.splice(-1, 1);
        pool.push((function (model, data, id) {
            cfg.time.start = (new Date()).getTime();
            getDomainReport(model, data, id);
        })(model, start.host, start._id));
        for (i in result) {
            pool.push((function (model, data, id) {
                getDomainReport(model, data, id);
            })(model, result[i].host, result[i]._id));
        }
        pool.push((function (model, data, id) {
            cfg.time.end = (new Date()).getTime();
            getDomainReport(model, data, id);
            setTimeout(function () {
                event.emit('end');
            }, cfg.timeout * 1e3);
        })(model, end.host, end._id));

    });
}
function loader() {
    if (pool.length > 0) {
        while (queue.free) {
            pool.splice(0, 1)();
        }
    }
}
function start() {
    if (!id) {
        check();
        id = setInterval(loader, 250);
    }
}
function stop() {
    if (id) {
        clearInterval(id);
        id = 0;
    }
}
module.exports = {
    cfg: cfg,
    db: db,
    start: start,
    stop: stop
};