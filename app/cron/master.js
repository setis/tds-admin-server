/**
 * Created by alex on 22.02.16.
 */
var logger = require('./logger'), loading = require('./loading');
require('./loader')(function (core) {

    var avcheck = new loading(require('./avcheck').process, {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 90,
        msg: {
            error: "avcheck time:%ss",
            success: "avcheck time:%ss"
        }
    }, 'avcheck');
    var traffic_global = new loading(require('./clear/traffic.global'), {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 90,
        msg: {
            error: "traffic.global time:%ss",
            success: "traffic.global time:%ss"
        }
    }, "traffic_global");
    var traffic_local = new loading(require('./clear/traffic.local'), {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 90,
        msg: {
            error: "traffic.local time:%ss",
            success: "traffic.local time:%ss"
        }
    }, 'traffic_local');
    var uniq_global = new loading(require('./clear/uniq.global'), {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 90,
        msg: {
            error: "uniq.global time:%ss",
            success: "uniq.global time:%ss"
        }
    }, 'uniq_global');
    var uniq_local = new loading(require('./clear/uniq.local'), {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 90,
        msg: {
            error: "uniq.local time:%ss",
            success: "uniq.local time:%ss"
        }
    }, 'uniq_local');
    var dynamic_domain = new loading(require('./dynamic/domain').process, {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 90,
        msg: {
            error: "dynamic.domain time:%ss",
            success: "dynamic.domain time:%ss"
        }
    }, 'dynamic_domain');
    var dynamic_exploit = new loading(require('./dynamic/exploit').process, {
        timeout: {
            error: 120,
            not_found: 60
        },
        interval: 120,
        msg: {
            error: "dynamic.exploit time:%ss",
            success: "dynamic.exploit time:%ss"
        }
    }, 'dynamic_exploit');
    var dynamic_short_url = new loading(require('./dynamic/short_url'), {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 90,
        msg: {
            error: "dynamic.short_url time:%ss",
            success: "dynamic.short_url time:%ss"
        }
    }, 'dynamic_short_url');
    var static_create = new loading(require('./static/create').process, {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 30,
        msg: {
            error: "static.create time:%ss",
            success: "static.create time:%ss"
        }
    }, 'static_create');

    var company_path = new loading(require('./company/path').process, {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 90,
        msg: {
            error: "company.path time:%ss",
            success: "company.path time:%ss"
        }
    }, 'company_path');

    var check_domain = new loading(require('./check/domain'), {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 90,
        msg: {
            error: "check.domain time:%ss",
            success: "check.domain time:%ss"
        }
    }, 'check_domain');
    var check_ip = new loading(require('./check/ip'), {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 90,
        msg: {
            error: "check.ip time:%ss",
            success: "check.ip time:%ss"
        }
    }, 'check_ip');
    //var static_counter = new loading(require('./static/counter').process, {
    //    timeout: {
    //        error: 30,
    //        not_found: 60
    //    },
    //    interval: 90,
    //    msg: {
    //        error: "static.counter time:%ss",
    //        success: "static.counter time:%ss"
    //    }
    //}, 'static_counter');
    //var check_server = new loading(require('./check/server'), {
    //    timeout: {
    //        error: 30,
    //        not_found: 60
    //    },
    //    interval: 90,
    //    msg: {
    //        error: "check.server time:%ss",
    //        success: "check.server time:%ss"
    //    }
    //}, 'check_server');
    var check_image = new loading(require('./check/image'), {
        timeout: {
            error: 3,
            not_found: 15
        },
        interval: 60,
        msg: {
            error: "check.image time:%ss",
            success: "check.image time:%ss"
        }
    }, 'check_image');
});

