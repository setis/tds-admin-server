/**
 * Created by alex on 23.02.16.
 */
var logger = require('./logger'), loading = require('./loading');
require('./loader')(function (core) {
    var banner = new loading(require('./company/banner').process, {
        timeout: {
            error: 3,
            not_found: 15
        },
        interval: 60,
        msg: {
            error: "company.banner time:%ss",
            success: "company.banner time:%ss"
        }
    },'banner');

    var layer = new loading(require('./layer').process, {
        timeout: {
            error: 3,
            not_found: 15
        },
        interval: 300,
        msg: {
            error: "layer time:%ss",
            success: "layer time:%ss"
        }
    },'layer');
    var check_image = new loading(require('./check/image'), {
        timeout: {
            error: 3,
            not_found: 15
        },
        interval: 60,
        msg: {
            error: "check.image time:%ss",
            success: "check.image time:%ss"
        }
    }, 'check_image');
});