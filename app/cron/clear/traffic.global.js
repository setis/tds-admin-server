/**
 * Created by alex on 12.02.16.
 */
var logger = require('../logger'),
    traffic;

require('../loader')(function (core) {
    traffic = core.models.odm.traffic;
});
module.exports = function (callback) {
    var time = new Date();
    time.setSeconds(-120);
    traffic.remove({time: {'$lt': time}}, function (err, result) {
        if (err) {
            logger.error('clear.traffic.global model.remove', err);
            callback(err, null);
            return;
        }
        callback(null, result.result);
    });
};