/**
 * Created by alex on 12.02.16.
 */
var logger = require('../logger'),
    async = require('async'),
    uniq,
    company;

require('../loader')(function (core) {
    uniq = core.models.odm.uniq;
    company = core.models.odm.company;
});
module.exports = function (callback) {
    var time = new Date();
    time.setHours(-24);
    company.find({}, 'id', function (err, companies) {
        if (err) {
            logger.error('clear.uniq.local company.find', err);
            callback(err, null);
            return;
        }
        if (companies.length === 0) {
            callback(null, null);
            return;
        }
        var tasks = companies.map(function (v) {
            return function (callback) {
                uniq.base(v.id)
                    .remove({
                        "$or": [
                            {"time.uniq": {'$lt': time}},
                            {"time.out": {'$lt': time}},
                            {"time.drop": {'$lt': time}}
                        ]
                    },
                    function (err, result) {
                        if (err) {
                            logger.error('clear.uniq.local uniq.remove company.id:%s', v.id, err);
                        }
                        callback(err, result.result);
                    });
            };
        });
        async.parallel(tasks, callback);
    });
};