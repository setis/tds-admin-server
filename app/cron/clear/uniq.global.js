/**
 * Created by alex on 12.02.16.
 */
var logger = require('../logger'),
    uniq;

require('../loader')(function (core) {
    uniq = core.models.odm.uniq;
});
module.exports = function (callback) {
    var time = new Date();
    time.setHours(-24);
    uniq.remove({
            "$or": [
                {"time.uniq": {'$lt': time}},
                {"time.out": {'$lt': time}},
                {"time.drop": {'$lt': time}}
            ]
        },
        function (err, result) {
            if (err) {
                logger.error('clear.uniq.global uniq.remove', err);
                callback(err, null);
                return;
            }
            callback(null, result.result);
        });
};