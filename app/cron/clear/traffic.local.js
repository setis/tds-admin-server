/**
 * Created by alex on 12.02.16.
 */
var logger = require('../logger'),
    async = require('async'),
    traffic,
    company;
require('../loader')(function (core) {
    traffic = core.models.odm.traffic;
    domain = core.models.odm.domain;
});
module.exports = function (callback) {
    var time = new Date();
    time.setSeconds(-120);
    domain.find({}, 'id', function (err, result) {
        if (err) {
            logger.error('clear.traffic.local company.find', err);
            callback(err, null);
            return;
        }
        if (result.length === 0) {
            callback(null, null);
            return;
        }
        var tasks = result.map(function (v) {
            return function (callback) {
                traffic.base(v.id)
                    .remove({time: {'$lt': time}}, function (err) {
                        logger.debug('traffic.local id:%s time:%s', v.id, time);
                        if (err) {
                            logger.error('clear.traffic.local model.remove comapny.id:' + v.id, err);
                        }
                        callback(err, v.id);
                    });
            };
        });
        async.parallel(tasks, callback);
    });
};