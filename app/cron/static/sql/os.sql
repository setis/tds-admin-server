CREATE TABLE `s_os` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `os` varchar(50) NULL,
  `in` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `pre` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `drop` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `ban` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `unban` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `uniq` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `out` int(6) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  INDEX IDX_s (company_id, os, date)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1;