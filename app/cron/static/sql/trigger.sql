CREATE 
	DEFINER = CURRENT_USER
TRIGGER `{{trigger}}`
	AFTER INSERT
	ON `{{table}}`
	FOR EACH ROW
BEGIN
DECLARE company_id int default {{company_id}};
DECLARE cdate date default CURRENT_DATE();
if NEW.browser IS NULL then
    if NEW.version IS NULL then
    SELECT count(id),s.id,s.in,s.out,s.pre,s.drop,s.ban,s.unban,s.uniq into @check,@id,@in,@out,@pre,@drop,@ban,@unban,@uniq FROM `s_browser` as s WHERE s.browser IS NULL AND s.version IS NULL AND s.date = cdate AND s.company_id = company_id LIMIT 1;
    else
    SELECT count(id),s.id,s.in,s.out,s.pre,s.drop,s.ban,s.unban,s.uniq into @check,@id,@in,@out,@pre,@drop,@ban,@unban,@uniq FROM `s_browser` as s WHERE s.browser IS NULL AND s.version = NEW.version AND s.date = cdate AND s.company_id = company_id LIMIT 1;
    end if;
else
    if NEW.version IS NULL then
    SELECT count(id),s.id,s.in,s.out,s.pre,s.drop,s.ban,s.unban,s.uniq into @check,@id,@in,@out,@pre,@drop,@ban,@unban,@uniq FROM `s_browser` as s WHERE s.browser = NEW.browser AND s.version IS NULL AND s.date = cdate AND s.company_id = company_id LIMIT 1;
    else
    SELECT count(id),s.id,s.in,s.out,s.pre,s.drop,s.ban,s.unban,s.uniq into @check,@id,@in,@out,@pre,@drop,@ban,@unban,@uniq FROM `s_browser` as s WHERE s.browser = NEW.browser AND s.version = NEW.version AND s.date = cdate AND s.company_id = company_id LIMIT 1;
    end if;
end if;
if @check = 0 then
    if(NEW.action = 1) then
       set @unban:= 1;
       set @ban:= 0;
    elseif(NEW.action = 0) then
       set @ban:= 1;
       set @unban:= 0;
    end if;
 if(NEW.type = 0) then
    insert into s_browser (`browser`,`version`,`in`,`ban`,`unban`,`company_id`,`date`) VALUES(NEW.browser,NEW.version,1,@ban,@unban, company_id,cdate);
  elseif(NEW.type = 1) then 
    if(NEW.uniq)then
    insert into s_browser (`browser`,`version`,`pre`,`uniq`,`ban`,`unban`,`company_id`,`date`) VALUES(NEW.browser,NEW.version,1,1,@ban,@unban, company_id,cdate);
    else
    insert into s_browser (`browser`,`version`,`pre`,`ban`,`unban`,`company_id`,`date`) VALUES(NEW.browser,NEW.version,1,@ban,@unban, company_id,cdate);
    end if;
  elseif(NEW.type = 2) then
        insert into s_browser (`browser`,`version`,`out`,`ban`,`unban`,`company_id`,`date`) VALUES(NEW.browser,NEW.version,1,@ban,@unban, company_id,cdate);
  elseif(NEW.type = 3) then
    insert into s_browser (`browser`,`version`,`drop`,`ban`,`unban`,`company_id`,`date`) VALUES(NEW.browser,NEW.version,1,@ban,@unban, company_id,cdate);
  end if;
else
    if(NEW.action = 1) then
       set @unban:= 1;
       set @ban:= 0;
    elseif(NEW.action = 0) then
       set @ban:= 1;
       set @unban:= 0;
    end if;
  if(NEW.type = 0) then
    set @in:=@in+1;
    UPDATE s_browser SET s_browser.in = @in ,s_browser.ban = @ban, s_browser.unban = @unban WHERE id =@id; 
  elseif(NEW.type = 1) then 
       if(NEW.uniq)then
        set @uniq:=@uniq+1;
        set @pre:=@pre+1;
        UPDATE s_browser SET s_browser.uniq = @uniq,s_browser.pre = @pre,s_browser.ban = @ban, s_browser.unban = @unban  WHERE id =@id; 
       else
        set @pre:=@pre+1;
        UPDATE s_browser SET s_browser.pre = @pre,s_browser.ban = @ban, s_browser.unban = @unban  WHERE id =@id; 
       end if;
  elseif(NEW.type = 2) then
    set @out:=@out+1;
    UPDATE s_browser SET s_browser.out = @out,s_browser.ban = @ban, s_browser.unban = @unban  WHERE id =@id; 
  elseif(NEW.type = 3) then
    set @drop:=@drop+1;
    UPDATE s_browser SET s_browser.drop = @drop,s_browser.ban = @ban, s_browser.unban = @unban  WHERE id =@id; 
    end if;
end if;
if NEW.os IS NULL then
SELECT count(id),s.id,s.in,s.out,s.pre,s.drop,s.ban,s.unban,s.uniq into @check,@id,@in,@out,@pre,@drop,@ban,@unban,@uniq  FROM `s_os` as s WHERE s.os IS NULL AND s.date = cdate AND s.company_id = company_id LIMIT 1;
else
SELECT count(id),s.id,s.in,s.out,s.pre,s.drop,s.ban,s.unban,s.uniq into @check,@id,@in,@out,@pre,@drop,@ban,@unban,@uniq  FROM `s_os` as s WHERE s.os = NEW.os AND s.date = cdate AND s.company_id = company_id LIMIT 1;
end if;
if @check = 0 then
    if(NEW.action = 1) then
       set @unban:= 1;
       set @ban:= 0;
    elseif(NEW.action = 0) then
       set @ban:= 1;
       set @unban:= 0;
    end if;
  if(NEW.type = 0) then
    insert into s_os (`os`,`in`,`company_id`,`date`) VALUES(NEW.os,1,company_id,cdate);
  elseif(NEW.type = 1) then 
    if(NEW.uniq)then
        insert into s_os (`os`,`uniq`,`pre`,`company_id`,`date`) VALUES(NEW.os,1,1,company_id,cdate);
    else
        insert into s_os (`os`,`pre`,`company_id`,`date`) VALUES(NEW.os,1,company_id,cdate);
    end if;
  elseif(NEW.type = 2) then
    insert into s_os (`os`,`out`,`company_id`,`date`) VALUES(NEW.os,1,company_id,cdate);
  elseif(NEW.type = 3) then
    insert into s_os (`os`,`drop`,`company_id`,`date`) VALUES(NEW.os,1,company_id,cdate);
  end if;
else
  if(NEW.type = 0) then
    set @in:=@in+1;
    UPDATE s_os SET s_os.in = @in  WHERE id =@id; 
  elseif(NEW.type = 1) then
    if(NEW.uniq)then
        set @uniq:=@uniq+1;
        set @pre:=@pre+1;
        UPDATE s_os SET s_os.pre = @pre, s_os.uniq=@uniq  WHERE id =@id; 
    else
        set @pre:=@pre+1;
        UPDATE s_os SET s_os.pre = @pre  WHERE id =@id; 
    end if;
  elseif(NEW.type = 2) then
    set @out:=@out+1;
    UPDATE s_os SET s_os.out = @out  WHERE id =@id; 
  elseif(NEW.type = 3) then
    set @drop:=@drop+1;
    UPDATE s_os SET s_os.drop = @drop  WHERE id =@id; 
  end if;
end if;
if NEW.country IS NULL then
SELECT count(id),s.id,s.in,s.out,s.pre,s.drop,s.ban,s.unban,s.uniq into @check,@id,@in,@out,@pre,@drop,@ban,@unban,@uniq  FROM `s_country` as s WHERE s.country IS NULL AND s.date = cdate AND s.company_id = company_id LIMIT 1;
else
SELECT count(id),s.id,s.in,s.out,s.pre,s.drop,s.ban,s.unban,s.uniq into @check,@id,@in,@out,@pre,@drop,@ban,@unban,@uniq  FROM `s_country` as s WHERE s.country = NEW.country AND s.date = cdate AND s.company_id = company_id LIMIT 1;
end if;
if @check = 0 then
    if(NEW.action = 1) then
       set @unban:= 1;
       set @ban:= 0;
    elseif(NEW.action = 0) then
       set @ban:= 1;
       set @unban:= 0;
    end if;
  if(NEW.type = 0) then
    insert into s_country (`country`,`in`,`company_id`,`date`) VALUES(NEW.country,1,company_id,cdate);
  elseif(NEW.type = 1) then 
    if(NEW.uniq)then
        insert into s_country (`country`,`pre`,`uniq`,`company_id`,`date`) VALUES(NEW.country,1,1,company_id,cdate);
    else
        insert into s_country (`country`,`pre`,`company_id`,`date`) VALUES(NEW.country,1,company_id,cdate);
    end if;
  elseif(NEW.type = 2) then
    insert into s_country (`country`,`out`,`company_id`,`date`) VALUES(NEW.country,1,company_id,cdate);
  elseif(NEW.type = 3) then
    insert into s_country (`country`,`drop`,`company_id`,`date`) VALUES(NEW.country,1,company_id,cdate);
  end if;
else
  if(NEW.type = 0) then
    set @in:=@in+1;
    UPDATE s_country SET s_country.in = @in  WHERE id =@id; 
  elseif(NEW.type = 1) then 
    if(NEW.uniq)then
        set @pre:=@pre+1;
        set @uniq:=@uniq+1;
        UPDATE s_country SET s_country.pre = @pre,s_country.uniq = @uniq  WHERE id =@id; 
    else
        set @pre:=@pre+1;
        UPDATE s_country SET s_country.pre = @pre  WHERE id =@id; 
    end if;
  elseif(NEW.type = 2) then
    set @out:=@out+1;
    UPDATE s_country SET s_country.out = @out  WHERE id =@id; 
  elseif(NEW.type = 3) then
    set @drop:=@drop+1;
    UPDATE s_country SET s_country.drop = @drop  WHERE id =@id; 
  end if;
end if;
SELECT count(id),s.id,s.in,s.out,s.pre,s.drop,s.ban,s.unban,s.uniq into @check,@id,@in,@out,@pre,@drop,@ban,@unban,@uniq  FROM `s_general` as s WHERE  s.date = cdate AND s.company_id = company_id LIMIT 1;
if @check = 0 then
    if(NEW.action = 1) then
       set @unban:= 1;
       set @ban:= 0;
    elseif(NEW.action = 0) then
       set @ban:= 1;
       set @unban:= 0;
    end if;
  if(NEW.type = 0) then
    insert into s_general (`in`,`company_id`,`date`) VALUES(1,company_id,cdate);
  elseif(NEW.type = 1) then 
    if(NEW.uniq)then
        insert into s_general (`uniq`,`pre`,`company_id`,`date`) VALUES(1,1,company_id,cdate);
    else
        insert into s_general (`pre`,`company_id`,`date`) VALUES(1,company_id,cdate);
    end if;
  elseif(NEW.type = 2) then
    insert into s_general (`out`,`company_id`,`date`) VALUES(1,company_id,cdate);
  elseif(NEW.type = 3) then
    insert into s_general (`drop`,`company_id`,`date`) VALUES(1,company_id,cdate);
  end if;
else
  if(NEW.type = 0) then
    set @in:=@in+1;
    UPDATE s_general SET s_general.in = @in  WHERE id =@id; 
  elseif(NEW.type = 1) then 
    if(NEW.uniq)then
        set @pre:=@pre+1;
        set @uniq:=@uniq+1;
        UPDATE s_general SET s_general.pre = @pre,s_general.uniq = @uniq  WHERE id =@id; 
    else
        set @pre:=@pre+1;
        UPDATE s_general SET s_general.pre = @pre  WHERE id =@id; 
    end if;
  elseif(NEW.type = 2) then
    set @out:=@out+1;
    UPDATE s_general SET s_general.out = @out  WHERE id =@id; 
  elseif(NEW.type = 3) then
    set @drop:=@drop+1;
    UPDATE s_general SET s_general.drop = @drop  WHERE id =@id; 
  elseif(NEW.type = 4) then
    set @ban:=@ban+1;
    UPDATE s_general SET s_general.ban = @ban  WHERE id =@id; 
  end if;
end if;
if NEW.referer_host IS NULL then
SELECT count(id),s.id,s.in,s.out,s.pre,s.drop,s.ban,s.unban,s.uniq into @check,@id,@in,@out,@pre,@drop,@ban,@unban,@uniq  FROM `s_referer` as s WHERE s.referer IS NULL AND s.date = cdate AND s.company_id = company_id LIMIT 1;
else
SELECT count(id),s.id,s.in,s.out,s.pre,s.drop,s.ban,s.unban,s.uniq into @check,@id,@in,@out,@pre,@drop,@ban,@unban,@uniq  FROM `s_referer` as s WHERE s.referer = NEW.referer_host AND s.date = cdate AND s.company_id = company_id LIMIT 1;
end if;
if @check = 0 then
    if(NEW.action = 1) then
       set @unban:= 1;
       set @ban:= 0;
    elseif(NEW.action = 0) then
       set @ban:= 1;
       set @unban:= 0;
    end if;
  if(NEW.type = 0) then
    insert into s_referer (`referer`,`in`,`company_id`,`date`) VALUES(NEW.referer_host,1,company_id,cdate);
  elseif(NEW.type = 1) then 
    if(NEW.uniq)then
        insert into s_referer (`referer`,`pre`,`uniq`,`company_id`,`date`) VALUES(NEW.referer_host,1,1,company_id,cdate);
    else
        insert into s_referer (`referer`,`pre`,`company_id`,`date`) VALUES(NEW.referer_host,1,company_id,cdate);
    end if;
  elseif(NEW.type = 2) then
    insert into s_referer (`referer`,`out`,`company_id`,`date`) VALUES(NEW.referer_host,1,company_id,cdate);
  elseif(NEW.type = 3) then
    insert into s_referer (`referer`,`drop`,`company_id`,`date`) VALUES(NEW.referer_host,1,company_id,cdate);
  end if;
else
  if(NEW.type = 0) then
    set @in:=@in+1;
    UPDATE s_referer SET s_referer.in = @in  WHERE id =@id; 
  elseif(NEW.type = 1) then 
    if(NEW.uniq)then
        set @pre:=@pre+1;
        set @uniq:=@uniq+1;
        UPDATE s_referer SET s_referer.pre = @pre,s_referer.uniq = @uniq  WHERE id =@id; 
    else
        set @pre:=@pre+1;
        UPDATE s_referer SET s_referer.pre = @pre  WHERE id =@id; 
    end if;
  elseif(NEW.type = 2) then
    set @out:=@out+1;
    UPDATE s_referer SET s_referer.out = @out  WHERE id =@id; 
  elseif(NEW.type = 3) then
    set @drop:=@drop+1;
    UPDATE s_referer SET s_referer.drop = @drop  WHERE id =@id;
  end if;
end if;
END
