CREATE TABLE `s_referer` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) NOT NULL DEFAULT 0,
  `date` date NOT NULL,
  `referer` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `in` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `pre` int(6) UNSIGNED NOT NULL  DEFAULT 0,
  `drop` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `ban` int(6) UNSIGNED NOT NULL DEFAULT 0,
`unban` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `uniq` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `out` int(6) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  INDEX IDX_s (company_id, date,referer)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1;
