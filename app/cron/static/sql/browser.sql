CREATE TABLE `s_browser` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `browser` varchar(255) NOT NULL,
  `version` int(5) NOT NULL,
  `in` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `pre` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `drop` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `ban` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `unban` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `uniq` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `out` int(6) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  INDEX IDX_s (company_id,browser,version, date)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1;
