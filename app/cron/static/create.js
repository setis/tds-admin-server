var mysql,
    sql = {},
    company,
    async = require('async'),
    dateFormat = require('dateformat'),
    logger = require('../logger');
function load() {
    var fs = require('fs'),
        list = ['browser', 'os', 'referer', 'country', 'general', 'browser_exploit', 'os_exploit', 'referer_exploit', 'country_exploit', 'query'],
        dir = __dirname + '/sql/';
    var start = new Date();
    var tasks = list.map(function (name) {
        var path = dir + name + '.sql';
        return function (callback) {
            fs.readFile(path, function (err, data) {
                if (err) {
                    logger.error('static.create not read file name:%s path:%s', name, path, err);
                } else {
                    sql[name] = data.toString();
                    logger.info('static.create read file name:%s path:%s', name, path);
                }
                callback(err);
            });
        };
    });
    async.parallel(tasks, function (err) {
        end = new Date();
        logger.info('static.create load time: %ss', (end.getTime() - start.getTime()) / 1000);
    });
}
load();
//function createTrigger(postfix, company_id) {
//    var sqlQuery = mustache.render(sql.trigger, {
//        table: 'q_' + postfix,
//        company_id: company_id,
//        trigger: 't_' + postfix
//    });
//    mysql.query(sqlQuery, function (err, result) {
//        if (err) {
//            console.error('not create trigger:%s %s', 't_' + postfix, err);
//            return;
//        }
//        console.log('create trigger:%s ', 't_' + postfix);
//
//    });
//}
function table_static(callback) {
    var list = ['browser', 'os', 'referer', 'country', 'general', 'browser_exploit', 'os_exploit', 'referer_exploit', 'country_exploit'];
    var tasks = list.map(function (name) {
        var table = 's_' + name;
        return function (callback) {
            mysql.query("SHOW TABLES LIKE '" + table + "'", function (err, result) {
                if (err) {
                    logger.error('static.create table_static table:%s', table, err);
                    callback(err);
                    return;
                }
                if (result.rows.length) {
                    callback(null, null);
                    return;
                }
                mysql.query(sql[name], function (err, result) {
                    if (err) {
                        if (err.code !== 1050) {
                            callback(null, name);
                            return
                        }
                        logger.error('static.create tables_static create name:%s table:%s', name, table, err);
                        callback(err, null);
                        return;
                    }
                    logger.info('static.create tables_static create name:%s table:%s', name, table);
                    callback(null, name);
                });
            });
        };
    });
    async.parallel(tasks, callback);
}
function table_dynamic(date, callback) {
    if (date === undefined) {
        date = new Date();
    }
    if (typeof date === 'number') {
        var date2 = new Date();
        date2.setTime(date);
        date = date2;
    }
    var prefix = 'q_', postfix = dateFormat(date, "_yy_mm_dd");
    company.find({}, 'id', function (err, result) {
        if (err) {
            logger.error('static.create table_dynamic model.find', err);
            callback(err, null);
            return;
        }
        if (result.length === 0) {
            callback(null, null);
            return;
        }
        var tasks = result.map(function (doc) {
            var table = prefix + doc._doc.id + postfix, company = doc._doc.id;
            return function (callback) {
                mysql.query("SHOW TABLES LIKE '" + table + "'", function (err, result) {
                    if (err) {
                        logger.error('static.create table_dynamic table:%s', table, err);
                        callback(err);
                        return;
                    }
                    if (result.rows.length) {
                        callback(null, null);
                        return;
                    }
                    mysql.query(sql.query.replace('{table}', table), function (err, result) {
                        if (err) {
                            if (err.code !== 1050) {
                                callback(null, table);
                                return
                            }
                            logger.error('static.create tables_dynamic create company:%s table:%s', company, table, err);
                            callback(err, null);
                            return;
                        }
                        logger.info('static.create tables_dynamic create company:%s table:%s', company, table);
                        callback(null, table);
                    });
                });
            };

        });
        async.parallel(tasks, callback);
    });
}
function process(callback) {
    async.parallel([
        table_static,
        function (callback) {
            table_dynamic(new Date(), callback);
        },
        function (callback) {
            table_dynamic((new Date()).setHours(24), callback);
        }
    ], callback);
}
exports.process = process;
exports.static = table_static;
exports.dynamic = table_dynamic;
require('../loader')(function (core) {
    company = core.models.odm.company;
    mysql = core.clients.mariasql;
});