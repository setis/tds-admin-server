/**
 * Created by alex on 06.01.16.
 */
var async = require('async'),
    dateFormat = require('dateformat'),
    moment = require('moment'),
    logger = require('../logger'),
    sql,
    company,
    counter,
    limit = 1000;
function key(date) {
    if (date === undefined) {
        date = new Date();
    } else if (typeof date === 'number') {
        var date2 = new Date();
        date2.setTime(date);
        date = date2;
    }
    return dateFormat(date, "yy_mm_dd");
}
function process(callback) {
    company.find({}, 'id', function (err, result) {
        if (err) {
            logger.error('static.counter company.find', err);
            callback(err, null);
            return;
        }
        if (result.length === 0) {
            callback(null, null);
            return;
        }
        var current = result.map(function (doc) {
            var date = new Date(), _key = key(date), table = ['q', doc.id, _key].join('_');
            var data = {company: doc.id, table: table};
            return function (callback) {
                isTable(table, function (err, status) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (!status) {
                        callback(null, null);
                        return;
                    }
                    counter.findOne(data, function (err, result) {
                        if (err) {
                            logger.error('static.counter counter.findOne', err);
                            return;
                        }
                        if (result !== null) {
                            query(result, date, callback);
                            return;
                        }
                        counter.create(data, function (err, result) {
                            if (err) {
                                logger.error('static.counter', err);
                                callback(err, null);
                                return;
                            }
                            logger.debug(date);
                            query(result, date, callback);
                        });
                    });
                });
            };
        });
        var prev = result.map(function (doc) {
            var date = new Date();
            date.setHours(-24);
            var _key = key(date), table = ['q', doc.id, _key].join('_');
            var data = {company: doc.id, table: table};
            return function (callback) {
                isTable(table, function (err, status) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (!status) {
                        callback(null, null);
                        return;
                    }
                    counter.findOne(data, function (err, result) {
                        if (err) {
                            logger.error('static.counter counter.findOne', err);
                            return;
                        }
                        if (result !== null) {
                            query(result, date, callback);
                            return;
                        }
                        counter.create(data, function (err, result) {
                            if (err) {
                                logger.error('static.counter', err);
                                callback(err, null);
                                return;
                            }
                            query(result, date, callback);
                        });
                    });
                });
            };
        });
        async.parallel({
            //prev: function (callback) {
            //    async.series(prev, callback);
            //},
            current: function (callback) {
                async.series(current, callback);
            }
        }, callback);
    });
}

function isTable(table, callback) {
    sql.query("SHOW TABLES LIKE '" + table + "'", function (err, result) {
        if (err) {
            logger.error('static.create isTable table:%s', table, err);
            callback(err);
            return;
        }
        //logger.debug('isTable:%s', table, (result.rows.length === 0) ? false : true);
        if (result.rows.length === 0) {
            callback(null, false);
            return;
        }
        callback(null, true);
        return;
    });
}

function query(data, date, callback) {
    var cdate = moment(date).format('YYYY-MM-DD'), company_id = data.company, table = data.table;
    sql.query("SELECT * FROM " + data.table + " s  WHERE s.id > :id LIMIT " + limit + ";", {
        id: data.id || 0,
    }, function (err, result) {
        if (err) {
            logger.error('static.counter table:%s code:%s sql:%s', table, err.code, result.query._parent._query);
            callback(err, null);
            return;
        }
        logger.debug('result:', result.rows.length);
        if (result.rows.length === 0) {
            callback(null, null);
            return;
        }
        var country = {},
            os = {},
            referer = {},
            browser = {},
            general = {
                in: 0,
                pre: 0,
                uniq: 0,
                drop: 0,
                out: 0,
                ban: 0,
                unban: 0
            };
        result.rows.map(function (row) {
            if (country[row.country] === undefined) {
                country[row.country] = {
                    in: 0,
                    pre: 0,
                    uniq: 0,
                    drop: 0,
                    out: 0,
                    ban: 0,
                    unban: 0
                };
            }
            if (os[row.os] === undefined) {
                os[row.os] = {
                    in: 0,
                    pre: 0,
                    uniq: 0,
                    drop: 0,
                    out: 0,
                    ban: 0,
                    unban: 0
                };
            }
            if (referer[row.referer_host] === undefined) {
                referer[row.referer_host] = {
                    in: 0,
                    pre: 0,
                    uniq: 0,
                    drop: 0,
                    out: 0,
                    ban: 0,
                    unban: 0
                };
            }
            var key = row.browser + ':' + row.version;
            if (browser[key] === undefined) {
                browser[key] = {
                    in: 0,
                    pre: 0,
                    uniq: 0,
                    drop: 0,
                    out: 0,
                    ban: 0,
                    unban: 0,
                    browser: row.browser,
                    version: row.version
                };
            }
            var type;
            switch (Number(row.type)) {
                case 0:
                    type = 'in';
                    break;
                case 1:
                    type = 'pre';
                    break;
                case 2:
                    type = 'out';
                    break;
                case 3:
                    type = 'drop';
                    break;
                case 4:
                    type = 'ban';
                    break;
                case 5:
                    type = 'unban';
                case 6:
                    type = 'uniq';
                    break;
            }
            country[row.country][type]++;
            os[row.os][type]++;
            referer[row.referer_host][type]++;
            browser[key][type]++;
            general[type]++;
        });

        var last_id = Math.max.apply(Math, result.rows.map(function (row) {
            return row.id;
        }));

        async.parallel({
                general: function (callback) {
                    sql.query("SELECT * FROM `s_general` s WHERE s.company_id = :company AND s.date  = :date ", {
                            company: company_id,
                            date: cdate
                        }, function (err, result) {
                            if (err) {
                                logger.error('static.counter table:%s code:%s sql:%s', 's_general', err.code, result.query._parent._query);
                                callback(err, null);
                                return;
                            }
                            if (result.rows.length === 0) {
                                general.company_id = company_id;
                                general.date = cdate;
                                sql.query("insert into  `s_general` (`in`,`pre`,`out`,`drop`,`uniq`,`ban`,`unban`,`company_id`,`date`) VALUES(:in,:pre,:out,:drop,:uniq,:ban,:unban,:company_id,:date);", general, function (err, result) {
                                    if (err) {
                                        logger.error('static.counter table:%s msg:%s code:%s sql:%s', 's_general', err.message, err.code, result.query._parent._query);
                                    }
                                    callback(err, result);

                                });
                                return;
                            }
                            var list = ['in', 'pre', 'uniq', 'drop', 'out', 'ban', 'unban'];
                            var row = result.rows[0];
                            list.map(function (type) {
                                if (general[type]) {
                                    row[type] = Number(row[type]) + general[type];
                                }
                            });
                            sql.query("UPDATE `s_general` s SET s.out = :out,s.drop = :drop,s.in = :in,s.pre = :pre,s.uniq = :uniq, s.unban = :unban,s.ban =:ban WHERE id =:id; ", row, function (err, result) {
                                if (err) {
                                    logger.error('static.counter table:%s code:%s sql:%s', 's_general', err.code, result.query._parent._query);
                                }
                                callback(err, result);
                            });
                        }
                    )
                    ;
                },
                country: function (callback) {
                    sql.query("SELECT * FROM `s_country` s WHERE s.company_id = :company AND s.date  = :date ", {
                        company: company_id,
                        date: cdate
                    }, function (err, result) {
                        if (err) {
                            logger.error('static.counter table:%s code:%s sql:%s', 's_country', err.code, result.query._parent._query);
                            callback(err, null);
                            return;
                        }
                        var tasks = [];
                        if (result.rows.length === 0) {
                            async.forEachOf(country, function (data, country, callback) {
                                data.company_id = company_id;
                                data.date = cdate;
                                data.country = country;
                                tasks.push(function (callback) {
                                    sql.query("insert into  `s_country` (`country`,`in`,`pre`,`out`,`drop`,`uniq`,`ban`,`unban`,`company_id`,`date`) VALUES(:country,:in,:pre,:out,:drop,:uniq,:ban,:unban,:company_id,:date);", data, function (err, result) {
                                        if (err) {
                                            logger.error('static.counter table:%s code:%s sql:%s', 's_country', err.code, result.query._parent._query);
                                        }
                                        callback(err, result);

                                    });
                                });
                                callback();
                            }, function (err) {
                                if (err) {
                                    logger.error(err);
                                }
                                async.parallel(tasks, callback);
                            });
                            return;
                        }
                        var list = ['in', 'pre', 'uniq', 'drop', 'out', 'ban', 'unban'];
                        var tasks = result.rows
                            .filter(function (row) {
                                return (country[row.country]);
                            })
                            .map(function (row) {
                                var val = country[row.country];
                                list.map(function (type) {
                                    if (val) {
                                        row[type] = Number(row[type]) + val[type];
                                    }
                                });
                                delete country[row.country];
                                return function (callback) {
                                    sql.query("UPDATE `s_country` s SET s.out = :out,s.drop = :drop,s.in = :in,s.pre = :pre,s.uniq = :uniq, s.unban = :unban,s.ban =:ban WHERE id =:id; ", row, function (err, result) {
                                        if (err) {
                                            logger.error('static.counter table:%s code:%s sql:%s', 's_country', err.code, result.query._parent._query);
                                        }
                                        callback(err, result);
                                    });
                                };
                            });
                        async.forEachOf(country, function (value, country, callback) {
                            value.company_id = company_id;
                            value.date = cdate;
                            value.country = country;
                            tasks.push(function (callback) {
                                sql.query("insert into  `s_country` (`country`,`in`,`pre`,`out`,`drop`,`uniq`,`ban`,`unban`,`company_id`,`date`) VALUES(:country,:in,:pre,:out,:drop,:uniq,:ban,:unban,:company_id,:date);", value, function (err, result) {
                                    if (err) {
                                        logger.error('static.counter table:%s code:%s sql:%s', 's_country', err.code, result.query._parent._query);
                                    }
                                    callback(err, result);
                                });
                            });
                            callback();
                        }, function (err) {
                            async.parallel(tasks, callback);
                        });
                    });
                }

                ,
                os: function (callback) {
                    sql.query("SELECT * FROM `s_os` s WHERE s.company_id = :company AND s.date  = :date ", {
                        company: company_id,
                        date: cdate
                    }, function (err, result) {
                        if (err) {
                            logger.error('static.counter table:%s code:%s sql:%s', 's_os', err.code, result.query._parent._query);
                            callback(err, null);
                            return;
                        }
                        var tasks = [];
                        if (result.rows.length === 0) {
                            async.forEachOf(os, function (data, os, callback) {
                                data.company_id = company_id;
                                data.date = cdate;
                                data.os = os;
                                tasks.push(function (callback) {
                                    sql.query("insert into  `s_os` (`os`,`in`,`pre`,`out`,`drop`,`uniq`,`ban`,`unban`,`company_id`,`date`) VALUES(:os,:in,:pre,:out,:drop,:uniq,:ban,:unban,:company_id,:date);", data, function (err, result) {
                                        if (err) {
                                            logger.error('static.counter table:%s code:%s sql:%s', 's_os', err.code, result.query._parent._query);
                                        }
                                        callback(err, result);

                                    });
                                });
                                callback();
                            }, function (err) {
                                if (err) {
                                    logger.error(err);
                                }
                                async.parallel(tasks, callback);
                            });
                            return;
                        }
                        var list = ['in', 'pre', 'uniq', 'drop', 'out', 'ban', 'unban'];
                        var tasks = result.rows
                            .filter(function (row) {
                                return (os[row.os]);
                            })
                            .map(function (row) {
                                var val = os[row.os];
                                list.map(function (type) {
                                    if (val) {
                                        row[type] = Number(row[type]) + val[type];
                                    }
                                });
                                delete os[row.os];
                                return function (callback) {
                                    sql.query("UPDATE `s_os` s SET s.out = :out,s.drop = :drop,s.in = :in,s.pre = :pre,s.uniq = :uniq, s.unban = :unban,s.ban =:ban WHERE id =:id; ", row, function (err, result) {
                                        if (err) {
                                            logger.error('static.counter table:%s code:%s sql:%s', 's_os', err.code, result.query._parent._query);
                                        }
                                        callback(err, result);
                                    });
                                };
                            });
                        async.forEachOf(os, function (value, os, callback) {
                            value.company_id = company_id;
                            value.date = cdate;
                            value.os = os;
                            tasks.push(function (callback) {
                                sql.query("insert into  `s_os` (`os`,`in`,`pre`,`out`,`drop`,`uniq`,`ban`,`unban`,`company_id`,`date`) VALUES(:os,:in,:pre,:out,:drop,:uniq,:ban,:unban,:company_id,:date);", value, function (err, result) {
                                    if (err) {
                                        logger.error('static.counter table:%s code:%s sql:%s', 's_os', err.code, result.query._parent._query);
                                    }
                                    callback(err, result);
                                });
                            });
                            callback();
                        }, function (err) {
                            async.parallel(tasks, callback);
                        });
                    });
                }
                ,
                referer: function (callback) {
                    sql.query("SELECT * FROM `s_referer` s WHERE s.company_id = :company AND s.date  = :date ", {
                        company: company_id,
                        date: cdate
                    }, function (err, result) {
                        if (err) {
                            logger.error('static.counter table:%s code:%s sql:%s', 's_referer', err.code, result.query._parent._query);
                            callback(err, null);
                            return;
                        }
                        var tasks = [];
                        if (result.rows.length === 0) {
                            async.forEachOf(referer, function (data, referer, callback) {
                                data.company_id = company_id;
                                data.date = cdate;
                                data.referer = referer;
                                tasks.push(function (callback) {
                                    sql.query("insert into  `s_referer` (`referer`,`in`,`pre`,`out`,`drop`,`uniq`,`ban`,`unban`,`company_id`,`date`) VALUES(:referer,:in,:pre,:out,:drop,:uniq,:ban,:unban,:company_id,:date);", data, function (err, result) {
                                        if (err) {
                                            logger.error('static.counter table:%s code:%s sql:%s', 's_referer', err.code, result.query._parent._query, err.message);
                                        }
                                        callback(err, result);

                                    });
                                });
                                callback();
                            }, function (err) {
                                if (err) {
                                    logger.error(err);
                                }
                                async.parallel(tasks, callback);
                            });
                            return;
                        }
                        var list = ['in', 'pre', 'uniq', 'drop', 'out', 'ban', 'unban'];
                        var tasks = result.rows
                            .filter(function (row) {
                                return (referer[row.referer]);
                            })
                            .map(function (row) {
                                var val = referer[row.referer];
                                list.map(function (type) {
                                    if (val) {
                                        row[type] = Number(row[type]) + val[type];
                                    }
                                });
                                delete referer[row.referer];
                                return function (callback) {
                                    sql.query("UPDATE `s_referer` s SET s.out = :out,s.drop = :drop,s.in = :in,s.pre = :pre,s.uniq = :uniq, s.unban = :unban,s.ban =:ban WHERE id =:id; ", row, function (err, result) {
                                        if (err) {
                                            logger.error('static.counter table:%s code:%s sql:%s', 's_referer', err.code, result.query._parent._query, err.message);
                                        }
                                        callback(err, result);
                                    });
                                };
                            });
                        async.forEachOf(referer, function (value, referer, callback) {
                            value.company_id = company_id;
                            value.date = cdate;
                            value.referer = referer;
                            tasks.push(function (callback) {
                                sql.query("insert into  `s_referer` (`referer`,`in`,`pre`,`out`,`drop`,`uniq`,`ban`,`unban`,`company_id`,`date`) VALUES(:referer,:in,:pre,:out,:drop,:uniq,:ban,:unban,:company_id,:date);", value, function (err, result) {
                                    if (err) {
                                        logger.error('static.counter table:%s code:%s sql:%s', 's_referer', err.code, result.query._parent._query);
                                    }
                                    callback(err, result);
                                });
                            });
                            callback();
                        }, function (err) {
                            async.parallel(tasks, callback);
                        });
                    });
                }
                ,
                browser: function (callback) {
                    sql.query("SELECT * FROM `s_browser` s WHERE s.company_id = :company AND s.date  = :date ", {
                        company: company_id,
                        date: cdate
                    }, function (err, result) {
                        if (err) {
                            logger.error('static.counter table:%s code:%s sql:%s', 's_browser', err.code, result.query._parent._query);
                            callback(err, null);
                            return;
                        }
                        var tasks = [];
                        if (result.rows.length === 0) {
                            async.forEachOf(browser, function (data, browser, callback) {
                                data.company_id = company_id;
                                data.date = cdate;
                                tasks.push(function (callback) {
                                    sql.query("insert into  `s_browser` (`browser`,`version`,`in`,`pre`,`out`,`drop`,`uniq`,`ban`,`unban`,`company_id`,`date`) VALUES(:browser,:version,:in,:pre,:out,:drop,:uniq,:ban,:unban,:company_id,:date);", data, function (err, result) {
                                        if (err) {
                                            logger.error('static.counter table:%s code:%s sql:%s', 's_browser', err.code, result.query._parent._query);
                                        }
                                        callback(err, result);

                                    });
                                });
                                callback();
                            }, function (err) {
                                if (err) {
                                    logger.error(err);
                                }
                                async.parallel(tasks, callback);
                            });
                            return;
                        }
                        var list = ['in', 'pre', 'uniq', 'drop', 'out', 'ban', 'unban'];
                        var tasks = result.rows
                            .filter(function (row) {
                                return (browser[row.browser + ':' + row.version]);
                            })
                            .map(function (row) {
                                var key = row.browser + ':' + row.version;
                                var val = browser[key];
                                list.map(function (type) {
                                    if (val) {
                                        row[type] = Number(row[type]) + val[type];
                                    }
                                });
                                delete browser[key];
                                return function (callback) {
                                    sql.query("UPDATE `s_browser` s SET s.out = :out,s.drop = :drop,s.in = :in,s.pre = :pre,s.uniq = :uniq, s.unban = :unban,s.ban =:ban WHERE id =:id; ", row, function (err, result) {
                                        if (err) {
                                            logger.error('static.counter table:%s code:%s sql:%s', 's_browser', err.code, result.query._parent._query);
                                        }
                                        callback(err, result);
                                    });
                                };
                            });
                        async.forEachOf(browser, function (value, key, callback) {
                            value.company_id = company_id;
                            value.date = cdate;
                            tasks.push(function (callback) {
                                sql.query("insert into  `s_browser` (`browser`,`version`,`in`,`pre`,`out`,`drop`,`uniq`,`ban`,`unban`,`company_id`,`date`) VALUES(:browser,:version,:in,:pre,:out,:drop,:uniq,:ban,:unban,:company_id,:date);", value, function (err, result) {
                                    if (err) {
                                        logger.error('static.counter sql.insert table:s_browser', err);
                                    }
                                    callback(err, result);
                                });
                            });
                            callback();
                        }, function (err) {
                            async.parallel(tasks, callback);
                        });
                    });
                }
                ,

            }, function (err) {
                counter.update({_id: data._id}, {'$set': {id: last_id, time: new Date()}}, function (err) {
                    if (err) {
                        logger.error('static.counter counter-6', err);
                    }
                    callback(err, null);
                });
            }
        )
        ;

    });
}

exports.process = process;
exports.key = key;
exports.isTable = isTable;

require('../loader')(function (core) {
    sql = core.clients.mariasql;
    company = core.models.odm.company;
    counter = core.models.odm.counter;
    //var loading = require('../loading');
    //var static_counter = new loading(process, {
    //    timeout: {
    //        error: 30,
    //        not_found: 60
    //    },
    //    interval: 90,
    //    msg: {
    //        error: "static.counter time:%ss",
    //        success: "static.counter time:%ss"
    //    }
    //}, 'static_counter');
});
