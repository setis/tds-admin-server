/**
 * Created by alex on 27.02.16.
 */
var loading = require('../loading')
require('../loader')(function (core) {
    var static_counter = new loading(require('./counter').process, {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 90,
        msg: {
            error: "static.counter time:%ss",
            success: "static.counter time:%ss"
        }
    }, 'static_counter');
});