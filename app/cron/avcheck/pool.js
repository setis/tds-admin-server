/**
 * Created by alex on 10.02.16.
 */

var async = require('async'), util = require('util'), api = require('./api');
function Pool(cfg) {
    this.thread = cfg.thread || 6;
    this.timeout = cfg.timeout || 0;
    this.key = cfg.key;
    this.timer = {
        begin: null,
        end: null
    };
    var self = this;
    var pool = async.queue(function (task, callback) {
        console.log('queue', self.key, task.type, task.data);
        api[task.type](self.key, task.data, function (err, result) {
            if (err) {
                self.emit('error', err, task);
            } else {
                self.emit('success', result, task);
                task.cb(err, result);
            }
            self.emit('done', err, result, task);
            if (self.timeout > 0) {
                setTimeout(function () {
                    callback(err, result);
                }, self.timeout * 1e3);
            } else {
                callback(err, result);
            }
        });
    }, this.thread);
    pool.drain(function () {
        self.timer.end = (new Date()).getTime();
        //if (self.timer.begin !== null && (self.timer.end - self.timer.begin) < self.timeout) {
        //    pool.pause();
        //    setTimeout(pool.resume, self.timeout * 1e3);
        //}
        pool.pause();
        self.emit('process');
    });
    this.pool = pool;
    this.on('cicle', function () {
        self.timer.begin = (new Date()).getTime();
    });
    this.on('data', function (result) {
        pool.push(result);
    });
}

util.inherits(Pool, require('events').EventEmitter);
module.exports = Pool;