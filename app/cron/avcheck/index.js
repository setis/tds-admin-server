/**
 * Created by alex on 10.02.16.
 */

var domain, shortUrl, ip,
    logger = require('../logger'),
    pool = require('./pool'),
    cfg = require('../../../cfg/cron.json').avcheck,
    async = require('async');
var avcheck = new pool(cfg);
avcheck.on('success', function(result,task){
    logger.debug(result);
});
avcheck.on('error', function(err,task){
    logger.error(err.message);
});
avcheck.on('process', process);
function wrapper(model, v, field) {
    return {
        type: 'domain',
        data: v[field],
        cb: function (err, result) {
            var update;
            if (err) {
                update = {
                    '$set': {
                        "avcheck.error": err.message,
                        "avcheck.time":new Date()
                    }
                };
            } else {
                update = {
                    '$set': {
                        avcheck: {
                            time: new Date(),
                            error: null,
                            total: result.totalavs,
                            detect: result.detectavs,
                            link: result.link,
                            scan: result.result
                        }
                    }
                };
            }
            //console.log(v[field], update);
            model.update({_id: v._id}, update, function (err) {
                if (err) {
                    avcheck.emit('error', err, result._doc, {type: 'domain', data: v[field]});
                }
            });
        }
    };
}
function process(callback) {
    avcheck.pool.resume();
    async.parallel({
            domain: function (callback) {
                domain.find({}, function (err, result) {
                    //console.log('domain', err, result.length);
                    callback(err, result);
                });
            },
            shortUrl: function (callback) {
                shortUrl.find({}, function (err, result) {
                    //console.log('shortUrl', err, result.length);
                    callback(err, result);
                });
            },
            ip: function (callback) {
                ip.find({}, function (err, result) {
                    //console.log('ip', err, result.length);
                    callback(err, result);
                });
            }
        }, function (err, result) {
            avcheck.pool.pause();
            callback(err, result);
            if (result.shortUrl.length) {
                result.shortUrl.forEach(function (v) {
                    avcheck.emit('data', wrapper(shortUrl, v, 'host'));
                });
            }
            if (result.domain.length) {
                result.domain.forEach(function (v) {
                    avcheck.emit('data', wrapper(domain, v, 'host'));
                });
            }
            if (result.ip.length) {
                result.ip.forEach(function (v) {
                    avcheck.emit('data', wrapper(ip, v, 'ip'));
                });
            }
        }
    );
}
require('../loader')(function (core) {
    domain = core.models.odm.domain;
    shortUrl = core.models.odm.shortUrl;
    ip = core.models.odm.ip;
});
exports.kernel = avcheck;
exports.process = process;
