/**
 * Created by alex on 10.02.16.
 */
var request = require('request');
const server = 'https://avdetect.com/api/';
function domain(key, data, callback) {
    request.post({
        url: server,
        form: {
            api_key: key,
            check_type: 'domain',
            data: data
        }
    }, function (err, response, body) {
        if (err) {
            callback(err, null);
            return;
        }
        try {
            var result = JSON.parse(body);
        } catch (e) {
            callback(e, null);
            return;
        }
        if (result.status === 'ERROR') {
            callback(new Error(result.message + ' ' + data), null);
            return;
        }

        callback(null, result[0]);
    });
}
exports.domain = domain;