/**
 * Created by alex on 16.02.16.
 */
var model,
    logger = require('../logger'),
    async = require('async'),
    price = {
        alive: 1e5,
        avcheck: 1e3
    };
function weight(result) {
    var host = {}, arr = [], i, data, c, resource = {};
    for (i in result) {
        data = result[i];
        c = (data.avcheck.total * price.avcheck) - (data.avcheck.detect * price.avcheck)(data.alive.mode) ? +price.alive : -price.alive;
        host[c] = data.host;
        arr.push(c);
    }
    arr
        .sort(function (a, b) {
            return b - a;
        })
        .forEach(function (c) {
            resource.push({host: host[c], weigth: c});
        });
    return resource;
}
require('../loader')(function (core) {
    model = core.models.odm.shortUrl;
});
module.exports = function (callback) {
    model.find({}, function (err, result) {
        if (err) {
            logger.error('dynamic.short_url model.find', err);
            callback(err, null);
            return;
        }
        if (result.length === 0) {
            callback(null, null);
            return;
        }
        var weights = weight(result);
        var use = weights.shift();
        var tasks = weights.map(function (v) {
            return function (callback) {
                model.update({host: v.host}, {
                    $set: {
                        weigth: v.weigth,
                        time: new Date(),
                        use: false
                    }
                }, function (err, result) {
                    if (err) {
                        logger.error('dynamic.short_url model.update', {host: v.host, weigth: v.weigth}, err);
                        callback(err, null);
                        return;
                    }

                    callback(null, result.result);
                });
            };
        });
        tasks.unshift(function (callback) {
            model.update({host: v.host}, {$set: {weigth: v.weigth, time: new Date(), use: true}}, function (err) {
                if (err) {
                    logger.error('dynamic.short_url model.update', {host: v.host, weigth: v.weigth}, err);
                    callback(err, null);
                    return;
                }

                callback(null, result.result);
            });
        });
        async.parallel(tasks, callback);
    });
};