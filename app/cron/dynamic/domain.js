/**
 * Created by alex on 16.02.16.
 */
var domain,
    company,
    logger = require('../logger'),
    async = require('async');
function process(callback) {
    async.series({
        clean: clean,
        use: use,
        install: install,
        reinstall: reinstall
    }, callback);

}
function use(callback) {
    company.find({}, function (err, result) {
        if (err) {
            logger.error('avstat.use.company.find', err);
            callback(err, null);
            return;
        }
        var use = [], hosts = [];
        ;
        result.forEach(function (company) {
            if (company.front.key !== undefined) {
                use.push(company.front.key);
                hosts.push(company.front.host);
            }
            if (company.layer !== undefined && company.layer.key !== undefined) {
                use.push(company.layer.key);
                hosts.push(company.layer.host);
            }
        });
        logger.debug('domain.use hosts:%s', hosts.join(','));
        async.parallel({
            free: function (callback) {
                domain.update({"_id": {"$nin": use}}, {"$set": {"use": false}}, {multi: true}, function (err, result) {
                    if (err) {
                        logger.error('dynamic.domain free', err);
                        callback(err, null);
                        return;
                    }
                    callback(null, result);
                });
            },
            use: function (callback) {
                domain.update({"_id": {"$in": use}}, {"$set": {"use": true}}, {multi: true}, function (err) {
                    if (err) {
                        logger.error('dynamic.domain use', err);
                        callback(err, null);
                        return;
                    }
                    callback(null, result);
                });
            }
        }, callback);
    });
}
function filter_success(v) {
    return (v.alive.mode && v.ip.ip !== null && (v.avcheck !== undefined && v.avcheck.error === null && v.avcheck.total !== 0 && v.avcheck.detect === 0 ));
}
function filter_fail(v) {
    //logger.debug('filter_fail host:%s', v.host, !v.alive.mode, v.ip.ip === null, v.avcheck === undefined, v.avcheck.error !== null, v.avcheck.total === 0, v.avcheck.detect > 0)
    return (!v.alive.mode || v.ip.ip === null || (v.avcheck === undefined || v.avcheck.error !== null || v.avcheck.total === 0 || v.avcheck.detect > 0 ));
}
function install(callback) {
    company.find({layer: {"$exists": false}}, 'id', function (err, companies) {
        if (err) {
            logger.error('cron.avstat.install company.find', err);
            callback(err, null);
            return;
        }
        if (companies.length === 0) {
            callback(null, null);
            return;
        }
        domain.find({use: false, user: null}, function (err, result) {
            if (err) {
                logger.error('avstat.install domain.find use=false', err);
                callback(err, null);
                return;
            }
            if (result.length === 0) {
                callback(null, null);
                return;
            }
            var success = result.filter(filter_success), tasks = [];
            async.whilst(
                function () {
                    return ( companies.length !== 0 && success.length !== 0);
                },
                function (callback) {
                    var c = companies.shift(), d = success.shift();
                    tasks.push(function (callback) {
                        logger.debug('domain.install fail:%s success:%s', c.layer.host, d.host);
                        async.waterfall([
                            function (callback) {
                                company.update({"_id": c._id}, {
                                    "$set": {
                                        layer: {
                                            id: d.id,
                                            host: d.host,
                                            key: d._id
                                        }
                                    }
                                }, function (err) {
                                    if (err) {
                                        logger.error('avstat.install company.update newlayer', err);
                                    }
                                    callback(err);
                                });
                            },
                            function (callback) {
                                domain.update({"_id": d._id}, {use: true}, function (err) {
                                    if (err) {
                                        logger.error('avstat.install domain.update use', err);
                                    }
                                    callback(err);
                                });
                            },
                            function (callback) {
                                domain.update({"_id": d._id}, {use: false}, function (err) {
                                    if (err) {
                                        logger.error('avstat.install domain.update free', err);
                                    }
                                    callback(err);
                                });
                            }
                        ], callback);
                    });
                    callback();
                },
                function (err) {
                    async.parallel(tasks, callback);
                }
            );
        });
    });
}
var price = {
    alive: 1e5,
    avcheck: 1e3
};
function weight(result) {
    var host = {}, arr = [], i, data, c, resource = [];
    for (i in result) {
        data = Object.create(result[i]._doc);
        c = 0;
        if (data.avcheck !== undefined && data.avcheck.error !== null) {
            c = (data.avcheck.total * price.avcheck) - (data.avcheck.detect * price.avcheck);
        }
        c = (data.alive.mode) ? c + price.alive : c - price.alive;
        if (host[c] === undefined) {
            host[c] = [];
        }
        host[c].push(data);
        arr.push(c);
    }
    arr
        .sort(function (a, b) {
            return b - a;
        })
        .forEach(function (c) {
            host[c].forEach(function (data) {
                resource.push({data: data, weigth: c});
            });
        });
    return resource;
}
function reinstall(callback) {
    domain.find({user: null}, function (err, result) {
            if (err) {
                logger.error('avstat.check.domain.find use', err);
                callback(err);
                return;
            }

            if (result.length === 0) {
                callback();
                return;
            }

            var fails = result
                    .filter(function (v) {
                        return (v.use === true);
                    })
                    .filter(filter_fail),
                success = result
                    .filter(function (v) {
                        return (v.use === false);
                    })
                    .filter(filter_success), tasks = [];
            if (fails.length === 0) {
                callback();
                return;
            }
            if (success.length === 0) {
                logger.warn('not found clear layer');
                callback();
                return;
            }
            if (fails.length > success.length) {
                var weights = weight(result);
                var size = fails.length - success.length;
                logger.warn('layer fail>success', weights.length, size, fails.length, success.length);
                weights.splice(0, size).forEach(function (v) {
                    //logger.debug(v.data);
                    if (v.data) {
                        success.push(v.data);
                    }
                });
            }
            company.find({"layer.key": {"$in": fails}}, function (err, companies) {
                if (err) {
                    logger.error('avstat.check.domain.find use', err);
                    callback(err);
                    return;
                }

                if (companies.length === 0) {
                    callback();
                    return;
                }
                var tasks = [];
                async.whilst(
                    function () {
                        return (success.length !== 0 && companies.length !== 0);
                    },
                    function (callback) {
                        var c = companies.shift(), d = success.shift();
                        tasks.push(function (callback) {
                            logger.debug('domain.reinstall fail:%s success:%s', c.layer.host, d.host);
                            async.waterfall([
                                function (callback) {
                                    company.update({"_id": c._id}, {
                                        "$set": {
                                            layer: {
                                                id: d.id,
                                                host: d.host,
                                                key: d._id
                                            }
                                        }
                                    }, function (err) {
                                        if (err) {
                                            logger.error('avstat.install company.update newlayer', err);
                                        }
                                        callback(err);
                                    });
                                },
                                function (callback) {
                                    domain.update({"_id": d._id}, {use: true}, function (err) {
                                        if (err) {
                                            logger.error('avstat.install domain.update use', err);
                                        }
                                        callback(err);
                                    });
                                },
                                function (callback) {
                                    domain.update({"_id": d._id}, {use: false}, function (err) {
                                        if (err) {
                                            logger.error('avstat.install domain.update free', err);
                                        }
                                        callback(err);
                                    });
                                }
                            ], callback);
                        });
                        callback();
                    },
                    function (err) {
                        async.parallel(tasks, callback);
                    }
                );
            });
        }
    )
    ;
}
function clean(callback) {
    domain.find({user: null}, function (err, result) {
        if (err) {
            logger.error('check.domain.clear model.find', err);
            callback(err, null);
            return;
        }
        if (result.length === 0) {
            callback(null, null);
            return;
        }
        var domains = result.map(function (doc) {
            return doc._id;
        });
        company.update({"layer.key": {"$nin": domains}}, {'$unset': {"layer": true}}, {multi: true}, function (err) {
            if (err) {
                logger.error('dynamic.domain.clean company.update', err);
                callback(err, null);
                return;
            }
            callback(null, result);
        });
    });
}

require('../loader')(function (core) {
    domain = core.models.odm.domain;
    company = core.models.odm.company;
});
exports.clean = clean;
exports.reinstall = reinstall;
exports.install = install;
exports.use = use;
exports.check = {
    success: filter_success,
    fail: filter_success
};
exports.process = process;
