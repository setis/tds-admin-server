var winston = require('winston'),
        cfg = require('../../cfg/cron').logger;
module.exports = new winston.Logger({
    level: cfg.level,
    transports: [
        new (winston.transports.Console)({
            timestamp: true,
            colorize: true
        }),
        new (winston.transports.File)({
            timestamp: true,
            filename: cfg.dir + '/' + cfg.file
        })
    ]
});