/**
 * Created by alex on 16.02.16.
 */
var model,
    redis,
    clone = require('node-v8-clone').clone,
    key = "tds:path:uri",
    cfg = {
        front: {
            iframe: {
                count: 1,
                ext: ['.html']
            },
            script: {
                count: 1,
                ext: ['.js']
            },
            loader: {
                count: 1,
                ext: ['.js']
            },
            rotation: {
                count: 1,
                ext: ['.json']
            }
        },
        layer: {
            pre: {
                count: 20,
                ext: ['.js']
            },
            out: {
                count: 20,
                ext: ['.html', '']
            },
            drop: {
                count: 20,
                ext: ['.gif', '.png', '.jpeg', '.jpg', '.ico']
            },
            fiddler: {
                count: 20,
                ext: ['.gif', '.png', '.jpeg', '.jpg']
            }
        }
    },
    extname = require('path').extname,
    logger = require('../logger'),
    async = require('async');
function rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
function process(callback) {
    model.find({}, function (err, result) {
        if (err) {
            logger.error('company.path model.find', err);
            callback(err, null);
            return;
        }
        if (result.length === 0) {
            callback(null, null);
            return;
        }
        var tasks = result.map(function (company) {
            return function (callback) {
                check(company._doc, callback);
            };
        });
        async.parallel(tasks, callback);
    });
}
function check(doc, callback) {
    var exception = [],
        tasks = [],
        conf = clone(cfg, false),
        banners = doc.banners.filter(function (banner) {
            if (banner.path === undefined) {
                return true;
            } else {
                exception.push(banner.path);
                return false;
            }
        });
    if (banners.length) {
        tasks.push(function (callback) {
            var store = doc.banners.filter(function (banner) {
                return (banner.path !== undefined);
            });
            var tasks = banners.map(function (banner) {
                var path, ext = extname(banner.link);
                return function (callback) {
                    async.whilst(
                        function () {
                            return (path === undefined && exception.indexOf(path) === -1);
                        },
                        function (callback) {
                            redis.getClient(function (client, done) {
                                client.srandmember(key, function (err, result) {
                                    done();
                                    if (err) {
                                        logger.error('company.path.banners redis.srandmember ', err);
                                        callback(err);
                                        return;
                                    }
                                    //logger.debug('banner.path',result,ext);
                                    path = result + ext;
                                    callback();
                                });
                            });
                        },
                        function (err) {
                            //logger.debug('banner.path ', doc.id, path);
                            exception.push(path);
                            banner.path = path;
                            store.push(banner);
                            callback();
                        }
                    );
                };
            });
            async.series(tasks, function (err) {
                model.update({_id: doc._id}, {banners: store}, callback);
            });
        });
    }
    async.forEachOf(conf,
        function (conf1, name, callback) {
            async.forEachOf(
                conf1,
                function (conf2, name1, callback) {
                    var count = conf2.count,
                        list = conf2.ext,
                        key = ['path', name, name1].join('.'),
                        ext;

                    var value = doc.path[name][name1] || [];
                    if (value.length) {
                        value = value.filter(function (v) {
                            return (v !== undefined || v !== null);
                        });
                        value.map(function (path) {
                            exception.push(path);
                        });
                    }
                    count -= value.length;
                    if (list.length === 1) {
                        ext = function (path) {
                            return path + list[0];
                        };
                    } else {
                        ext = function (path) {
                            return path + list[rand(0, list.length - 1)];
                        };
                    }
                    if (count > 0) {
                        tasks.push(function (callback) {
                            path(exception, count, ext, function (result) {
                                console.log(result, !result instanceof Array);
                                if (result instanceof Array) {
                                    result.forEach(function (path) {
                                        value.push(path);
                                    });
                                    var update = {$set: {}};
                                    update['$set'][key] = value;
                                    model.update({_id: doc._id}, update, callback);
                                    return;
                                }
                                callback(null, null);
                                //console.log(key, count, arguments);

                            });
                        });
                        callback(null, count);
                        return;
                    }
                    callback(null, null);

                },
                callback
            );
        },
        function (err, result) {
            async.series(tasks, function (err, result) {
                model.update({_id: doc._id}, {"$set": {time: new Date()}}, callback);
            });
        }
    );
}
function path(exception, count, ext, callback) {
    var pool = [];
    async.whilst(
        function () {
            return count > pool.length;
        },
        function (callback) {
            redis.getClient(function (client, done) {
                client.srandmember(key, function (err, result) {
                    done();
                    if (err) {
                        logger.error('company.path.create redis.srandmember ', err);
                        callback(err);
                        return;
                    }
                    var path = ext(result);
                    if (exception.indexOf(path) === -1) {
                        pool.push(path);
                    }
                    callback(null, pool);
                });
            });
        },
        function (err, pool) {
            callback(pool);
        }
    );
}

require('../loader')(function (core) {
    model = core.models.odm.company;
    redis = core.clients.redis;
});
exports.process = process;
exports.check = check;
exports.path = path;
