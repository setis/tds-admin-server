var company,
    redis,
    layer,
    shortUrl,
    script = require('path').normalize(__dirname + '/../../tools/layer/BootStrap.php'),
    logger = require('../logger'),
    crypto = require('crypto'),
    php = require('uniter').createEngine('PHP'),
    async = require('async');
var id, start, end, timeout = 60e3;
var exec = require('child_process').exec;
var company, layer, logger, redis, client;
var ShortUrlService = 'media.bambinidream.com';
var timeout = 86400;
function generator(hostSuccess, success, hostFail, fail, hostFiddler, fiddler, cb) {
    logger.debug('php -f ' + script + ' ' + [hostSuccess, success, hostFail, fail, hostFiddler, fiddler].join(' '));
    exec('php -f ' + script + ' ' + [hostSuccess, success, hostFail, fail, hostFiddler, fiddler].join(' '), {maxBuffer: 1024 * 500}, function (err, stdout, stderr) {
        if (err) {
            cb(err, null);
            return;
        }
        if (stderr) {
            logger.error(stderr)
            cb(new Error(stderr), null);
            return;
        }
        cb(null, stdout);
    });
}
function key(company, short_url_service) {
    var list = [company.id, company.front.id, company.layer.id];
    if (company.jslayer.short.mode) {
        list.push(short_url_service.id);
    }
    return 'l:' + list.join('-');
}
function process() {
    company.find({layer: {"$exists": true}}, function (err, result) {
        if (err) {
            logger.error('cron.layer.checkAll.company.find ', err);
            return;
        }
        if (result.length === 0) {
            return;
        }
        var companies = {}, keys = [];
        for (var i in result) {
            var k = key(result[i]);
            companies[k] = result[i];
            keys.push(k);
        }
        redis.getClient(function (client, done) {
            client.keys('l:*', function (err, result) {
                done();
                if (err) {
                    logger.error('company.layer redis.keys ', err);
                    return;
                }
                async.parallel([
                    function (callback) {
                        var insert = keys.filter(function (key) {
                            return (result.indexOf(key) === -1);
                        });
                        if (insert.length === 0) {
                            callback();
                            return;
                        }
                        var tasks = keys
                            .map(function (key) {
                                var company = companies[key];
                                if (company === undefined) {
                                    return;
                                }
                                return function (callback) {
                                    create(key, company, callback);
                                };
                            })
                            .filter(function (v) {
                                return (v !== undefined);
                            });
                        async.parallel(tasks, callback);
                    },
                    function (callback) {
                        var drop = result.filter(function (key) {
                            return (keys.indexOf(key) === -1);
                        });
                        if (drop.length === 0) {
                            callback();
                            return;
                        }
                        var tasks = drop.map(function (key) {
                            return function (callback) {
                                redis.getClient(function (client, done) {
                                    client.del(key, function (err) {
                                        done();
                                        callback();
                                        if (err) {
                                            logger.error('cron.layer.checkAll.client.del ', err);
                                        }
                                    });
                                });
                            }
                        });
                        async.parallel(tasks, callback);
                    },
                    function (callback) {
                        var use = result.filter(function (key) {
                            return (keys.indexOf(key) !== -1);
                        });
                        if (use.length === 0) {
                            callback();
                            return;
                        }
                        var tasks = use
                            .map(function (key) {
                                var company = companies[key];
                                if (company === undefined) {
                                    return;
                                }
                                return function (callback) {
                                    redis.getClient(function (client, done) {
                                        client.scard(key, function (err, count) {
                                            done();
                                            if (err) {
                                                logger.error('layer redis.scard ', err);
                                                callback();
                                                return;
                                            }
                                            var size = company.jslayer.js.count || 20;
                                            logger.debug('layer.size', company.id, count !== size, count, size);
                                            if (count !== size) {
                                                logger.info('layer not size', company.id, key, size);
                                                create(company);
                                            }
                                        });
                                    });
                                }
                            })
                            .filter(function (v) {
                                return (v !== undefined);
                            });
                        async.parallel(tasks, callback);
                    }
                ], function (err, result) {
                });
            });

        });
    });
}
function short_url_service(callback) {
    shortUrl.findOne({use: true}, function (err, result) {
        if (err) {
            callback(err);
            return;
        }
        if (result === null) {
            return;
        }
        callback(null, result);
    });
}
function check(key, company, short_url_service, callback) {
    async.parallel([
        function (callback) {
            redis.getClient(function (client, done) {
                client.scard(key, function (err, count) {
                    done();
                    if (err) {
                        logger.error('layer redis.scard ', err);
                        callback(err);
                        return;
                    }

                    var size = company.jslayer.js.count || 20;
                    logger.debug('layer.size', v.company, count >= size, count, size);
                    if (count !== size) {
                        callback();
                    }
                });
            });
        },
        function (callback) {
            redis.getClient(function (client, done) {
                client.ttl(key, function (err, live) {
                    done();
                    if (err) {
                        logger.error('layer redis.scard ', err);
                        callback(err);
                        return;
                    }

                });
            });
        },
        function (callback) {
            layer.findOne({key: key}, function (err, result) {
                if (err) {
                    callback(err);
                    return;
                }
                //проверка параметров
            });
        }
    ], callback);
}
function _check(ids, companies) {
    layer.find({company: {"$in": ids}}, function (err, result) {
        if (err) {
            logger.error('cron.layer.check.layer.find ', err);
            return;
        }
//        logger.debug('check', ids);
        if (result.length === 0) {
            logger.debug('checkAll', ids);
            createAll(ids, companies);
            return;
        }
        var use = result.filter(function (v) {
//            logger.debug(v.company, ids.indexOf(v.company));
            return (ids.indexOf(v.company) !== -1);
        });
        var create = ids.filter(function (id) {
//            logger.debug(id, use.indexOf(id));
            return (use.indexOf(id) !== -1);
        });
        logger.debug('use', use.map(function (v) {
            return v.company;
        }));
        logger.debug('creaate', create);
        if (create.length > 0) {
            logger.debug('create', ids);
            createAll(create, companies);
        }

        use.map(function (v) {
            var company = companies[v.company];
            var ttl = ((company.jslayer.js.ttl) ? company.jslayer.js.ttl : timeout) * 1e3;
            logger.debug('layer.ttl', v.company, (new Date().getTime() - v.time.getTime()), (new Date().getTime() - v.time.getTime()) >= ttl);
            if (company.front.host !== v.front || company.layer.host !== v.layer || (new Date().getTime() - v.time.getTime()) >= ttl) {
                logger.info('layer update ttl');
                update(v, company);
                return;
            }
            logger.debug('layer.short', v.company, company.jslayer.short.mode && ShortUrlService !== v.short);
            if (company.jslayer.short.mode && ShortUrlService !== v.short) {
                logger.info('layer short mode');
                update(v, company);
                return;
            }
            redis.getClient(function (client, done) {
                client.scard(v.key, function (err, count) {
                    done();
                    if (err) {
                        logger.error('layer redis.scard ', err);
                        return;
                    }

                    var size = company.jslayer.js.count || 20;
                    logger.debug('layer.size', v.company, count >= size, count, size);
                    if (count !== size) {
                        logger.info('layer not size', v.company, size);
                        update(v, company);
                    }
                });
            });

        });


    });
}
function update(layer, company) {
    redis.getClient(function (client, done) {
        client.del(layer.key, function (err) {
            done();
            if (err) {
                logger.error('cron.layer.update.client.del ', err);
            }
        });
    });
    layer.remove(function (err) {
        if (err) {
            logger.error('cron.layer.update.layer.remove ', err);
        }
    });
    create(company);
}
function remove(ids) {
    layer.find({company: {"$nin": ids}}, function (err, result) {
        if (err) {
            logger.error('cron.layer.remove.layer.find ', err);
            return;
        }
        if (result.length === 0) {
            return;
        }
        result.forEach(function (data) {
            redis.getClient(function (client, done) {
                client.del(data.key, function (err) {
                    done();
                    if (err) {
                        logger.error('cron.layer.remove.client.del ', err);
                    }
                });
            });
            data.remove(function (err) {
                if (err) {
                    logger.error('cron.layer.remove.layer.remove ', err);
                }
            });
        });
    });
}
function create(key, short_url_service, company, callback) {
    var store = {
        time: new Date(),
        company: {
            key: company._id,
            id: company.id
        },
        front: company.front,
        layer: company.layer,
        short: (company.jslayer.short.mode) ? {
            id: short_url_service.id,
            key: short_url_service._id,
            host: short_url_service.host
        } : false,
        key: key
    };
    var info = [];
    var options = {
        hostname: (company.jslayer.short.mode) ? 'https://' + short_url_service : 'http://' + company.layer.host,

    };
    var count = company.jslayer.js.count || 20, ttl = (company.jslayer.js.ttl) ? company.jslayer.js.ttl : 86400;
    var tasks = [];
    for (var i = 0; i < count; i++) {
        task.push(function (callback) {
            generate(options, function (err, result) {
                info.push({
                    hash: crypto.createHash('sha256').update(data, 'utf8').digest(),
                    size: result.length
                });
                redis.getClient(function (client, done) {
                    client.sadd(key, result, function (err) {
                        done();
                        callback(err);
                        if (err) {
                            logger.error('cron.layer.create.generator.client.set', err);
                        }
                    });
                });
            });
        });
    }
    async.waterfall(task, function (err) {
        async.parallel([
            function (callback) {
                redis.getClient(function (client, done) {
                    client.expire(key, ttl, function (err) {
                        done();
                        callback(err);
                        if (err) {
                            logger.error('cron.layer.create.generator.client.ttl', err);
                        }
                    });
                });
            },
            function (callback) {
                store.options = options;
                store.info = info;
                store.time = new Date();
                layer.create(store, function (err, result) {
                    logger.info('cron.layer.create.generator.layer.create', data.key);
                    if (err && err.code === 11000) {
                        layer.update(
                            {key: key},
                            store,
                            function (err) {
                                callback(err);
                                if (err) {
                                    logger.error('cron.layer.create.generator.layer.create', err);
                                }
                            });
                        return;
                    }
                    callback(err);
                });
            }
        ], callback);
    });
}
function generate(options, callback) {
    php.getStdout().on('data', function (text) {
        console.log(text);
    });
    php.execute('<?php require()";?>');
}

require('../loader')(function () {
    company = core.models.odm.company;
    layer = core.models.odm.layer;
    shortUrl = core.models.odm.shortUrl;
    redis = core.clients.redis;
});
module.exports = loop;
