var company,
    redis,
    logger = require('../logger'),
    async = require('async');
function key(company) {
    return 'b:' + company.id + '-' + company.front.id;
}
function build(host, click, banner) {
    var link;

    if (!banner.url) {
        link = 'http://' + host + banner.path;
    } else {
        link = banner.link;
    }
    //logger.debug(banner,link);
    return "<a href='" + click + "' target='_blank'><img src='" + link + "'  border='0' width='" + banner.width + "' height='" + banner.height + "'></a>";
}
function create(key, company, callback) {
    var host = company.front.host,
        click = company.click;
    var tasks = company.banners.map(function (banner) {
        return function (callback) {
            redis.getClient(function (client, done) {
                client.sadd(key, build(host, click, banner), function (err) {
                    done();
                    callback();
                    if (err) {
                        logger.error('company.banner.create redis.sadd', err);
                    }
                });
            });
        };
    });
    async.parallel(tasks, callback);
}

function update(key, company, callback) {
    var host = company.front.host,
        click = company.click;
    if (company.banners.length === 0) {
        redis.getClient(function (client, done) {
            client.del(key, function (err) {
                done();
                callback();
                if (err) {
                    logger.error('company.banner.update redis.del', err);
                    return;
                }
            });
        });
        return;
    }
    var banners = company.banners.map(function (banner) {
        return build(host, click, banner);
    });
    redis.getClient(function (client, done) {
        client.smembers(key, function (err, list) {
            done();
            if (err) {
                logger.error('company.banner.update redis.smembers', err);
                callback(err);
                return;
            }
            if (list.length === 0) {
                result.forEach(function (html) {
                    redis.getClient(function (client, done) {
                        client.sadd(k, html, function (err) {
                            done();
                            if (err) {
                                logger.error('company.banner.update redis.sadd', err);
                                return;
                            }
                        });
                    });
                });
                callback();
                return;
            }
            async.parallel([
                function (callback) {
                    var data = list.filter(function (html) {
                        return (banners.indexOf(html) === -1);
                    });
                    if (data.length === 0) {
                        callback();
                        return;
                    }
                    var tasks = data.map(function (html) {
                        return function (callback) {
                            redis.getClient(function (client, done) {
                                client.srem(key, html, function (err) {
                                    done();
                                    callback();
                                    if (err) {
                                        logger.error('company.banner.update redis.srem', err);
                                        return;
                                    }
                                });
                            });
                        };
                    });
                    async.parallel(tasks, callback);
                },
                function (callback) {
                    var data = banners.filter(function (html) {
                        return (list.indexOf(html) === -1);
                    });
                    if (data.length === 0) {
                        callback();
                        return;
                    }
                    var tasks = data.map(function (html) {
                        return function (callback) {
                            redis.getClient(function (client, done) {
                                client.sadd(key, html, function (err) {
                                    done();
                                    callback();
                                    if (err) {
                                        logger.error('company.banner.update redis.sadd', err);
                                        return;
                                    }
                                });
                            });
                        };
                    });
                    async.parallel(tasks, callback);

                }

            ], callback);
        });
    });

}
function process(callback) {
    company.find({}, function (err, result) {
        if (err) {
            logger.error('company.banner company.find', err);
            callback(err, null);
            return;
        }
        if (result.length === 0) {
            callback(null, null);
            return;
        }
        var tasks = result.map(function (company) {
            return function (callback) {
                var k = key(company);
                redis.getClient(function (client, done) {
                    client.exists(k, function (err, status) {
                        done();
                        if (err) {
                            logger.error('company.banner redis.keys', err);
                            callback(err);
                            return;
                        }
                        if (status) {
                            update(k, company, callback);
                        } else {
                            create(k, company, callback);

                        }
                    });
                });
            };
        });
        async.parallel(tasks, callback);
    });
}

require('../loader')(function (core) {
    company = core.models.odm.company;
    redis = core.clients.redis;
});
exports.process = process;
exports.create = create;
exports.update = update;
exports.build = build;
exports.key = key;
