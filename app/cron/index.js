var check = require('./check/kernel'),
        virustotal = require('./virustotal/kernel'),
        avcheck = require('./avcheck/kernel');
require('./loader')(function (data) {
    data.logger = require('./logger');
    require('./static/create')(data);
    require('./static/counter')(data);
    require('./traffic')(data);
    require('./layer')(data);
    require('./avstat')(data);
    require('./banner')(data,true);
    require('./path')(data);
    require('./image')(data);
    avcheck.models = check.models = {
        ip: data.models.odm.ip,
        domain: data.models.odm.domain
    };
    avcheck.init();
    avcheck.start();
    setTimeout(function () {
        check.run.domain();
        check.run.ip();
    }, 60e3);
    check.run.domain();
    check.run.ip();
});


