/**
 * Created by alex on 13.02.16.
 */
var shortUrl,
    logger = require('../logger'),
    cfg = require('../../../cfg/cron.json').check,
    dns = require('dns'),
    ping = require('ping'),
    request = require('request'),
    async = require('async');
var id, start, end, timeout = 60e3;
function api(server, key, callback) {
    async.waterfall([
        function (callback) {
            request.get({
                url: 'http://' + server + '/api2/',
                qs: {
                    key: key,
                    action: 'link.add',
                    ttl: 15,
                    url: 'http://ya.ru'
                },
                json: true,
                strictSSL: false,
                rejectUnauthorized: false
            }, function (error, response, body) {
                if (error) {
                    callback(error, response);
                } else if (response.statusCode !== 200) {
                    callback(new Error('shortURL server:' + server + ' status code:' + response.statusCode, response.statusCode), body);
                } else {
                    callback(null, body.short || null);
                }
            });
        },
        function (result, callback) {
            console.log(result);
            if (result === null) {
                logger.error('not found short url', result);
                callback(new Error('not found short url: ' + server));
                return;
            }
            var url = 'http://' + server + '/' + result;
            request.get({
                url: url,
                strictSSL: false,
                rejectUnauthorized: false
            }, function (error, response, body) {
                if (error) {
                    callback(new Error('shortURL url :' + url + ' msg:' + error.message, error.code), null);
                } else if (response.statusCode !== 200) {
                    callback(new Error('shortURL server:' + server + ' status code:' + response.statusCode, response.statusCode), null);
                } else {
                    callback(null, url, (body.length !== 0));
                }
            });
        },
        function (url, result, callback) {
            console.log(url, result);
            if (!result) {
                logger.error('not found open long url', url);
                callback(new Error('not found open long url:' + url));
                return;
            }
            setTimeout(function () {
                request.get({
                    url: url,
                    strictSSL: false,
                    rejectUnauthorized: false
                }, function (error, response, body) {
                    if (error) {
                        callback(new Error('shortURL url :' + url + ' msg:' + error.message, error.code), null);
                    } else if (response.statusCode === 404) {
                        callback(null, true);
                    } else {
                        callback(null, false);
                    }
                });
            }, 60e3);

        }
    ], function (err, result) {
        callback(err, result);
    });
}
function process() {
    start = new Date();
    shortUrl.find({}, function (err, result) {
        if (err) {
            logger.error('check.shortUrl model.find', err);
            if (id === null) {
                setTimeout(process, timeout);
            }
            return;
        }
        if (result.length === 0) {
            end = new Date();
            logger.info('check.shortUrl not found records time: %ss', (end.getTime() - start.getTime()) / 1000);
            if (id === null) {
                setTimeout(process, timeout);
            }
            return;
        }
        var tasks = result.map(function (item) {
            return function (callback) {
                async.parallel({
                    dns: function (callback) {
                        dns.lookup(item.host, callback);
                    },
                    ping: function (callback) {
                        ping.sys.probe(item.host, function (alive) {
                            callback(null, alive);
                        });
                    },
                    www: function (callback) {
                        var url = 'http://' + item.host + '/';
                        request.get({
                            url: url,
                            strictSSL: false,
                            rejectUnauthorized: false
                        }, function (error, response, body) {
                            if (error) {
                                logger.error('shortUrl.www url:%s', url, error);
                                cb(null, false);
                            } else if (response.statusCode !== 200) {
                                cb(null, false);
                            } else {
                                cb(null, true);
                            }
                        });
                    },
                    api: function (callback) {
                        api(item.host,item.key,function(err,result){

                        });
                    }
                }, function (err, result) {
                    var data = {
                        host: item.host,
                        ip: (result.dns === undefined) ? null : result.dns[0],
                        alive: (result.ping === null) ? false : true

                    };
                    shortUrl.update({_id: item._id}, {
                        '$set': {
                            alive: {
                                time: new Date(),
                                mode: (result.ping === null) ? false : true
                            },
                            ip: {
                                time: new Date(),
                                ip: (result.dns === undefined) ? null : result.dns[0]
                            },
                            www: {
                                time: new Date(),
                                mode: result.www
                            }
                        }
                    }, function (err) {
                        if (err) {
                            logger.error('check.shortUrl model.update', err, item.host);
                        }
                        callback(null, data);
                    });


                });
            };
        });
        async.parallel(tasks, function (err, result) {
            //console.log('async.each', err, result);
            end = new Date();
            logger.info('check.shortUrl time: %ss', (end.getTime() - start.getTime()) / 1000);
            if (end.getTime() - start.getTime() > timeout) {
                clearInterval(id);
                id = null;
                process();
            } else {
                if (id === null) {
                    id = setInterval(process, timeout);
                }
            }
        });
    });

}
//function loop(core) {
//    shortUrl = core.models.odm.shortUrl;
//    id = setInterval(process, timeout);
//    process();
//}
//
//require('../loader')(loop);
//module.exports = loop;

api('media.bambinidream.com', 'f329s50x8ov1z9ei6zsp3xizy34lnb2l', console.log);