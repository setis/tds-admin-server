/**
 * Created by alex on 14.02.16.
 */
var logger = require('../logger'),
    fs = require('fs'),
    async = require('async'),
    cfg,
    dir = require('path').normalize(__dirname + '/../../../uploads') + '/',
    model;
function setDir(path, callback) {
    fs.stat(path, function (err, stats) {
        if (err && err.errno === 34) {
            fs.mkdir(directory, callback);
        } else {
            callback(err)
        }
    });
}
//cfg = require('../../../cfg/cron.json').image;
//dir = require('path').normalize(cfg.dir) + '/' || require('path').normalize(__dirname + '/../../uploads') + '/';
setDir(dir, function (err) {
    if (err) {
        logger.error('check.image not set dir:' + dir, err);
        return;
    }
    logger.info('check.image dir:', dir);
});
require('../loader')(function (core) {
    model = core.models.odm.banner;
});
module.exports = function (callback) {
    fs.readdir(dir, function (err, files) {
        if (err) {
            logger.error('check.image model.find', err);
            callback(err, null);
            return;
        }
        if (files.length === 0) {
            logger.info('check.image not found files fs.readdir');
            model.find({url: false}, function (err, result) {
                if (err) {
                    logger.error('check.image mode.find create files', err);
                    callback(err, null);
                    return;
                }
                if (result.length === 0) {
                    callback(null, null);
                    return;
                }
                var tasks = result.map(function (banner) {
                    return function (callback) {
                        var path = dir + banner.link;
                        fs.open(path, 'w', function (err, fd) {
                            if (err) {
                                logger.error('check.image fs.open  path:%s banner._id:%s', path, banner._id.toString(), err);
                                callback(err, null);
                                return;
                            }
                            var buffer = banner.path.data;
                            fs.write(fd, buffer, 0, buffer.length, null, function (err) {
                                if (err) {
                                    logger.error('fs.write', path, err);
                                    callback(err, null);
                                    return;
                                }
                                fs.close(fd);
                                callback(null,path);
                            });
                        });
                    };
                });
                async.parallel(tasks, callback);
            });
            return;
        }
        model.find({url: false, link: {$nin: files}}, function (err, result) {
            if (err) {
                logger.error('check.image mode.find files', err);
                callback(err, null);
                return;
            }
            if (result.length === 0) {
                callback(null, null);
                return;
            }
            var tasks = result.map(function (banner) {
                return function (callback) {
                    var path = dir + banner.link;
                    fs.exists(path, function (status) {
                        if (!status) {
                            fs.open(path, 'w', function (err, fd) {
                                if (err) {
                                    logger.error('check.image fs.open  path:%s banner._id:%s', path, banner._id.toString(), err);
                                    callback(err, null);
                                    return;
                                }
                                var buffer = banner.path.data;
                                fs.write(fd, buffer, 0, buffer.length, null, function (err) {
                                    if (err) {
                                        logger.error('fs.write', path, err);
                                        callback(err, null);
                                        return;
                                    }
                                    fs.close(fd);
                                    callback(null,path);
                                });
                            });
                        } else {
                            fs.unlink(path, function (err) {
                                if (err) {
                                    logger.error('check.image fs.unlink:%s', path, err);
                                }
                                callback(err, path);
                            });
                        }
                    });
                };
            });
            async.parallel(tasks, callback);
        });
    });
};
