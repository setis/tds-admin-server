/**
 * Created by alex on 13.02.16.
 */
var domain,
    logger = require('../logger'),
    dns = require('dns'),
    ping = require('ping'),
    async = require('async');
require('../loader')(function (core) {
    domain = core.models.odm.domain;
});
module.exports = function (callback) {
    domain.find({}, function (err, result) {
        if (err) {
            logger.error('check.domain model.find', err);
            callback(err, null);
            return;
        }
        if (result.length === 0) {
            callback(null, null);
            return;
        }
        var tasks = result.map(function (item) {
            return function (callback) {
                async.parallel({
                    dns: function (callback) {
                        dns.lookup(item.host, callback);
                    },
                    ping: function (callback) {
                        ping.sys.probe(item.host, function (alive) {
                            callback(null, alive);
                        });
                    }
                }, function (err, result) {
                    var data = {
                        host: item.host,
                        ip: (result.dns === undefined) ? null : result.dns[0],
                        alive: (result.ping === null) ? false : true

                    };
                    domain.update({_id: item._id}, {
                        '$set': {
                            alive: {
                                time: new Date(),
                                mode: (result.ping === null) ? false : true
                            },
                            ip: {
                                time: new Date(),
                                ip: (result.dns === undefined) ? null : result.dns[0]
                            }
                        }
                    }, function (err) {
                        if (err) {
                            logger.error('check.domain model.update', err, item.host);
                        }
                        callback(err, data);
                    });
                });
            };
        });
        async.parallel(tasks, callback);
    });

};
