/**
 * Created by alex on 13.02.16.
 */
var server,
    logger = require('../logger'),
    ping = require('ping'),
    Client = require('ssh2').Client,
    agent = require('proxysocket').createAgent('127.0.0.1', 9050),
    async = require('async');

require('../loader')(function(core){
    server = core.models.odm.server;
});
module.exports = function (callback) {
    server.find({}, function (err, result) {
        if (err) {
            logger.error('check.domain model.find', err);
            callback(err,null);
            return;
        }
        if (result.length === 0) {
            callback(null,null);
            return;
        }
        var tasks = result.map(function (item) {
            return function (callback) {
                async.parallel({
                    ssh: function (callback) {
                        var cfg = {
                            host: item.ip.main,
                            port: item.port || 22,
                            username: item.login || 'root',
                            tryKeyboard: false,
                            agent: agent,
                            privateKey: require('fs').readFileSync('/home/alex/.ssh/id_rsa'),
                            publicKey: require('fs').readFileSync('/home/alex/.ssh/id_rsa.pub'),
                        };
                        var conn = new Client();
                        conn.on('ready', function () {
                            conn.exec('uptime', function (err, stream) {
                                if (err) {
                                    logger.error('ssh.exec uptime', err);
                                    callback(null, {type: 'error', data: err});
                                    return;
                                }
                                var result = {};
                                stream
                                    .on('close', function (code, signal) {
                                        callback(null, result);
                                        conn.end();
                                    })
                                    .on('data', function (data) {
                                        result = {
                                            data: data.toString(),
                                            type: 'success'
                                        };
                                    })
                                    .on('error', function (data) {
                                        result = {
                                            data: data.toString(),
                                            type: 'error'
                                        };
                                    });
                            });
                        });
                        conn.on('error', function (err) {
                            logger.error('ssh.connect', err, cfg);
                            callback(null, {type: 'error', data: err});
                        });
                        conn.connect(cfg);
                    },
                    ping: function (callback) {
                        ping.sys.probe(item.host, function (alive) {
                            callback(null, alive);
                        });
                    }
                }, function (err, result) {
                    server.update({_id: item._id}, {
                        '$set': {
                            alive: {
                                time: new Date(),
                                mode: (result.ping === null) ? false : true
                            },
                            ssh: {
                                time: new Date(),
                                mode: (result.ssh.type === 'error') ? false : true,
                                result: result.ssh
                            }
                        }
                    }, function (err) {
                        if (err) {
                            logger.error('check.server model.update', err, item.host);
                        }
                        callback();
                    });


                });
            };
        });
        async.parallel(tasks, callback);
    });
};
