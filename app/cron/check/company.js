/**
 * Created by alex on 13.02.16.
 */
var domain, shortUrl, ip,
    logger = require('../logger'),
    pool = require('./pool'),
    cfg = require('../../../cfg/cron.json').check,
    dns = require('dns'),
    ping = require('ping'),
    request = require('request'),
    async = require('async');
function layer(){}
function html(item){
    var url = 'http://' + item.front.host + item.path.front.html[0];
    request.get({
        url: url,
        strictSSL: false,
        rejectUnauthorized: false
    }, function (error, response, body) {
        if (error) {
            logger.error('shortUrl.www url:%s', url, err);
            cb(null, false);
        } else if (response.statusCode !== 200) {
            cb(null, false);
        } else {
            cb(null, true);
        }
    });
}
function js(item){
    var url = 'http://' + item.front.host + item.path.front.html[0];
    request.get({
        url: url,
        strictSSL: false,
        rejectUnauthorized: false
    }, function (error, response, body) {
        if (error) {
            logger.error('shortUrl.www url:%s', url, err);
            cb(null, false);
        } else if (response.statusCode !== 200) {
            cb(null, false);
        } else {
            cb(null, true);
        }
    });
}
function image(item){
    var host = 'http://' + item.front.host;
    var tasks = item.banners.map(function (banner) {
        var url;
        if (!banner.url) {
            url = host + banner.path;
        } else {
            url = banner.url;
        }
        return function (callback) {
            request.get({
                url: url,
                strictSSL: false,
                rejectUnauthorized: false
            }, function (error, response, body) {
                if (error) {
                    logger.error('shortUrl.www url:%s', url, err);
                    cb(null, false);
                } else if (response.statusCode !== 200) {
                    cb(null, false);
                } else {
                    cb(null, true);
                }
            });
        };
    });
    async.parallel(tasks, callback);
}
function loop(core) {
    domain = core.models.odm.domain;
    shortUrl = core.models.odm.shortUrl;
    ip = core.models.odm.ip;
    avcheck.emit('process');
}
require('../loader')(loop);
module.exports = loop;
