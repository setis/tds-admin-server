/**
 * Created by alex on 13.02.16.
 */
var domain, shortUrl, ip,
    logger = require('../logger'),
    pool = require('./pool'),
    cfg = require('../../../cfg/cron.json').check,
    dns = require('dns'),
    ping = require('ping'),
    request = require('request'),
    async = require('async');
function check(item) {
    async.parallel({
        dns: function (callback) {
            dns.lookup(item.host, callback);
        },
        ping: function (callback) {
            ping.sys.probe(item.host, callback);
        },
        www: function (callback) {
            var url = 'https://' + item.host + '/';
            request.get({
                url: url,
                strictSSL: false,
                rejectUnauthorized: false
            }, function (error, response, body) {
                if (error) {
                    logger.error('shortUrl.www url:%s', url, err);
                    cb(null, false);
                } else if (response.statusCode !== 200) {
                    cb(null, false);
                } else {
                    cb(null, true);
                }
            });
        },
        api:function(callback){
            request.get({
                url: 'https://'+item.host+'/api2/',
                qs: {
                    key:item.api,
                    action:'view',
                    limit:10
                },
                json: true,
                strictSSL: false,
                rejectUnauthorized: false
            }, function (error, response, body) {
                if (error) {
                    cb(new Error('shortURL:' + cfg.server + ' msg:' + error.message, error.code), body);
                } else if (response.statusCode !== 200) {
                    cb(new Error('shortURL:' + cfg.server + ' status code:' + response.statusCode, response.statusCode), body);
                } else {
                    cb(null, body);
                }
            });
        }
    }, function (err, result) {
    });
}
function loop(core) {
    domain = core.models.odm.domain;
    shortUrl = core.models.odm.shortUrl;
    ip = core.models.odm.ip;
    avcheck.emit('process');
}
require('../loader')(loop);
module.exports = loop;
