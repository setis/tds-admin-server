/**
 * Created by alex on 13.02.16.
 */
var ip,
    logger = require('../logger'),
    ping = require('ping'),
    async = require('async');
require('../loader')(function (core) {
    ip = core.models.odm.ip;
});
module.exports = function(callback) {
    ip.find({}, function (err, result) {
        if (err) {
            logger.error('check.ip model.find', err);
            callback(err, null);
            return;
        }
        if (result.length === 0) {
            callback(null, null);
            return;
        }
        var tasks = result.map(function (item) {
            return function (callback) {
                ping.sys.probe(item.ip, function (alive) {
                    //logger.debug(item.ip, alive);
                    ip.update({_id: item._id}, {
                        '$set': {
                            alive: {
                                time: new Date(),
                                mode: alive
                            }
                        }
                    }, function (err, result) {
                        if (err) {
                            logger.error('check.ip model.update', err, item.ip);
                            callback(err, null);
                            return;
                        }
                        callback(null, result.result);
                    });
                });
            };
        });
        async.parallel(tasks, callback);
    });
};
