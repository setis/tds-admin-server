var csv = require("fast-csv"),
        fs = require('fs'),
        model, full;
var headers = [
    "ip",
    "country"

];
require('../loader')(function (core) {
    full = core.models.odm.isp_full;
    model = core.models.odm.isp;
    run();
});

var file = process.argv[2];
var list = [
    process.cwd() + '/' + file,
    __dirname + '/' + file,
    file
];
var f = false;
for (var i in list) {
    var path = list[i];
    try {
        fs.statSync(path);
        console.log('read file: %s', path);
        f = true;
        break;
    } catch (e) {
        if (e.code == 'ENOENT') {
            console.log('not found file: %s', path);
        }
    }
}
if (!f) {
    console.log('not file: %s', file);
    process.exit();
}
function run() {
    read = fs.createReadStream(path);
    read.on("finish", function () {
        console.log("DONE!");
    });
    var start = new Date();
//    write = fs.createWriteStream(__dirname + '/handler.csv');
    var i = 0;
    var d = 0;
    var d2 = 0;
    csv
            .fromStream(read, {headers: headers})
            .on("data", function (data) {
                if (i === 0) {
                    return;
                }
                full
                        .ip(data.ip, function (err, result) {
//                console.log(data.ip, err, result);
                            if (err) {
                                console.log('data:', data, err);
                                return;
                            }
                            if (result === null) {
                                console.log('not found', data.ip);
                                return;
                            }
//                            console.log(data.ip, result);
                            if (result.isp) {
//                                console.log(data.ip, result.use);
                                d++;
                            } 
                        });
//                        console.log(data.country);
                model
                        .base(data.country)
                        .ip(data.ip, function (err, result) {
//                console.log(data.ip, err, result);
                            if (err) {
                                console.log('data:', data, err);
                                return;
                            }
                            if (result === null) {
                                console.log('not found', data.ip);
                                return;
                            }

                            if (result.isp) {
//                                console.log(data.ip, result.use);
                                d2++;
                            } 
                        });

            })
            .on("record", function (data, line) {
                i = line;
            })

            .on('error', function (err) {
                console.log('line:' + i, err);
                console.log(start, new Date());
                process.exit();
            })
            .on("end", function () {
                console.log("end");
                console.log(start, new Date());
//                process.exit();
            })
            .on("finish", function () {
                setInterval(function () {
                    console.log("full is isp %s % %s/%s", d / (i / 100), d, i);
                    console.log("model is isp %s % %s/%s", d2 / (i / 100), d2, i);
                }, 15e3);
                console.log(start, new Date());
//                process.exit();
            });


}