<?php

ini_set("display_errors", "1");
ini_set("display_startup_errors", "1");
ini_set('error_reporting', E_ALL);
set_time_limit(0);

function check($str) {
    $pattern = "/[\\\~^°!\"§$%\/()=?`';,\.:_{\[\]}\|<>@+#]/";
    return (!preg_match($pattern, $str));
}

$pathBase = 'tds:path:base';
$pathUri = 'tds:path:uri';
$n1 = 2;
$n2 = 6;
$fp = fopen(__DIR__."/english words", "r");
$redis = new Redis();
$redis->connect('127.0.0.1');
//$redis->connect('136.243.49.100');
//$redis->auth("1a{PjGz~KIP~");
$redis->del($pathBase);
$redis->del($pathUri);
echo "step-1 load redis\n";
while (($buffer = fgets($fp, 4096)) !== false) {
    $buffer = trim(str_replace(["\n", "\t", "\r"], '', $buffer));
    if ($buffer !== '' && strlen($buffer) > 1 && check($buffer)) {
//        echo $buffer . "\n";
        $redis->rPush($pathBase, $buffer);
    }
}
fclose($fp);

function generic_rand($count, $lcount, $size, $redis, $pathBase, $pathUri) {
    while ($count--) {
        $path = [];
        $i = rand($size[0], $size[1]);
        while ($i--) {
            $path[] = $redis->lindex($pathBase, rand(0, $lcount));
        }
        echo '/' . implode('/', $path) . "\n";
        $redis->sadd($pathUri, '/' . implode('/', $path));
    }
}

function generic_all($count, $size, $redis, $pathBase, $pathUri) {
    list($n1, $n2) = $size;

    function generic($arr, $count, $redis, $pathBase, $pathUri) {
        $i = count($arr) - 1;
        $t = 0;
        while ($arr[$i] <= $count) {
            for ($t = 0; $t <= $i; $t++) {
                if ($t === 0) {
                    $arr[$t] ++;
                }
                if ($arr[$t] >= $count) {
                    if ($t === $i) {
                        return;
                    }
                    $arr[$t] = 0;
                    $arr[$t + 1] ++;
                }
            }
            $path = [];
            foreach ($arr as $d => $v) {
                $path[$d] = $redis->lindex($pathBase, $v);
            }
//            echo '/' . implode('/', $path) . "\n";
            $redis->sadd($pathUri, '/' . implode('/', $path));
        }
    }

    for ($i = $n1; $i <= $n2; $i++) {
        $arr = [];
        for ($t = 0; $t <= $i; $t++) {
            $arr[$t] = 0;
        }
        generic($arr, $count, $redis, $pathBase, $pathUri);
    }
}

echo "step-2 generic-rand path\n";
$count = $redis->lLen($pathBase);
$records = 10000;
$size = [$n1, $n2];
echo "count:$count\n";
generic_rand($records, $count, $size, $redis, $pathBase, $pathUri);


$redis->close();
