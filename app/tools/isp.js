/**
 * 
 * https://www.npmjs.com/package/fast-csv
 */
var csv = require("fast-csv"),
        fs = require('fs'),
        model, full;
var headers = [
    "IP_FROM",
    "IP_TO",
    "COUNTRY_CODE",
    "COUNTRY_NAME",
    "REGION",
    "CITY",
    "LATITUDE",
    "LONGITUDE",
    "ISP_NAME",
    "DOMAIN_NAME",
    "MCC",
    "MNC",
    "MOBILE_BRAND",
    "USAGE_TYPE"
];

var file = process.argv[2];
var list = [
    process.cwd() + '/' + file,
    __dirname + '/' + file,
    file
];
var f = false;
for (var i in list) {
    var path = list[i];
    try {
        fs.statSync(path);
        console.log('read file: %s', path);
        f = true;
        break;
    } catch (e) {
        if (e.code == 'ENOENT') {
            console.log('not found file: %s', path);
        }
    }
}
if (!f) {
    console.log('not file: %s', file);
    process.exit();
}
var _ip = require('noi');
function run() {
    read = fs.createReadStream(path);
    read.on("finish", function () {
        console.log("DONE!");
    });
    var start = new Date();
//    write = fs.createWriteStream(__dirname + '/handler.csv');
    var i = 0;
    csv
            .fromStream(read, {headers: headers, ignoreEmpty: true})
//            .validate(function (data) {
//                return (data.USAGE_TYPE === 'ISP' || data.USAGE_TYPE === 'ISP/MOB' || data.USAGE_TYPE === 'MOB');
////                return (data.COUNTRY_CODE !== '-' && (data.IP_FROM.toString().length <= 10 && data.IP_TO.toString().length <= 10) && (data.USAGE_TYPE === 'ISP' || data.USAGE_TYPE === 'ISP/MOB'));
//            })
            .on("data", function (data) {
                var result = {
                    to: data.IP_TO,
                    from: data.IP_FROM,
//                    text: {
//                        to: _ip.decode(data.IP_TO),
//                        from: _ip.decode(data.IP_FROM)
//                    },
                    use: data.USAGE_TYPE,
                    isp: (data.USAGE_TYPE === 'ISP' || data.USAGE_TYPE === 'ISP/MOB' || data.USAGE_TYPE === 'MOB')?true:false,
//                    ip4: (data.IP_FROM.toString().length <= 10)

                };
                model
                        .base(data.COUNTRY_CODE)
                        .create(result, function (err) {
                            if (err) {
                                console.warn('data:' + JSON.stringify(result), err);
                            }
                        });
                result.country = data.COUNTRY_CODE;
                full.create(result, function (err) {

                    if (err && err.code !== 11000) {
                        console.warn('data:' + JSON.stringify(result), err);
                    }
                });
            })
            .on("record", function (data, line) {
                i = line;
            })

            .on('error', function (err) {
                console.log('line:' + i, err);
                console.log(start, new Date());
//                process.exit();
            })
            .on("end", function () {
                console.log("finish");
                console.log(start, new Date());
//                process.exit();
            })
            .on("finish", function () {
                console.log("finish");
                console.log(start, new Date());
//                process.exit();
            });
    setInterval(function () {
        console.log(i);
    }, 60e3).unref();
//            .pipe(csv.createWriteStream({headers: false}))
//            .pipe(fs.createWriteStream(__dirname + '/handler.csv', {encoding: "utf8"}));

}
require('../loader')(function (core) {
    model = core.models.odm.isp;
    full = core.models.odm.isp_full;
    run();
});
