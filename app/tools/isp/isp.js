/**
 * 
 * https://www.npmjs.com/package/fast-csv
 */
var csv = require("fast-csv"),
        fs = require('fs'),
        model, full;
var headers = [
    "IP_FROM",
    "IP_TO",
    "COUNTRY_CODE",
    "COUNTRY_NAME",
    "REGION",
    "CITY",
    "LATITUDE",
    "LONGITUDE",
    "ISP_NAME",
    "DOMAIN_NAME",
    "MCC",
    "MNC",
    "MOBILE_BRAND",
    "USAGE_TYPE"
];

var file = process.argv[2];
var list = [
    process.cwd() + '/' + file,
    __dirname + '/' + file,
    file
];
var f = false;
for (var i in list) {
    var path = list[i];
    try {
        fs.statSync(path);
        console.log('read file: %s', path);
        f = true;
        break;
    } catch (e) {
        if (e.code == 'ENOENT') {
            console.log('not found file: %s', path);
        }
    }
}
if (!f) {
    console.log('not file: %s', file);
    process.exit();
}
var _ip = require('noi');
var max = process.argv[3];
var d;
function run(cb) {
    read = fs.createReadStream(path);
    read.on("finish", function () {
        console.log("DONE!");
    });
    var start = new Date();
    var i = 0;
    csv
            .fromStream(read)

            .on("record", function (data, line) {
                i = line;
            })

            .on('error', function (err) {
                console.log('line:' + i, err);
                console.log(start, new Date());
//                process.exit();
            })
            .on("end", function () {
                console.log("finish", i);
                console.log(start, new Date());
                cb(i);
            })
            .on("finish", function () {
                console.log("finish", i);
                console.log(start, new Date());
                cb(i);
            });
    d = setInterval(function () {
        console.log('line: ' + i, path);
    }, 15e3).unref();
}
var exec = require('child_process').exec;
var script = __dirname + '/handler.js';

var cluster = require('cluster');
function handler(path, start, end) {

    console.log('node ' + script + ' ' + [path, start, end].join(' '));

    if (cluster.isMaster) {
        cluster.setupMaster({
            exec: script,
            args: [path, start, end]
        });
        cluster.fork();
    }
//    exec('node ' + script + ' ' + [path, start, end].join(' '), {maxBuffer: 1024 * 500}, function (err, stdout, stderr) {
//        if (err) {
//            console.log(err);
//            return;
//        }
//        if (stderr) {
//            console.log(stderr);
//            return;
//        }
//        cb(null, stdout);
//    });
}

var thread = 20;
if (cluster.isMaster) {
    if (max === undefined) {
        run(function (i) {
            clearInterval(d);
            var data = Math.floor(i / thread);
            var e = 0;
            var offset = 0;
            var end = 0;
            while (e !== thread) {
                e++;
                i -= data;
                end += data;
                handler(path, offset, end);
                offset += data;

            }
            if (i > 0) {
                handler(path, offset, end + i);
            }
        });
    } else {
        (function (i) {
            var data = Math.floor(i / thread);
            var e = 0;
            var offset = 0;
            var end = 0;
            while (e !== thread) {
                e++;
                i -= data;
                end += data;
                handler(path, offset, end);
                offset += data;

            }
            if (i > 0) {
                handler(path, offset, end + i);
            }
        })(max);
    }
}