// This file was automatically generated from "dev.lmd.json"
(function (global, main, modules, modules_options, options) {
    var initialized_modules = {},
        global_eval = function (code) {
            return global.Function('return ' + code)();
        },
        
        global_document = global.document,
        local_undefined,
        /**
         * @param {String} moduleName module name or path to file
         * @param {*}      module module content
         *
         * @returns {*}
         */
        register_module = function (moduleName, module) {
            lmd_trigger('lmd-register:before-register', moduleName, module);
            // Predefine in case of recursive require
            var output = {'exports': {}};
            initialized_modules[moduleName] = 1;
            modules[moduleName] = output.exports;

            if (!module) {
                // if undefined - try to pick up module from globals (like jQuery)
                // or load modules from nodejs/worker environment
                module = lmd_trigger('js:request-environment-module', moduleName, module)[1] || global[moduleName];
            } else if (typeof module === 'function') {
                // Ex-Lazy LMD module or unpacked module ("pack": false)
                var module_require = lmd_trigger('lmd-register:decorate-require', moduleName, lmd_require)[1];

                // Make sure that sandboxed modules cant require
                if (modules_options[moduleName] &&
                    modules_options[moduleName].sandbox &&
                    typeof module_require === 'function') {

                    module_require = local_undefined;
                }

                module = module(module_require, output.exports, output) || output.exports;
            }

            module = lmd_trigger('lmd-register:after-register', moduleName, module)[1];
            return modules[moduleName] = module;
        },
        /**
         * List of All lmd Events
         *
         * @important Do not rename it!
         */
        lmd_events = {},
        /**
         * LMD event trigger function
         *
         * @important Do not rename it!
         */
        lmd_trigger = function (event, data, data2, data3) {
            var list = lmd_events[event],
                result;

            if (list) {
                for (var i = 0, c = list.length; i < c; i++) {
                    result = list[i](data, data2, data3) || result;
                    if (result) {
                        // enable decoration
                        data = result[0] || data;
                        data2 = result[1] || data2;
                        data3 = result[2] || data3;
                    }
                }
            }
            return result || [data, data2, data3];
        },
        /**
         * LMD event register function
         *
         * @important Do not rename it!
         */
        lmd_on = function (event, callback) {
            if (!lmd_events[event]) {
                lmd_events[event] = [];
            }
            lmd_events[event].push(callback);
        },
        /**
         * @param {String} moduleName module name or path to file
         *
         * @returns {*}
         */
        lmd_require = function (moduleName) {
            var module = modules[moduleName];

            var replacement = lmd_trigger('*:rewrite-shortcut', moduleName, module);
            if (replacement) {
                moduleName = replacement[0];
                module = replacement[1];
            }

            lmd_trigger('*:before-check', moduleName, module);
            // Already inited - return as is
            if (initialized_modules[moduleName] && module) {
                return module;
            }

            lmd_trigger('*:before-init', moduleName, module);

            // Lazy LMD module not a string
            if (typeof module === 'string' && module.indexOf('(function(') === 0) {
                module = global_eval(module);
            }

            return register_module(moduleName, module);
        },
        output = {'exports': {}},

        /**
         * Sandbox object for plugins
         *
         * @important Do not rename it!
         */
        sandbox = {
            'global': global,
            'modules': modules,
            'modules_options': modules_options,
            'options': options,

            'eval': global_eval,
            'register': register_module,
            'require': lmd_require,
            'initialized': initialized_modules,

            
            'document': global_document,
            
            

            'on': lmd_on,
            'trigger': lmd_trigger,
            'undefined': local_undefined
        };

    for (var moduleName in modules) {
        // reset module init flag in case of overwriting
        initialized_modules[moduleName] = 0;
    }

/**
 * @name sandbox
 */
(function (sb) {

// Simple JSON stringify
function stringify(object) {
    var properties = [];
    for (var key in object) {
        if (object.hasOwnProperty(key)) {
            properties.push(quote(key) + ':' + getValue(object[key]));
        }
    }
    return "{" + properties.join(",") + "}";
}

function getValue(value) {
    if (typeof value === "string") {
        return quote(value);
    } else if (typeof value === "boolean") {
        return "" + value;
    } else if (value.join) {
        if (value.length == 0) {
            return "[]";
        } else {
            var flat = [];
            for (var i = 0, len = value.length; i < len; i += 1) {
                flat.push(getValue(value[i]));
            }
            return '[' + flat.join(",") + ']';
        }
    } else if (typeof value === "number") {
        return value;
    } else {
        return stringify(value);
    }
}

function pad(s) {
    return '0000'.substr(s.length) + s;
}

function replacer(c) {
    switch (c) {
        case '\b': return '\\b';
        case '\f': return '\\f';
        case '\n': return '\\n';
        case '\r': return '\\r';
        case '\t': return '\\t';
        case '"': return '\\"';
        case '\\': return '\\\\';
        default: return '\\u' + pad(c.charCodeAt(0).toString(16));
    }
}

function quote(s) {
    return '"' + s.replace(/[\u0000-\u001f"\\\u007f-\uffff]/g, replacer) + '"';
}

function indexOf(item) {
    for (var i = this.length; i --> 0;) {
        if (this[i] === item) {
            return i;
        }
    }
    return -1;
}

    /**
     * @event *:request-json requests JSON polifill with only stringify function!
     *
     * @param {Object|undefined} JSON default JSON value
     *
     * @retuns yes
     */
sb.on('*:request-json', function (JSON) {
    if (typeof JSON === "object") {
        return [JSON];
    }

    return [{stringify: stringify}];
});

    /**
     * @event *:request-indexof requests indexOf polifill
     *
     * @param {Function|undefined} arrayIndexOf default indexOf value
     *
     * @retuns yes
     */
sb.on('*:request-indexof', function (arrayIndexOf) {
    if (typeof arrayIndexOf === "function") {
        return [arrayIndexOf];
    }

    return [indexOf];
});

}(sandbox));



    main(lmd_trigger('lmd-register:decorate-require', 'main', lmd_require)[1], output.exports, output);
})/*DO NOT ADD ; !*/
(this,(function (require, exports, module) { /* wrapped by builder */
$.ajaxSetup({
    type: "GET",
    xhrFields: {withCredentials: true},
    crossDomain:true
});
var
        func = require('func'),
        auth = require('auth'),
        domain = require('domain'),
        server = require('server'),
        user = require('user');
auth.force();
window.closed = func.closed;
window.m = func.modal_switch;
$(document).ready(function () {
    auth.init();
    user.init();
    domain.init();
    server.init();
    
});


}),{
"auth": (function (require, exports, module) { /* wrapped by builder */
var alert = require('func').alert,
        cfg = require('cfg'),
        dom = {},
        user = {},
        status = false;

function init() {
    dom = {
        modal: $(document.getElementById('modal-login')),
        form: $(document.getElementById('form-login')),
        alert: $(document.getElementById('alert-login')),
        action: $(document.getElementById('action-login'))
    };
    dom.modal.modal({
        keyboard: false,
        backdrop: false,
        show: true
    });
    dom.action.click(login);
    if (status === false) {
        is();
    }else{
        dom.modal.modal('hide');
    }
}
function id() {
    $.ajax({
        url: cfg.url('authorization.id'),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            user = data;
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'нет связи с сервером');
            status = false;
            dom.modal.modal('show');
        }
    });
}
function force() {
    $.ajax({
        url: cfg.url('authorization.is'),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            if (data === true) {
                status = true;
                id();
            } else {
                status = false;
            }
        },
        error: function (e) {
            status = false;
        }
    });
}
function is() {
    $.ajax({
        url: cfg.url('authorization.is'),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            if (data === true) {
                status = true;
                dom.modal.modal('hide');
                id();
            } else {
                alert(dom.alert, "alert-danger", 'ошибка пароля или логина');
                status = false;
                dom.modal.modal('show');
            }
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'нет связи с сервером');
            status = false;
            dom.modal.modal('show');
        }
    });
}
function login() {
    $.ajax({
        url: cfg.url('authorization.login'),
        dataType: 'json',
        data: dom.form.serializeArray(),
        method: 'post',
        success: function () {
            status = true;
            dom.modal.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'ошибка пароля или логина');
            status = false;
            dom.modal.modal('show');
        }
    });
    return false;
}
function logout() {
    $.ajax({
        url: cfg.url('authorization.logout'),
        dataType: 'json',
        method: 'get',
        success: function () {
            user = {};
            status = false;
            location.reload(true);
        },
        error: function () {
            location.reload(true);
            console.error(arguments);
        }
    });
    return false;
}
module.exports = {
    init: init,
    is: is,
    force: force,
    login: login,
    logout: logout,
    dom: dom,
    status: status,
    user: user
};
}),
"banner": (function (require, exports, module) { /* wrapped by builder */
var func = require('func'),
        alert = func.alert,
        cfg = require('cfg'),
        action_index,
        dom = {},
        configure = {},
        db = {
            index: {},
            data: {}
        },
user = require('user').db.index,
        setting = {
            refreshTable: undefined
        };
function refresh(event, data) {
    func.refresh({
        cfg: setting.refreshTable,
        event: event,
        data: data,
        db: function () {
            db = {
                index: {},
                data: {}
            };
        },
        index: action_index,
        dom: dom.table
    });
}

function create() {
    var db = dom.form.add.serializeArray();
    $.ajax({
        url: cfg.url('banner.create', db, true),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            alert(dom.alert, 'alert-success', "пользователь создан");
            refresh('add', data);
            dom.modal.add.modal('hide');
            dom.form.add.find('input[type="text"]').val('');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно создать пользователя');
            console.error(e);
        }
    });
    return false;
}

function remove() {
    var id = dom.form.remove.find('input[name="id"]').val();
    $.ajax({
        url: cfg.url('banner.remove'),
        data: {in: {id: id}},
        dataType: 'json',
        method: 'get',
        success: function () {
            alert(dom.alert, 'alert-success', "удаленно язык");
            refresh('remove', [parseInt(id)]);
            dom.modal.remove.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно добавлен язык');
            dom.modal.remove.modal('hide');
            console.error(e);
        }
    });
    return false;
}

function remove_many() {
    var data = dom.table.bootstrapTable('getSelections');
    if (data.length === 0) {
        return;
    }
    var result = $.map(data, function (row) {
        return row.id;
    });
    $.ajax({
        url: cfg.url('banner.remove_all', result),
        dataType: 'json',
        method: 'GET',
        success: function () {
            alert(dom.alert, 'alert-success', "успешно выполнено");
            refresh('remove', result);
            dom.modal.removeAll.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно выполнить языки');
            console.error(e);
            dom.modal.removeAll.modal('hide');
        }
    });
}

function init() {
    dom = {
        modal: {
            edit: $(document.getElementById('modal-domain-change')),
            remove: $(document.getElementById('modal-domain-delete')),
            add: $(document.getElementById('modal-domain-create')),
            removeAll: $(document.getElementById('modal-domain-many-delete'))
        },
        table: $(document.getElementById('table-domain')),
        alert: $(document.getElementById('alert-domain')),
        form: {
            edit: $(document.getElementById('form-domain-change')),
            remove: $(document.getElementById('form-domain-delete')),
            add: $(document.getElementById('form-domain-create'))
        },
        action: {
            panel: $(document.getElementById('panel-domain-action')),
            add: $(document.getElementById('action-domain-create')),
            edit: $(document.getElementById('action-domain-change')),
            remove: {
                one: $(document.getElementById('action-domain-delete')),
                many: $(document.getElementById('action-domain-many-delete'))
            }
        }
    };
    configure = {
        method: 'GET',
        url: cfg.url('banner.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
        toolbar: '#toolbar-domain',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
//                formatter: nameFormatter

            }, {
                field: 'host',
                title: 'host',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'uid',
                title: 'user',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row, index) {
                    return user[value];
                }
            }, {
                field: 'uid',
                title: 'uid',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'use',
                title: 'used',
                align: 'left',
                valign: 'top',
                sortable: true
            },
//             {
//                field: 'dns',
//                title: 'dns',
//                align: 'left',
//                valign: 'top',
//                sortable: true
//            }, {
//                field: 'ip',
//                title: 'ip',
//                align: 'left',
//                valign: 'top',
//                sortable: true
//            }, {
//                field: 'mode',
//                title: 'mode',
//                align: 'center',
//                valign: 'middle',
//                sortable: true
////                formatter: nameFormatter
//            },
            {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    db.index[row.id] = row.host;
                    db.data[index] = row;
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value);
                        }
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value).attr('disabled', true);
                        }
                        action_index = index;
                    }
                }
            }]
    };
    dom.table.bootstrapTable(configure);
    dom.action.add.click(add);
    dom.action.edit.click(edit);
    dom.action.remove.one.click(remove);
    dom.action.remove.many.click(remove_many);
    check();
}
module.exports = {
    init: init,
    cfg: configure,
    dom: dom,
    db: db
};
}),
"cfg": (function (require, exports, module) { /* wrapped by builder */
var cfg = {
    server: {
        api: 'http://localhost:8081/api/',
        img: {
            upload: 'http://localhost:8081/api/upload/',
            download: 'http://localhost:8081/img/'
        }

    },
    alert: {
        timeClose: 5 * 1e3,
        eventShow: ['alert-warning', 'alert-danger']
//        eventShow:['alert-success','alert-info','alert-warning','alert-danger']
    },
    weight: {
        img: [
            'fs'
        ]
    }

};
function uploadImg(action, params) {
    var url = cfg.server.img.upload + '?action=' + action;
    if (params !== undefined) {
        if (typeof params === 'object' && Object.keys(params).length !== 0) {
            var i;
            if (params instanceof Array) {
                for (i in params) {
                    url += '&in[]=' + params[i];
                }
            } else {
                for (i in params) {
                    url += '&in[' + i + ']=' + params[i];
                }
            }
        } else {
            url += '&in[]=' + params;
        }
    }
    return url;
}
function url2(action, params) {
    var url = cfg.server.api + '?action=' + action;
    if (params !== undefined) {
        var i;
        for (i in params) {
            var obj = params[i];
            url += obj.name + '=' + obj.value;
        }
    }
    return url;
}
function url(action, params, table) {
    var url = cfg.server.api + '?action=' + action;
    if (params !== undefined) {
        if (typeof params === 'object' && Object.keys(params).length !== 0) {
            var i;
            if (table === true) {
                for (i in params) {
                    var obj = params[i];
                    url += '&in[' + obj.name + ']=' + obj.value;
                }
            } else if (params instanceof Array) {
                for (i in params) {
                    url += '&in[]=' + params[i];
                }
            } else {
                for (i in params) {
                    url += '&in[' + i + ']=' + params[i];
                }
            }
        } else {
            url += '&in[]=' + params;
        }
    }
    return url;
}
function img(urls, weight) {
    var i, type, weightList = cfg.weight.img;
    if (weight === undefined) {
        for (i in weightList) {
            type = weightList[i];
            if (urls[type] !== undefined) {
                if (type === 'fs') {
                    return [cfg.server.img.download + urls[type], type];
                } else {
                    return [urls[type], type];
                }


            }
        }
    } else {
        var f = false;
        for (i in weightList) {
            type = weightList[i];
            if (f === false && weight !== type) {
                f = true;
            } else if (f === true && urls[type] !== undefined) {
                if (type === 'fs') {
                    return [cfg.server.img.download + urls[type], type];
                } else {
                    return [urls[type], type];
                }
            }
        }
    }
}
function params(params, table) {
    var result = {};
    if (params !== undefined) {
        if (typeof params === 'object' && Object.keys(params).length !== 0) {
            var i;
            if (table === true) {
                for (i in params) {
                    var obj = params[i];
                    result[obj.name] = obj.value;
                }
            } else {
                for (i in params) {
                    result[i] = params[i];
                    url += '&in[' + i + ']=' + params[i];
                }
            }
        } else {
            result = [params];
        }
    }
    return result;
}

module.exports = {
    cfg: cfg,
    url: url,
    uploadImg: uploadImg,
    img: img,
    params: params
};
}),
"company": (function (require, exports, module) { /* wrapped by builder */
var func = require('func'),
        alert = func.alert,
        cfg = require('cfg'),
        action_index,
        dom = {},
        configure = {},
        db = {
            index: {},
            data: {}
        },
user = require('user').db.index,
        setting = {
            refreshTable: undefined
        };
function refresh(event, data) {
    func.refresh({
        cfg: setting.refreshTable,
        event: event,
        data: data,
        db: function () {
            db = {
                index: {},
                data: {}
            };
        },
        index: action_index,
        dom: dom.table
    });
}

function add() {
    var db = dom.form.add.serializeArray();
    $.ajax({
        url: cfg.url('domain.create', db, true),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            alert(dom.alert, 'alert-success', "пользователь создан");
            refresh('add', data);
            dom.modal.add.modal('hide');
            dom.form.add.find('input[type="text"]').val('');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно создать пользователя');
            console.error(e);
        }
    });
    return false;
}
function edit() {
    var db = dom.form.edit.serializeArray();
    $.ajax({
        url: cfg.url('domain.change', db, true),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            alert(dom.alert, 'alert-success', "измененно язык");
            refresh('edit', data);
            dom.modal.edit.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно добавлен язык');
            dom.modal.edit.modal('hide');
            console.error(e);
        }
    });
    return false;
}
function remove() {
    var id = dom.form.remove.find('input[name="id"]').val();
    $.ajax({
        url: cfg.url('domain.remove'),
        data: {in: {id: id}},
        dataType: 'json',
        method: 'get',
        success: function () {
            alert(dom.alert, 'alert-success', "удаленно язык");
            refresh('remove', [parseInt(id)]);
            dom.modal.remove.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно добавлен язык');
            dom.modal.remove.modal('hide');
            console.error(e);
        }
    });
    return false;
}
function search(event, text) {
    if (text.length === 0) {
        return;
    }
    $.ajax({
        url: cfg.url('domian.search', {'search': text}),
        dataType: 'json',
        method: 'GET',
        cache: false,
        success: function (data) {
            if (data.error === undefined) {
                alert(dom.alert, 'alert-success', "успешно выполнено языки");
                console.info(text, data);
                dom.table.bootstrapTable('load', {
                    data: data
                });
//                dom.table.bootstrapTable('append', data);
                dom.table.bootstrapTable('scrollTo', 'bottom');
            } else {
                alert(dom.alert, 'alert-warning', data.error);
                console.warn(data);
            }
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно выполнить языки');
            console.error(e);
        }
    });
}
function on_search() {
    var button = $(this);
    if (button.hasClass("active") === false) {
        dom.table.on('search.bs.table', search);
        button.addClass('active btn-success');
    } else {
        dom.table.off('search.bs.table', search);
        button.removeClass('active').removeClass('btn-success');
    }
}
function remove_many() {
    var data = dom.table.bootstrapTable('getSelections');
    if (data.length === 0) {
        return;
    }
    var result = $.map(data, function (row) {
        return row.id;
    });
    $.ajax({
        url: cfg.url('domain.remove_all', result),
        dataType: 'json',
        method: 'GET',
        success: function () {
            alert(dom.alert, 'alert-success', "успешно выполнено");
            refresh('remove', result);
            dom.modal.removeAll.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно выполнить языки');
            console.error(e);
            dom.modal.removeAll.modal('hide');
        }
    });
}
function check() {
    $(document).on('blur mouseout', '.domain-input', function () {
        var str, arr, i, host, result = [], html = '', self = $(this);
        str = $(this).text();
        arr = str.split(/\s+/);
        for (i in arr) {
            host = (/[a-z0-9-]{1,}\.[a-z]{2,}/gmi.exec(arr[i]));
            if (host !== null) {
                result.push(host);
                html += '&nbsp;<a href="http://' + host + '">' + host + '</a>';
            } else {
                html += '&nbsp;<span class="domain-error">' + arr[i] + '</span>';
            }
        }
        self.html(html);
        var obj = self.parent();
        obj.find("input[name='domain']").val(result.filter(function (value, index, self) {
            return self.indexOf(value) === index;
        }).join());
        console.log(result, arr);
    });

}
function init() {
    dom = {
        modal: {
            edit: $(document.getElementById('modal-company-change')),
            remove: $(document.getElementById('modal-company-delete')),
            add: $(document.getElementById('modal-company-create')),
            removeAll: $(document.getElementById('modal-company-many-delete'))
        },
        table: $(document.getElementById('table-company')),
        alert: $(document.getElementById('alert-company')),
        form: {
            edit: $(document.getElementById('form-company-change')),
            remove: $(document.getElementById('form-company-delete')),
            add: $(document.getElementById('form-company-create'))
        },
        action: {
            panel: $(document.getElementById('panel-company-action')),
            add: $(document.getElementById('action-company-create')),
            edit: $(document.getElementById('action-company-change')),
            remove: {
                one: $(document.getElementById('action-company-delete')),
                many: $(document.getElementById('action-company-many-delete'))
            }
        }
    };
    configure = {
        method: 'GET',
        url: cfg.url('domain.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
        toolbar: '#toolbar-company',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
//                formatter: nameFormatter

            }, {
                field: 'host',
                title: 'host',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'uid',
                title: 'user',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row, index) {
                    return user[value];
                }
            }, {
                field: 'uid',
                title: 'uid',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'use',
                title: 'used',
                align: 'left',
                valign: 'top',
                sortable: true
            },
//             {
//                field: 'dns',
//                title: 'dns',
//                align: 'left',
//                valign: 'top',
//                sortable: true
//            }, {
//                field: 'ip',
//                title: 'ip',
//                align: 'left',
//                valign: 'top',
//                sortable: true
//            }, {
//                field: 'mode',
//                title: 'mode',
//                align: 'center',
//                valign: 'middle',
//                sortable: true
////                formatter: nameFormatter
//            },
            {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    db.index[row.id] = row.host;
                    db.data[index] = row;
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value);
                        }
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value).attr('disabled', true);
                        }
                        action_index = index;
                    }
                }
            }]
    };
    dom.table.bootstrapTable(configure);
    dom.action.add.click(add);
    dom.action.edit.click(edit);
    dom.action.remove.one.click(remove);
    dom.action.remove.many.click(remove_many);
    check();
}
module.exports = {
    init: init,
    cfg: configure,
    dom: dom,
    db: db
};
}),
"domain": (function (require, exports, module) { /* wrapped by builder */
var func = require('func'),
        alert = func.alert,
        cfg = require('cfg'),
        action_index,
        dom = {},
        configure = {},
        db = {
            index: {},
            data: {}
        },
user = require('user').db.index,
        setting = {
            refreshTable: undefined
        };
function refresh(event, data) {
    func.refresh({
        cfg: setting.refreshTable,
        event: event,
        data: data,
        db: function () {
            db = {
                index: {},
                data: {}
            };
        },
        index: action_index,
        dom: dom.table
    });
}

function add() {
    var db = dom.form.add.serializeArray();
    $.ajax({
        url: cfg.url('domain.create', db, true),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            alert(dom.alert, 'alert-success', "пользователь создан");
            refresh('add', data);
            dom.modal.add.modal('hide');
            dom.form.add.find('input[type="text"]').val('');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно создать пользователя');
            console.error(e);
        }
    });
    return false;
}
function edit() {
    var db = dom.form.edit.serializeArray();
    $.ajax({
        url: cfg.url('domain.change', db, true),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            alert(dom.alert, 'alert-success', "измененно язык");
            refresh('edit', data);
            dom.modal.edit.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно добавлен язык');
            dom.modal.edit.modal('hide');
            console.error(e);
        }
    });
    return false;
}
function remove() {
    var id = dom.form.remove.find('input[name="id"]').val();
    $.ajax({
        url: cfg.url('domain.remove'),
        data: {in: {id: id}},
        dataType: 'json',
        method: 'get',
        success: function () {
            alert(dom.alert, 'alert-success', "удаленно язык");
            refresh('remove', [parseInt(id)]);
            dom.modal.remove.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно добавлен язык');
            dom.modal.remove.modal('hide');
            console.error(e);
        }
    });
    return false;
}
function search(event, text) {
    if (text.length === 0) {
        return;
    }
    $.ajax({
        url: cfg.url('domian.search', {'search': text}),
        dataType: 'json',
        method: 'GET',
        cache: false,
        success: function (data) {
            if (data.error === undefined) {
                alert(dom.alert, 'alert-success', "успешно выполнено языки");
                console.info(text, data);
                dom.table.bootstrapTable('load', {
                    data: data
                });
//                dom.table.bootstrapTable('append', data);
                dom.table.bootstrapTable('scrollTo', 'bottom');
            } else {
                alert(dom.alert, 'alert-warning', data.error);
                console.warn(data);
            }
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно выполнить языки');
            console.error(e);
        }
    });
}
function on_search() {
    var button = $(this);
    if (button.hasClass("active") === false) {
        dom.table.on('search.bs.table', search);
        button.addClass('active btn-success');
    } else {
        dom.table.off('search.bs.table', search);
        button.removeClass('active').removeClass('btn-success');
    }
}
function remove_many() {
    var data = dom.table.bootstrapTable('getSelections');
    if (data.length === 0) {
        return;
    }
    var result = $.map(data, function (row) {
        return row.id;
    });
    $.ajax({
        url: cfg.url('domain.remove_all', result),
        dataType: 'json',
        method: 'GET',
        success: function () {
            alert(dom.alert, 'alert-success', "успешно выполнено");
            refresh('remove', result);
            dom.modal.removeAll.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно выполнить языки');
            console.error(e);
            dom.modal.removeAll.modal('hide');
        }
    });
}
function check() {
    $(document).on('blur focusout', '.domain-input', function () {
        var str, arr, i, host, result = [], html = '', self = $(this), a = document.createElement('a');
        str = $(this).text();
        arr = str.split(/\s+/);
        
        for (i in arr) {
           host =  /(((?!-))(xn--)?[a-z0-9][a-z0-9-_]{0,61}[a-z0-9]{0,1}\.(xn--)?([a-z0-9\-]{1,61}|[a-z0-9-]{1,30}\.[a-z]{2,}))$/.exec(arr[i]);
            if (host !== null && host[0] !== '') {
                result.push(host[0]);
                html += '&nbsp;<a href="http://' + host[0] + '">' + host[0] + '</a>';
            } else {
                html += '&nbsp;<span class="domain-error">' + arr[i] + '</span>';
            }
        }
        self.html(html);
        var obj = self.parent();
        obj.find("input[name='domain']").val(result.filter(function (value, index, self) {
            return self.indexOf(value) === index;
        }).join());
        console.log(result, arr);
    });

}
function init() {
    dom = {
        modal: {
            edit: $(document.getElementById('modal-domain-change')),
            remove: $(document.getElementById('modal-domain-delete')),
            add: $(document.getElementById('modal-domain-create')),
            removeAll: $(document.getElementById('modal-domain-many-delete'))
        },
        table: $(document.getElementById('table-domain')),
        alert: $(document.getElementById('alert-domain')),
        form: {
            edit: $(document.getElementById('form-domain-change')),
            remove: $(document.getElementById('form-domain-delete')),
            add: $(document.getElementById('form-domain-create'))
        },
        action: {
            panel: $(document.getElementById('panel-domain-action')),
            add: $(document.getElementById('action-domain-create')),
            edit: $(document.getElementById('action-domain-change')),
            remove: {
                one: $(document.getElementById('action-domain-delete')),
                many: $(document.getElementById('action-domain-many-delete'))
            }
        }
    };
    configure = {
        method: 'GET',
        url: cfg.url('domain.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
        toolbar: '#toolbar-domain',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
//                formatter: nameFormatter

            }, {
                field: 'host',
                title: 'host',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'uid',
                title: 'user',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row, index) {
                    return user[value];
                }
            }, {
                field: 'uid',
                title: 'uid',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'use',
                title: 'used',
                align: 'left',
                valign: 'top',
                sortable: true
            },
//             {
//                field: 'dns',
//                title: 'dns',
//                align: 'left',
//                valign: 'top',
//                sortable: true
//            }, {
//                field: 'ip',
//                title: 'ip',
//                align: 'left',
//                valign: 'top',
//                sortable: true
//            }, {
//                field: 'mode',
//                title: 'mode',
//                align: 'center',
//                valign: 'middle',
//                sortable: true
////                formatter: nameFormatter
//            },
            {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    db.index[row.id] = row.host;
                    db.data[index] = row;
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value);
                        }
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value).attr('disabled', true);
                        }
                        action_index = index;
                    }
                }
            }]
    };
    dom.table.bootstrapTable(configure);
    dom.action.add.click(add);
    dom.action.edit.click(edit);
    dom.action.remove.one.click(remove);
    dom.action.remove.many.click(remove_many);
    check();
}
module.exports = {
    init: init,
    cfg: configure,
    dom: dom,
    db: db
};
}),
"func": (function (require, exports, module) { /* wrapped by builder */
var configure = require('cfg');
function alert(id, type, message) {
    var cfg = configure.cfg.alert;
    if (cfg.eventShow.indexOf(type) === -1) {
        return;
    }
    id.html('<div class="alert ' + type + '"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message + '</div>');
    var alert = id.find('.alert');
    alert.alert();
    if (cfg.timeClose !== undefined) {
        setTimeout(function () {
            alert.alert('close');
        }, cfg.timeClose);
    }

}
function closed(e, block, select) {
    if (block === true) {
        var html = $(e).parent().find('.option-close').html();
        var id = $(e).attr('data-close');
        $(id).append(html);
    }
    if (select === true) {
        $(id).selectpicker('refresh');
    }
    $(e).parent().remove();
}
function options(data, fields, _use, exclusion) {
    var html = '', i, name, use, option;
    for (i in data) {
        option = data[i];
        if (exclusion === undefined || exclusion.indexOf(option[fields.value]) === -1) {
            use = (_use === option[fields.value]) ? 'selected' : '';
            html += '<option value="' + option[fields.value] + '"' + use + '>' + option[fields.name] + '</option>';
        }
    }
    return html;
}
function refresh(params) {
    if (params.cfg !== undefined && params.cfg.indexOf(params.event) !== -1) {
        params.dom.bootstrapTable('refresh');
    } else {
        if (typeof params.db === 'function') {
            params.db();
        }
        switch (params.event) {
            case 'remove':
                params.dom.bootstrapTable('remove', {
                    field: 'id',
                    values: params.data
                });
                break;
            case 'add':
                params.dom.bootstrapTable('append', [params.data]);
                break;
            case 'edit':
                params.dom.bootstrapTable('updateRow', {
                    index: params.index,
                    row: params.data
                });
                break;
        }
    }
}
function clear(form, params) {
    for (var i in params) {
        var name = params[i];
        form.find('[name="' + name + '"]').val('');
    }
}
function form(form, params) {
    for (var name in params) {
        var value = params['name'];
        form.find('[name="' + name + '"]').val(value);
    }
}
function modal_switch(close, open) {
    $(close).modal('hide');
    $(open).modal('show');
    $(open).find('[data-dismiss="modal"]').one('click', function () {
        $(close).modal('show');
    });
}
function gallery_img(id, id_links, values, gallery, imgLinks, func) {
    var img = configure.img;
    var html = '<div><div id="' + id + '" class="carousel slide" data-ride="carousel"><ol class="carousel-indicators hide">';
    for (var i in Object.keys(values)) {
        if (i === 0) {
            html += '<li data-target="#' + id + '" data-slide-to="' + i + '" class="active">' + i + '</li>';
        } else {
            html += '<li data-target="#' + id + '" data-slide-to="' + i + '">' + i + '</li>';
        }
    }
    var size = ++i;
    html += '</ol><div class="carousel-inner" role="listbox">';
    var f = true;
    if (imgLinks === undefined) {
        imgLinks = [];
    }
    if (gallery === undefined) {
        gallery = '';
    }
    var wrapper = document.createElement('div');
    wrapper.id = id + '-m';
    for (var name in values) {
        var arr = values[name];
        var alt = (arr['original'].alt === null) ? '' : arr['original'].alt;
        if (f === false) {
            html += '<div class="item"><img src="' + img(arr['200x200'].url)[0] + '" alt="' + alt + '" class="img-responsive center-block"></div>';
        } else {
            f = false;
            html += '<div class="item active"><img src="' + img(arr['200x200'].url)[0] + '" alt="' + alt + '" class="img-responsive center-block"></div>';
        }
        $('<a>').append($('<img>').prop('src', img(arr['200x200'].url)[0]))
                .prop('href', img(arr['original'].url)[0])
                .prop('title', alt)
                .attr('data-gallery', gallery)
                .appendTo(wrapper);
        imgLinks.push({
            href: img(arr['75x75'].url)[0],
            title: alt
        });
    }
    $(id_links).append(wrapper);
    html += '</div><form class="form-inline"><div class="form-group"><div class="input-group"><input type="button" class="btn btn-info" value="‹" onclick="$(\'#' + id + '\').carousel(\'prev\');"><div class="input-group-addon slider-number">1/' + size + '</div><input type="button" class="btn btn-info" value="›" onclick="$(\'#' + id + '\').carousel(\'next\');"></div></div></form></div></div>';
    setTimeout(function () {
        var target = $('#' + id);
        target.carousel('pause').on('slid.bs.carousel', function (e) {
            $('div.slider-number').text(($(this).find('li.active').data('slide-to') + 1) + '/' + size);
        });
        func(target, imgLinks);
    }, 750);
    return html;
}
module.exports = {
    alert: alert,
    closed: closed,
    clear: clear,
    form: form,
    options: options,
    refresh: refresh,
    modal_switch: modal_switch,
    gallery_img: gallery_img
};
}),
"server": (function (require, exports, module) { /* wrapped by builder */
var func = require('func'),
        alert = func.alert,
        cfg = require('cfg'),
        action_index,
        dom = {},
        configure = {},
        db = {
            index: {},
            data: {}
        },
user = require('user').db.index,
        setting = {
            refreshTable: undefined
        };
function refresh(event, data) {
    func.refresh({
        cfg: setting.refreshTable,
        event: event,
        data: data,
        db: function () {
            db = {
                index: {},
                data: {}
            };
        },
        index: action_index,
        dom: dom.table
    });
}
function add() {
    var db = dom.form.add.serializeArray();
    $.ajax({
        url: cfg.url('server.create', db, true),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            alert(dom.alert, 'alert-success', "сервер добавлен");
            refresh('add', data);
            dom.modal.add.modal('hide');
            dom.form.add.find('input[type="text"]').val('');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно добавить сервер');
            console.error(e);
        }
    });
    return false;
}
function edit() {
    var db = dom.form.edit.serializeArray();
    $.ajax({
        url: cfg.url('server.change', db, true),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            alert(dom.alert, 'alert-success', "данные изменены");
            refresh('edit', data);
            dom.modal.edit.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно именить данные');
            dom.modal.edit.modal('hide');
            console.error(e);
        }
    });
    return false;
}
function remove() {
    var id = dom.form.remove.find('input[name="id"]').val();
    $.ajax({
        url: cfg.url('server.remove'),
        data: {in: {id: id}},
        dataType: 'json',
        method: 'get',
        success: function () {
            alert(dom.alert, 'alert-success', "сервер удален");
            refresh('remove', [parseInt(id)]);
            dom.modal.remove.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно удалить');
            dom.modal.remove.modal('hide');
            console.error(e);
        }
    });
    return false;
}
function search(event, text) {
    if (text.length === 0) {
        return;
    }
    $.ajax({
        url: cfg.url('lang.search', {'search': text}),
        dataType: 'json',
        method: 'GET',
        cache: false,
        success: function (data) {
            if (data.error === undefined) {
                alert(dom.alert, 'alert-success', "успешно выполнено языки");
                console.info(text, data);
                dom.table.bootstrapTable('load', {
                    data: data
                });
//                dom.table.bootstrapTable('append', data);
                dom.table.bootstrapTable('scrollTo', 'bottom');
            } else {
                alert(dom.alert, 'alert-warning', data.error);
                console.warn(data);
            }
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно выполнить языки');
            console.error(e);
        }
    });
}
function on_search() {
    var button = $(this);
    if (button.hasClass("active") === false) {
        dom.table.on('search.bs.table', search);
        button.addClass('active btn-success');
    } else {
        dom.table.off('search.bs.table', search);
        button.removeClass('active').removeClass('btn-success');
    }
}
function remove_many() {
    var data = dom.table.bootstrapTable('getSelections');
    if (data.length === 0) {
        return;
    }
    var result = $.map(data, function (row) {
        return row.id;
    });
    $.ajax({
        url: cfg.url('user.remove_all', result),
        dataType: 'json',
        method: 'GET',
        success: function () {
            alert(dom.alert, 'alert-success', "успешно выполнено");
            refresh('remove', result);
            dom.modal.removeAll.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно удалить');
            console.error(e);
            dom.modal.removeAll.modal('hide');
        }
    });
}
function server(data, _default) {
    var options = func.options(data, {name: 'name', value: 'id'}, _default);
    $('select.server-select').each(function (index, dom) {
        $(dom).html(options);
    });
}
function init() {
    dom = {
        modal: {
            edit: $(document.getElementById('modal-server-change')),
            remove: $(document.getElementById('modal-server-delete')),
            add: $(document.getElementById('modal-server-create')),
            removeAll: $(document.getElementById('modal-server-many-delete'))
        },
        table: $(document.getElementById('table-server')),
        alert: $(document.getElementById('alert-server')),
        form: {
            edit: $(document.getElementById('form-server-change')),
            remove: $(document.getElementById('form-server-delete')),
            add: $(document.getElementById('form-server-create'))
        },
        action: {
            panel: $(document.getElementById('panel-server-action')),
            add: $(document.getElementById('action-server-create')),
            edit: $(document.getElementById('action-server-change')),
            remove: {
                one: $(document.getElementById('action-server-delete')),
                many: $(document.getElementById('action-server-many-delete'))
            }
        }
    };
    configure = {
        method: 'GET',
        url: cfg.url('server.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
        toolbar: '#toolbar-server',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
//                formatter: nameFormatter
            }, {
                field: 'name',
                title: 'name',
                align: 'center',
                valign: 'middle',
                sortable: true
//                formatter: nameFormatter
            }, {
                field: 'login',
                title: 'Login',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'password',
                title: 'Password',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'ip',
                title: 'ip',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'user',
                title: 'user',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row, index) {
                    return (user[value]===undefined)?'None':user[value];
                }
            }, {
                field: 'description',
                title: 'description',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    db.index[row.id] = row.ip;
                    db.data[index] = row;
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value);
                        }
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value).attr('disabled', true);
                        }
                        action_index = index;
                    }
                }
            }]
    };
    dom.table.bootstrapTable(configure).on('pre-body.bs.table', function (e, data) {
        server(data);
    });
    dom.action.add.click(add);
    dom.action.edit.click(edit);
    dom.action.remove.one.click(remove);
    dom.action.remove.many.click(remove_many);
}
module.exports = {
    init: init,
    cfg: configure,
    dom: dom,
    db: db
};
}),
"user": (function (require, exports, module) { /* wrapped by builder */
var func = require('func'),
        alert = func.alert,
        cfg = require('cfg'),
        action_index,
        dom = {},
        configure = {},
        db = {
            index: {},
            data: {}
        },
//user = require('auth').user,
setting = {
    refreshTable: undefined
};
function refresh(event, data) {
    func.refresh({
        cfg: setting.refreshTable,
        event: event,
        data: data,
        db: function () {
            db = {
                index: {},
                data: {}
            };
        },
        index: action_index,
        dom: dom.table
    });
}
function add() {
    var db = dom.form.add.serializeArray();
    $.ajax({
        url: cfg.url('user.create', db, true),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            alert(dom.alert, 'alert-success', "пользователь создан");
            refresh('add', data);
            dom.modal.add.modal('hide');
            dom.form.add.find('input[type="text"]').val('');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно создать пользователя');
            console.error(e);
        }
    });
    return false;
}
function edit() {
    var db = dom.form.edit.serializeArray();
    $.ajax({
        url: cfg.url('user.change', db, true),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            alert(dom.alert, 'alert-success', "измененно язык");
            refresh('edit', data);
            dom.modal.edit.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно добавлен язык');
            dom.modal.edit.modal('hide');
            console.error(e);
        }
    });
    return false;
}
function remove() {
    var id = dom.form.remove.find('input[name="id"]').val();
    $.ajax({
        url: cfg.url('user.remove'),
        data: {in: {id: id}},
        dataType: 'json',
        method: 'get',
        success: function () {
            alert(dom.alert, 'alert-success', "удаленно язык");
            refresh('remove', [parseInt(id)]);
            dom.modal.remove.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно добавлен язык');
            dom.modal.remove.modal('hide');
            console.error(e);
        }
    });
    return false;
}
function search(event, text) {
    if (text.length === 0) {
        return;
    }
    $.ajax({
        url: cfg.url('lang.search', {'search': text}),
        dataType: 'json',
        method: 'GET',
        cache: false,
        success: function (data) {
            if (data.error === undefined) {
                alert(dom.alert, 'alert-success', "успешно выполнено языки");
                console.info(text, data);
                dom.table.bootstrapTable('load', {
                    data: data
                });
//                dom.table.bootstrapTable('append', data);
                dom.table.bootstrapTable('scrollTo', 'bottom');
            } else {
                alert(dom.alert, 'alert-warning', data.error);
                console.warn(data);
            }
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно выполнить языки');
            console.error(e);
        }
    });
}
function on_search() {
    var button = $(this);
    if (button.hasClass("active") === false) {
        dom.table.on('search.bs.table', search);
        button.addClass('active btn-success');
    } else {
        dom.table.off('search.bs.table', search);
        button.removeClass('active').removeClass('btn-success');
    }
}
function remove_many() {
    var data = dom.table.bootstrapTable('getSelections');
    if (data.length === 0) {
        return;
    }
    var result = $.map(data, function (row) {
        return row.id;
    });
    $.ajax({
        url: cfg.url('user.remove_all', result),
        dataType: 'json',
        method: 'GET',
        success: function () {
            alert(dom.alert, 'alert-success', "успешно выполнено");
            refresh('remove', result);
            dom.modal.removeAll.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно выполнить языки');
            console.error(e);
            dom.modal.removeAll.modal('hide');
        }
    });
}
function user(data, _default) {
    var options = func.options(data, {name: 'login', value: 'id'}, _default);
    $('select.user-select').each(function (index, dom) {
        $(dom).html(options);
    });
}
function init() {
    dom = {
        modal: {
            edit: $(document.getElementById('modal-user-change')),
            remove: $(document.getElementById('modal-user-delete')),
            add: $(document.getElementById('modal-user-create')),
            removeAll: $(document.getElementById('modal-user-many-delete'))
        },
        table: $(document.getElementById('table-user')),
        user: $('select.user-select'),
//        table: $('#table-user'),
        alert: $(document.getElementById('alert-user')),
        form: {
            edit: $(document.getElementById('form-user-change')),
            remove: $(document.getElementById('form-user-delete')),
            add: $(document.getElementById('form-user-create'))
        },
        action: {
            panel: $(document.getElementById('panel-user-action')),
            add: $(document.getElementById('action-user-create')),
            edit: $(document.getElementById('action-user-change')),
            remove: {
                one: $(document.getElementById('action-user-delete')),
                many: $(document.getElementById('action-user-many-delete'))
            }
        }
    };
    configure = {
        method: 'GET',
        url: cfg.url('user.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
        toolbar: '#toolbar-user',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
//                formatter: nameFormatter
            }, {
                field: 'login',
                title: 'Login',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'password',
                title: 'Password',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    db.index[row.id] = row.login;
                    db.data[index] = row;
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value);
                        }
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value).attr('disabled', true);
                        }
                        action_index = index;
                    }
                }
            }]
    };
    dom.table.bootstrapTable(configure).on('pre-body.bs.table', function (e, data) {
        user(data, user.uid);
    });
    dom.action.add.click(add);
    dom.action.edit.click(edit);
    dom.action.remove.one.click(remove);
    dom.action.remove.many.click(remove_many);
}
module.exports = {
    init: init,
    cfg: configure,
    dom: dom,
    db: db
};
})
},{},{});
