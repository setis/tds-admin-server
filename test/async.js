/**
 * Created by alex on 17.02.16.
 */

var async = require('async');

async.waterfall([
    function (callback) {
        console.log('async.waterfall err');
        callback(new Error('api test'), 'success');
    },
    function (url, callback) {
        console.log('async.waterfall err args', url);
        callback(null, 'done');
    }], function (err, result) {
    console.log('async.waterfall err', err, result);
});
async.waterfall([
    function (callback) {
        console.log('async.waterfall');
        callback(null, 'success');
    },
    function (url, callback) {
        console.log('async.waterfall args', url);
        callback(null, 'done');
    }], function (err, result) {
    console.log('async.waterfall', err, result);
});