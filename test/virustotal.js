var cfg = require('../cfg/general.json'),
        virustotal = require('virustotal.js'),
        mongoose = require('mongoose'),
        autoIncrement = require('mongoose-auto-increment'),
        fs = require('fs'),
        path = require('path'),
        clients = require('../kernel/clients'),
        thread = {
            virustotal: cfg.thread || 20
        },
poll = {
    virustotal: []
},
models = {
    mongoose: {},
    orm: {}
},
db = {
    mongoose: null,
    orm: require("orm").connect(cfg.orm)
};

var dir_odm = path.normalize(path.join(__dirname, '../models/odm'));
clients.mongoose(cfg.mongoose, db.mongoose, undefined, function (connection) {
    autoIncrement.initialize(connection);
    fs.readdir(dir_odm, function (err, result) {
        if (err) {
            console.error(err);
        }
        var i, link;
        for (i in result) {
            link = path.join(dir_odm, result[i]);
            try {
                require(link)(mongoose, models.mongoose, {autoIncrement: autoIncrement});
            } catch (e) {
                console.error('not load models odm: ' + link);
                throw e;

            }
            console.info('load models odm: ', link);
        }
    });
});
var dir_result = path.normalize(path.join(__dirname, '/result/virustotal')) + '/';
virustotal.setKey('381c1bbd19bd226e5d2fcfb38274f2142ae5745787da66da5419c57c6d1c0020');
//virustotal.getDomainReport('mail.ru', function (err, result) {
//    console.log(err);
//    fs.writeFile(dir_result + 'domain.json', JSON.stringify(result), 'utf8', function (err) {
//        console.log(err);
//    });
//});
//virustotal.scanUrl('http://mail.ru', function (err, result) {
//    console.log(err, result);
//    virustotal.getUrlReport(
//            result.resource,
//            function (err, result) {
//                if (err) {
//                    console.error(err);
//                    return;
//                }
//                fs.writeFile(dir_result + 'Resource.json', JSON.stringify(result), 'utf8', function (err) {
//                    console.log(err);
//                });
//            }
//    );
//    fs.writeFile(dir_result + 'url.json', JSON.stringify(result), 'utf8', function (err) {
//        console.log(err);
//    });
//});
virustotal.getIpReport('93.171.133.110', function (err, result) {
    console.log(err);
    fs.writeFile(dir_result + 'ip.json', JSON.stringify(result), 'utf8', function (err) {
        console.log(err);
    });
});
//setTimeout(function () {
//    process.exit(1);
//}, 5e3);