var cfg = function () {
    var c = require('../cfg/cron.json'),
            cfg = require('../cfg/general.json');
    for (var i in c) {
        cfg[i] = c[i];
    }
    return cfg;
}(),
        virustotal = require('virustotal.js'),
        mongoose = require('mongoose'),
        request = require('request'),
        autoIncrement = require('mongoose-auto-increment'),
        fs = require('fs'),
        path = require('path'),
        clients = require('../kernel/clients'),
        thread = {
            virustotal: cfg.thread || 20
        },
poll = {
    virustotal: []
},
models = {
    mongoose: {},
    orm: {}
},
db = {
    mongoose: null,
    orm: require("orm").connect(cfg.orm)
};

var dir_odm = path.normalize(path.join(__dirname, '../models/odm'));
clients.mongoose(cfg.mongoose, db.mongoose, undefined, function (connection) {
    autoIncrement.initialize(connection);
    fs.readdir(dir_odm, function (err, result) {
        if (err) {
            console.error(err);
        }
        var i, link;
        for (i in result) {
            link = path.join(dir_odm, result[i]);
            try {
                require(link)(mongoose, models.mongoose, {autoIncrement: autoIncrement});
            } catch (e) {
                console.error('not load models odm: ' + link);
                throw e;

            }
//            console.info('load models odm: ', link);
        }
    });
});
var avcheck = cfg.avcheck;
var dir_result = path.normalize(path.join(__dirname, '/result/avcheck')) + '/';
var i = 2;
request.post({
    url: avcheck.server,
    form: {
        api_key: avcheck.key,
        check_type: 'domain',
        data: 'ya.ru'
    }
}, function (err, httpResponse, body) {
    if (err) {
        return console.error('upload failed:', err);
    }
    console.log(body);
    fs.writeFile(dir_result + 'domain.json', body, 'utf8', function (err) {
        console.log(err);
        i--;
    });
});
request.post({
    url: avcheck.server,
    form: {
        api_key: avcheck.key,
        check_type: 'domain',
        data: '213.180.204.3'
    }
}, function (err, httpResponse, body) {
    if (err) {
        return console.error('upload failed:', err);
    }
    console.log(body);
    fs.writeFile(dir_result + 'ip.json', body, 'utf8', function (err) {
        console.log(err);
        i--;
    });
});

setInterval(function () {
    if (i === 0) {
        process.exit(1);
    }
}, 1e3);