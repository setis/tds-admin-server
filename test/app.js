var express = require('express'),
        app = express();
var r1 = express.Router();
r1.get('/', function (req, res, next) {
    console.log('1');
    req.app.use(r2);
    next();
});

var r2 = express.Router();
r2.get('/', function (req, res, next) {
    console.log('2');
    next();
});

app.use('/', r1);
app.use(function (req, res, next) {
    res.end();
});
app.listen(3333, '127.0.0.1', function () {
    console.log('worker:%d server listening on http://127.0.0.1:3333 ');
});