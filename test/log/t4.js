var hub = require('mag-hub');

// Formatters
var info = require('mag-process-info');
var format = require('mag-format-message');
var colored = require('mag-colored-output');
var logger = require('mag')('example');
logger.info('example message', {meta: 'some metadata'});
logger.log('something to log');
hub.pipe(info())
  .pipe(format())
  .pipe(colored())
  .pipe(process.stdout);