var cfg = {
    ttl: 3, period: 5
};
var NodeCache = require("node-cache"),
        cache = new NodeCache({stdTTL: cfg.ttl, checkperiod: cfg.period});
cache.get('asdasd', function (err, result) {
    if (err) {
        throw err;
    }
    console.log('not found value ', result);
});
cache.set(1, ['sd', 'dd', 1, 2, 5], function (err) {
    if (err) {
        throw err;
    }
    cache.ttl(1, function () {
        console.log('get ttl:', 1, arguments);
        cache.ttl(1, cfg.ttl, function () {
            console.log('set ttl:', 1, arguments);
            cache.ttl(1, function () {
                console.log('get ttl:', 1, arguments);
            });
        });
    });
});
cache.set(2, ['sd', 'dd', 1, 2, 5], function (err) {
    if (err) {
        throw err;
    }
});
cache.on("expired", function (key, value) {
    console.log("expired", key, value);
    cache.get(key,function(){
        console.log('get:', key, arguments);
    });
});
