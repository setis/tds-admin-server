module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
            model = new Schema({
                host: {type: String, required: true, lowercase: true, unique: true},
                token: {type: String},
                avcheck: {
                    total: {type: Number, default: 0},
                    detect: {type: Number, default: 0},
                    time: {type: Date, default: Date.now},
                    scan: {type: Schema.Types.Mixed},
                    link: {type: String},
                    error: {type: Schema.Types.Mixed, default: null}
                },
                alive: {
                    mode: {type: Boolean, default: false},
                    time: {type: Date, default: Date.now}
                },
                www: {
                    mode: {type: Boolean, default: false},
                    time: {type: Date, default: Date.now}
                },
                ip: {
                    ip: {type: String, default: null},
                    time: {type: Date, default: Date.now}
                },
                history: {
                    create: {type: Date, default: Date.now},
                    remove: {type: Date, default: null}
                }
            });

    models['shortUrl'] = connection('static').model('shortUrl', model);
};