module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
            model = new Schema({
                key: {type: String, required: true, unique: true},
                time: {type: Date, default: Date.now},
                hash: {type: String},
                company: {type: Number, index: true},
                i: {type: Number},
                size: {type: Number},
                short: {type: String},
                front: {type: String},
                layer: {type: String}
            });
    models['layer'] = connection('isp').model('layer', model);
};