module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
            model = new Schema({
                ip: {type: String, required: true, unique: true},
                uid: {type: Number, index: true, default: 0},
                sid: {
                    id: {type: Number, ref: 'sid', unique: true},
                    key: {type: Schema.ObjectId, unique: true},
                    name: {type: String}
                },
                avcheck: {
                    total: {type: Number, default: 0},
                    detect: {type: Number, default: 0},
                    time: {type: Date, default: Date.now},
                    scan: {type: Schema.Types.Mixed},
                    link: {type: String},
                    error: {type: Schema.Types.Mixed, default: null}
                },
                virustotal: {type: Schema.Types.Mixed},
                alive: {
                    mode: {type: Boolean, default: false},
                    time: {type: Date, default: Date.now}
                },
                www: {
                    mode: {type: Boolean, default: false},
                    time: {type: Date, default: Date.now}
                },
                dnsbl: {
                    total: {type: Number},
                    detect: {type: Number},
                    time: {type: Date, default: Date.now},
                    result: {type: Schema.Types.Mixed}
                },
                history: {
                    create: {type: Date, default: Date.now},
                    remove: {type: Date, default: null}
                }

            }, {autoIndex: true});
//    model.methods.ips = function(){
//        
//    };
    models['ip'] = connection('static').model('ip', model);

};