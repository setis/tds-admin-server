module.exports = function (mongoose, connection, models) {
    var Schema = mongoose.Schema,
            model = new Schema({
                company: {type: Number, required: true, index: true},
                table: {type: String, required: true, index: true},
                id: {type: Number,default:0},
                time: {type: Date, default: Date.now}
            });
    return models['counter'] = connection('static').model('counter', model);

};