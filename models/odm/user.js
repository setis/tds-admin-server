module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
            model = new Schema({
                login: {type: String, required: true, unique: true},
                password: {type: String, required: true, min: 6, max: 128},
                id: {type: Number, ref: 'id', unique: true},
                mode: {type: Boolean, default: true},
                history: {
                    create: {type: Date, default: Date.now},
                    remove: {type: Date, default: null},
                    mode: [
                        {
                            mode: {type: Boolean},
                            date: {type: Date, default: Date.now}
                        }
                    ]
                }
            });
    model.plugin(plugins.autoIncrement.plugin, {
        model: 'user',
        field: 'id',
        startAt: 1
    });
    models['user'] = connection('static').model('user', model);
};