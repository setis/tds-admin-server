module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
        model = new Schema({
            uid: {type: Number, default: 0},
            id: {type: Number, unique: true},
            ip: {
                main: {type: String, required: true, unique: true},
                addition: [
                    {type: String}
                ]
            },
            name: {type: String, unique: true},
            login: {type: String, default: 'root'},
            password: {type: String, default: null},
            description: {type: String},
            alive: {
                mode: {type: Boolean, default: false},
                time: {type: Date, default: Date.now}
            },
            ssh: {
                mode: {type: Boolean, default: false},
                time: {type: Date, default: Date.now},
                result: {type: Schema.Types.Mixed, default: null}
            },
            history: {
                create: {type: Date, default: Date.now},
                remove: {type: Date, default: null}
            }

        }, {autoIndex: true});
    model.plugin(plugins.autoIncrement.plugin, {
        model: 'server',
        field: 'id',
        startAt: 1
    });
    models['server'] = connection('static').model('server', model);
};