module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
            model = new Schema({
                login: {type: String, required: true, unique: true},
                password: {type: String, required: true, min: 6},
                email: {type: String, required: true, unique: true},
                key: {type: String, required: true, unique: true},
                limit: {
                    minute: {
                        request: {type: Number, default: 4},
                        time: {type: Number}
                    },
                    day: {
                        request: {type: Number, default: 5760},
                        time: {type: Number}
                    },
                    month: {
                        request: {type: Number, default: 178560},
                        time: {type: Number}
                    }
                }
            });
    function month(d) {
        var year = d.getYear(), month = d.getMonth();
        var date = new Date(year, month, 0, 0, 0);
        return date.getTime();
    }
    model.pre('save', function (next) {
        if (this.limit.minute.time === undefined) {
            this.limit.minute.time = (new Date()).getTime() + 60;
        }
        if (this.limit.month.time === undefined) {
            this.limit.month.time = month(new Date());
        }
        if (this.limit.day.time === undefined) {
            this.limit.day.time = (new Date()).setHours(23, 59, 59);
        }
        next();
    });
    models['virustotal'] = connection('static').model('virustotal', model);
};