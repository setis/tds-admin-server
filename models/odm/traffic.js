module.exports = function (mongoose, connection, models) {
    var _connection = connection('dynamic');
    var Schema = mongoose.Schema,
            model = new Schema({
                time: {type: Date, default: Date.now},
                uniq: {type: Buffer, required: true},
                token: {type: String, required: true},
                referer: {type: String}
            });
    function collection(data) {
        var collection = 'ts_' + data;
        if (models[collection] === undefined) {
            models[collection] = _connection.model(collection, model, collection);
        }
        return models[collection];
    }
    model.statics.base = collection;
    return models['traffic'] = _connection.model('traffic', model);

};