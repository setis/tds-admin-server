var _ip = require('noi');
module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
            model = new Schema({
                ip: {type: Number, required: true, unique: true},
                time: {type: Date, default: Date.now},
                code: {type: Number}
            });

    model.statics.is = function (ip, cb) {
        try {
            var numip = _ip.encode(ip);
        } catch (e) {
            console.error('ip2num ', ip);
            cb(e, null);
            return;
        }
        this.findOne({
            ip: numip
        }, cb);
    };
    model.statics.ban = function (ip, code) {
        try {
            var numip = _ip.encode(ip);
        } catch (e) {
            console.error('ip2num ', ip);
            cb(e, null);
            return;
        }
        var model = this;
        this.findOne({ip: numip}, function (err, result) {
            if (err) {
                throw err;
            }
            if (result === null) {
                model.create({
                    ip: numip,
                    time: new Date(),
                    code: code
                }, function (err) {
                    if (err)
                        throw err;
                });
            } else {
                result.time = new Date();
                result.code = code;
                result.save(function (err) {
                    if (err) {
                        throw err;
                    }
                });
            }
        });

    };
    model.statics.unban = function (ip) {
        this.remove({ip: ip2num(ip)}, function (err) {
            if (err) {
                throw err;
            }
        });
    };
    models['ban_ip'] = connection('dynamic').model('ban_ip', model);
};