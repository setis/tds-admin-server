module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
            model = new Schema({
                uid: {type: Number, required: true},
                id: {type: Number, ref: 'id'},
                url: {type: Boolean, default: false},
                path: {
                    data: {type: Buffer},
                    originalname: {type: String},
                    size: {type: Number}
                },
                link: {type: String},
                width: {type: Number},
                height: {type: Number},
                time: {type: Date, default: Date.now}
            });
    model.plugin(plugins.autoIncrement.plugin, {
        model: 'banner',
        field: 'id'
    });
    models['banner'] = connection('static').model('banner', model);
};