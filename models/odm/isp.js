var _ip = require('noi');
module.exports = function (mongoose, connection, models) {
    var _connection = connection('isp');
    var Schema = mongoose.Schema,
            model = new Schema({
                from: {type: Number, required: true,  index: true},
                to: {type: Number, required: true, index: true},
//                text: {
//                    from: {type: String, required: true, index: true},
//                    to: {type: String, required: true, index: true}
//                },
//                ip4: {type: Boolean, required: true, index: true},
                use: {type: String, required: true, index: true},
                isp: {type: Boolean, required: true, index: true}
            });
 model.statics.ip = function (ip, cb) {
        try {
            var numip = _ip.encode(ip);
        } catch (e) {
            console.error('ip2num ', ip);
            cb(e, null);
            return;
        }
        this.findOne({
            from: {"$lte": numip},
            to: {"$gte": numip}
        }, function(err,result){
            if(err){
                cb(err,null);
                return;
            }
            if(result === null){
                cb(null,false);
                return;
            }
            cb(null,result.isp);
        });
    };
    function collection(country) {
        if (models[country] === undefined) {
            models[country] = _connection.model(country, model, country);
        }
        return models[country];
    }
    model.statics.base = collection;
    return models['isp'] = _connection.model('isp', model);

};