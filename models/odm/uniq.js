module.exports = function (mongoose, connection, models, plugins) {
    var _connection = connection('dynamic');
    var Schema = mongoose.Schema,
            model = new Schema({
                uniq: {type: Buffer, unique: true, index: true},
                time: {
                    in: {type: Date},
                    drop: {type: Date},
                    out: {type: Date},
                    pre: {type: Date},
                    uniq: {type: Date},
                },
                layer: {
                    try: {type: Number, default: 0},
                    time: {type: Date, default: Date.now}
                },
                exploits: {type: Array}
            });

    function collection(data) {
        var collection = 'uniq_' + data;
        if (models[collection] === undefined) {
            models[collection] = _connection.model(collection, model, collection);
        }
        return models[collection];
    }
    model.statics.base = collection;
    models['uniq'] = _connection.model('uniq', model);
};