module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
            model = new Schema({
                host: {type: String, required: true, lowercase: true, unique: true},
                user: {
                    id: {type: Number},
                    key: {type: Schema.Types.ObjectId, ref: 'user'}
                },
//                type: {type: String, index: true, lowercase: true, enum: ['user', 'adult', 'noadult'], default: 'user'},
                id: {type: Number, ref: 'id', unique: true},
                description: {type: String},
                avcheck: {
                    total: {type: Number, default: 0},
                    detect: {type: Number, default: 0},
                    time: {type: Date, default: Date.now},
                    scan: {type: Schema.Types.Mixed},
                    link: {type: String},
                    error: {type: Schema.Types.Mixed, default: null}
                },
                alive: {
                    mode: {type: Boolean, default: false},
                    time: {type: Date, default: Date.now}
                },
                www: {
                    mode: {type: Boolean, default: false},
                    time: {type: Date, default: Date.now}
                },
                ip: {
                    ip: {type: String, default: null},
                    time: {type: Date, default: Date.now}
                },
                mode: {type: Boolean, default: true},
                use: {type: Boolean, default: false},
                virustotal: {
                    url: [{
                            permalink: {type: String},
                            url: {type: String},
                            scan_date: {type: Date},
                            scan_id: {type: String},
                            positives: {type: Number},
                            total: {type: Number},
                            scans: {type: Schema.Types.Mixed}
                        }],
                    queue: [
                        {
                            scan_date: {type: Date},
                            scan_id: {type: String},
                            permalink: {type: String}
                        }
                    ],
                    domain: {
                        time: {type: Date, default: Date.now},
                        urls: [
                            {
                                url: {type: String},
                                positives: {type: Number},
                                total: {type: Number},
                                scan_date: {type: Date}
                            }
                        ]
                    }
                },
                history: {
                    create: {type: Date, default: Date.now},
                    remove: {type: Date, default: null}
                }

            });
    model.plugin(plugins.autoIncrement.plugin, {
        model: 'domain',
        field: 'id'
    });
    models['domain'] = connection('static').model('domain', model);
};