module.exports = function (mongoose, connection, models, plugins) {
    var _connection = connection('dynamic');
    var Schema = mongoose.Schema,
            model = new Schema({
                uniq: {type: Buffer, required: true, unique: true},
                time: {type: Date, default: Date.now}
            });
    model.is = function (uniq, cb) {
        model.findOne({
            uniq: uniq,
            time: {
                "$gte": new Date()
            }
        }, function (err, result) {
            if (err) {
                throw err;
            }
            cb((result === null));
        });
    };
    model.ban = function (uniq, ttl) {
        var time = (new Date()).setTime((new Date()).getTime() + ttl);
        model.create({
            uniq: uniq,
            time: time
        }, function (err) {
            if (err) {
                model.update({uniq: uniq}, {"$set": {time: time}}, function (err) {
                    if (err) {
                        throw err;
                    }
                });
            }
        });
    };
    model.unban = function () {
        model.remove({time: {"$gte": new Date()}}, function (err) {
            if (err) {
                throw err;
            }
        });
    };
    function collection(data) {
        var collection = 'in_' + data;
        if (models[collection] === undefined) {
            models[collection] = _connection.model(collection, model, collection);
        }
        return models[collection];
    }
    model.statics.base = collection;
    models['ban'] = _connection.model('ban', model);
};