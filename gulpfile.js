var gulp = require('gulp'),
        rename = require("gulp-rename"),
        replace = require('gulp-replace'),
        uglifyjs = require('uglify-js'),
        minifier = require('gulp-uglify/minifier'),
        uglify = require('gulp-uglifyjs');
gulp.task('uglify-js-ie8', function () {
    //http://javascript.sg/using-uglifys-screw-ie8-option-in-gulp/
    //https://davidwalsh.name/compress-uglify
    //http://javascript.sg/tag/uglify/
    return gulp.src('app/tds/js/src/ie.lte8.js')
            .pipe(minifier({
                compress: {
                    screw_ie8: true,
                    sequences: true,
                    //properties: true,
                    dead_code: true,
                    drop_debugger: true,
                    comparisons: true,
                    conditionals: true,
                    evaluate: true,
                    booleans: true,
                    loops: true,
                    unused: true,
                    hoist_funs: true,
                    if_return: true,
                    join_vars: true,
                    cascade: true,
                    //negate_iife: true,
                    drop_console: true
                },
                mangle: {
                    screw_ie8: true
                }
            }, uglifyjs))
            .pipe(replace('"{{{cfg}}}"', '{{{cfg}}}'))
            .pipe(gulp.dest('app/tds/js/dist'));
});
gulp.task('uglify-js-ie', function () {
    //http://javascript.sg/using-uglifys-screw-ie8-option-in-gulp/
    //https://davidwalsh.name/compress-uglify
    //http://javascript.sg/tag/uglify/
    return gulp.src('app/tds/js/src/ie.js')
            .pipe(uglify())
            .pipe(replace('"{{{cfg}}}"', '{{{cfg}}}'))
            .pipe(gulp.dest('app/tds/js/dist'));
});
gulp.task('uglify-js-tmt', function () {
    return gulp.src('app/tds/js/src/tmt.js')
            .pipe(uglify())
            .pipe(replace('"{{{cfg}}}"', '{{{cfg}}}'))
            .pipe(gulp.dest('app/tds/js/dist'));
});
gulp.task('uglify-js-not_ie', function () {
    return gulp.src('app/tds/js/src/not_ie.js')
            .pipe(uglify())
            .pipe(replace('"{{{cfg}}}"', '{{{cfg}}}'))
            .pipe(gulp.dest('app/tds/js/dist'));
});
gulp.task('default', [
    'uglify-js-tmt',
    //'uglify-js-ie',
    'uglify-js-ie8',
    'uglify-js-not_ie',
]);